package com.appcoholic.bible;

import android.content.Context;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

class BibleStyleBO
{
    private Context context = null;
    final private Map<String, String> mapStyle = new HashMap<>();
    final private Map<String, String> mapColor = new HashMap<>();

    BibleStyleBO(final Context context)
    {
        try
        {
            this.context = context;

            this.mapStyle.clear();
            this.mapColor.clear();

            //-- Colors (Thanks all guys, if you read it)
            //Grimm Fairytale colors
            this.mapColor.put("GrimmStyleRed0", "#690202");
            this.mapColor.put("GrimmStyleRed1", "#822626");
            this.mapColor.put("GrimmStyleWhite0", "#e6ddbc");
            this.mapColor.put("GrimmStyleGray0", "#525252");
            this.mapColor.put("GrimmStyleBlack0", "#262525");

            //The Life, The Light
            this.mapColor.put("App0StyleRed0", "#fa0201");
            this.mapColor.put("App0StyleBlue0", "#2196f3");
            this.mapColor.put("App0StyleBlack0", "#000000");
            this.mapColor.put("App0StyleBlack1", "#303030");
            this.mapColor.put("App0StyleBlack2", "#424242");
            this.mapColor.put("App0StyleWhite0", "#fafafa");

            //KJV Bible app colors
            this.mapColor.put("App1StyleColor0", "#bd635e");
            this.mapColor.put("App1StyleColor1", "#af8254");
            this.mapColor.put("App1StyleColor2", "#8c6587");
            this.mapColor.put("App1StyleColor3", "#565daa");
            this.mapColor.put("App1StyleColor4", "#4a759c");
            this.mapColor.put("App1StyleColor5", "#e2863f");
            this.mapColor.put("App1StyleColor6", "#e1a239");
            this.mapColor.put("App1StyleColor7", "#6ab06a");
            this.mapColor.put("App1StyleColor8", "#6abdb5");
            this.mapColor.put("App1StyleColor9", "#5ca5da");

            //BLB app colors
            this.mapColor.put("App2StyleColor0", "#ffffa9");
            this.mapColor.put("App2StyleColor1", "#bcffba");
            this.mapColor.put("App2StyleColor2", "#cdddff");
            this.mapColor.put("App2StyleColor3", "#fecccb");
            this.mapColor.put("App2StyleColor4", "#fecca9");
            this.mapColor.put("App2StyleColor5", "#ffaaff");

            //Kaki colors
            this.mapColor.put("KakiStyleColor0", "#56aa60");
            this.mapColor.put("KakiStyleColor1", "#96923f");
            this.mapColor.put("KakiStyleColor2", "#8a512b");
            this.mapColor.put("KakiStyleColor3", "#6f2020");
            this.mapColor.put("KakiStyleColor4", "#4b0a2c");

            //Daffodil bible colors
            this.mapColor.put("DaffodilStyleColor0", "#f4deac");
            this.mapColor.put("DaffodilStyleColor1", "#f0cb7d");
            this.mapColor.put("DaffodilStyleColor2", "#e6ac2e");
            this.mapColor.put("DaffodilStyleColor3", "#949b55");
            this.mapColor.put("DaffodilStyleColor4", "#964b34");

            //-- Styles (in min when possible)
            CreateStyle("GrimmStyle0", "GrimmStyleWhite0", "GrimmStyleBlack0");
            CreateStyle("GrimmStyle1", "GrimmStyleBlack0", "GrimmStyleWhite0");
            CreateStyle("GrimmStyle2", "GrimmStyleWhite0", "GrimmStyleGray0");
            CreateStyle("GrimmStyle3", "GrimmStyleWhite0", "GrimmStyleRed0");
            CreateStyle("GrimmStyle4", "GrimmStyleWhite0", "GrimmStyleRed1");

            CreateStyle("App0Style0a", "App0StyleRed0", "App0StyleWhite0");
            CreateStyle("App0Style0b", "App0StyleRed0", "App0StyleBlack1");
            CreateStyle("App0Style0c", "App0StyleRed0", "App0StyleBlack2");
            CreateStyle("App0Style0d", "App0StyleRed0", "App0StyleBlack0");
            CreateStyle("App0Style1a", "App0StyleBlue0", "App0StyleWhite0");
            CreateStyle("App0Style1b", "App0StyleBlue0", "App0StyleBlack1");
            CreateStyle("App0Style1c", "App0StyleBlue0", "App0StyleBlack2");
            CreateStyle("App0Style1d", "App0StyleBlue0", "App0StyleBlack0");

            CreateStyle("App1Style0a", "GrimmStyleWhite0", "App1StyleColor0");
            CreateStyle("App1Style0b", "GrimmStyleBlack0", "App1StyleColor0");
            CreateStyle("App1Style1a", "GrimmStyleWhite0", "App1StyleColor1");
            CreateStyle("App1Style1b", "GrimmStyleBlack0", "App1StyleColor1");
            CreateStyle("App1Style2a", "GrimmStyleWhite0", "App1StyleColor2");
            CreateStyle("App1Style2b", "GrimmStyleBlack0", "App1StyleColor2");
            CreateStyle("App1Style3a", "GrimmStyleWhite0", "App1StyleColor3");
            CreateStyle("App1Style3b", "GrimmStyleBlack0", "App1StyleColor3");
            CreateStyle("App1Style4a", "GrimmStyleWhite0", "App1StyleColor4");
            CreateStyle("App1Style4b", "GrimmStyleBlack0", "App1StyleColor4");
            CreateStyle("App1Style5a", "GrimmStyleWhite0", "App1StyleColor5");
            CreateStyle("App1Style5b", "GrimmStyleBlack0", "App1StyleColor5");
            CreateStyle("App1Style6a", "GrimmStyleWhite0", "App1StyleColor6");
            CreateStyle("App1Style6b", "GrimmStyleBlack0", "App1StyleColor6");
            CreateStyle("App1Style7a", "GrimmStyleWhite0", "App1StyleColor7");
            CreateStyle("App1Style7b", "GrimmStyleBlack0", "App1StyleColor7");
            CreateStyle("App1Style8a", "GrimmStyleWhite0", "App1StyleColor8");
            CreateStyle("App1Style8b", "GrimmStyleBlack0", "App1StyleColor8");
            CreateStyle("App1Style9a", "GrimmStyleWhite0", "App1StyleColor9");
            CreateStyle("App1Style9b", "GrimmStyleBlack0", "App1StyleColor9");

            CreateStyle("App2Style0a", "GrimmStyleBlack0", "App2StyleColor0");
            CreateStyle("App2Style1a", "GrimmStyleBlack0", "App2StyleColor1");
            CreateStyle("App2Style2a", "GrimmStyleBlack0", "App2StyleColor2");
            CreateStyle("App2Style3a", "GrimmStyleBlack0", "App2StyleColor3");
            CreateStyle("App2Style4a", "GrimmStyleBlack0", "App2StyleColor4");
            CreateStyle("App2Style5a", "GrimmStyleBlack0", "App2StyleColor5");

            CreateStyle("KakiStyle0a", "GrimmStyleWhite0", "KakiStyleColor0");
            CreateStyle("KakiStyle0b", "GrimmStyleBlack0", "KakiStyleColor0");
            CreateStyle("KakiStyle1a", "GrimmStyleWhite0", "KakiStyleColor1");
            CreateStyle("KakiStyle1b", "GrimmStyleBlack0", "KakiStyleColor1");
            CreateStyle("KakiStyle2a", "GrimmStyleWhite0", "KakiStyleColor2");
            CreateStyle("KakiStyle2b", "GrimmStyleBlack0", "KakiStyleColor2");
            CreateStyle("KakiStyle3a", "GrimmStyleWhite0", "KakiStyleColor3");
            CreateStyle("KakiStyle3b", "App2StyleColor0", "KakiStyleColor3");
            CreateStyle("KakiStyle4a", "GrimmStyleWhite0", "KakiStyleColor4");
            CreateStyle("KakiStyle4b", "KakiStyleColor1", "KakiStyleColor4");

            CreateStyle("DaffodilStyle0a", "GrimmStyleBlack0", "DaffodilStyleColor0");
            CreateStyle("DaffodilStyle0b", "DaffodilStyleColor4", "DaffodilStyleColor0");
            CreateStyle("DaffodilStyle1a", "GrimmStyleBlack0", "DaffodilStyleColor1");
            CreateStyle("DaffodilStyle1b", "DaffodilStyleColor4", "DaffodilStyleColor1");
            CreateStyle("DaffodilStyle2a", "GrimmStyleBlack0", "DaffodilStyleColor2");
            CreateStyle("DaffodilStyle2b", "DaffodilStyleColor4", "DaffodilStyleColor2");
            CreateStyle("DaffodilStyle3a", "GrimmStyleWhite0", "DaffodilStyleColor3");
            CreateStyle("DaffodilStyle3b", "GrimmStyleBlack0", "DaffodilStyleColor3");
            CreateStyle("DaffodilStyle3c", "DaffodilStyleColor4", "DaffodilStyleColor3");
            CreateStyle("DaffodilStyle4a", "GrimmStyleWhite0", "DaffodilStyleColor4");
            CreateStyle("DaffodilStyle4b", "DaffodilStyleColor0", "DaffodilStyleColor4");
            CreateStyle("DaffodilStyle4c", "DaffodilStyleColor1", "DaffodilStyleColor4");
            CreateStyle("DaffodilStyle4d", "DaffodilStyleColor2", "DaffodilStyleColor4");

        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(this.context, ex);
        }
    }

    private void CreateStyle(final String keyStyle, final String keyColorFg, final String keyColorBg)
    {
        try
        {
            if (!this.mapStyle.containsKey(keyStyle) && this.mapColor.containsKey(keyColorFg) && this.mapColor.containsKey(keyColorBg)) {
                this.mapStyle.put(keyStyle, PCommon.ConcaT(keyColorFg, "/", keyColorBg));
            } else {
                if (PCommon._isDebug) System.out.println(PCommon.ConcaT("Invalid Style: ", keyStyle));
            }
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(this.context, ex);
        }
    }

    private String GetColorFromId(final String keyColor)
    {
        try
        {
            return mapColor.get(keyColor);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(this.context, ex);
        }

        return null;
    }

    /***
     * Get style
     * @param keyStyle Style Id
     * @return Map with properties: 'fg', 'bg'. Null if not found
     */
    protected Map<String, String> GetStylePropertiesFromId(final String keyStyle)
    {
        Map<String, String> mapProp = null;

        try
        {
            final String styleProp = mapStyle.get(keyStyle);
            if (styleProp == null) return null;

            final String[] arrColor = styleProp.split("/");
            if (arrColor.length != 2) return null;

            final String fg = GetColorFromId(arrColor[0]);
            if (fg == null) return null;

            final String bg = GetColorFromId(arrColor[1]);
            if (bg == null) return null;

            mapProp = new HashMap<>();
            mapProp.put("fg", fg);
            mapProp.put("bg", bg);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(this.context, ex);
        }

        return mapProp;
    }

    /***
     * Get all styles sorted
     * @return all styles sorted
     */
    protected TreeMap<String, String> GetAllStyles()
    {
        try
        {
            return new TreeMap<>(this.mapStyle);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(this.context, ex);
        }

        return null;
    }
}
