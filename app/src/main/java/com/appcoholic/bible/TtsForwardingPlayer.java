package com.appcoholic.bible;

import android.content.Context;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.media3.common.ForwardingPlayer;
import androidx.media3.common.Player;

public class TtsForwardingPlayer extends ForwardingPlayer
{
    protected TtsForwardingPlayer(Context context, Looper applicationLooper, Player player)
    {
        super(player);
    }

    @NonNull
    @Override
    public Commands getAvailableCommands()
    {
        if (PCommon._isDebug) System.out.println("TtsForwardingPlayer.getAvailableCommands...");

        return super.getAvailableCommands()
                .buildUpon()
                .removeAll(COMMAND_SEEK_TO_DEFAULT_POSITION,
                        COMMAND_SEEK_IN_CURRENT_MEDIA_ITEM,
                        COMMAND_SEEK_TO_PREVIOUS_MEDIA_ITEM,
                        COMMAND_SEEK_TO_PREVIOUS,
                        COMMAND_SEEK_TO_NEXT_MEDIA_ITEM,
                        COMMAND_SEEK_TO_NEXT,
                        COMMAND_SEEK_TO_MEDIA_ITEM,
                        COMMAND_SEEK_BACK,
                        COMMAND_SEEK_FORWARD)
                .build();
    }

    @Override
    public boolean isCommandAvailable(int command)
    {
        if (PCommon._isDebug) System.out.println(PCommon.ConcaT("TtsForwardingPlayer.isCommandAvailable: command=", command));

        if (command == COMMAND_SEEK_TO_DEFAULT_POSITION ||
            command == COMMAND_SEEK_IN_CURRENT_MEDIA_ITEM ||
            command == COMMAND_SEEK_TO_PREVIOUS_MEDIA_ITEM ||
            command == COMMAND_SEEK_TO_PREVIOUS ||
            command == COMMAND_SEEK_TO_NEXT_MEDIA_ITEM ||
            command == COMMAND_SEEK_TO_NEXT ||
            command == COMMAND_SEEK_TO_MEDIA_ITEM ||
            command == COMMAND_SEEK_BACK ||
            command == COMMAND_SEEK_FORWARD) return false;

        return super.isCommandAvailable(command);
    }
}
