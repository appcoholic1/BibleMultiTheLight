
package com.appcoholic.bible;

class PlanDescBO
{
    enum PLAN_TYPE {
        VERSE_TYPE,
        CHAPTER_TYPE
    }

    int planId;

    PLAN_TYPE planType;
    String planRef;
    String startDt;
    String endDt;
    int bCount;
    int cCount;
    int vCount;
    int origVCount;
    int vDayCount; //Verse or chapter count
    int dayCount;

    PlanDescBO()
    {
        planType = PLAN_TYPE.VERSE_TYPE;
        planId = -1;
        planRef = null;
        startDt = null;
        endDt = null;
        bCount = 0;
        cCount = 0;
        vCount = 0;
        origVCount = 0;
        vDayCount = 0;
        dayCount = 0;
    }
}
