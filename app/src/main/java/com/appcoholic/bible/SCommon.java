
package com.appcoholic.bible;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Looper;
import android.text.format.DateFormat;

import androidx.media3.session.MediaSession;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

/***
 * Singleton
 */
class SCommon
{
    //<editor-fold defaultstate="collapsed" desc="-- Variables --">

    @SuppressLint("StaticFieldLeak")
    private static SCommon uniqInstance = null;
    @SuppressLint("StaticFieldLeak")
    private static Dal _dal = null;
    @SuppressLint("StaticFieldLeak")
    private static Context _context = null;
    private TtsPlayer ttsPlayer = null;
    private MediaSession ttsMediaSession = null;
    private static Thread ttsThread = null;
    private static TtsManager ttsManager = null;
    private static volatile int ttsCurrentVerseNumber;
    private static volatile String ttsCurrentListenPosition;

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="-- Constructor(s) --">

    private SCommon(Context ctx)
    {
        try
        {
            InitDbOpening(ctx);

            ttsCurrentListenPosition = GetListnPosition(_context);

            final String[] arr = ttsCurrentListenPosition.split(",");
            final int vNumber =  (arr.length < 4) ? 1 : Integer.parseInt(arr[3]);

            ttsCurrentVerseNumber = vNumber;
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(ctx, ex);
        }
    }

    //</editor-fold>

    static synchronized SCommon GetInstance(Context ctx)
    {
        if (_context == null)
        {
            _context = ctx.getApplicationContext();
        }

        if (uniqInstance == null)
        {
            uniqInstance = new SCommon(_context);
        }
        else
        {
            InitDbOpening(_context);
        }

        return uniqInstance;
    }

    //<editor-fold defaultstate="collapsed" desc="-- Open/Close/Utils --">

    private static void InitDbOpening(Context ctx)
    {
        try
        {
            if (_dal == null)
            {
                _dal = new Dal(ctx);
            }

            if (!_dal.IsDbOpen())
            {
                _dal.OpenReadWrite();
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * CloseDb db
     */
    void CloseDb()
    {
        try
        {
            if (_dal != null)
            {
                _dal.CloseDb();
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * Shrink db
     */
    @SuppressWarnings("unused")
    private void ShrinkDb(final Context context)
    {
        try
        {
            _dal.ShrinkDb(context);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Get db version
     * @return db version
     */
    int GetDbVersion()
    {
        int dbVersion = -1;

        try
        {
            dbVersion = _dal.GetDbVersion();
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return dbVersion;
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="-- Log --">


    /***
     * Get list of log
     * @return list all logs
     */
    ArrayList<String> GetListAllLog()
    {
        return _dal.GetListAllLog();
    }

    /***
     * Add a log (db should be open for writing)
     * @param msg
     */
    @SuppressWarnings("JavaDoc")
    void AddLog(final String msg)
    {
        try
        {
            _dal.AddLog(msg);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * Delete all logs (db should be open for writing)
     * @param isForceDelete True=force delete
     */
    void DeleteAllLogs(final boolean isForceDelete)
    {
        try
        {
            _dal.DeleteAllLogs(isForceDelete);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="-- Bible --">

    /***
     * Get a verse
     * @param bibleId
     * @return verse
     */
    @SuppressWarnings("JavaDoc")
    VerseBO GetVerse(final int bibleId)
    {
        VerseBO verse = null;

        try
        {
            verse = _dal.GetVerse(bibleId);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return verse;
    }

    /***
     * Get a verse
     * @param tbbName
     * @param bNumber
     * @param cNumber
     * @param vNumber
     * @return verse
     */
    @SuppressWarnings("JavaDoc")
    ArrayList<VerseBO> GetVerse(final String tbbName, final int bNumber, final int cNumber, final int vNumber)
    {
        ArrayList<VerseBO> lstVerse = new ArrayList<>();

        try
        {
            lstVerse = _dal.GetVerse(tbbName, bNumber, cNumber, vNumber);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return lstVerse;
    }

    /***
     * Get list of verses
     * @param tbbName
     * @param bNumber
     * @param cNumber
     * @param vNumberFrom
     * @param vNumberTo
     * @return list of verses
     */
    @SuppressWarnings("JavaDoc")
    ArrayList<VerseBO> GetVerses(final String tbbName, final int bNumber, final int cNumber, final int vNumberFrom, final int vNumberTo)
    {
        ArrayList<VerseBO> lstVerse = new ArrayList<>();

        try
        {
            lstVerse = _dal.GetVerses(tbbName, bNumber, cNumber, vNumberFrom, vNumberTo);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return lstVerse;
    }

    /***
     * Get Id of verses only for bbName
     * @param bbName
     * @param bNumber
     * @param cNumber
     * @param vNumberFrom
     * @param vNumberTo
     * @return list of verses Id
     */
    @SuppressWarnings("JavaDoc")
    ArrayList<Integer> GetVersesId(final String bbName, final int bNumber, final int cNumber, final int vNumberFrom, final int vNumberTo)
    {
        ArrayList<Integer> lstId = new ArrayList<>();

        try
        {
            lstId = _dal.GetVersesId(bbName, bNumber, cNumber, vNumberFrom, vNumberTo);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return lstId;
    }

    /***
     * Get cross references
     * @param tbbName
     * @param bNumber
     * @param cNumber
     * @param vNumber
     * @return null or list of verses...
     */
    @SuppressWarnings("JavaDoc")
    Map<String, Object> GetCrossReferences(final String tbbName, final int bNumber, final int cNumber, final int vNumber)
    {
        Map<String, Object> mapSearchBible = null;

        try
        {
            mapSearchBible = _dal.GetCrossReferences(tbbName, bNumber, cNumber, vNumber);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return mapSearchBible;
    }

    /***
     * Get cross references count
     * @param bNumber
     * @param cNumber
     * @param vNumber
     * @return Count of cross references found
     */
    @SuppressWarnings("JavaDoc")
    int GetCrossReferencesCount(final int bNumber, final int cNumber, final int vNumber)
    {
        int count = 0;

        try
        {
            count = _dal.GetCrossReferencesCount(bNumber, cNumber, vNumber);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return count;
    }

    /***
     * Copy cache search for other bible (data are deleted before copying)
     * @param tabIdTo
     * @param tbbName
     * @param bNumberStart
     * @param cNumberStart
     * @param vNumberStart
     * @param bNumberEnd
     * @param cNumberEnd
     * @param vNumberEnd
     * @return true if copy was successful
     */
    @SuppressWarnings("JavaDoc")
    boolean CopyCacheSearchForOtherBible(final int tabIdTo, final String tbbName, final int bNumberStart, final int cNumberStart, final int vNumberStart, final int bNumberEnd, final int cNumberEnd, final int vNumberEnd)
    {
        try
        {
            return _dal.CopyCacheSearchForOtherBible(tabIdTo, tbbName, bNumberStart, cNumberStart, vNumberStart, bNumberEnd, cNumberEnd, vNumberEnd);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return false;
    }

    /***
     * Get list of verses in Html
     * @param bbName
     * @param bNumber
     * @param cNumber
     * @param vNumberFrom
     * @param vNumberTo
     * @return list of verses
     */
    @SuppressWarnings("JavaDoc")
    String GetVersesHtml(final String bbName, final int bNumber, final int cNumber, final int vNumberFrom, final int vNumberTo)
    {
        final StringBuilder sbVerses = new StringBuilder("<blockquote>");

        try
        {
            String crCountString;
            final ArrayList<VerseBO> lstVerse = _dal.GetVerses(bbName, bNumber, cNumber, vNumberFrom, vNumberTo);

            for (VerseBO v : lstVerse)
            {
                crCountString = (v.crCount > 0) ? PCommon.ConcaT(" (", v.crCount, ")") : "";

                sbVerses.append(PCommon.ConcaT("<b>", v.bName, " ", v.cNumber, ".", v.vNumber, crCountString, ": </b><br>", v.vText, "<br><br>"));
            }

            if (!lstVerse.isEmpty())
            {
                final int start = sbVerses.length() - 8;
                sbVerses.delete(start, sbVerses.length());
            }

            sbVerses.append("</blockquote>");
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return sbVerses.toString();
    }

    /***
     * Get verse text
     * @param tbbName
     * @param bNumber
     * @param cNumber
     * @param vNumber
     * @return text of verse
     */
    @SuppressWarnings("JavaDoc")
    String GetVerseText(final String tbbName, final int bNumber, final int cNumber, final int vNumber)
    {
        String text = "";

        try
        {
            text = _dal.GetVerseText(tbbName, bNumber, cNumber, vNumber);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return text;
    }

    /***
     * Get a chapter
     * @param tbbName
     * @param bNumber
     * @param cNumber
     * @return list of verses
     */
    @SuppressWarnings("JavaDoc")
    ArrayList<VerseBO> GetChapter(final String tbbName, final int bNumber, final int cNumber)
    {
        ArrayList<VerseBO> lstVerse = new ArrayList<>();

        try
        {
            lstVerse = _dal.GetChapterFromPos(tbbName, bNumber, cNumber, 1);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return lstVerse;
    }

    /***
     * Get chapter text
     * @param tbbName
     * @param bNumber
     * @param cNumber
     * @return text of chapter
     */
    @SuppressWarnings("JavaDoc")
    String GetChapterText(final String tbbName, final int bNumber, final int cNumber)
    {
        String text = "";

        try
        {
            text = _dal.GetChapterText(tbbName, bNumber, cNumber);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return text;
    }

    /***
     * Get result (all) text. Require to delete cache of the unused tab.
     * @param tbbName
     * @param tabIdFrom
     * @param tabIdTo
     * @return text of all.
     */
    @SuppressWarnings("JavaDoc")
    ArrayList<Integer> GetResultVersesId(final int tabIdFrom, final int tabIdTo, final String tbbName)
    {
        ArrayList<Integer> lstId = new ArrayList<>();

        try
        {
            lstId = _dal.GetResultVersesId(tabIdFrom, tabIdTo, tbbName);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return lstId;
    }

    /***
     * Search bible
     * @param bbName
     * @param bNumber
     * @param cNumber
     * @param searchString
     * @return list of verses
     */
    @SuppressWarnings("JavaDoc")
    ArrayList<VerseBO> SearchBible(final String bbName, final int bNumber, final int cNumber, final String searchString)
    {
        ArrayList<VerseBO> lstVerse = new ArrayList<>();

        try
        {
            lstVerse = _dal.SearchBible(bbName, bNumber, cNumber, searchString);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return lstVerse;
    }

    /***
     * Search bible
     * @param bbName
     * @param bNumber
     * @param searchString
     * @return list of verses
     */
    @SuppressWarnings("JavaDoc")
    ArrayList<VerseBO> SearchBible(final String bbName, final int bNumber, final String searchString)
    {
        ArrayList<VerseBO> lstVerse = new ArrayList<>();

        try
        {
            lstVerse = _dal.SearchBible(bbName, bNumber, searchString);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return lstVerse;
    }

    /***
     * Search bible
     * @param bbName
     * @param searchString
     * @return null or list of verses...
     */
    @SuppressWarnings("JavaDoc")
    Map<String, Object> SearchBible(final String bbName, final String searchString)
    {
        Map<String, Object> mapSearchBible = null;

        try
        {
            mapSearchBible = _dal.SearchBible(bbName, searchString);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return mapSearchBible;
    }

    /***
     * Search favorites
     * @param bbName
     * @param searchString  Give NULL to get all notes
     * @param orderBy       Order by
     * @param markType      Mark type
     * @return list of verses
     */
    @SuppressWarnings("JavaDoc")
    ArrayList<VerseBO> SearchFav(final String bbName, final String searchString, final int orderBy, final int markType)
    {
        ArrayList<VerseBO> lstVerse = new ArrayList<>();

        try
        {
            lstVerse = _dal.SearchFav(bbName, searchString, orderBy, markType);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return lstVerse;
    }

    /***
     * Get list book by name
     * @param bbName
     * @param searchString
     * @return list of books
     */
    @SuppressWarnings("JavaDoc")
    ArrayList<BibleRefBO> GetListBookByName(final String bbName, final String searchString)
    {
        return _dal.GetListBookByName(bbName, searchString);
    }

    /***
     * Get list all book by name
     * @param bbName
     * @return list all books
     */
    @SuppressWarnings("JavaDoc")
    ArrayList<BibleRefBO> GetListAllBookByName(final String bbName)
    {
        return _dal.GetListAllBookByName(bbName);
    }

    /***
     * Get list all book by name
     * @param bbName
     * @return list all books
     */
    @SuppressWarnings("JavaDoc")
    TreeMap<Integer, BibleRefBO> GetListAllBookMapByName(final String bbName)
    {
        return _dal.GetListAllBookMapByName(bbName);
    }

    /***
     * Get list of my articles Id
     * @return list of articles Id
     */
    String[] GetListMyArticlesId()
    {
        return _dal.GetListMyArticlesId();
    }

    /***
     * Get my article name
     * @param artId
     * @return article name
     */
    @SuppressWarnings("JavaDoc")
    String GetMyArticleName(final int artId)
    {
        return _dal.GetMyArticleName(artId);
    }

    /***
     * Get my article source
     * @param artId
     * @return article source
     */
    @SuppressWarnings("JavaDoc")
    String GetMyArticleSource(final int artId)
    {
        return _dal.GetMyArticleSource(artId);
    }

    /***
     * Get my article substitution
     * @param source    Source without substitution (except <R></R>)
     * @return  String with edit tags
     */
    private String GetMyArticleSourceSubstition(final String source)
    {
        return source
                .replaceAll("</u></h1>", "</H>")
                .replaceAll("<h1><u>", "<H>")
                .replaceAll("</u></span>", "</HS>")
                .replaceAll("<br><span><u>", "<HS>");
    }

    /***
     * Get new MyArticle Id
     * @return new article Id
     */
    int GetNewMyArticleId()
    {
        return _dal.GetNewMyArticleId();
    }

    /***
     * Update my article source
     * @param artId
     * @param source
     */
    @SuppressWarnings("JavaDoc")
    void UpdateMyArticleSource(final int artId, final String source)
    {
        try
        {
            final String substSource = GetMyArticleSourceSubstition(source);
            _dal.UpdateMyArticleSource(artId, substSource);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * Update my article title
     * @param artId     Article Id
     * @param title     Title
     */
    void UpdateMyArticleTitle(final int artId, final String title)
    {
        try
        {
            _dal.UpdateMyArticleTitle(artId, title);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * Add my article
     * @param ad    Article description
     */
    void AddMyArticle(final ArtDescBO ad)
    {
        try
        {
            _dal.AddMyArticle(ad);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * Delete my article
     * @param artId     Article Id
     */
    void DeleteMyArticle(final int artId)
    {
        try
        {
            _dal.DeleteMyArticle(artId);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * Search bible (cache)
     * @param searchId
     * @return null or list of verses...
     */
    @SuppressWarnings("JavaDoc")
    Map<String, Object> SearchBible(final int searchId)
    {
        Map<String, Object> mapSearchBible = null;

        try
        {
            mapSearchBible = _dal.SearchBible(searchId);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return mapSearchBible;
    }

    /***
     * Get tabId of an article
     * @param artName   ART_NAME
     * @return Negative value if not found
     */
    int GetArticleTabId(final String artName)
    {
        int tabId = -1;

        try
        {
            tabId = _dal.GetArticleTabId(artName);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return tabId;
    }

    /***
     * Get book number
     * @param bbName
     * @param bName
     * @return book number (0 if not found)
     */
    @SuppressWarnings("JavaDoc")
    int GetBookNumberByName(final String bbName, final String bName)
    {
        int bNumber = 0;

        try
        {
            bNumber = _dal.GetBookNumberByName(bbName, bName);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return bNumber;
    }

    /***
     * Get book number
     * @param bName
     * @return book number (0 if not found)
     */
    @SuppressWarnings("JavaDoc")
    int GetBookNumberByName(final String bName)
    {
        int bNumber = 0;

        try
        {
            bNumber = _dal.GetBookNumberByName(bName);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return bNumber;
    }

    /***
     * Get book ref
     * @param bbName
     * @param bNumber
     * @return book ref
     */
    @SuppressWarnings("JavaDoc")
    BibleRefBO GetBookRef(final String bbName, final int bNumber)
    {
        return _dal.GetBookRef(bbName, bNumber);
    }

    /***
     * Get cache tab
     * @param tabId
     */
    @SuppressWarnings("JavaDoc")
    CacheTabBO GetCacheTab(final int tabId)
    {
        CacheTabBO t = null;

        try
        {
            t = _dal.GetCacheTab(tabId);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return t;
    }

    /***
     * Get cache tab Fav
     */
    CacheTabBO GetCacheTabFav()
    {
        CacheTabBO t = null;

        try
        {
            t = _dal.GetCacheTabFav();
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return t;
    }

    /***
     * Get cache tab title
     * @param tabId
     */
    @SuppressWarnings("JavaDoc")
    String GetCacheTabTitle(final int tabId)
    {
        String title = null;

        try
        {
            title = _dal.GetCacheTabTitle(tabId);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return title;
    }

    /***
     * Save cache tab
     * @param t
     */
    @SuppressWarnings("JavaDoc")
    void SaveCacheTab(final CacheTabBO t)
    {
        try
        {
            _dal.SaveCacheTab(t);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * Save cache tab fav
     * @param t
     */
    @SuppressWarnings("JavaDoc")
    void SaveCacheTabFav(final CacheTabBO t)
    {
        try
        {
            _dal.SaveCacheTabFav(t);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * Get current cache tab
     * @return current cache
     */
    CacheTabBO GetCurrentCacheTab()
    {
        final int tabId = MainActivity.Tab.GetCurrentTabPosition();
        if (tabId < 0) return null;

        return GetCacheTab(tabId);
    }

    /***
     * Save cache search
     */
    void SaveCacheSearch(final ArrayList<Integer> lstBibleId)
    {
        try
        {
            final int tabId = MainActivity.Tab.GetCurrentTabPosition();

            _dal.SaveCacheSearch(tabId, lstBibleId);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * Copy cache search (deleting data) for other bible
     * @param tabIdFrom
     * @param tabIdTo
     * @param bbNameTo
     */
    @SuppressWarnings("JavaDoc")
    void CopyCacheSearchForOtherBible(final int tabIdFrom, final int tabIdTo, final String bbNameTo)
    {
        try
        {
            @SuppressWarnings({"unused"})
            final boolean res = _dal.CopyCacheSearchForOtherBible(tabIdFrom, tabIdTo, bbNameTo);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * Get cache tab count visible
     */
    int GetCacheTabCount()
    {
        int count = 0;

        try
        {
            count = _dal.GetCacheTabVisibleCount();
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return count;
    }

    /***
     * Delete cache
     * @param tabId
     */
    @SuppressWarnings("JavaDoc")
    void DeleteCache(final int tabId)
    {
        try
        {
            _dal.DeleteCache(tabId);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * Update cache Id
     * @param fromTabId
     * @param toTabId
     */
    @SuppressWarnings("JavaDoc")
    void UpdateCacheId(final int fromTabId, final int toTabId)
    {
        try
        {
            _dal.UpdateCacheId(fromTabId, toTabId);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * Sort all cache
     * @return true if all goes well
     */
    boolean SortAllCache()
    {
        try
        {
            return _dal.SortAllCache();
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return false;
    }

    /***
     * Save note
     * @param noteBO
     */
    @SuppressWarnings("JavaDoc")
    void SaveNote(final NoteBO noteBO)
    {
        try
        {
            _dal.SaveNote(noteBO);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * Delete note
     * @param bNumber
     * @param cNumber
     * @param vNumber
     */
    @SuppressWarnings("JavaDoc")
    void DeleteNote(final int bNumber, final int cNumber, final int vNumber)
    {
        try
        {
            _dal.DeleteNote(bNumber, cNumber, vNumber);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * Get bibleId min
     * @param bbName
     * @return bibleId
     */
    @SuppressWarnings("JavaDoc")
    int GetBibleIdMin(final String bbName)
    {
        int min = 0;

        try
        {
            min = _dal.GetBibleIdMin(bbName);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return min;
    }

    /***
     * Get bibleId max
     * @param bbName
     * @return bibleId
     */
    @SuppressWarnings("JavaDoc")
    int GetBibleIdMax(final String bbName)
    {
        int max = 0;

        try
        {
            max = _dal.GetBibleIdMax(bbName);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return max;
    }

    /***
     * Get bibleId count
     * @return count
     */
    int GetBibleIdCount()
    {
        int count = 0;

        try
        {
            count = _dal.GetBibleIdCount();
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return count;
    }

    /***
     * Get number of chapters in a book
     * @param bNumber
     * @return chapter count
     */
    @SuppressWarnings("JavaDoc")
    int GetBookChapterMax(final int bNumber)
    {
        int max = -1;

        try
        {
            max = _dal.GetBookChapterMax("k", bNumber);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return max;
    }

    /***
     * Get start id of a book
     * @param bbName
     * @param bNumber
     * @return First Id of a book, -1 in case of error
     */
    @SuppressWarnings("JavaDoc")
    int GetBookIdMinByBookNumber(final String bbName, final int bNumber)
    {
        int min = -1;

        try
        {
            min = _dal.GetBookIdMinByBookNumber(bbName, bNumber);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return min;
    }

    /***
     * Does a book exist?
     * @param bNumber
     * @return true/false
     */
    @SuppressWarnings("JavaDoc")
    boolean IsBookExist(final int bNumber)
    {
        boolean status = false;

        try
        {
            status = _dal.IsBookExist("k", bNumber);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return status;
    }

    /***
     * Get plan id max
     * @return Plan id max
     */
    int GetPlanDescIdMax()
    {
        int max = 0;

        try
        {
            max = _dal.GetPlanDescIdMax();
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return max;
    }

    /***
     * Get bible chapter info by book
     * @param bNumber
     * @return chapter count, verse count of the book
     */
    @SuppressWarnings("JavaDoc")
    Integer[] GetBibleCiByBook(final int bNumber)
    {
        Integer[] ci = { 0, 0 };

        try
        {
            //cCount, vCount
            ci = _dal.GetBibleCiByBook(bNumber);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return ci;
    }

    /***
     * Get verse count of chapter
     * @param bNumber
     * @param cNumber
     * @return verse count of chapter
     */
    @SuppressWarnings("JavaDoc")
    int GetChapterVerseCount(final int bNumber, final int cNumber)
    {
        int vCount = 0;

        try
        {
            vCount = _dal.GetChapterVerseCount(bNumber, cNumber);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return vCount;
    }

    /***
     * Delete a plan
     * @param planId    Plan Id
     */
    void DeletePlan(final int planId)
    {
        try
        {
            _dal.DeletePlan(planId);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * Create a plan
     * @param pd    Plan description
     * @param strBookNumbers List of book numbers
     */
    void AddPlan(final PlanDescBO pd, final String strBookNumbers)
    {
        try
        {
            _dal.AddPlan(pd, strBookNumbers);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * Get all plan descriptions
     * @return List of all plan descriptions
     */
    ArrayList<PlanDescBO> GetAllPlanDesc()
    {
        ArrayList<PlanDescBO> lst = new ArrayList<>();

        try
        {
            lst = _dal.GetAllPlanDesc();
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return lst;
    }

    /***
     * Get a plan desc
     * @return Plan description
     */
    PlanDescBO GetPlanDesc(final int planId)
    {
        PlanDescBO pd = null;

        try
        {
            pd = _dal.GetPlanDesc(planId);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return pd;
    }

    /***
     * Get plan calendar
     * @param bbName    Bible name
     * @param planId    Plan Id
     * @param pageNr    Page number
     * @return list of days
     */
    ArrayList<PlanCalBO> GetPlanCal(final String bbName, final int planId, final int pageNr)
    {
        ArrayList<PlanCalBO> lst = new ArrayList<>();

        try
        {
            lst = _dal.GetPlanCal(bbName, planId, pageNr);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return lst;
    }

    /***
     * Is a specific plan exist?
     * @param planRef
     * @return true/false
     */
    @SuppressWarnings("JavaDoc")
    boolean IsPlanDescExist(final String planRef)
    {
        boolean status = false;

        try
        {
            status = _dal.IsPlanDescExist(planRef);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return status;
    }

    /***
     * Get plan calendar of day
     * @param bbName    Bible name
     * @param planId    Plan Id
     * @param dayNumber Day number
     * @return Plan cal of day
     */
    PlanCalBO GetPlanCalByDay(final String bbName, final int planId, final int dayNumber)
    {
        PlanCalBO pc = null;

        try
        {
            pc = _dal.GetPlanCalByDay(bbName, planId, dayNumber);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return pc;
    }

    /***
     * Get plan calendar row count of a plan
     * @param bbName    Bible name
     * @param planId    Plan Id
     * @return Row count for this calendar
     */
    int GetPlanCalRowCount(final String bbName, final int planId)
    {
        int count = 0;

        try
        {
            count = _dal.GetPlanCalRowCount(bbName, planId);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return count;
    }

    /***
     * Get current day number of a plan calendar (for today)
     * @param planId
     * @return day number (0 if not found)
     */
    @SuppressWarnings("JavaDoc")
    int GetCurrentDayNumberOfPlanCal(final int planId)
    {
        int dayNumber = 0;

        try
        {
            dayNumber = _dal.GetCurrentDayNumberOfPlanCal(planId);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return dayNumber;
    }

    /***
     * Mark plan calendar of day
     * @param planId    Plan Id
     * @param dayNumber Day number
     * @param isRead    Is read
     */
    void MarkPlanCal(final int planId, final int dayNumber, final int isRead)
    {
        try
        {
            _dal.MarkPlanCal(planId, dayNumber, isRead);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * Mark plan calendar of day (all above)
     * @param planId    Plan Id
     * @param dayNumber Day number
     * @param isRead    Is read
     */
    void MarkAllAbovePlanCal(final int planId, final int dayNumber, final int isRead)
    {
        try
        {
            _dal.MarkAllAbovePlanCal(planId, dayNumber, isRead);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * Get plan calendar progress status
     * @param planId    Plan Id
     * @return Progress status as html code
     */
    String GetPlanCalProgressStatus(final int planId)
    {
        String pgrStatus = "";

        try
        {
            final PlanDescBO pd = _dal.GetPlanDesc(planId);
            final int daysCount = pd.dayCount;
            final int daysRead = _dal.GetPlanCalDaysReadCount(planId);
            final int perc = (daysRead * 100) / daysCount;
            if (perc != 100)
            {
                final String sign;
                final int diffDays;
                final int currentDayNumber = _dal.GetCurrentDayNumberOfPlanCal(planId);
                if (currentDayNumber == 0)
                {
                    final String dtFormat = "yyyyMMdd";
                    final Calendar now = Calendar.getInstance();
                    final String dayNow = DateFormat.format(dtFormat, now).toString();
                    if (dayNow.compareTo(pd.startDt) < 0)
                    {
                        sign = "+";
                        diffDays = daysRead;
                    }
                    else
                    {
                        sign = "-";
                        diffDays = daysCount - daysRead;
                    }
                }
                else
                {
                    sign = (daysRead > currentDayNumber) ? "+" : (daysRead < currentDayNumber) ? "-" : "";
                    diffDays = Math.abs(daysRead - currentDayNumber);
                }
                final String verboseDays = diffDays > 1 ? _context.getString(R.string.planDaysSymbol) : _context.getString(R.string.planDaySymbol);
                final String verboseLate = sign.compareTo("+") == 0 ? PCommon.ConcaT("&nbsp;", _context.getString(R.string.planDayEarlySymbol))
                                         : sign.compareTo("-") == 0 ? PCommon.ConcaT("&nbsp;", _context.getString(R.string.planDayLateSymbol))
                                         : PCommon.ConcaT("&nbsp;", _context.getString(R.string.planDayLateSymbol));

                pgrStatus = PCommon.ConcaT("<blockquote>&nbsp;", perc, "%&nbsp;&nbsp;<small>(", daysRead, "/", daysCount,")</small><br>&nbsp;", diffDays, "&nbsp;", verboseDays, verboseLate, "</blockquote>");
            }
            else
            {
                final String planCompleted = _context.getString(R.string.planCompleted);
                pgrStatus = PCommon.ConcaT("<blockquote>&nbsp;", perc, "%&nbsp;&nbsp;<small>(", daysRead, "/", daysCount,")</small><br>&nbsp;", planCompleted, "</blockquote>");
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return pgrStatus;
    }

    protected String TTSGetNotificationTitle()
    {
        String notifcationTitle = null;

        try
        {
            final String[] arrListen = this.GetListenPosition(_context);
            if (arrListen == null || arrListen.length < 4) {
                return notifcationTitle;
            } else {
                final String listen_bbname = arrListen[0];
                final int listen_bnumber = Integer.parseInt(arrListen[1]);
                final BibleRefBO bookRef = this.GetBookRef(listen_bbname, listen_bnumber);

                notifcationTitle = bookRef.bName;
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return notifcationTitle;
    }

    private void SayNotification()
    {
        try
        {
            if (PCommon._isDebug) PCommon.LogR(_context, "SayNotification...");

            final Intent intentNowPlaying = new Intent(_context, MainActivity.class);
            final PendingIntent pendingIntentNowPlaying = PendingIntent.getActivity(_context, 0, intentNowPlaying,
                    PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_IMMUTABLE);

            ttsMediaSession = new MediaSession.Builder(_context, ttsPlayer)
                    .setId("THELIGHT_MEDIASESSION")
                    .setSessionActivity(pendingIntentNowPlaying)
                    //.setCallback(new CustomMediaSessionCallback())
                    .build();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * Simplified version to start at current position
     */
    void Say()
    {
        try
        {
            final String[] arr = GetListenPosition(_context);
            if (arr == null || arr.length < 4) return;

            final String bbName = arr[0];
            final int bNumber = Integer.parseInt(arr[1]);
            final int cNumber = Integer.parseInt(arr[2]);
            final int vNumber = Integer.parseInt(arr[3]);

            this.Say(bbName, bNumber, cNumber, vNumber);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /**
     * Say chapter
     * @param bbName       Bible name
     * @param bNumberFrom  From Book number
     * @param cNumberFrom  From Chapter number
     * @param vNumberFrom  From Verse number
     */
    void Say(final String bbName, final int bNumberFrom, final int cNumberFrom, final int vNumberFrom)
    {
        try
        {
            if (ttsManager != null)
            {
                ttsManager.ShutDown();

                ttsManager = null;
            }

            if (ttsThread != null)
            {
                ttsThread.interrupt();
            }

            final ThreadGroup threadGroup = new ThreadGroup(_context.getString(R.string.threadNfoGroup));
            final String threadName = PCommon.ConcaT(_context.getString(R.string.threadNfoPrefix), PCommon.TimeFuncShort(), _context.getString(R.string.threadNfoListen));

            ttsPlayer = new TtsPlayer(_context, Looper.getMainLooper());
            SayNotification();

            ttsThread = new Thread(threadGroup, threadName)
            {
                @Override
                public void run()
                {
                    Say();
                }

                private void Say()
                {
                    try
                    {
                        int cNumber = cNumberFrom;
                        int vNumber;

                        /* works but it's better to read all the book:
                        final int chapterLimit = 2;
                        */

                        final int chapterMax = _dal.GetBookChapterMax(bbName, bNumberFrom);
                        final Locale ttsLocale = PCommon.GetLocale(_context, bbName, bNumberFrom, true);
                        final String chapterVerbose = _context.getString(PCommon.GetResId(_context, PCommon.ConcaT("tts", ttsLocale.getLanguage().toUpperCase(), "Chapter")));
                        String verseText;

                        ttsManager = new TtsManager(_context, ttsLocale);
                        final boolean isReady = ttsManager.WaitForReady();
                        if (isReady)
                        {
                            //SayNotification(); //TODO: disabled since targeting Android 12

                            while(cNumber <= chapterMax)
                            {
                                /* works but it's better to read all the book:
                                if (cNumber - cNumberFrom > chapterLimit - 1) return;
                                */

                                vNumber = cNumber == cNumberFrom ? vNumberFrom : 1;
                                final ArrayList<VerseBO> lstVerse = _dal.GetChapterFromPos(bbName, bNumberFrom, cNumber, vNumber);
                                if (lstVerse != null)
                                {
                                    if (!lstVerse.isEmpty())
                                    {
                                        if (ttsManager == null) break;

                                        SetListenPosition(_context, bbName, bNumberFrom, cNumber, vNumber);

                                        for(final VerseBO verse : lstVerse)
                                        {
                                            if (ttsManager == null) break;

                                            if (verse.vNumber == 1) {
                                                if (verse.cNumber == 1) ttsManager.SayAdd(PCommon.ConcaT(verse.bName, " "));

                                                ttsManager.SayAdd(PCommon.ConcaT(chapterVerbose, " ", verse.cNumber));
                                            }

                                            verseText = verse.vText.replaceAll("\\([0-9]*:[0-9]*\\)", "");
                                            ttsManager.SayAdd(verseText);

                                            SaveVerseNumber(verse.vNumber);
                                        }
                                    }
                                }

                                cNumber++;
                            }
                        }
                    }
                    catch(Exception ex)
                    {
                        if (PCommon._isDebug) PCommon.LogR(_context, ex);
                    }
                }

                private synchronized void SaveVerseNumber(final int vNumber)
                {
                    ttsCurrentVerseNumber = vNumber;
                }
            };
            ttsThread.setPriority(Thread.NORM_PRIORITY);
            ttsThread.start();
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * Shutdown TTS
     * @return current listen position or null if empty
     */
    String SayStop()
    {
        try
        {
            if (ttsMediaSession != null) ttsMediaSession.release();

            if (ttsPlayer != null) ttsPlayer.stop();

            if (ttsManager != null)
            {
                ttsManager.ShutDown();

                ttsManager = null;
            }

            if (ttsThread != null) ttsThread.interrupt();

            final String listenPosition;
            final String[] arr = GetListenPosition(_context);
            if (!(arr == null || arr.length < 4))
            {
                final String bbName = arr[0];
                final int bNumber = Integer.parseInt(arr[1]);
                final int cNumber = Integer.parseInt(arr[2]);
                final int vNumber = ttsCurrentVerseNumber;

                listenPosition = SetListenPosition(_context, bbName, bNumber, cNumber, vNumber);
            }
            else
            {
                listenPosition = null;
            }

            return listenPosition;
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            ttsManager = null;
        }

        return null;
    }

    /***
     * Back replay
     */
    synchronized void SayGetPreviousVerseNumber()
    {
        ttsCurrentVerseNumber = ttsCurrentVerseNumber - 5;

        if (ttsCurrentVerseNumber < 1) ttsCurrentVerseNumber = 1;
    }

    /***
     * Set current verse number
     * @param vNumber   vNumber
     */
    synchronized void SaySetCurrentVerseNumber(final int vNumber)
    {
        ttsCurrentVerseNumber = vNumber;
    }

    /***
     * Set current listen position
     * @param listenPosition    listenPosition
     */
    private synchronized void SaySetCurrentListenPosition(final String listenPosition)
    {
        ttsCurrentListenPosition = listenPosition;
    }

    /***
     * Get listen position
     * @return array of string with bbname, bnumber, cnumber, vnumber OR null
     */
    synchronized String[] GetListenPosition(final Context context)
    {
        final String listenPosition = ttsCurrentListenPosition;

        if (PCommon._isDebug)
        {
            System.out.println(PCommon.ConcaT("PCommon:GetListenPosition => ", listenPosition.isEmpty() ? "(empty)" : listenPosition));
        }

        return listenPosition.isEmpty() ? null : listenPosition.split(",");
    }

    /***
     * Get current verse number
     * @return ttsCurrentVerseNumber
     */
    synchronized int GetListenVerseNumber() { return ttsCurrentVerseNumber; }

    /***
     * Get listen position
     * @return  string with bbname, bnumber, cnumber, vnumber OR null
     */
    private synchronized String GetListnPosition(final Context context)
    {
        final String listenPosition = PCommon.GetPref(context, IProject.APP_PREF_KEY.LISTEN_POSITION, "");

        if (PCommon._isDebug)
        {
            System.out.println(PCommon.ConcaT("PCommon:GetListnPosition => ", listenPosition.isEmpty() ? "(empty)" : listenPosition));
        }

        return listenPosition.isEmpty() ? null : listenPosition;
    }

    /***
     * Save listen position
     */
    synchronized String SetListenPosition(final Context context, final String bbName, final int bNumber, final int cNumber, final int vNumber)
    {
        final String listenPosition = PCommon.ConcaT(bbName, ",", bNumber, ",", cNumber, ",", vNumber);

        try
        {
            this.SaySetCurrentListenPosition(listenPosition);

            PCommon.SavePref(context, IProject.APP_PREF_KEY.LISTEN_POSITION, listenPosition);

            if (PCommon._isDebug)
            {
                System.out.println(PCommon.ConcaT("PCommon:SetListenPosition => ", listenPosition));
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return listenPosition;
    }

    /*
     * Show notification
     *
    private void SayNotificationOld()
    {
        try
        {
            final String contentText;
            final String[] arrListen = GetListenPosition(_context);
            if (arrListen == null || arrListen.length < 4)
            {
                contentText = "";
            }
            else
            {
                final String listen_bbname = arrListen[0];
                final int listen_bnumber = Integer.parseInt(arrListen[1]);
                final BibleRefBO bookRef = GetBookRef(listen_bbname, listen_bnumber);

                contentText = bookRef.bName;
            }

            final String channelId = "thelight_channel_id";
            final NotificationManager notificationManager = (NotificationManager) _context.getSystemService(_context.NOTIFICATION_SERVICE);

            final PendingIntent pendingIntent_STOP_CLICK;
            final PendingIntent pendingIntent_PREVIOUS_CLICK;
            final int flags = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) ? PendingIntent.FLAG_IMMUTABLE : 0;

            final Intent intentSayStop = new Intent(_context, BibleIntentService.class);
            intentSayStop.setAction("STOP");

            final Intent intentSayPrevious = new Intent(_context, BibleIntentService.class);
            intentSayPrevious.setAction("PREVIOUS");

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            {
                pendingIntent_STOP_CLICK = PendingIntent.getForegroundService(_context, 0, intentSayStop, flags);
                pendingIntent_PREVIOUS_CLICK = PendingIntent.getForegroundService(_context, 0, intentSayPrevious, flags);
            }
            else
            {
                pendingIntent_STOP_CLICK = PendingIntent.getService(_context, 0, intentSayStop, flags);
                pendingIntent_PREVIOUS_CLICK = PendingIntent.getService(_context, 0, intentSayPrevious, flags);
            }

            final NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(_context, channelId)
                    .setAutoCancel(false)
                    .setPriority(NotificationCompat.PRIORITY_LOW)
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                    .setSmallIcon(R.drawable.thelightnotif)
                    .addAction(R.drawable.ic_skip_previous_24dp, _context.getString(R.string.mnuListenReplay), pendingIntent_PREVIOUS_CLICK)
                    .addAction(R.drawable.ic_stop_white_24dp, _context.getString(R.string.mnuListenStop), pendingIntent_STOP_CLICK)
                    .setContentTitle(_context.getString(R.string.channelAudioBibleName))
                    .setContentText(contentText)
                    .setCategory(NotificationCompat.CATEGORY_STATUS)
                    .setColorized(true)
                    .setWhen(System.currentTimeMillis())
                    .setShowWhen(false)
                    .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            {
                final int channelImportance = NotificationManager.IMPORTANCE_LOW;
                final String channelName = _context.getString(R.string.channelAudioBibleName);
                final NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, channelImportance);
                notificationChannel.setDescription(channelName);
                //notificationChannel.setAllowBubbles(false);   //TODO NEXT: bug on my phone
                notificationChannel.setShowBadge(false);
                notificationChannel.enableLights(false);
                notificationChannel.enableVibration(false);

                Objects.requireNonNull(notificationManager).createNotificationChannel(notificationChannel);
            }

            Objects.requireNonNull(notificationManager).notify(1, notificationBuilder.build());
        }
        catch (Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(_context, ex);
        }
    }
    */

    /***
     * Add todo
     * @param td todo
     */
    void AddTodo(final TodoBO td)
    {
        try
        {
            _dal.AddTodo(td);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * Update todo
     * @param td    todo
     */
    void UpdateTodo(final TodoBO td)
    {
        try
        {
            _dal.UpdateTodo(td);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * Reset todo status for todos in status TODO, DONE
     */
    void ResetTodoStatus()
    {
        try
        {
            _dal.ResetTodoStatus();
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * Delete todo
     * @param tdId  Id
     */
    void DeleteTodo(final int tdId)
    {
        try
        {
            _dal.DeleteTodo(tdId);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * Get new Todo Id
     * @return new todo Id
     */
    int GetNewTodoId()
    {
        return _dal.GetNewTodoId();
    }

    /***
     * Get Todo Id count
     * @return Todo Id count
     */
    int GetTodoIdCount()
    {
        return _dal.GetTodoIdCount();
    }

    /***
     * Get list todos
     * @return List todos by status
     */
    ArrayList<TodoBO> GetListTodoByStatus(final String tdStatus)
    {
        return _dal.GetListTodoByStatus(tdStatus);
    }

    /***
     * Get Todo count by status
     * @param tdStatus
     * @return Count
     */
    @SuppressWarnings("JavaDoc")
    int GetTodoCountByStatus(final String tdStatus)
    {
        return _dal.GetTodoCountByStatus(tdStatus);
    }

    /***
     * Get todo
     * @param tdId  Todo Id
     * @return List todos by status
     */
    TodoBO GetTodo(final int tdId)
    {
        TodoBO td = null;

        try
        {
            td = _dal.GetTodo(tdId);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return td;
    }

    /***
     * Get list random verses
     * @param bbName
     * @param booksSelection
     * @param quantity
     * @return list of verses
     */
    @SuppressWarnings("JavaDoc")
    ArrayList<VerseBO> GetListRandomVerses(final Context context, final String bbName, final String booksSelection, int quantity)
    {
        ArrayList<VerseBO> lstVerse = new ArrayList<>();
        ArrayList<Integer> lstBooksSelected = new ArrayList<>();

        try
        {
            if (booksSelection.length() != 66) return null;
            if (quantity <= 0) quantity = 5;

            //Get books selected
            for(int j=0; j <= 65; j++) {
                if (booksSelection.substring(j, j+1).equalsIgnoreCase("1")) {
                    lstBooksSelected.add(j+1);
                }
            }
            if (lstBooksSelected.isEmpty()) {
                for(int j=0; j <= 65; j++) {
                    lstBooksSelected.add(j);
                }
            }

            //Add copies for better random
            final int copyCount = 20 - lstBooksSelected.size();
            for(int copy=0; copy < copyCount; copy++) {
                lstBooksSelected.add(lstBooksSelected.get(copy));
            }

            //Limits
            final int maxBookIndex = lstBooksSelected.size() - 1;
            int rndBibleId, rndBookIndex, min, max, bNumber;
            Integer[] ci;
            VerseBO verse;

            //Generate
            for(int i=1; i <= quantity; i++)
            {
                //Get book
                rndBookIndex = PCommon.GetRandomInt(context, 0, maxBookIndex);
                bNumber = lstBooksSelected.get(rndBookIndex);
                ci = GetBibleCiByBook(bNumber);
                min = GetBookIdMinByBookNumber(bbName, bNumber);
                max = min + ci[1] - 1;

                //Get verse
                rndBibleId = PCommon.GetRandomInt(context, min, max);
                verse = GetVerse(rndBibleId);
                lstVerse.add(verse);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return lstVerse;
    }

    /***
     * Get list of bookmarks by ID
     * @param resIdItem0Emo Give -1 for not changing it
     * @param resIdItem0Desc Give -1 for not changing it
     * @return list all bookmarks
     */
    @SuppressWarnings("JavaDoc")
    TreeMap<Integer, BookmarkBO> GenerateBookmarkCompleteMap(final Context context, final int resIdItem0Emo, final int resIdItem0Desc)
    {
        final TreeMap<Integer, BookmarkBO> lstBookmarkUpd = new TreeMap<>();

        try
        {
            final ArrayList<BookmarkBO> lstBookmark = _dal.GetListAllBookmark();
            for (BookmarkBO bm : lstBookmark) {
                if (bm.bmId < 1000) {
                    final int resId = PCommon.GetResId(context, PCommon.ConcaT("fav", bm.bmId));
                    bm.bmDesc = context.getString(resId);
                }
                bm.bmRepresentation = PCommon.ConcaT(bm.bmCurrent, " ", bm.bmDesc);
                lstBookmarkUpd.put(bm.bmId, bm);
            }

            //Item0
            if (resIdItem0Emo >= 0 || resIdItem0Desc >= 0) {
                final BookmarkBO bmItem0 = lstBookmarkUpd.get(0);
                if (bmItem0 != null) {
                    if (resIdItem0Emo >= 0) bmItem0.bmCurrent = context.getString(resIdItem0Emo);
                    if (resIdItem0Desc >= 0) bmItem0.bmDesc = context.getString(resIdItem0Desc);
                    bmItem0.bmRepresentation = PCommon.ConcaT(bmItem0.bmCurrent, " ", bmItem0.bmDesc);
                    lstBookmarkUpd.put(0, bmItem0);
                }
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return lstBookmarkUpd;
    }

    /***
     * Get map of bookmarks with only emojis through _fav.
     * @return Map of bookmarks with only emojis
     */
    Map<Integer, String> GenerateBookmarkShortMap(final Context context)
    {
        final Map<Integer, String> favMap = new HashMap<>();

        try
        {
            final Map<Integer, BookmarkBO> mapBm = this.GenerateBookmarkCompleteMap(context,-1,-1);
            for (BookmarkBO bm : mapBm.values()) favMap.put(bm.bmId, bm.bmCurrent);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }

        return favMap;
    }

    /***
     * Get list all bookmark IDs
     * @return list all bookmark IDs
     */
    ArrayList<Integer> GetListAllBookmarkId()
    {
        return _dal.GetListAllBookmarkId();
    }

    /***
     * Get LexAggr count
     * @return count
     */
    int GetLexAggrCount()
    {
        int count = 0;

        try
        {
            count = _dal.GetLexAggrCount();
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);

            return count;
        }

        return count;
    }

    /***
     * Get interlinear of a verse
     * @param tbbName
     * @param bNumber
     * @param cNumber
     * @param vNumber
     * @param bibleId
     * @return Map of lists
     */
    @SuppressWarnings("JavaDoc")
    Map<String, ArrayList<String>> GetVerseInterlinear(final String tbbName, final int bNumber, final int cNumber, final int vNumber, final int bibleId)
    {
        return _dal.GetVerseInterlinear(tbbName, bNumber, cNumber, vNumber, bibleId);
    }

    /***
     * Get Lex Details by HGNr
     * @param hgNr  Ref: H9999-G9999
     * @return List of words
     */
    @SuppressWarnings("JavaDoc")
    ArrayList<LexTbesBO> GetLexDetailByHGNr(final String hgNr)
    {
        return _dal.GetLexDetailByHGNr(hgNr);
    }

    /***
     * Has Lex Detail by HGNr
     * @param hgNr  Ref H9999-G9999
     * @return True/False
     */
    @SuppressWarnings("JavaDoc")
    boolean HasLexDetailByHGNr(final String hgNr) {
        return _dal.HasLexDetailByHGNr(hgNr);
    }

    /***
     * Get Lex Details by word
     * @param word  Word to search
     * @param lex_detail_search_type    Search type
     * @return List of words
     */
    @SuppressWarnings("JavaDoc")
    ArrayList<LexTbesBO> GetLexDetailByWord(final String word, final PCommon.LEX_DETAIL_SEARCH_TYPE lex_detail_search_type)
    {
        return _dal.GetLexDetailByWord(word, lex_detail_search_type);
    }

    /***
     * Get Lex Details by word Extended (full search)
     * @param word  Word to search (will be not trimmed)
     * @return Map lex: "LEX" (list: ArrayList<LexTbesBO>), "LEX_EXPR" (LEX_DETAIL_SEARCH_FIELD)
     */
    @SuppressWarnings("JavaDoc")
    Map<String, Object> GetLexDetailByWordExtended(final String word)
    {
        return _dal.GetLexDetailByWordExtended(word);
    }

    /***
     * Has Lex Detail by word
     * @param word  Word to search
     * @param lex_detail_search_type    Search type
     * @return True/False
     */
    boolean HasLexDetailByWord(final String word, final PCommon.LEX_DETAIL_SEARCH_TYPE lex_detail_search_type) {
        return _dal.HasLexDetailByWord(word, lex_detail_search_type);
    }

    /***
     * Get Lex Detail by Id
     */
    @SuppressWarnings("JavaDoc")
    LexTbesBO GetLexDetail(final int lexId) {
        return _dal.GetLexDetail(lexId);
    }

    /***
     * Add all Lex refs to cache search
     * @param bbName
     * @param word
     * @param tabId
     */
    @SuppressWarnings("JavaDoc")
    void AddAllLexRefToCacheSearch(final String bbName, final String word, final int tabId) {
        _dal.AddAllLexRefToCacheSearch(bbName, word, tabId);
    }

    //</editor-fold>
}
