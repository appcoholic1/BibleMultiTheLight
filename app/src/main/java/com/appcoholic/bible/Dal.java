
package com.appcoholic.bible;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.format.DateFormat;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Dal
{
    //<editor-fold defaultstate="collapsed" desc="-- Variables --">

    private SQLiteDatabase _db = null;
    private DbHelper _dbHelper = null;
    private Context _context = null;

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="-- Constructors --">

    /***
     * Using specified context
     * @param ctx   context
     */
    Dal(final Context ctx)
    {
        //TODO: method with context may be deprecated
        Init(ctx);
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="-- Open/Close/Utils --">

    @SuppressLint("UnsafeOptInUsageError")
    private void Init(Context ctx)
    {
        try
        {
            _context = ctx;
            _dbHelper = new DbHelper(_context);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * Open db connection
     */
    void OpenReadWrite()
    {
        try
        {
            _db = _dbHelper.getWritableDatabase();
            //deprecated _db.setLockingEnabled(false);
            _db.execSQL("PRAGMA synchronous=OFF");
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * CloseDb db connection
     */
    void CloseDb()
    {
        try
        {
            if (_dbHelper != null) _dbHelper.close();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * Db is open?
     * @return  true/false
     */
    boolean IsDbOpen()
    {
        boolean isDbOpen = false;

        try
        {
            if (_db == null)
            {
                return false;
            }

            isDbOpen = _db.isOpen();
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return isDbOpen;
    }

    /**
     * Shrink db
     */
    void ShrinkDb(final Context context)
    {
        String sql = "VACUUM";

        try
        {
            _db.execSQL(sql);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;
        }
    }

   /***
     * Get db version
     * @return db version
     */
    int GetDbVersion()
    {
        int dbVersion = -1;

        try
        {
            dbVersion = _db.getVersion();
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return dbVersion;
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="-- Log --">

    /***
     * Get list of log
     * @return list all logs
     */
    @SuppressWarnings("JavaDoc")
    ArrayList<String> GetListAllLog()
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        ArrayList<String> lst = new ArrayList<>();

        try
        {
            String log;
            sql = PCommon.ConcaT("SELECT * FROM log");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            while (!c.isAfterLast())
            {
                log = c.getString(0);
                lst.add(log);

                c.moveToNext();
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return lst;
    }

    /***
     * Add a act
     * @param msg
     */
    @SuppressWarnings("JavaDoc")
    void AddLog(final String msg)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;

        try
        {
            final String formatedSqlField = PCommon.RQ(msg);
            sql = PCommon.ConcaT("INSERT INTO log (msg) VALUES ('", formatedSqlField, "')");

            _db.execSQL(sql);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;
        }
    }

    /***
     * Delete all logs
     * @param isForceDelete True=force delete
     */
    void DeleteAllLogs(final boolean isForceDelete)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        int count = 0;

        try
        {
            sql = PCommon.ConcaT("SELECT COUNT(*) FROM LOG");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast()) count = c.getInt(0);

            if (isForceDelete || count > 100) _db.execSQL("DELETE FROM log");
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null) {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="-- Bible --">

    /***
     * Get tabId of an article
     * @param artName   ART_NAME
     * @return Negative value if not found
     */
    int GetArticleTabId(final String artName)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        int tabId = -1;

        try
        {
            sql = PCommon.ConcaT("SELECT tabId FROM cacheTab WHERE fullQuery=", PCommon.AQ(artName), " LIMIT 1");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast())
            {
                tabId = c.getInt(0);
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return tabId;
    }

    /***
     * Get a verse
     * @param bibleId
     * @return verse
     */
    @SuppressWarnings("JavaDoc")
    VerseBO GetVerse(final int bibleId)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        VerseBO verse = null;

        try
        {
            sql = PCommon.ConcaT("SELECT b.bbName, b.bNumber, b.cNumber, b.vNumber, b.vText, n.mark, r.bName, r.bsName, t.tot FROM bible b",
                    " INNER JOIN bibleRef r ON r.bbName=b.bbName AND r.bNumber=b.bNumber",
                    " LEFT OUTER JOIN bibleNote n ON n.bNumber=b.bNumber AND n.cNumber=b.cNumber AND n.vNumber=b.vNumber",
                    " LEFT OUTER JOIN bibleCrossRefi t ON t.bNumber=b.bNumber AND t.cNumber=b.cNumber AND t.vNumber=b.vNumber",
                    " WHERE b.id=", bibleId);

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast())
            {
                verse = new VerseBO();
                verse.id = bibleId;
                verse.bbName = c.getString(0);
                verse.bName = c.getString(6);
                verse.bsName = c.getString(7);
                verse.bNumber = c.getInt(1);
                verse.cNumber = c.getInt(2);
                verse.vNumber = c.getInt(3);
                verse.vText = c.getString(4);
                verse.mark = c.getInt(5);
                verse.crCount = c.getInt(8);
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return verse;
    }

    /***
     * Get a verse
     * @param tbbName
     * @param bNumber
     * @param cNumber
     * @param vNumber
     * @return verse
     */
    @SuppressWarnings("JavaDoc")
    ArrayList<VerseBO> GetVerse(final String tbbName, final int bNumber, final int cNumber, final int vNumber)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        ArrayList<VerseBO> lstVerse = new ArrayList<>();

        try
        {
            VerseBO verse;

            sql = PCommon.ConcaT("SELECT b.id, b.vText, r.bName, r.bsName, n.mark, b.bbName, ", this.CaseBible("b.bbName", tbbName), ", t.tot",
                    " FROM bible b",
                    " INNER JOIN bibleRef r ON r.bbName=b.bbName AND r.bNumber=b.bNumber",
                    " LEFT OUTER JOIN bibleNote n ON n.bNumber=b.bNumber AND n.cNumber=b.cNumber AND n.vNumber=b.vNumber",
                    " LEFT OUTER JOIN bibleCrossRefi t ON t.bNumber=b.bNumber AND t.cNumber=b.cNumber AND t.vNumber=b.vNumber",
                    " WHERE b.bbName IN ", this.InBible(tbbName),
                    " AND b.bNumber=", bNumber,
                    " AND b.cNumber=", cNumber,
                    " AND b.vNumber=", vNumber,
                    " ORDER BY bbNameOrder ASC");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            while (!c.isAfterLast())
            {
                verse = new VerseBO();
                verse.id = c.getInt(0);
                verse.bbName = c.getString(5);
                verse.bName = c.getString(2);
                verse.bsName = c.getString(3);
                verse.bNumber = bNumber;
                verse.cNumber = cNumber;
                verse.vNumber = vNumber;
                verse.vText = c.getString(1);
                verse.mark = c.getInt(4);
                verse.crCount = c.getInt(7);
                lstVerse.add(verse);

                c.moveToNext();
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return lstVerse;
    }

    /***
     * Get a list of verses
     * @param tbbName
     * @param bNumber
     * @param cNumber
     * @param vNumberFrom
     * @param vNumberTo
     * @return list of verses
     */
    @SuppressWarnings("JavaDoc")
    ArrayList<VerseBO> GetVerses(final String tbbName, final int bNumber, final int cNumber, final int vNumberFrom, final int vNumberTo)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        ArrayList<VerseBO> lstVerse = new ArrayList<>();

        try
        {
            VerseBO verse;
            final int vNumberToFix = vNumberTo < vNumberFrom ? vNumberFrom : vNumberTo;

            sql = PCommon.ConcaT("SELECT b.id, b.vNumber, b.vText, r.bName, r.bsName, n.mark, b.bbName, ", this.CaseBible("b.bbName", tbbName), ", t.tot",
                    " FROM bible b",
                    " INNER JOIN bibleRef r ON r.bbName=b.bbName AND r.bNumber=b.bNumber",
                    " LEFT OUTER JOIN bibleNote n ON n.bNumber=b.bNumber AND n.cNumber=b.cNumber AND n.vNumber=b.vNumber",
                    " LEFT OUTER JOIN bibleCrossRefi t ON t.bNumber=b.bNumber AND t.cNumber=b.cNumber AND t.vNumber=b.vNumber",
                    " WHERE b.bbName IN ", this.InBible(tbbName),
                    " AND b.bNumber=", bNumber,
                    " AND b.cNumber=", cNumber,
                    " AND b.vNumber >= ", vNumberFrom,
                    " AND b.vNumber <= ", vNumberToFix,
                    " ORDER BY b.vNumber ASC, bbNameOrder ASC");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            while (!c.isAfterLast())
            {
                verse = new VerseBO();
                verse.id = c.getInt(0);
                verse.bbName = c.getString(6);
                verse.bName = c.getString(3);
                verse.bsName = c.getString(4);
                verse.bNumber = bNumber;
                verse.cNumber = cNumber;
                verse.vNumber = c.getInt(1);
                verse.vText = c.getString(2);
                verse.mark = c.getInt(5);
                verse.crCount = c.getInt(8);
                lstVerse.add(verse);

                c.moveToNext();
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return lstVerse;
    }

    /***
     * Get Id of verses only for bbName
     * @param bbName
     * @param bNumber
     * @param cNumber
     * @param vNumberFrom
     * @param vNumberTo
     * @return list of verses Id
     */
    @SuppressWarnings("JavaDoc")
    ArrayList<Integer> GetVersesId(final String bbName, final int bNumber, final int cNumber, final int vNumberFrom, final int vNumberTo)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        ArrayList<Integer> lstId = new ArrayList<>();

        try
        {
            final String vNumberToClause = vNumberTo >= vNumberFrom ? PCommon.ConcaT(" AND b.vNumber <= ", vNumberTo) : "";

            sql = PCommon.ConcaT("SELECT b.id FROM bible b",
                    " WHERE b.bbName=", PCommon.AQ(bbName),
                    " AND b.bNumber=", bNumber,
                    " AND b.cNumber=", cNumber,
                    " AND b.vNumber >= ", vNumberFrom,
                    vNumberToClause,
                    " ORDER BY b.vNumber ASC");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            while (!c.isAfterLast())
            {
                lstId.add(c.getInt(0));

                c.moveToNext();
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return lstId;
    }

    /***
     * Copy cache search for other bible (data are deleted before copying)
     * @param tabIdTo
     * @param tbbName
     * @param bNumberStart
     * @param cNumberStart
     * @param vNumberStart
     * @param bNumberEnd
     * @param cNumberEnd
     * @param vNumberEnd
     * @return true if copy was successful
     */
    @SuppressWarnings("JavaDoc")
    boolean CopyCacheSearchForOtherBible(final int tabIdTo, final String tbbName, final int bNumberStart, final int cNumberStart, final int vNumberStart, final int bNumberEnd, final int cNumberEnd, final int vNumberEnd)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;

        try
        {
            this.DeleteCacheSearch(tabIdTo);

            VerseBO verse;
            ArrayList<VerseBO> lstVerseK;
            lstVerseK = this.GetVerse("k", bNumberStart, cNumberStart, vNumberStart);
            if (lstVerseK.size() <= 0) return false;
            verse = lstVerseK.get(0);
            final int idKStart = verse.id;
            lstVerseK.clear();
            //noinspection UnusedAssignment
            lstVerseK = null;

            lstVerseK = this.GetVerse("k", bNumberEnd, cNumberEnd, vNumberEnd);
            if (lstVerseK.size() <= 0) return false;
            verse = lstVerseK.get(0);
            final int idKEnd = verse.id;
            lstVerseK.clear();
            //noinspection UnusedAssignment
            lstVerseK = null;

            String bbnameTo;
            int size = tbbName.length();
            for (int i = 0; i < size; i++)
            {
                bbnameTo = tbbName.substring(i, i + 1);

                sql = PCommon.ConcaT(
                        "INSERT INTO cacheSearch (tabId, bibleId) ",
                        "SELECT ", tabIdTo ,", (SELECT i.id FROM bible i WHERE i.bbName=", PCommon.AQ(bbnameTo)," AND i.bNumber=r.bNumber AND i.cNumber=r.cNumber AND i.vNumber=r.vNumber) ",
                        "FROM (",
                        " SELECT distinct b.bNumber, b.cNumber, b.vNumber",
                        " FROM bible b",
                        " WHERE b.id >= ", idKStart, " AND b.id <= ", idKEnd,
                        " ORDER BY b.bNumber, b.cNumber, b.vNumber ASC) r" );

                _db.execSQL(sql);
            }

            return true;
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;
        }

        return false;
    }

    /***
     * Get verse text
     * @param tbbName
     * @param bNumber
     * @param cNumber
     * @param vNumber
     * @return Text with verse
     */
    @SuppressWarnings("JavaDoc")
    String GetVerseText(final String tbbName, final int bNumber, final int cNumber, final int vNumber)
    {
        ArrayList<VerseBO> lstVerse = null;
        StringBuilder sb = new StringBuilder();

        try
        {
            String verseText;

            lstVerse = GetVerse(tbbName, bNumber, cNumber, vNumber);

            if (lstVerse != null)
            {
                for (VerseBO v : lstVerse)
                {
                    verseText =  PCommon.ConcaT(v.bsName, " ", cNumber, ".", v.vNumber, ": ", v.vText, "\n\n");

                    sb.append(verseText);
                }
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            if (lstVerse != null)
            {
                lstVerse.clear();
                //noinspection UnusedAssignment
                lstVerse = null;
            }
        }

        return sb.toString();
    }

    /***
     * Get a chapter from vNumber
     * @param tbbName
     * @param bNumber
     * @param cNumber
     * @param vNumberFrom From vNumber
     * @return list of verses
     */
    @SuppressWarnings("JavaDoc")
    ArrayList<VerseBO> GetChapterFromPos(final String tbbName, final int bNumber, final int cNumber, final int vNumberFrom)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        ArrayList<VerseBO> lstVerse = new ArrayList<>();

        try
        {
            VerseBO verse;

            sql = PCommon.ConcaT("SELECT b.id, b.vNumber, b.vText, r.bName, r.bsName, n.mark, b.bbName, ", this.CaseBible("b.bbName", tbbName), ", t.tot",
                    " FROM bible b",
                    " INNER JOIN bibleRef r ON r.bbName=b.bbName AND r.bNumber=b.bNumber",
                    " LEFT OUTER JOIN bibleNote n ON n.bNumber=b.bNumber AND n.cNumber=b.cNumber AND n.vNumber=b.vNumber",
                    " LEFT OUTER JOIN bibleCrossRefi t ON t.bNumber=b.bNumber AND t.cNumber=b.cNumber AND t.vNumber=b.vNumber",
                    " WHERE b.bbName IN ", this.InBible(tbbName),
                    " AND b.bNumber=", bNumber,
                    " AND b.cNumber=", cNumber,
                    " AND b.vNumber >= ", vNumberFrom,
                    " ORDER BY b.vNumber ASC, bbNameOrder ASC");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            while (!c.isAfterLast())
            {
                verse = new VerseBO();
                verse.id = c.getInt(0);
                verse.bbName = c.getString(6);  //Was: bbName;
                verse.bName = c.getString(3);
                verse.bsName = c.getString(4);
                verse.bNumber = bNumber;
                verse.cNumber = cNumber;
                verse.vNumber = c.getInt(1);
                verse.vText = c.getString(2);
                verse.mark = c.getInt(5);
                verse.crCount = c.getInt(8);
                lstVerse.add(verse);

                c.moveToNext();
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return lstVerse;
    }

    /***
     * Get cross references
     * @param tbbName
     * @param bNumber
     * @param cNumber
     * @param vNumber
     * @return map
     */
    @SuppressWarnings("JavaDoc")
    Map<String, Object> GetCrossReferences(final String tbbName, final int bNumber, final int cNumber, final int vNumber)
    {
        String sql;
        Cursor c = null;
        VerseBO verse;
        ArrayList<VerseBO> lstVerse = new ArrayList<>();
        TreeMap<Integer, Integer> mapBookJump = new TreeMap<>();
        Map<String, Object> mapResult = new HashMap<>();

        try
        {
            //1: LSTVERSE
            int i = 0;
            while (i <= 1)
            {
                if (i == 0)
                {
                    sql = PCommon.ConcaT(
                            "SELECT b.id, b.vNumber, b.vText, r.bName, r.bsName, n.mark, b.bbName, ", this.CaseBible("b.bbName", tbbName), ",",
                            " t.tot, b.bNumber, b.cNumber",
                            " FROM bible b",
                            " INNER JOIN bibleRef r ON r.bbName=b.bbName AND r.bNumber=b.bNumber",
                            " LEFT OUTER JOIN bibleNote n ON n.bNumber=b.bNumber AND n.cNumber=b.cNumber AND n.vNumber=b.vNumber",
                            " LEFT OUTER JOIN bibleCrossRefi t ON t.bNumber=b.bNumber AND t.cNumber=b.cNumber AND t.vNumber=b.vNumber",
                            " WHERE b.bbName IN ", this.InBible(tbbName),
                            " AND b.bNumber=", bNumber, " and b.cNumber=", cNumber, " and b.vNumber=", vNumber,
                            " ORDER BY b.vNumber ASC, bbNameOrder ASC");
                }
                else
                {
                    sql = PCommon.ConcaT("SELECT b.id, b.vNumber, b.vText, r.bName, r.bsName, n.mark, b.bbName, ", this.CaseBible("b.bbName", tbbName), ",",
                            " t.tot, b.bNumber, b.cNumber",
                            " FROM bibleCrossRef c",
                            " INNER JOIN bible b ON b.bNumber=c.bNumberTo AND b.cNumber=c.cNumberTo AND b.vNumber=c.vNumberTo",
                            " INNER JOIN bibleRef r ON r.bbName=b.bbName AND r.bNumber=b.bNumber",
                            " LEFT OUTER JOIN bibleNote n ON n.bNumber=b.bNumber AND n.cNumber=b.cNumber AND n.vNumber=b.vNumber",
                            " LEFT OUTER JOIN bibleCrossRefi t ON t.bNumber=b.bNumber AND t.cNumber=b.cNumber AND t.vNumber=b.vNumber",
                            " WHERE b.bbName IN ", this.InBible(tbbName),
                            " AND c.bNumberFrom=", bNumber, " and c.cNumberFrom=", cNumber, " and c.vNumberFrom=", vNumber,
                            " ORDER BY c.crId ASC, bbNameOrder ASC");
                }

                c =  _db.rawQuery(sql, null);
                c.moveToFirst();

                while (!c.isAfterLast())
                {
                    verse = new VerseBO();
                    verse.id = c.getInt(0);
                    verse.bbName = c.getString(6);  //Was: bbName;
                    verse.bName = c.getString(3);
                    verse.bsName = c.getString(4);
                    verse.bNumber = c.getInt(9);
                    verse.cNumber = c.getInt(10);
                    verse.vNumber = c.getInt(1);
                    verse.vText = c.getString(2);
                    verse.mark = c.getInt(5);
                    verse.crCount = c.getInt(8);
                    lstVerse.add(verse);

                    c.moveToNext();
                }

                i++;
            }

            //2: BOOKJUMP
            final int kStart = tbbName.length();
            for (int k = kStart; k < lstVerse.size(); k++)
            {
                verse = lstVerse.get(k);

                if (!mapBookJump.containsKey(verse.bNumber)) mapBookJump.put(verse.bNumber, k);
            }

            //---
            mapResult.put("LSTVERSE", lstVerse);
            mapResult.put("BOOKJUMP", mapBookJump);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return mapResult;
    }

    /***
     * Get cross references count
     * @param bNumber
     * @param cNumber
     * @param vNumber
     * @return Count of cross references found
     */
    @SuppressWarnings("JavaDoc")
    int GetCrossReferencesCount(final int bNumber, final int cNumber, final int vNumber)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        int count = 0;

        try
        {
            sql = PCommon.ConcaT("SELECT COUNT(*) FROM bibleCrossRefi WHERE bNumber=", bNumber, " AND cNumber=", cNumber, " AND vNumber=", vNumber);

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast()) {
                count = c.getInt(0);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null) {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return count;
    }

    /***
     * Get chapter text
     * @param tbbName
     * @param bNumber
     * @param cNumber
     * @return text of chapter
     */
    @SuppressWarnings("JavaDoc")
    String GetChapterText(final String tbbName, final int bNumber, final int cNumber)
    {
        ArrayList<VerseBO> lstVerse = null;
        StringBuilder sb = new StringBuilder();

        try
        {
            String verseText;

            lstVerse = GetChapterFromPos(tbbName, bNumber, cNumber, 1);

            if (lstVerse != null)
            {
                for (VerseBO v : lstVerse)
                {
                    verseText =  PCommon.ConcaT(v.bsName, " ", cNumber, ".", v.vNumber, ": ", v.vText, "\n\n");

                    sb.append(verseText);
                }
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            if (lstVerse != null)
            {
                lstVerse.clear();
                //noinspection UnusedAssignment
                lstVerse = null;
            }
        }

        return sb.toString();
    }

    /***
     * Search bible
     * @param bbName
     * @param bNumber
     * @param cNumber
     * @param searchString
     * @return list of verses
     */
    @SuppressWarnings("JavaDoc")
    ArrayList<VerseBO> SearchBible(final String bbName, final int bNumber, final int cNumber, final String searchString)
    {
        //TODO: maybe add ID in this call :)  for SearchFragment and GetVerse

        String sql;
        Cursor c = null;
        ArrayList<VerseBO> lstVerse = new ArrayList<>();

        try
        {
            //TODO: check if % was entered and SQL INJECTION with '
            VerseBO verse;
            final String searchStringClause = SearchStringClause(searchString, true);
            if (searchStringClause.isEmpty()) return null;

            sql = PCommon.ConcaT("SELECT b.id, b.vNumber, b.vText, r.bName, r.bsName, n.mark, t.tot FROM bible b",
                    " INNER JOIN bibleRef r ON r.bbName=b.bbName AND r.bNumber=b.bNumber",
                    " LEFT OUTER JOIN bibleNote n ON n.bNumber=b.bNumber AND n.cNumber=b.cNumber AND n.vNumber=b.vNumber",
                    " LEFT OUTER JOIN bibleCrossRefi t ON t.bNumber=b.bNumber AND t.cNumber=b.cNumber AND t.vNumber=b.vNumber",
                    " WHERE b.bbName=", PCommon.AQ(bbName),
                    " AND b.bNumber=", bNumber,
                    " AND b.cNumber=", cNumber,
                    searchStringClause,
                    " ORDER BY b.bNumber, b.cNumber, b.vNumber ASC");

            //" AND b.vText like ", PCommon.AQ(PCommon.RQ(searchString)),

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            while (!c.isAfterLast())
            {
                verse = new VerseBO();
                verse.id = c.getInt(0);
                verse.bbName = bbName;
                verse.bName = c.getString(3);
                verse.bsName = c.getString(4);
                verse.bNumber = bNumber;
                verse.cNumber = cNumber;
                verse.vNumber = c.getInt(1);
                verse.vText = c.getString(2);
                verse.mark = c.getInt(5);
                verse.crCount = c.getInt(6);
                lstVerse.add(verse);

                c.moveToNext();
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return lstVerse;
    }

    /***
     * Search bible
     * @param bbName
     * @param bNumber
     * @param searchString
     * @return list of verses
     */
    @SuppressWarnings("JavaDoc")
    ArrayList<VerseBO> SearchBible(final String bbName, final int bNumber, final String searchString)
    {
        //TODO: maybe add ID in this call :)  for SearchFragment and GetVerse

        String sql;
        Cursor c = null;
        ArrayList<VerseBO> lstVerse = new ArrayList<>();

        try
        {
            //TODO: check if % was entered and SQL INJECTION with '
            VerseBO verse;
            final String searchStringClause = SearchStringClause(searchString, true);
            if (searchStringClause.isEmpty()) return null;

            sql = PCommon.ConcaT("SELECT b.id, b.cNumber, b.vNumber, b.vText, r.bName, r.bsName, n.mark, t.tot FROM bible b",
                    " INNER JOIN bibleRef r ON r.bbName=b.bbName AND r.bNumber=b.bNumber",
                    " LEFT OUTER JOIN bibleNote n ON n.bNumber=b.bNumber AND n.cNumber=b.cNumber AND n.vNumber=b.vNumber",
                    " LEFT OUTER JOIN bibleCrossRefi t ON t.bNumber=b.bNumber AND t.cNumber=b.cNumber AND t.vNumber=b.vNumber",
                    " WHERE b.bbName=", PCommon.AQ(bbName),
                    " AND b.bNumber=", bNumber,
                    searchStringClause,
                    " ORDER BY b.bNumber, b.cNumber, b.vNumber ASC");

            //was: " AND b.vText like ", PCommon.AQ(PCommon.RQ(searchString)),

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            while (!c.isAfterLast())
            {
                verse = new VerseBO();
                verse.id = c.getInt(0);
                verse.bbName = bbName;
                verse.bName = c.getString(4);
                verse.bsName = c.getString(5);
                verse.bNumber = bNumber;
                verse.cNumber = c.getInt(1);
                verse.vNumber = c.getInt(2);
                verse.vText = c.getString(3);
                verse.mark = c.getInt(6);
                verse.crCount = c.getInt(7);
                lstVerse.add(verse);

                c.moveToNext();
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return lstVerse;
    }

    /***
     * Search bible
     * @param bbName
     * @param searchString
     * @return map
     */
    @SuppressWarnings("JavaDoc")
    Map<String, Object> SearchBible(final String bbName, final String searchString)
    {
        //TODO: maybe add ID in this call :)  for SearchFragment and GetVerse

        String sql;
        Cursor c = null;
        VerseBO verse;
        ArrayList<VerseBO> lstVerse = new ArrayList<>();
        TreeMap<Integer, Integer> mapBookJump = new TreeMap<>();
        Map<String, Object> mapResult = new HashMap<>();

        try
        {
            //TODO: check if % was entered and SQL INJECTION with '

            //1: LSTVERSE
            final String searchStringClause = SearchStringClause(searchString,true);
            if (searchStringClause.isEmpty()) return null;

            //was: " AND b.vText like ", PCommon.AQ(PCommon.RQ(searchString)),
            sql = PCommon.ConcaT("SELECT b.id, b.bNumber, b.cNumber, b.vNumber, b.vText, r.bName, r.bsName, n.mark, t.tot FROM bible b",
                    " INNER JOIN bibleRef r ON r.bbName=b.bbName AND r.bNumber=b.bNumber",
                    " LEFT OUTER JOIN bibleNote n ON n.bNumber=b.bNumber AND n.cNumber=b.cNumber AND n.vNumber=b.vNumber",
                    " LEFT OUTER JOIN bibleCrossRefi t ON t.bNumber=b.bNumber AND t.cNumber=b.cNumber AND t.vNumber=b.vNumber",
                    " WHERE b.bbName=", PCommon.AQ(bbName),
                    searchStringClause,
                    " ORDER BY b.bNumber ASC, b.cNumber ASC, b.vNumber ASC");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            while (!c.isAfterLast())
            {
                verse = new VerseBO();
                verse.id = c.getInt(0);
                verse.bbName = bbName;
                verse.bName = c.getString(5);
                verse.bsName = c.getString(6);
                verse.bNumber = c.getInt(1);
                verse.cNumber = c.getInt(2);
                verse.vNumber = c.getInt(3);
                verse.vText = c.getString(4);
                verse.mark = c.getInt(7);
                verse.crCount = c.getInt(8);
                lstVerse.add(verse);

                c.moveToNext();
            }
            if (!c.isClosed()) c.close();

            //2: BOOKJUMP
            sql = PCommon.ConcaT(
" SELECT bNumber ",
"FROM ",
"(",
" SELECT b.bNumber, b.bbName, b.cNumber, b.vNumber",
" FROM bible b",
" INNER JOIN bibleRef r ON r.bbName=b.bbName AND r.bNumber=b.bNumber",
" WHERE b.bbName=", PCommon.AQ(bbName),
searchStringClause,
" ORDER BY b.bNumber ASC, b.cNumber ASC, b.vNumber ASC",
")");
            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            int bNumber, rowNr = 0;
            while (!c.isAfterLast())
            {
                bNumber = c.getInt(0);
                if (!mapBookJump.containsKey(bNumber)) mapBookJump.put(bNumber, rowNr);

                c.moveToNext();
                rowNr++;
            }

            //---
            mapResult.put("LSTVERSE", lstVerse);
            mapResult.put("BOOKJUMP", mapBookJump);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return mapResult;
    }

    /***
     * Search favorites
     * @param bbName
     * @param searchString  Give NULL to get all notes
     * @param orderBy       Order by
     * @param markType      Mark type
     * @return list of verses
     */
    @SuppressWarnings("JavaDoc")
    ArrayList<VerseBO> SearchFav(final String bbName, final String searchString, final int orderBy, final int markType)
    {
        //TODO: maybe add ID in this call :)  for SearchFragment and GetVerse

        String sql;
        Cursor c = null;
        ArrayList<VerseBO> lstVerse = new ArrayList<>();

        try
        {
            //TODO: check if % was entered and SQL INJECTION with '

            String searchStringClause;
            if (searchString.compareTo("") == 0)
            {
                searchStringClause = "";
            }
            else
            {
                searchStringClause = SearchStringClause(searchString, true);
                if (searchStringClause.isEmpty()) return null;
            }

            final String orderByClause;
            switch (orderBy)
            {
                case 2:
                    orderByClause = "b.bNumber ASC, b.cNumber ASC, b.vNumber ASC";
                    break;
                case 1:
                default:
                    orderByClause = "n.changeDt DESC, b.bNumber ASC, b.cNumber ASC, b.vNumber ASC";
                    break;
            }

            sql = PCommon.ConcaT("SELECT b.id, b.bNumber, b.cNumber, b.vNumber, b.vText, r.bName, r.bsName, n.mark, t.tot FROM bible b",
                    " INNER JOIN bibleRef r ON r.bbName=b.bbName AND r.bNumber=b.bNumber",
                    " INNER JOIN bibleNote n ON n.bNumber=b.bNumber AND n.cNumber=b.cNumber AND n.vNumber=b.vNumber",
                    " LEFT OUTER JOIN bibleCrossRefi t ON t.bNumber=b.bNumber AND t.cNumber=b.cNumber AND t.vNumber=b.vNumber",
                    " WHERE b.bbName=", PCommon.AQ(bbName));

            if (!searchStringClause.isEmpty()) sql = PCommon.ConcaT(sql, searchStringClause);

            if (markType > 0) sql = PCommon.ConcaT(sql, " AND n.mark=", markType);

            sql = PCommon.ConcaT(sql, " ORDER BY ", orderByClause);

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            VerseBO verse;
            while (!c.isAfterLast())
            {
                verse = new VerseBO();
                verse.id = c.getInt(0);
                verse.bbName = bbName;
                verse.bName = c.getString(5);
                verse.bsName = c.getString(6);
                verse.bNumber = c.getInt(1);
                verse.cNumber = c.getInt(2);
                verse.vNumber = c.getInt(3);
                verse.vText = c.getString(4);
                verse.mark = c.getInt(7);
                verse.crCount = c.getInt(8);
                lstVerse.add(verse);

                c.moveToNext();
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return lstVerse;
    }

    /***
     * Search bible (from cache)
     * @param searchId  tabId
     * @return map
     */
    @SuppressWarnings("JavaDoc")
    Map<String, Object> SearchBible(final int searchId)
    {
        String sql;
        Cursor c = null;
        VerseBO verse;
        ArrayList<VerseBO> lstVerse = new ArrayList<>();
        TreeMap<Integer, Integer> mapBookJump = new TreeMap<>();
        Map<String, Object> mapResult = new HashMap<>();

        try
        {
            //1: LSTVERSE
            sql = PCommon.ConcaT("SELECT b.id, b.bNumber, b.cNumber, b.vNumber, b.vText, b.bbName, r.bName, r.bsName, n.mark, t.tot FROM bible b",
                    " INNER JOIN bibleRef r ON r.bbName=b.bbName AND r.bNumber=b.bNumber",
                    " INNER JOIN cacheSearch s ON s.bibleId=b.id",
                    " LEFT OUTER JOIN bibleNote n ON n.bNumber=b.bNumber AND n.cNumber=b.cNumber AND n.vNumber=b.vNumber",
                    " LEFT OUTER JOIN bibleCrossRefi t ON t.bNumber=b.bNumber AND t.cNumber=b.cNumber AND t.vNumber=b.vNumber",
                    " WHERE s.tabId=", searchId,
                    " ORDER BY b.bNumber ASC, b.cNumber ASC, b.vNumber ASC");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            while (!c.isAfterLast())
            {
                verse = new VerseBO();
                verse.id = c.getInt(0);
                verse.bbName = c.getString(5);
                verse.bName = c.getString(6);
                verse.bsName = c.getString(7);
                verse.bNumber = c.getInt(1);
                verse.cNumber = c.getInt(2);
                verse.vNumber = c.getInt(3);
                verse.vText = c.getString(4);
                verse.mark = c.getInt(8);
                verse.crCount = c.getInt(9);
                lstVerse.add(verse);

                c.moveToNext();
            }
            if (!c.isClosed()) c.close();

            //2: BOOKJUMP
            sql = PCommon.ConcaT(
"SELECT bNumber ",
"FROM ",
"(",
" SELECT b.bNumber, b.bbName, b.cNumber, b.vNumber",
" FROM bible b",
" INNER JOIN bibleRef r ON r.bbName=b.bbName AND r.bNumber=b.bNumber",
" INNER JOIN cacheSearch s ON s.bibleId=b.id",
" WHERE s.tabId=", searchId,
" ORDER BY b.bNumber ASC, b.cNumber ASC, b.vNumber ASC",
")");
            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            int bNumber, rowNr = 0;
            while (!c.isAfterLast())
            {
                bNumber = c.getInt(0);
                if (!mapBookJump.containsKey(bNumber)) mapBookJump.put(bNumber, rowNr);

                c.moveToNext();
                rowNr++;
            }

            //---
            mapResult.put("LSTVERSE", lstVerse);
            mapResult.put("BOOKJUMP", mapBookJump);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return mapResult;
    }

    /***
     * Get list of books by name
     * @param bbName
     * @param searchString
     * @return list of books
     */
    @SuppressWarnings("JavaDoc")
    ArrayList<BibleRefBO> GetListBookByName(final String bbName, String searchString)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        ArrayList<BibleRefBO> lst = new ArrayList<>();

        try
        {
            //TODO: check if % was entered and SQL INJECTION with '
            searchString = PCommon.ConcaT("%", searchString, "%");
            BibleRefBO r;

            sql = PCommon.ConcaT("SELECT bNumber, bName from bibleRef WHERE",
                    " bbName=", PCommon.AQ(bbName),
                    " AND bName like ", PCommon.AQ(PCommon.RQ(searchString)),
                    " ORDER BY bName ASC");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            while (!c.isAfterLast())
            {
                r = new BibleRefBO();
                r.bNumber = c.getInt(0);
                r.bName = c.getString(1);

                lst.add(r);

                c.moveToNext();
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return lst;
    }

    /***
     * Get list of my articles Id
     * @return list of articles Id
     */
    String[] GetListMyArticlesId()
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        ArrayList<String> arr = new ArrayList<>();

        try
        {
            ArtDescBO r;

            sql = PCommon.ConcaT("SELECT artId from artDesc ORDER BY artUpdatedDt DESC, artId desc;");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            while (!c.isAfterLast())
            {
                r = new ArtDescBO();
                r.artId = c.getInt(0);

                arr.add(String.valueOf(r.artId));

                c.moveToNext();
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return arr.toArray(new String[0]);
    }

    /***
     * Get my article name
     * @param artId
     * @return article name
     */
    @SuppressWarnings("JavaDoc")
    String GetMyArticleName(final int artId)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        String artDesc = "";

        try
        {
            sql = PCommon.ConcaT("SELECT artTitle from artDesc WHERE artId=", artId);

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast())
            {
                artDesc = c.getString(0);
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return artDesc;
    }

    /***
     * Get my article source
     * @param artId
     * @return article source
     */
    @SuppressWarnings("JavaDoc")
    String GetMyArticleSource(final int artId)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        String artSrc = "";

        try
        {
            sql = PCommon.ConcaT("SELECT artSrc from artDesc WHERE artId=", artId);

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast())
            {
                artSrc = c.getString(0);
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return artSrc;
    }

    /***
     * Get new MyArticle Id
     * @return bibleId
     */
    int GetNewMyArticleId()
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        int max = 0;

        try
        {
            sql = PCommon.ConcaT("SELECT MAX(artId) from artDesc");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast()) {
                max = c.getInt(0);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null) {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return max + 1;
    }

    /***
     * Update my article source
     * @param artId         Article Id
     * @param substSource   Source substitued
     */
    void UpdateMyArticleSource(final int artId, final String substSource)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;

        try
        {
            final String quotedSource = PCommon.AQ(PCommon.RQ(substSource));

            sql = PCommon.ConcaT("UPDATE artDesc SET artSrc=", quotedSource, " WHERE artId=", artId);
            _db.execSQL(sql);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;
        }
    }

    /***
     * Update my article title
     * @param artId     Article Id
     * @param title     Title
     */
    void UpdateMyArticleTitle(final int artId, final String title)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;

        try
        {
            sql = PCommon.ConcaT("UPDATE artDesc",
                    " SET artTitle=", PCommon.AQ(PCommon.RQ(title)),
                    " WHERE artId=", artId);
            _db.execSQL(sql);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;
        }
    }

    /***
     * Add my article
     * @param ad    Article description
     */
    void AddMyArticle(final ArtDescBO ad)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;

        try
        {
            sql = PCommon.ConcaT("INSERT INTO artDesc (artId, artUpdatedDt, artTitle, artSrc) VALUES (",
                ad.artId, ",",
                PCommon.AQ(ad.artUpdatedDt), ",",
                PCommon.AQ(PCommon.RQ(ad.artTitle)), ",",
                PCommon.AQ(PCommon.RQ(ad.artSrc)),
                ")");

            _db.execSQL(sql);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;
        }
    }

    /***
     * Delete my article
     * @param artId         Article Id
     */
    void DeleteMyArticle(final int artId)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;

        try
        {
            sql = PCommon.ConcaT("DELETE FROM artDesc WHERE artId=", artId);
            _db.execSQL(sql);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;
        }
    }

    /***
     * Get list of books by name
     * @param bbName
     * @return list all books
     */
    @SuppressWarnings("JavaDoc")
    ArrayList<BibleRefBO> GetListAllBookByName(final String bbName)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        ArrayList<BibleRefBO> lst = new ArrayList<>();

        try
        {
            BibleRefBO r;

            sql = PCommon.ConcaT("SELECT bNumber, bName, bsName from bibleRef WHERE",
                    " bbName=", PCommon.AQ(bbName),
                    " ORDER BY bNumber ASC");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            while (!c.isAfterLast())
            {
                r = new BibleRefBO();
                r.bNumber = c.getInt(0);
                r.bName = c.getString(1);
                r.bsName = c.getString(2);

                lst.add(r);

                c.moveToNext();
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return lst;
    }

    /***
     * Get list of books by name
     * @param bbName
     * @return list all books
     */
    @SuppressWarnings("JavaDoc")
    TreeMap<Integer, BibleRefBO> GetListAllBookMapByName(final String bbName)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        TreeMap<Integer, BibleRefBO> mapBook = new TreeMap<>();

        try
        {
            BibleRefBO r;

            sql = PCommon.ConcaT("SELECT bNumber, bName, bsName from bibleRef WHERE",
                    " bbName=", PCommon.AQ(bbName),
                    " ORDER BY bNumber ASC");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            while (!c.isAfterLast())
            {
                r = new BibleRefBO();
                r.bNumber = c.getInt(0);
                r.bName = c.getString(1);
                r.bsName = c.getString(2);
                r.bbName = bbName;

                mapBook.put(r.bNumber, r);

                c.moveToNext();
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return mapBook;
    }

    /***
     * Get book number by name
     * @param bbName
     * @param bName
     * @return book number (0 if not found)
     */
    @SuppressWarnings("JavaDoc")
    int GetBookNumberByName(final String bbName, final String bName)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        int bNumber = 0;

        try
        {
            sql = PCommon.ConcaT("SELECT bNumber from bibleRef WHERE",
                    " bbName=", PCommon.AQ(bbName),
                    " AND bName=", PCommon.AQ(PCommon.RQ(bName)),
                    " LIMIT 1");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast())
            {
                bNumber = c.getInt(0);
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return bNumber;
    }

    /***
     * Get book number by name
     * @param bName
     * @return book number (0 if not found)
     */
    @SuppressWarnings("JavaDoc")
    int GetBookNumberByName(final String bName)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        int bNumber = 0;

        try
        {
            sql = PCommon.ConcaT("SELECT bNumber from bibleRef WHERE",
                    " bName=", PCommon.AQ(PCommon.RQ(bName)),
                    " LIMIT 1");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast())
            {
                bNumber = c.getInt(0);
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return bNumber;
    }

    /***
     * Get book ref
     * @param bbName
     * @param bNumber
     * @return book ref
     */
    @SuppressWarnings("JavaDoc")
    BibleRefBO GetBookRef(final String bbName, final int bNumber)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        BibleRefBO ref = null;

        try
        {
            sql = PCommon.ConcaT("SELECT bName, bsName from bibleRef WHERE",
                    " bbName=", PCommon.AQ(bbName),
                    " AND bNumber=", bNumber);

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast())
            {
                ref = new BibleRefBO();
                ref.bbName = bbName;
                ref.bNumber = bNumber;
                ref.bName = c.getString(0);
                ref.bsName = c.getString(1);
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return ref;
    }

    /***
     * Get cache tab
     * @param tabId
     */
    @SuppressWarnings("JavaDoc")
    CacheTabBO GetCacheTab(final int tabId)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        CacheTabBO t = null;
        Cursor c = null;

        try
        {
            sql = PCommon.ConcaT("SELECT tabType, tabTitle, fullQuery, scrollPosY, bbName, isBook, isChapter, isVerse, bNumber, cNumber, vNumber, trad from cacheTab WHERE tabId=", tabId);

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast())
            {
                t = new CacheTabBO();
                t.tabNumber = tabId;
                t.tabType = c.getString(0);
                t.tabTitle = c.getString(1);
                t.fullQuery = c.getString(2);
                t.scrollPosY = c.getInt(3);
                t.bbName = c.getString(4);
                t.isBook = (c.getInt(5) == 1);
                t.isChapter =(c.getInt(6) == 1);
                t.isVerse = (c.getInt(7) == 1);
                t.bNumber = c.getInt(8);
                t.cNumber = c.getInt(9);
                t.vNumber = c.getInt(10);
                t.trad = c.getString(11);
            }
        }
        catch (Exception ex)
        {
            //Following solves a problem when quitting the app and the app tries to save current position in OnPause()
            //Don't output the exception here.
            return null;
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return t;
    }

    /***
     * Get cache tab Fav
     */
    CacheTabBO GetCacheTabFav()
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        CacheTabBO t = null;
        Cursor c = null;

        try
        {
            sql = PCommon.ConcaT("SELECT tabId, tabType, tabTitle, fullQuery, scrollPosY, bbName, isBook, isChapter, isVerse, bNumber, cNumber, vNumber, trad from cacheTab WHERE tabType='F'");
            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast())
            {
                t = new CacheTabBO();
                t.tabNumber = c.getInt(0);
                t.tabType = c.getString(1);
                t.tabTitle = c.getString(2);
                t.fullQuery = c.getString(3);
                t.scrollPosY = c.getInt(4);
                t.bbName = c.getString(5);
                t.isBook = (c.getInt(6) == 1);
                t.isChapter = (c.getInt(7) == 1);
                t.isVerse = (c.getInt(8) == 1);
                t.bNumber = c.getInt(9);
                t.cNumber = c.getInt(10);
                t.vNumber = c.getInt(11);
                t.trad = c.getString(12);
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null) {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return t;
    }

    /***
     * Get cache tab title
     * @param tabId
     */
    @SuppressWarnings("JavaDoc")
    String GetCacheTabTitle(final int tabId)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        String title = null;
        Cursor c = null;

        try
        {
            sql = PCommon.ConcaT("SELECT tabTitle from cacheTab WHERE tabId=", tabId);

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast())
            {
                title = c.getString(0);
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return title;
    }

    /***
     * Save cache tab (insert or replace)
     * @param t
     */
    @SuppressWarnings("JavaDoc")
    void SaveCacheTab(final CacheTabBO t)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;

        try
        {
            sql = PCommon.ConcaT("INSERT OR REPLACE INTO cacheTab (tabId, tabType, tabTitle, fullQuery, scrollPosY, bbName, isBook, isChapter, isVerse, bNumber, cNumber, vNumber, trad) VALUES (",
                    t.tabNumber, ",",
                    PCommon.AQ(PCommon.RQ(t.tabType)), ",",
                    PCommon.AQ(PCommon.RQ(t.tabTitle)), ",",
                    PCommon.AQ(PCommon.RQ(t.fullQuery)), ",",
                    t.scrollPosY, ",",
                    PCommon.AQ(PCommon.RQ(t.bbName)), ",",
                    (t.isBook) ? "1" : "0", ",",
                    (t.isChapter) ? "1" : "0", ",",
                    (t.isVerse) ? "1" : "0", ",",
                    t.bNumber, ",",
                    t.cNumber, ",",
                    t.vNumber, ",",
                    PCommon.AQ(t.trad), ")" );

            //_db.beginTransaction();
            _db.execSQL(sql);
            //_db.setTransactionSuccessful();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;
        }
    }

    /***
     * Save cache tab fav (delete and insert), should be unique
     * @param t
     */
    @SuppressWarnings("JavaDoc")
    void SaveCacheTabFav(final CacheTabBO t)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;

        try
        {
            sql = "DELETE FROM cacheSearch WHERE tabId IN (SELECT tabId FROM cacheTab WHERE tabType='F')";
            _db.execSQL(sql);

            sql = "DELETE FROM cacheTab WHERE tabType='F'";
            _db.execSQL(sql);

            sql = PCommon.ConcaT("INSERT INTO cacheTab (tabId, tabType, tabTitle, fullQuery, scrollPosY, bbName, isBook, isChapter, isVerse, bNumber, cNumber, vNumber, trad) VALUES (",
                    t.tabNumber, ",",
                    PCommon.AQ("F"), ",",
                    PCommon.AQ(PCommon.RQ(t.tabTitle)), ",",
                    PCommon.AQ(PCommon.RQ(t.fullQuery)), ",",
                    t.scrollPosY, ",",
                    PCommon.AQ(PCommon.RQ(t.bbName)), ",",
                    (t.isBook) ? "1" : "0", ",",
                    (t.isChapter) ? "1" : "0", ",",
                    (t.isVerse) ? "1" : "0", ",",
                    t.bNumber, ",",
                    t.cNumber, ",",
                    t.vNumber, ",",
                    PCommon.AQ(t.trad), ")" );

            //_db.beginTransaction();
            _db.execSQL(sql);
            //_db.setTransactionSuccessful();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;
        }
    }

    /***
     * Save cache search (before deleting data)
     * @param tabId
     * @param lstBibleId
     */
    @SuppressWarnings("JavaDoc")
    void SaveCacheSearch(final int tabId, final ArrayList<Integer> lstBibleId)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;

        try
        {
            this.DeleteCacheSearch(tabId);

            if (lstBibleId == null) return;

            final String tmp = PCommon.ConcaT("INSERT INTO cacheSearch (tabId, bibleId) VALUES (", tabId, ",");
            for (int bibleId:lstBibleId)
            {
                sql = PCommon.ConcaT(tmp, bibleId, ")");
                _db.execSQL(sql);
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;
        }
    }

    /***
     * Copy cache search for other bible (data are deleted before copying)
     * @param tabIdFrom
     * @param tabIdTo
     * @param tbbName
     * @return true if copy was successful
     */
    @SuppressWarnings("JavaDoc")
    boolean CopyCacheSearchForOtherBible(final int tabIdFrom, final int tabIdTo, final String tbbName)
    {
        @SuppressWarnings("UnusedAssignment") boolean insert = false;

        try
        {
            insert = this.AddCacheSearch(tabIdFrom, tabIdTo, tbbName);

            return insert;
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);

            return false;
        }
    }

    /***
     * Add cache search for other bible (data are deleted before copying)
     * @param tabIdFrom
     * @param tabIdTo
     * @param tbbName
     * @return true if copy was successful
     */
    @SuppressWarnings("JavaDoc")
    private boolean AddCacheSearch(final int tabIdFrom, final int tabIdTo, final String tbbName)
    {
        try
        {
            this.DeleteCacheSearch(tabIdTo);

            String sql, bbnameTo;
            int size = tbbName.length();

            for (int i = 0; i < size; i++)
            {
                bbnameTo = tbbName.substring(i, i+1);

                sql = PCommon.ConcaT(
                        "INSERT INTO cacheSearch (tabId, bibleId) ",
                        "SELECT ", tabIdTo ,", (SELECT i.id FROM bible i WHERE i.bbName=", PCommon.AQ(bbnameTo)," AND i.bNumber=r.bNumber AND i.cNumber=r.cNumber AND i.vNumber=r.vNumber) ",
                        "FROM (",
                        " SELECT distinct b.bNumber, b.cNumber, b.vNumber",
                        " FROM cacheSearch s",
                        " INNER JOIN bible b ON b.Id=s.bibleId",
                        " WHERE s.tabId=", tabIdFrom,
                        " ORDER BY b.bNumber, b.cNumber, b.vNumber ASC) r" );

                _db.execSQL(sql);
            }

            return true;
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);

            return false;
        }
    }

    /***
     * Get result (all) verse Id
     * @param tbbName
     * @param tabIdFrom
     * @param tabIdTo
     * @return list of verse Id
     */
    @SuppressWarnings("JavaDoc")
    ArrayList<Integer> GetResultVersesId(final int tabIdFrom, final int tabIdTo, final String tbbName)
    {
        Cursor c = null;
        ArrayList<Integer> lstId = new ArrayList<>();

        try
        {
            final boolean isCopySuccessful = this.CopyCacheSearchForOtherBible(tabIdFrom, tabIdTo, tbbName);
            if (!isCopySuccessful) return lstId;

            final String sql = PCommon.ConcaT("SELECT b.id ",
                    "FROM cacheSearch s ",
                    "INNER JOIN bible b ON b.Id=s.bibleId ",
                    "INNER JOIN bibleRef r ON r.bbName=b.bbName AND r.bNumber=b.bNumber ",
                    "WHERE s.tabId=", tabIdTo, " ORDER BY b.bNumber, b.cNumber, b.vNumber ASC");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            while (!c.isAfterLast())
            {
                lstId.add(c.getInt(0));

                c.moveToNext();
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            if (c != null) {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return lstId;
    }

    /***
     * Delete cache search
     * @param tabId
     */
    @SuppressWarnings("JavaDoc")
    private void DeleteCacheSearch(final int tabId)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;

        try
        {
            sql = PCommon.ConcaT("DELETE FROM cacheSearch WHERE tabId=", tabId);
            _db.execSQL(sql);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;
        }
    }

    /***
     * Delete cache
     * @param tabId
     */
    @SuppressWarnings("JavaDoc")
    void DeleteCache(final int tabId)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;

        try
        {
            sql = PCommon.ConcaT("DELETE FROM cacheSearch WHERE tabId=", tabId);
            _db.execSQL(sql);

            sql = PCommon.ConcaT("DELETE FROM cacheTab WHERE tabId=", tabId);
            _db.execSQL(sql);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;
        }
    }

    /***
     * Get max cachetab Id
     */
    private int GetCacheTabIdMax()
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        int id = -1;

        try
        {
            sql = "SELECT MAX(tabId) FROM cacheTab";

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast())
            {
                id = c.getInt(0);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return id;
    }

    /***
     * Sort all cache
     * @return true if all goes well
     *
     * 0|S|Ex1|2 1|-1|l|1|1|0|2|1|0|l
     *
     * 1|S|Gn1|1 1         |0|l|1|1|0|1 |1 |0|l
     * 2|S|Jésus|Jésus     |0|l|1|0|0|0 |0 |0|l
     * 4|S|PRBL1|43 10 1 17|0|l|1|0|0|43|10|1|l
     * 7|S|CR|6 1 5 CR:    |0|l|1|0|0|6 |1 |5|l
     *
     * 3|A|Aide|ART_APP_HELP|0|l|1|0|0|0|0|0|l
     * 5|A|MYART1|1|0|l|1|0|0|0|0|0|l
     * 6|P|Josué|1 1 0|0|l|1|0|0|0|0|0|l
     * --
     0|F|☆||-1|k|0|0|0|0|0|0|k
     1|S|Gn1|1 1|0|l|1|1|0|1|1|0|l
     2|P|Josué|1 1 0|3|l|1|0|0|0|0|0|l
     3|S|Ex1|2 1|0|l|1|1|0|2|1|0|l
     4|S|Dt2|5 2|0|l|1|1|0|5|2|0|l
     5|S|Dt3|5 3|0|l|1|1|0|5|3|0|l
     6|A|Aide|ART_APP_HELP|0|l|1|0|0|0|0|0|l
     7|A|MYART1|1|0|l|1|0|0|0|0|0|l
     8|A|Youtubers|ART26|0|l|1|0|0|0|0|0|l
     9|S|PRBL4|40 13 45 46|0|l|1|0|0|40|13|45|l
     10|S|PRBL1|43 10 1 17|0|l|1|0|0|43|10|1|l
     11|S|Jésus|Matthieu Jésus|0|l|1|0|0|40|0|0|l
     12|S|CR|6 1 5 CR:|0|l|1|0|0|6|1|5|l
     13|S|Jésus|41 Jésus|0|l|1|0|0|41|0|0|l
     14|S|Jn2 3-4|43 2 3 4|0|l|1|0|0|43|2|3|l
     15|S|Ap22 10|66 22 10|0|l|1|1|1|66|22|10|l
     16|A|MYART2|2|0|l|1|0|0|0|0|0|l
     17|S|Lc2 10-15|Luc 2 10 15|0|l|1|0|0|42|2|10|l
     18|S|1Jn2 1|1Jean 2 1|0|l|1|1|1|62|2|1|l
     19|S|Jésus|Jean 10 Jésus|0|l|1|0|0|43|10|0|l

     -- TO DEBUG:
     0|F|☆||-1|k|0|0|0|0|0|0|k
     1|P|Josué|1 10 0|0|l|1|0|0|0|0|0|l
     2|P|Josué|1 2 0|0|l|1|0|0|0|0|0|l
     3|P|Nombres|2 3 0|0|l|1|0|0|0|0|0|l
     4|S|Gn4|1 4|0|l|1|1|0|1|4|0|l
     5|S|Ex1|2 1|0|l|1|1|0|2|1|0|l
     6|S|Dt2|5 2|0|l|1|1|0|5|2|0|l
     7|A|Youtubers|ART26|0|l|1|0|0|0|0|0|l
     8|A|Signes|ART6|0|l|1|0|0|0|0|0|l
     9|A|Aide|ART_APP_HELP|0|l|1|0|0|0|0|0|l
     10|A|MYART1|1|0|l|1|0|0|0|0|0|l
     11|S|PRBL1|43 10 1 17|0|l|1|0|0|43|10|1|l
     12|S|PRBL6|40 18 23 35|0|l|1|0|0|40|18|23|l
     13|S|CR|1 4 5 CR:|0|l|1|0|0|1|4|5|l
     14|S|CR|6 1 5 CR:|0|l|1|0|0|6|1|5|l
     15|S|Jésus|Matthieu Jésus|0|l|1|0|0|40|0|0|l
     16|S|Jésus|41 Jésus|0|l|1|0|0|41|0|0|l
     17|S|Lc2 10-15|Luc 2 10 15|0|l|1|0|0|42|2|10|l
     *18|S|Éternel |Éternel |0|l|1|0|0|0|0|0|l
     *19|S|les cieux et la terre|les cieux et la terre|0|l|1|0|0|0|0|0|l
     *20|S|grands poissons|grands poissons|0|l|1|0|0|0|0|0|l
     21|S|Jn2 3-4|43 2 3 4|0|l|1|0|0|43|2|3|l
     22|S|1Jn2 1|1Jean 2 1|0|l|1|1|1|62|2|1|l

     -- RESULT
     0|0||☆
     1|1|1 10 0|Josué
     2|1|1 2 0|Josué
     3|1|2 3 0|Nombres
     4|2| 1  4  0|Gn4
     5|2| 2  1  0|Ex1
     6|2| 5  2  0|Dt2
     7|3|ART26|Youtubers
     8|3|ART6|Signes
     9|3|ART_APP_HELP|Aide
     10|4|MYART1|MYART1
     11|5|PRBL1|PRBL1
     12|5|PRBL6|PRBL6
     13|6| 1  4  5CR|CR
     14|6| 6  1  5CR|CR
     15|6|40  0  0Jésus|Jésus
     16|6|41  0  0Jésus|Jésus
     17|6|42  2 10|Lc2 10-15
     21|6|43  2  3|Jn2 3-4
     22|6|62  2  1|1Jn2 1

     */
    boolean SortAllCache()
    {
        Cursor c = null;

        try
        {
            String sqlPlan = "";
            c = _db.rawQuery("SELECT tabId, fullQuery, tabTitle FROM cacheTab WHERE tabType='P'", null);
            c.moveToFirst();
            while (!c.isAfterLast())
            {
                final int fromTabId = c.getInt(0);
                final String fromFullQuery = c.getString(1);
                final String fromTabTitle = c.getString(2);
                final String[] arrFromFullQuery = fromFullQuery.split(" ");
                if (arrFromFullQuery.length != 3) return false;

                final int planId = Integer.parseInt(arrFromFullQuery[0]);
                final int dayNumber = Integer.parseInt(arrFromFullQuery[1]);
                final Cursor cPlan = _db.rawQuery(PCommon.ConcaT("SELECT bNumberStart FROM planCal c WHERE planId=", planId," AND dayNumber=1"),null);
                cPlan.moveToFirst();
                if (!cPlan.isAfterLast())
                {
                    final int bNumberStart = cPlan.getInt(0);
                    @SuppressLint("DefaultLocale") final String toFullQuery = String.format("%2d%5d", bNumberStart, dayNumber);
                    sqlPlan = PCommon.ConcaT(sqlPlan, " UNION SELECT ", fromTabId,
                            ",1 AS sortOrder,'",
                            toFullQuery, "' AS titleOrder,'",
                            PCommon.RQ(fromTabTitle), "' AS tabTitle");
                }
                if (!cPlan.isClosed()) cPlan.close();
                c.moveToNext();
            }
            if (!c.isClosed()) c.close();

            final String sqlCache = PCommon.ConcaT("SELECT DISTINCT tabId, tabTitle FROM ( ",
                " SELECT tabId, 0 AS sortOrder, '' AS titleOrder, tabTitle FROM cacheTab WHERE tabType='F'",
                sqlPlan,
                " UNION SELECT tabId, 3 AS sortOrder, printf('ART%3d', REPLACE(fullQuery, 'ART', '')) AS titleOrder, tabTitle FROM cacheTab WHERE tabType='A' AND fullQuery like 'ART%'",
                " UNION SELECT tabId, 4 AS sortOrder, printf('MYART%5d', REPLACE(tabTitle, 'MYART', '')) AS titleOrder, tabTitle FROM cacheTab WHERE tabType='A' AND tabTitle like 'MYART%'",
                //Prbl
                " UNION SELECT tabId, 5 AS sortOrder, printf('PRBL%3d', REPLACE(tabTitle, 'PRBL', '')) AS titleOrder, tabTitle FROM cacheTab WHERE tabType='S' AND isBook=1 AND isChapter=0 AND bNumber>0 AND cNumber>0 AND tabTitle like 'PRBL%'",
                //Book
                " UNION SELECT tabId, 6 AS sortOrder, printf('%2d%3d%3d', bNumber, cNumber, vNumber) AS titleOrder, tabTitle FROM cacheTab WHERE tabType='S' AND isBook=1 AND isChapter=1 AND isVerse=0 AND bNumber>0 AND cNumber>0 AND vNumber=0",
                //CR
                " UNION SELECT tabId, 6 AS sortOrder, printf('%2d%3d%3d%s', bNumber, cNumber, vNumber, tabTitle) AS titleOrder, tabTitle FROM cacheTab WHERE tabType='S' AND isBook=1 AND isChapter=0 AND bNumber>0 AND cNumber>0 AND tabTitle like 'CR'",
                //Range
                " UNION SELECT tabId, 6 AS sortOrder, printf('%2d%3d%3d', bNumber, cNumber, vNumber) AS titleOrder, tabTitle FROM cacheTab WHERE tabType='S' AND isBook=1 AND isChapter=0 AND bNumber>0 AND cNumber>0 AND tabTitle not like 'CR' AND tabTitle not like 'PRBL%'",
                //Verse
                " UNION SELECT tabId, 6 AS sortOrder, printf('%2d%3d%3d', bNumber, cNumber, vNumber) AS titleOrder, tabTitle FROM cacheTab WHERE tabType='S' AND isBook=1 AND isChapter=1 AND isVerse=1 AND bNumber>0 AND cNumber>0 AND vNumber>0",
                //Search in book
                " UNION SELECT tabId, 6 AS sortOrder, printf('%2d%3d%3d%s', bNumber, cNumber, vNumber, tabTitle) AS titleOrder, tabTitle FROM cacheTab WHERE tabType='S' AND isBook=1 AND isChapter=0 AND bNumber>0 AND cNumber=0 AND tabTitle not like 'CR' AND tabTitle not like 'PRBL%'",
                //Search expr
                " UNION SELECT tabId, 6 AS sortOrder, printf('99999999%s', tabTitle) AS titleOrder, tabTitle FROM cacheTab WHERE tabType='S' AND isBook=1 AND isChapter=0 AND isVerse=0 AND bNumber=0 AND cNumber=0 AND vNumber=0 AND tabTitle not like 'CR' AND tabTitle not like 'PRBL%'",
                " )",
                " ORDER BY sortOrder ASC, titleOrder ASC, tabId ASC");

            //Check consistency
            int countBefore = -1;
            c =_db.rawQuery("SELECT COUNT(*) FROM cacheTab", null);
            c.moveToFirst();
            if (!c.isAfterLast()) {
                countBefore = c.getInt(0);
            }
            if (!c.isClosed()) c.close();

            int countAfter = -2;
            c =_db.rawQuery(PCommon.ConcaT("SELECT COUNT(*) FROM ( ", sqlCache, " )"), null);
            c.moveToFirst();
            if (!c.isAfterLast()) {
                countAfter = c.getInt(0);
            }
            if (!c.isClosed()) c.close();

            if (countBefore != countAfter && countBefore != 0 && countAfter != 0) return false;

            final int tabIdBottom = this.GetCacheTabIdMax() + 1;
            c = _db.rawQuery(sqlCache, null);
            for (int i = 0; i <= 1; i++)
            {
                int rowId = 0;
                c.moveToFirst();
                while (!c.isAfterLast())
                {
                    final int fromTabId = c.getInt(0);
                    final int toTabId = rowId;

                    if (i == 0)
                        this.UpdateCacheId(fromTabId, toTabId + tabIdBottom);
                    else
                        this.UpdateCacheId(toTabId + tabIdBottom, toTabId);

                    c.moveToNext();
                    rowId++;
                }
            }

            return true;
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            if (c != null) {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return false;
    }

    /***
     * Update cache Id
     * @param fromTabId
     * @param toTabId
     */
    @SuppressWarnings("JavaDoc")
    void UpdateCacheId(final int fromTabId, final int toTabId)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;

        try
        {
            sql = PCommon.ConcaT("UPDATE cacheSearch SET tabId=", toTabId, " WHERE tabId=", fromTabId);
            _db.execSQL(sql);

            sql = PCommon.ConcaT("UPDATE cacheTab SET tabId=", toTabId, " WHERE tabId=", fromTabId);
            _db.execSQL(sql);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;
        }
    }

    /***
     * Get cache tab visible count
     * Rem: minimum is 1
     */
    int GetCacheTabVisibleCount()
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        int count = 0;
        int max = 0;
        int res = 0;

        try
        {
            sql = PCommon.ConcaT("SELECT MAX(tabId) FROM cacheTab WHERE tabId >= 0");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast()) {
                max = c.getInt(0);
            }
            if (!c.isClosed()) c.close();

            sql = PCommon.ConcaT("SELECT COUNT(*) FROM cacheTab WHERE tabId >= 0");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast()) {
                count = c.getInt(0);
            }

            if (count == 0)
            {
                res = 0;    //was: res = 1
            }
            else
            {
                if (max == 0)
                {
                    res = 1;
                }
                else
                {
                    res = max + 1;
                }
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return res;
    }

    /***
     * Save note
     * @param noteBO
     */
    @SuppressWarnings("JavaDoc")
    void SaveNote(final NoteBO noteBO)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;

        try
        {
            sql = PCommon.ConcaT("INSERT OR REPLACE INTO bibleNote (bNumber, cNumber, vNumber, changeDt, mark, note) VALUES(",
                    noteBO.bNumber, ",",
                    noteBO.cNumber, ",",
                    noteBO.vNumber, ",",
                    PCommon.AQ(noteBO.changeDt), ",",
                    noteBO.mark, ",",
                    PCommon.AQ(noteBO.note),
                    ")" );

            _db.execSQL(sql);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;
        }
    }

    /***
     * Delete note
     * @param bNumber
     * @param cNumber
     * @param vNumber
     */
    @SuppressWarnings("JavaDoc")
    void DeleteNote(final int bNumber, final int cNumber, final int vNumber)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;

        try
        {
            sql = PCommon.ConcaT("DELETE FROM bibleNote WHERE",
                    " bNumber=", bNumber,
                    " AND cNumber=", cNumber,
                    " AND vNumber=", vNumber);

            _db.execSQL(sql);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;
        }
    }

    /***
     * Get bibleId min
     * @param bbName
     * @return bibleId
     */
    @SuppressWarnings("JavaDoc")
    int GetBibleIdMin(final String bbName)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        int min = 0;

        try
        {
            sql = PCommon.ConcaT("SELECT MIN(id) from bible b WHERE b.bbName=", PCommon.AQ(bbName));

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast()) {
                min = c.getInt(0);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null) {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return min;
    }

    /***
     * Get bibleId max
     * @param bbName
     * @return bibleId
     */
    @SuppressWarnings("JavaDoc")
    int GetBibleIdMax(final String bbName)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        int max = 0;

        try
        {
            sql = PCommon.ConcaT("SELECT MAX(id) from bible b WHERE b.bbName=", PCommon.AQ(bbName));

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast()) {
                max = c.getInt(0);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null) {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return max;
    }

    /***
     * Get Bible Id count
     * @return bibleId
     */
    int GetBibleIdCount()
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        int count = 0;

        try
        {
            sql = PCommon.ConcaT("SELECT COUNT(*) from bible");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast()) {
                count = c.getInt(0);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null) {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return count;
    }

    /***
     * Get number of chapters in a book
     * @param bbName
     * @param bNumber
     * @return bibleId
     */
    @SuppressWarnings("JavaDoc")
    int GetBookChapterMax(@SuppressWarnings("SameParameterValue") final String bbName, final int bNumber)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        int max = -1;

        try
        {
            sql = PCommon.ConcaT("SELECT MAX(b.cNumber) from bible b WHERE b.bbName=", PCommon.AQ(bbName),
                    " AND b.bNumber=", bNumber);

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast()) {
                max = c.getInt(0);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null) {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return max;
    }

    /***
     * Does a book exist?
     * @param bbName
     * @param bNumber
     * @return true/false
     */
    @SuppressWarnings("JavaDoc")
    boolean IsBookExist(@SuppressWarnings("SameParameterValue") final String bbName, final int bNumber)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;

        try
        {
            sql = PCommon.ConcaT("SELECT b.bNumber from bible b WHERE b.bbName=", PCommon.AQ(bbName),
                    " AND b.bNumber=", bNumber, " LIMIT 1");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast()) {
                return true;
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null) {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return false;
    }

    /**
     * Construct search text clause
     * @param searchStringOrig  Text entered by user without manipulation
     * @param showToast Show toast
     * @return clause as: AND (vText LIKE 'xx' OR vText LIKE 'yy') or ""
     */
    private String SearchStringClause(final String searchStringOrig, final boolean showToast)
    {
        final StringBuilder sbSql = new StringBuilder();

        try
        {
            final String[] arrExpr = searchStringOrig.split(",");

            int commaCount = 0;
            for (int c=0; c < searchStringOrig.length(); c++)
            {
                final String car = searchStringOrig.substring(c, c + 1);
                if (car.compareToIgnoreCase(",") == 0) commaCount++;
            }

            if ((commaCount + 1) != arrExpr.length)
            {
                if (showToast) PCommon.ShowToast(_context, R.string.toastEmpty3, Toast.LENGTH_SHORT);
                return "";
            }

            final int searchFullQueryLimit = PCommon.GetSearchFullQueryLimit();
            for (String expr : arrExpr)
            {
                if (expr.length() < searchFullQueryLimit)
                {
                    if (showToast) PCommon.ShowToast(_context, R.string.toastEmpty3, Toast.LENGTH_SHORT);
                    return "";
                }

                final String searchString = PCommon.ConcaT("%", expr, "%");
                sbSql.append("b.vText LIKE ");
                sbSql.append(PCommon.AQ(PCommon.RQ(searchString)));
                sbSql.append(" OR ");
            }
            if (sbSql.length() >= 4) sbSql.delete(sbSql.length() - 4, sbSql.length());
            if (sbSql.length() > 0)
            {
                sbSql.insert(0, " AND (");
                sbSql.append(") ");
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return sbSql.toString();
    }

    /**
     * Construct CASE clause Bible
     * @param trad  TRAD
     * @param fld   Table field to case (ex: b.bbName)
     * @return string with CASE clause for bbNameOrder
     */
    private String CaseBible(@SuppressWarnings("SameParameterValue") final String fld, final String trad)
    {
        //EX: CASE b.bbName WHEN 'f' THEN 1 WHEN 'k' THEN 2 END bbNameOrder
        final int size = trad.length();
        final StringBuilder sb = new StringBuilder(PCommon.ConcaT("CASE ", fld));
        for (int i = 0; i < size; i++)
        {
            sb.append(" WHEN ");
            sb.append("'");
            sb.append(trad.charAt(i));
            sb.append("'");
            sb.append(" THEN ");
            sb.append(i+1);
        }
        sb.append(" END bbNameOrder");

        return sb.toString();
    }

    /**
     * Construct IN clause Bible with ( ).
     * @param str   Trad
     * @return string for IN clause
     * Rem: there is no check of the content, '". Works only for chars.
     */
    private String InBible(final String str)
    {
        final int size = str.length();
        if (size <= 1) return PCommon.ConcaT("('", str, "')");

        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++)
        {
            if (sb.length() > 0) sb.append(",");
            sb.append("'");
            sb.append(str.charAt(i));
            sb.append("'");
        }
        sb.insert(0, "(");
        sb.append(")");

        return sb.toString();
    }

    /***
     * Get plan id max
     * @return Plan id max
     */
    int GetPlanDescIdMax()
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        int max = 0;

        try
        {
            sql = PCommon.ConcaT("SELECT MAX(planId) from planDesc");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast()) {
                max = c.getInt(0);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null) {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return max;
    }

    /***
     * Get bible chapter info by book
     * @param bNumber
     * @return chapter count, verse count of the book
     */
    @SuppressWarnings("JavaDoc")
    Integer[] GetBibleCiByBook(final int bNumber)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        final Integer[] ci = { 0, 0 };

        try
        {
            //cCount, vCount
            sql = PCommon.ConcaT("SELECT COUNT(b.cNumber), SUM(b.vCount) from bibleCi b WHERE b.bNumber=", bNumber,
                    " GROUP BY b.bNumber");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast())
            {
                ci[ 0 ] = c.getInt(0);
                ci[ 1 ] = c.getInt(1);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null) {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return ci;
    }

    /***
     * Get bible chapter info from book, chapter for a limited number of chapters
     * @param bNumber
     * @param cNumber
     * @param cCount
     * @return chapter count, verse count
     */
    @SuppressWarnings("JavaDoc")
    Integer[] GetBibleCiFromBookChapter(final int bNumber, final int cNumber, final int cCount)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        final Integer[] ci = { 0, 0 };

        try
        {
            //cCount, vCount
            sql = PCommon.ConcaT("SELECT COUNT(ciId), SUM(vCount) FROM (SELECT ciId, vCount from bibleCi b WHERE (b.bNumber=", bNumber, " AND b.cNumber>=", cNumber, ") OR (b.bNumber>", bNumber,") ORDER BY b.bNumber ASC, b.cNumber ASC LIMIT ", cCount, ")");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast())
            {
                ci[ 0 ] = c.getInt(0);
                ci[ 1 ] = c.getInt(1);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null) {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return ci;
    }

    /***
     * Get verse count of chapter
     * @param bNumber
     * @param cNumber
     * @return verse count of chapter
     */
    @SuppressWarnings("JavaDoc")
    int GetChapterVerseCount(final int bNumber, final int cNumber)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        int vCount = 0;

        try
        {
            //vCount
            sql = PCommon.ConcaT("SELECT vCount from bibleCi b WHERE b.bNumber=", bNumber,
                    " AND b.cNumber=", cNumber,
                    " GROUP BY b.bNumber");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast())
            {
                vCount = c.getInt(0);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null) {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return vCount;
    }

    /***
     * Get start id of a book in King James
     * @param bNumber
     * @return First Id of a book, -1 in case of error
     */
    @SuppressWarnings("JavaDoc")
    private int GetBookIdMin(final int bNumber)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        int id = -1;

        try
        {
            sql = PCommon.ConcaT("SELECT MIN(id) FROM bible WHERE bbName='k' AND bNumber=", bNumber);

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast())
            {
                id = c.getInt(0);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return id;
    }

    /***
     * Get End id of a book in King James
     * @param bNumber
     * @return Max Id of a book, -1 in case of error
     */
    @SuppressWarnings("JavaDoc")
    private int GetBookIdMax(final int bNumber)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        int id = -1;

        try
        {
            sql = PCommon.ConcaT("SELECT MAX(id) FROM bible WHERE bbName='k' AND bNumber=", bNumber);

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast())
            {
                id = c.getInt(0);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return id;
    }

    /***
     * Get start id of a book
     * @param bbName
     * @param bNumber
     * @return First Id of a book, -1 in case of error
     */
    @SuppressWarnings("JavaDoc")
    int GetBookIdMinByBookNumber(final String bbName, final int bNumber)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        int id = -1;

        try
        {
            sql = PCommon.ConcaT("SELECT MIN(id) FROM bible WHERE bbName=",
                    PCommon.AQ(bbName),
                    " AND bNumber=", bNumber);

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast())
            {
                id = c.getInt(0);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return id;
    }

    /***
     * Delete a plan
     * @param planId    Plan Id
     */
    void DeletePlan(final int planId)
    {
        String sql;

        try
        {
            sql = PCommon.ConcaT("DELETE FROM planCal WHERE planId=", planId);
            _db.execSQL(sql);

            sql = PCommon.ConcaT("DELETE FROM planDesc WHERE planId=", planId);
            _db.execSQL(sql);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;
        }
    }

    /***
     * Create plan calendar
     * @param pd    Plan description
     * @param strBookNumbers List of book numbers
     */
    @SuppressWarnings("UnusedAssignment")
    void AddPlan(final PlanDescBO pd, final String strBookNumbers)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;

        try
        {
            final String insertSqlCal = "INSERT INTO planCal (planId, dayNumber, dayDt, isRead, bNumberStart, cNumberStart, vNumberStart, bNumberEnd, cNumberEnd, vNumberEnd) VALUES (";

            //Delete
            this.DeletePlan(pd.planId);

            //Plan Calendar
            int bNumberStart = 0, cNumberStart = 0, vNumberStart = 0;
            int bNumberEnd = 0, cNumberEnd = 0, vNumberEnd = 0;
            int dayNumber = 1;
            boolean shouldBreak = false;

            final String[] books = strBookNumbers.split(",");
            final int firstbNumber = Integer.parseInt( books[0] );
            final int lastbNumber = (books.length > 1) ? Integer.parseInt(books[ books.length - 1]) : firstbNumber;
            final int idMin = this.GetBookIdMin(firstbNumber);
            final int idMax = this.GetBookIdMax(lastbNumber);
            if (idMin == -1 || idMax == -1) return;

            //Add Desc
            sql = PCommon.ConcaT("INSERT INTO planDesc (planId, planRef, startDt, endDt, bCount, cCount, vCount, vDayCount, dayCount) VALUES (",
                    pd.planId, ",",
                    PCommon.AQ(pd.planRef), ",",
                    PCommon.AQ(pd.startDt), ",",
                    PCommon.AQ(pd.endDt), ",",
                    pd.bCount, ",",
                    pd.cCount, ",",
                    pd.origVCount, ",",
                    pd.vDayCount, ",",
                    pd.dayCount,
                    ")");
            _db.execSQL(sql);

            final int dCountForVerse = pd.vDayCount - 1;
            final String dtFormat = "yyyyMMdd";
            final Calendar cal = Calendar.getInstance();
            cal.set(Integer.parseInt(pd.startDt.substring(0, 4)),
                    Integer.parseInt(pd.startDt.substring(4, 6)) - 1,
                    Integer.parseInt(pd.startDt.substring(6, 8)));
            String startDtStr;
            int idCurrent = idMin;
            VerseBO v;

            while(true)
            {
                //Start
                v = this.GetVerse(idCurrent);
                if (idCurrent > idMax)
                {
                    break;
                }
                bNumberStart = v.bNumber;
                cNumberStart = v.cNumber;
                vNumberStart = v.vNumber;

                //End
                if (pd.planType == PlanDescBO.PLAN_TYPE.VERSE_TYPE) {
                    idCurrent += dCountForVerse;
                } else {
                    final Integer[] ci = GetBibleCiFromBookChapter(bNumberStart, cNumberStart, pd.vDayCount);
                    idCurrent += ci[1] - 1;
                }

                if (idCurrent > idMax)
                {
                    shouldBreak = true;
                    idCurrent = idMax;
                }

                v = this.GetVerse(idCurrent);
                bNumberEnd = v.bNumber;
                cNumberEnd = v.cNumber;
                vNumberEnd = v.vNumber;

                //Add Calendar
                startDtStr = PCommon.AQ(DateFormat.format(dtFormat, cal).toString());
                sql = PCommon.ConcaT(insertSqlCal,
                        pd.planId, ",",
                        dayNumber, ",",
                        startDtStr, ",",
                        0, ",",
                        bNumberStart, ",",
                        cNumberStart, ",",
                        vNumberStart, ",",
                        bNumberEnd, ",",
                        cNumberEnd, ",",
                        vNumberEnd,
                        ")");
                _db.execSQL(sql);
                //noinspection UnusedAssignment
                sql = null;
                //noinspection UnusedAssignment
                startDtStr = null;

                if (shouldBreak) break;

                dayNumber++;
                idCurrent++;
                cal.add(Calendar.DAY_OF_MONTH, 1);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;
        }
    }

    /***
     * Get all plan descriptions
     * @return List of all plan descriptions
     */
    ArrayList<PlanDescBO> GetAllPlanDesc()
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        ArrayList<PlanDescBO> lstPd = new ArrayList<>();

        try
        {
            PlanDescBO pd;
            sql = PCommon.ConcaT("SELECT d.planId, d.planRef, d.startDt, d.endDt, d.bCount, d.cCount, d.vCount, d.vDayCount, d.dayCount, ",
                                "(SELECT MIN(c.bNumberStart) FROM planCal c WHERE c.planId = d.planId) firstbNumberStart ",
                                "FROM planDesc d ",
                                "ORDER BY firstbNumberStart DESC, d.bCount ASC");
            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            while (!c.isAfterLast())
            {
                pd = new PlanDescBO();
                pd.planId = c.getInt(0);
                pd.planRef = c.getString(1);
                pd.startDt = c.getString(2);
                pd.endDt = c.getString(3);
                pd.bCount = c.getInt(4);
                pd.cCount = c.getInt(5);
                pd.vCount = c.getInt(6);
                pd.vDayCount = c.getInt(7);
                pd.dayCount = c.getInt(8);
                lstPd.add(pd);

                c.moveToNext();
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return lstPd;
    }

    /***
     * Get a plan desc
     * @return Plan description
     */
    PlanDescBO GetPlanDesc(final int planId)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        PlanDescBO pd = null;

        try
        {
            sql = PCommon.ConcaT("SELECT planId, planRef, startDt, endDt, bCount, cCount, vCount, vDayCount, dayCount ",
                    "FROM planDesc ",
                    "WHERE planId=", planId);

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast())
            {
                pd = new PlanDescBO();
                pd.planId = c.getInt(0);
                pd.planRef = c.getString(1);
                pd.startDt = c.getString(2);
                pd.endDt = c.getString(3);
                pd.bCount = c.getInt(4);
                pd.cCount = c.getInt(5);
                pd.vCount = c.getInt(6);
                pd.vDayCount = c.getInt(7);
                pd.dayCount = c.getInt(8);
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return pd;
    }

    /***
     * Is a specific plan exist?
     * @param planRef
     * @return true/false
     */
    @SuppressWarnings("JavaDoc")
    boolean IsPlanDescExist(final String planRef)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;

        try
        {
            sql = PCommon.ConcaT("SELECT planId from planDesc WHERE planRef=", PCommon.AQ(planRef),
                    " LIMIT 1");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast()) {
                return true;
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null) {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return false;
    }

    /***
     * Get plan calendar
     * @param bbName    Bible name
     * @param planId    Plan Id
     * @param pageNr    Page number
     * @return list of days
     */
    ArrayList<PlanCalBO> GetPlanCal(final String bbName, final int planId, final int pageNr)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        ArrayList<PlanCalBO> lst = new ArrayList<>();

        try
        {
            PlanCalBO pc;

            sql = PCommon.ConcaT("SELECT c.planId, c.dayNumber, c.dayDt, c.isRead,",
                                " c.bNumberStart, c.cNumberStart, c.vNumberStart, c.bNumberEnd, c.cNumberEnd, c.vNumberEnd,",
                                " rs.bsName, re.bsName",
                                " FROM planCal c",
                                " INNER JOIN bibleRef rs ON rs.bbName=", PCommon.AQ(bbName), " AND c.bNumberStart=rs.bNumber",
                                " INNER JOIN bibleRef re ON rs.bbName=", PCommon.AQ(bbName), " AND c.bNumberEnd=re.bNumber",
                                " WHERE c.planId=", planId, " AND rs.bbName=", PCommon.AQ(bbName), " AND re.bbName=", PCommon.AQ(bbName),
                                " ORDER BY c.dayNumber",
                                " LIMIT 31 OFFSET ", pageNr * 31);

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            while (!c.isAfterLast())
            {
                pc = new PlanCalBO();
                pc.planId = c.getInt(0);
                pc.dayNumber = c.getInt(1);
                pc.dayDt = c.getString(2);
                if (pc.dayDt == null)
                {
                    pc.dayDt = "";
                }
                else if (pc.dayDt.length() == 8)
                {
                    pc.dayDt = PCommon.ConcaT(pc.dayDt.substring(2, 4), "/", pc.dayDt.substring(4, 6), "/", pc.dayDt.substring(6, 8));
                }
                pc.isRead = c.getInt(3);
                pc.bNumberStart = c.getInt(4);
                pc.cNumberStart = c.getInt(5);
                pc.vNumberStart = c.getInt(6);
                pc.bNumberEnd = c.getInt(7);
                pc.cNumberEnd = c.getInt(8);
                pc.vNumberEnd = c.getInt(9);
                pc.bsNameStart = c.getString(10);
                pc.bsNameEnd = c.getString(11);
                lst.add(pc);

                c.moveToNext();
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return lst;
    }

    /***
     * Get current day number of a plan calendar (for today)
     * @param planId
     * @return day number (0 if not found)
     */
    @SuppressWarnings("JavaDoc")
    int GetCurrentDayNumberOfPlanCal(final int planId)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        int dayNumber = 0;
        final String dtFormat = "yyyyMMdd";

        try
        {
            final Calendar now = Calendar.getInstance();
            final String dayDt = DateFormat.format(dtFormat, now).toString();

            sql = PCommon.ConcaT("SELECT c.dayNumber ",
                    " FROM planCal c",
                    " WHERE c.planId=", planId, " AND c.dayDt=", PCommon.AQ(dayDt), " limit 1");

            c = _db.rawQuery(sql, null);
            if (c.getCount() > 0)
            {
                c.moveToFirst();
                if (!c.isAfterLast())
                {
                    dayNumber = c.getInt(0);
                }
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return dayNumber;
    }

    /***
     * Get plan calendar of day
     * @param bbName    Bible name
     * @param planId    Plan Id
     * @param dayNumber Day number
     * @return Plan cal of day
     */
    PlanCalBO GetPlanCalByDay(final String bbName, final int planId, final int dayNumber)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        PlanCalBO pc = null;

        try
        {
            sql = PCommon.ConcaT("SELECT c.planId, c.dayNumber, c.dayDt, c.isRead,",
                    " c.bNumberStart, c.cNumberStart, c.vNumberStart, c.bNumberEnd, c.cNumberEnd, c.vNumberEnd,",
                    " rs.bsName, re.bsName",
                    " FROM planCal c",
                    " INNER JOIN bibleRef rs ON rs.bbName=", PCommon.AQ(bbName), " AND c.bNumberStart=rs.bNumber",
                    " INNER JOIN bibleRef re ON rs.bbName=", PCommon.AQ(bbName), " AND c.bNumberEnd=re.bNumber",
                    " WHERE c.planId=", planId, " AND c.dayNumber=", dayNumber, " AND rs.bbName=", PCommon.AQ(bbName), " AND re.bbName=", PCommon.AQ(bbName));

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast())
            {
                pc = new PlanCalBO();
                pc.planId = c.getInt(0);
                pc.dayNumber = c.getInt(1);
                pc.dayDt = c.getString(2);
                pc.isRead = c.getInt(3);
                pc.bNumberStart = c.getInt(4);
                pc.cNumberStart = c.getInt(5);
                pc.vNumberStart = c.getInt(6);
                pc.bNumberEnd = c.getInt(7);
                pc.cNumberEnd = c.getInt(8);
                pc.vNumberEnd = c.getInt(9);
                pc.bsNameStart = c.getString(10);
                pc.bsNameEnd = c.getString(11);
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return pc;
    }

    /***
     * Get plan calendar row count of a plan
     * @param bbName    Bible name
     * @param planId    Plan Id
     * @return Row count for this calendar
     */
    int GetPlanCalRowCount(final String bbName, final int planId)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;

        try
        {
            sql = PCommon.ConcaT("SELECT COUNT(*)",
                    " FROM planCal c",
                    " INNER JOIN bibleRef rs ON rs.bbName=", PCommon.AQ(bbName), " AND c.bNumberStart=rs.bNumber",
                    " INNER JOIN bibleRef re ON rs.bbName=", PCommon.AQ(bbName), " AND c.bNumberEnd=re.bNumber",
                    " WHERE c.planId=", planId, " AND rs.bbName=", PCommon.AQ(bbName), " AND re.bbName=", PCommon.AQ(bbName),
                    " ORDER BY c.dayNumber");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast())
            {
                return c.getInt(0);
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return 0;
    }

    /***
     * Get number of days read
     * @param planId
     * @return Count of days read
     */
    @SuppressWarnings("JavaDoc")
    int GetPlanCalDaysReadCount(final int planId)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        int count = 0;

        try
        {
            sql = PCommon.ConcaT("SELECT COUNT(*) ",
                    " FROM planCal c",
                    " WHERE c.planId=", planId, " AND c.isRead=1",
                    " GROUP BY c.isRead");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast())
            {
                count = c.getInt(0);
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return count;
    }

    /***
     * Mark plan calendar of day
     * @param planId    Plan Id
     * @param dayNumber Day number
     * @param isRead    Is read
     */
    void MarkPlanCal(final int planId, final int dayNumber, final int isRead)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;

        try
        {
            sql = PCommon.ConcaT("UPDATE planCal SET isRead=", isRead," WHERE planId=", planId, " AND dayNumber=", dayNumber);

            _db.execSQL(sql);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;
        }
    }

    /***
     * Mark plan calendar of day (all above)
     * @param planId    Plan Id
     * @param dayNumber Day number
     * @param isRead    Is read
     */
    void MarkAllAbovePlanCal(final int planId, final int dayNumber, final int isRead)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;

        try
        {
            sql = PCommon.ConcaT("UPDATE planCal SET isRead=", isRead," WHERE planId=", planId, " AND dayNumber<=", dayNumber);

            _db.execSQL(sql);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;
        }
    }

    /***
     * Add todo
     * @param td
     */
    @SuppressWarnings("JavaDoc")
    void AddTodo(final TodoBO td)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;

        try
        {
            sql = PCommon.ConcaT("INSERT INTO td (tdId, tdDesc, tdCommentIssues, tdPriority, tdStatus) VALUES (",
                    td.tdId, ",",
                    PCommon.AQ(PCommon.RQ(td.tdDesc)), ",",
                    PCommon.AQ(PCommon.RQ(td.tdCommentIssues)), ",",
                    PCommon.AQ(PCommon.RQ(td.tdPriority)), ",",
                    PCommon.AQ(PCommon.RQ(td.tdStatus)),
                    ")");

            _db.execSQL(sql);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;
        }
    }

    /***
     * Update todo
     * @param td
     */
    @SuppressWarnings("JavaDoc")
    void UpdateTodo(final TodoBO td)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;

        try
        {
            sql = PCommon.ConcaT("UPDATE td SET",
                            " tdDesc=", PCommon.AQ(PCommon.RQ(td.tdDesc)), ",",
                            " tdCommentIssues=", PCommon.AQ(PCommon.RQ(td.tdCommentIssues)), ",",
                            " tdPriority=", PCommon.AQ(PCommon.RQ(td.tdPriority)), ",",
                            " tdStatus=", PCommon.AQ(PCommon.RQ(td.tdStatus)),
                            " WHERE tdId=", td.tdId);
            _db.execSQL(sql);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;
        }
    }

    /***
     * Reset todo status for todos in status TODO, DONE
     */
    void ResetTodoStatus()
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;

        try
        {
            sql = PCommon.ConcaT("UPDATE td SET",
                    " tdStatus='TODO'",
                    " WHERE tdStatus IN ('TODO', 'DONE')");
            _db.execSQL(sql);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;
        }
    }

    /***
     * Delete todo
     * @param tdId  Id
     */
    void DeleteTodo(final int tdId)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;

        try
        {
            sql = PCommon.ConcaT("DELETE FROM td WHERE tdId=", tdId);
            _db.execSQL(sql);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;
        }
    }

    /***
     * Get new Todo Id
     * @return Id
     */
    int GetNewTodoId()
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        int max = 0;

        try
        {
            sql = PCommon.ConcaT("SELECT MAX(tdId) from td");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast()) {
                max = c.getInt(0);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null) {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return max + 1;
    }

    /***
     * Get Todo Id count
     * @return Todo Id count
     */
    int GetTodoIdCount()
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        int count = 0;

        try
        {
            sql = PCommon.ConcaT("SELECT COUNT(tdId) from td");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast()) {
                count = c.getInt(0);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null) {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return count;
    }

    /***
     * Get Todo count by status
     * @param tdStatus
     * @return Count
     */
    @SuppressWarnings("JavaDoc")
    int GetTodoCountByStatus(final String tdStatus)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        int count = 0;

        try
        {
            sql = PCommon.ConcaT("SELECT COUNT(tdId) FROM td WHERE tdStatus=", PCommon.AQ(PCommon.RQ(tdStatus)));

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast()) {
                count = c.getInt(0);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null) {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return count;
    }

    /***
     * Get list todos
     * @return List todos by status
     */
    ArrayList<TodoBO> GetListTodoByStatus(final String tdStatus)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        ArrayList<TodoBO> lstTd = new ArrayList<>();

        try
        {
            TodoBO td;
            sql = PCommon.ConcaT("SELECT tdId, tdDesc, tdCommentIssues, tdPriority, CASE tdPriority WHEN 'LOW' THEN 0 WHEN 'NORMAL' THEN 1 WHEN 'HIGH' THEN 2 END priorityOrder",
                    " FROM td WHERE tdStatus=", PCommon.AQ(tdStatus),
                    " ORDER BY priorityOrder DESC, tdDesc ASC");
            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            while (!c.isAfterLast())
            {
                td = new TodoBO();
                td.tdId = c.getInt(0);
                td.tdDesc = c.getString(1);
                td.tdCommentIssues = c.getString(2);
                td.tdPriority = c.getString(3);
                td.tdStatus = tdStatus;
                lstTd.add(td);

                c.moveToNext();
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return lstTd;
    }

    /***
     * Get todo
     * @param tdId
     * @return List todos by status
     */
    @SuppressWarnings("JavaDoc")
    TodoBO GetTodo(final int tdId)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        TodoBO td = null;

        try
        {
            sql = PCommon.ConcaT("SELECT tdId, tdDesc, tdCommentIssues, tdPriority, tdStatus, CASE tdPriority WHEN 'LOW' THEN 0 WHEN 'NORMAL' THEN 1 WHEN 'HIGH' THEN 2 END priorityOrder",
                    " FROM td WHERE tdId=", tdId);
            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast())
            {
                td = new TodoBO();
                td.tdId = c.getInt(0);
                td.tdDesc = c.getString(1);
                td.tdCommentIssues = c.getString(2);
                td.tdPriority = c.getString(3);
                td.tdStatus = c.getString(4);
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return td;
    }

    /***
     * Get list of bookmarks by ID
     * Should not be called directly, call GenerateBookmarkList instead.
     * @return list all bookmarks
     */
    @SuppressWarnings("JavaDoc")
    ArrayList<BookmarkBO> GetListAllBookmark()
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        ArrayList<BookmarkBO> lst = new ArrayList<>();

        try
        {
            BookmarkBO b;

            sql = PCommon.ConcaT("SELECT bmId, bmCurrent, bmDesc from bookmark WHERE bmId >= 0 ORDER BY bmId ASC");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            while (!c.isAfterLast())
            {
                b = new BookmarkBO();
                b.bmId = c.getInt(0);
                b.bmCurrent = c.getString(1);
                b.bmDesc = c.getString(2);

                lst.add(b);

                c.moveToNext();
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return lst;
    }

    /***
     * Get list all bookmark IDs
     * @return list all bookmark IDs
     */
    ArrayList<Integer> GetListAllBookmarkId()
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        ArrayList<Integer> lst = new ArrayList<>();

        try
        {
            sql = PCommon.ConcaT("SELECT bmId from bookmark WHERE bmId >= 0 ORDER BY bmId ASC");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            while (!c.isAfterLast())
            {
                lst.add(c.getInt(0));

                c.moveToNext();
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return lst;
    }

    /***
     * Get LexAggr count
     * @return count
     */
    int GetLexAggrCount()
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        int count = 0;

        try
        {
            sql = PCommon.ConcaT("SELECT COUNT(*) from lexAggr");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast()) {
                count = c.getInt(0);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null) {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return count;
    }

    /***
     * Get interlinear of a verse
     * @param tbbName
     * @param bNumber
     * @param cNumber
     * @param vNumber
     * @param bibleId
     * @return Map of lists
     */
    @SuppressWarnings("JavaDoc")
    Map<String, ArrayList<String>> GetVerseInterlinear(final String tbbName, final int bNumber, final int cNumber, final int vNumber, final int bibleId)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        Map<String, ArrayList<String>> mapRes = new HashMap<>();
        ArrayList<String> lstHG = new ArrayList<>();
        ArrayList<String> lstEN = new ArrayList<>();
        ArrayList<String> lstWrd = new ArrayList<>();
        ArrayList<String> lstVar = new ArrayList<>();

        try
        {
            mapRes.put("HG", lstHG);
            mapRes.put("EN", lstEN);
            mapRes.put("WRD", lstWrd);
            mapRes.put("VAR", lstVar);

            final int idFrom = this.GetLexAggrIdOfVerse(bNumber, cNumber, vNumber);
            if (idFrom == -1) return mapRes;

            final VerseBO verseFrom = this.GetVerse(bibleId);
            final int idToTmp = verseFrom.id + 1;

            int idTo;
            final VerseBO verseTo = this.GetVerse(idToTmp);
            if (verseTo == null)
            {
                idTo = idFrom + 10;
            }
            else
            {
                idTo = this.GetLexAggrIdOfVerse(verseTo.bNumber, verseTo.cNumber, verseTo.vNumber);
                if (idTo == -1) return mapRes;
                if (idTo < idFrom) idTo = idFrom + 10;
            }
            final String verseRef = this.GetLexAggrVerseRef(bNumber, cNumber, vNumber);

            sql = PCommon.ConcaT("SELECT field0, field1, field2, field3, field4, field5, field6, field7, field8, field9, field10",
                    " FROM lexAggr",
                    " WHERE id >= ", idFrom, " AND id <= ", idTo,
                    " ORDER BY id ASC");

            c =  _db.rawQuery(sql, null);
            c.moveToFirst();

            String field0, field1, field2, field3, field4, field5, field6, field7, field8, field9, field10;
            while (!c.isAfterLast())
            {
                field0 = c.getString(0);
                field1 = c.getString(1);
                field2 = c.getString(2);
                field3 = c.getString(3);
                field4 = c.getString(4);
                field5 = c.getString(5);
                field6 = c.getString(6);
                field7 = c.getString(7);
                field8 = c.getString(8);
                field9 = c.getString(9);
                field10 = c.getString(10);

                if (field0.contains(verseRef)) {
                    lstHG.add(field1); lstHG.add(field2); lstHG.add(field3); lstHG.add(field4); lstHG.add(field5);
                    lstHG.add(field6); lstHG.add(field7); lstHG.add(field8); lstHG.add(field9); lstHG.add(field10);
                    for(int empty=1; empty <= 10; empty++) { lstVar.add(""); }
                } else if (field0.contains("Translation")) {
                    lstEN.add(field1); lstEN.add(field2); lstEN.add(field3); lstEN.add(field4); lstEN.add(field5);
                    lstEN.add(field6); lstEN.add(field7); lstEN.add(field8); lstEN.add(field9); lstEN.add(field10);
                    for(int empty=1; empty <= 10; empty++) { lstVar.add(""); }
                } else if (field0.contains("Word")) {
                    lstWrd.add(field1); lstWrd.add(field2); lstWrd.add(field3); lstWrd.add(field4); lstWrd.add(field5);
                    lstWrd.add(field6); lstWrd.add(field7); lstWrd.add(field8); lstWrd.add(field9); lstWrd.add(field10);
                    for(int empty=1; empty <= 10; empty++) { lstVar.add(""); }
                } else if (field0.contains("Significant")) {
                    lstVar.add(field1); lstVar.add(field2); lstVar.add(field3); lstVar.add(field4); lstVar.add(field5);
                    lstVar.add(field6); lstVar.add(field7); lstVar.add(field8); lstVar.add(field9); lstVar.add(field10);
                }

                c.moveToNext();
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return mapRes;
    }

    /***
     * Get Lex Details by HGNr
     * @param hgNr  Ref H9999-G9999
     * @return List of words
     */
    @SuppressWarnings("JavaDoc")
    ArrayList<LexTbesBO> GetLexDetailByHGNr(final String hgNr)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        ArrayList<LexTbesBO> lstLex = new ArrayList<>();

        try
        {
            LexTbesBO lex;
            ArrayList<String> lstWordToSearch = new ArrayList<>();
            final String regexHGNr = PCommon.GetHGNrRegex(true);

            String[] words = hgNr.split("[\\\\/=]");
            if (words.length == 0) return lstLex;
            for (String w : words) {
                final String wTrimmed = w.trim();
                if (wTrimmed.matches(regexHGNr))
                {
                    if (PCommon._isDebug) System.out.println(PCommon.ConcaT("YES: ", wTrimmed));
                    lstWordToSearch.add(wTrimmed);
                }
                else
                {
                    if (PCommon._isDebug) System.out.println(PCommon.ConcaT("NO : ", wTrimmed));
                }
            }
            if (lstWordToSearch.isEmpty()) return lstLex;

            StringBuilder sbWhereClause = new StringBuilder("");
            int wordToSearchIdx = 1;
            for (String wordToSearch : lstWordToSearch) {
                sbWhereClause.append( PCommon.ConcaT(sbWhereClause, " dStrong like ", PCommon.AQ(PCommon.ConcaT("%", wordToSearch, "%"))));
                if (wordToSearchIdx < lstWordToSearch.size()) sbWhereClause.append(" OR ");

                wordToSearchIdx++;
            }

            sql = PCommon.ConcaT("SELECT id, eStrong, dStrong, uStrong, hg, transliteration, grammar, english, meaning",
                    " FROM lexTbes",
                    " WHERE ", sbWhereClause.toString(),
                    " ORDER BY dStrong ASC, id ASC");

            c =  _db.rawQuery(sql, null);
            c.moveToFirst();

            while (!c.isAfterLast())
            {
                lex = new LexTbesBO();
                lex.id = c.getInt(0);
                lex.eStrong = c.getString(1);
                lex.dStrong = c.getString(2);
                lex.uStrong = c.getString(3);
                lex.hg = c.getString(4);
                lex.transliteration = c.getString(5);
                lex.grammar = c.getString(6);
                lex.english = c.getString(7);
                lex.meaning = c.getString(8);
                lstLex.add(lex);

                c.moveToNext();
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return lstLex;
    }

    /***
     * Has Lex Detail by HGNr
     * @param hgNr  Ref H9999-G9999
     * @return True/False
     */
    @SuppressWarnings("JavaDoc")
    boolean HasLexDetailByHGNr(final String hgNr)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        boolean hasDetail = false;

        try
        {
            final String hgNrTrimmed = hgNr.trim();

            sql = PCommon.ConcaT("SELECT id",
                    " FROM lexTbes",
                    " WHERE dStrong like ", PCommon.AQ(PCommon.ConcaT("%", hgNrTrimmed, "%")),
                    " LIMIT 1");

            c =  _db.rawQuery(sql, null);
            c.moveToFirst();

            hasDetail = !c.isAfterLast();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return hasDetail;
    }

    /***
     * Get Lex Detail by Id
     */
    @SuppressWarnings("JavaDoc")
    LexTbesBO GetLexDetail(final int lexId)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        LexTbesBO lex = null;

        try
        {
            sql = PCommon.ConcaT("SELECT id, eStrong, dStrong, uStrong, hg, transliteration, grammar, english, meaning",
                    " FROM lexTbes",
                    " WHERE id=", lexId);

            c =  _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast())
            {
                lex = new LexTbesBO();
                lex.id = c.getInt(0);
                lex.eStrong = c.getString(1);
                lex.dStrong = c.getString(2);
                lex.uStrong = c.getString(3);
                lex.hg = c.getString(4);
                lex.transliteration = c.getString(5);
                lex.grammar = c.getString(6);
                lex.english = c.getString(7);
                lex.meaning = c.getString(8);
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return lex;
    }

    /***
     * Get Lex Details by word
     * @param word  Word to search
     * @param lex_detail_search_type  Search type
     * @return List of words
     */
    @SuppressWarnings("JavaDoc")
    ArrayList<LexTbesBO> GetLexDetailByWord(final String word, final PCommon.LEX_DETAIL_SEARCH_TYPE lex_detail_search_type)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        ArrayList<LexTbesBO> lstLex = new ArrayList<>();

        try
        {
            LexTbesBO lex;

            final String strWhereClause = lex_detail_search_type.equals(PCommon.LEX_DETAIL_SEARCH_TYPE.EXACT_SEARCH)
                            ? PCommon.ConcaT(" hg like ", PCommon.AQ(PCommon.RQ(word)))
                            : PCommon.ConcaT(" hg like ", PCommon.AQ(PCommon.ConcaT("%", PCommon.RQ(word), "%")));

            sql = PCommon.ConcaT("SELECT id, eStrong, dStrong, uStrong, hg, transliteration, grammar, english, meaning",
                    " FROM lexTbes",
                    " WHERE ", strWhereClause,
                    " ORDER BY dStrong ASC, id ASC");

            c =  _db.rawQuery(sql, null);
            c.moveToFirst();

            while (!c.isAfterLast())
            {
                lex = new LexTbesBO();
                lex.id = c.getInt(0);
                lex.eStrong = c.getString(1);
                lex.dStrong = c.getString(2);
                lex.uStrong = c.getString(3);
                lex.hg = c.getString(4);
                lex.transliteration = c.getString(5);
                lex.grammar = c.getString(6);
                lex.english = c.getString(7);
                lex.meaning = c.getString(8);
                lstLex.add(lex);

                c.moveToNext();
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return lstLex;
    }

    /***
     * Get Lex Details by word Extended (full search)
     * @param word  Word to search (will be not trimmed)
     * @return Map lex: "LEX" (list: ArrayList<LexTbesBO>), "LEX_EXPR" (LEX_DETAIL_SEARCH_FIELD)
     */
    @SuppressWarnings("JavaDoc")
    Map<String, Object> GetLexDetailByWordExtended(final String word)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        ArrayList<LexTbesBO> lstLex = new ArrayList<>();
        Map<String, Object> mapLex = new HashMap<>();

        try
        {
            final Pattern pHebrew = Pattern.compile("\\p{InHebrew}", Pattern.UNICODE_CASE);
            final Matcher mHebrew = pHebrew.matcher(word);
            final Pattern pGreek = Pattern.compile("\\p{InGreek}", Pattern.UNICODE_CASE);
            final Matcher mGreek = pGreek.matcher(word);
            final Pattern pHGNr = Pattern.compile(PCommon.GetHGNrRegex(false), Pattern.UNICODE_CASE); //was without unicode_case
            final Matcher mHGNr = pHGNr.matcher(word);
            final PCommon.LEX_DETAIL_SEARCH_FIELD lexSearchField;
            final String strWhereClause;
            final String strOrderByClause;

            if (mHebrew.find() || mGreek.find()) {
                lexSearchField = PCommon.LEX_DETAIL_SEARCH_FIELD.HG_WORD;
                strWhereClause = PCommon.ConcaT(" hg like ", PCommon.AQ(PCommon.ConcaT("%", PCommon.RQ(word), "%")));
                strOrderByClause = "hg ASC, id ASC";
            } else if (mHGNr.find()) {
                lexSearchField = PCommon.LEX_DETAIL_SEARCH_FIELD.HG_NR;
                final String expr = PCommon.AQ(PCommon.ConcaT("%", PCommon.RQ(word), "%"));
                strWhereClause = PCommon.ConcaT(" dStrong like ", expr, " OR eStrong like ", expr, " OR uStrong like ", expr);
                strOrderByClause = "dStrong ASC, id ASC";
            } else {
                lexSearchField = PCommon.LEX_DETAIL_SEARCH_FIELD.ENGLISH;
                final String expr = PCommon.AQ(PCommon.ConcaT("%", PCommon.RQ(word), "%"));
                strWhereClause = PCommon.ConcaT(" english like ", expr, " OR transliteration like ", expr, " OR meaning like ", expr);
                strOrderByClause = "english ASC, id ASC";
            }
            mapLex.put("LEX", lstLex);
            mapLex.put("LEX_EXPR", lexSearchField);

            LexTbesBO lex;
            sql = PCommon.ConcaT("SELECT id, eStrong, dStrong, uStrong, hg, transliteration, grammar, english, meaning",
                    " FROM lexTbes",
                    " WHERE ", strWhereClause,
                    " ORDER BY ", strOrderByClause);

            c =  _db.rawQuery(sql, null);
            c.moveToFirst();

            while (!c.isAfterLast())
            {
                lex = new LexTbesBO();
                lex.id = c.getInt(0);
                lex.eStrong = c.getString(1);
                lex.dStrong = c.getString(2);
                lex.uStrong = c.getString(3);
                lex.hg = c.getString(4);
                lex.transliteration = c.getString(5);
                lex.grammar = c.getString(6);
                lex.english = c.getString(7);
                lex.meaning = c.getString(8);
                lstLex.add(lex);

                c.moveToNext();
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return mapLex;
    }

    /***
     * Has Lex Detail by word
     * @param word  Word to search
     * @param lex_detail_search_type    Search type
     * @return True/False
     */
    boolean HasLexDetailByWord(final String word, final PCommon.LEX_DETAIL_SEARCH_TYPE lex_detail_search_type)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        boolean hasDetail = false;

        try
        {
            final String wTrimmed = word.trim();

            final String strWhereClause = lex_detail_search_type.equals(PCommon.LEX_DETAIL_SEARCH_TYPE.EXACT_SEARCH)
                    ? PCommon.ConcaT(" hg like ", PCommon.AQ(PCommon.RQ(wTrimmed)))
                    : PCommon.ConcaT(" hg like ", PCommon.AQ(PCommon.ConcaT("%", PCommon.RQ(wTrimmed), "%")));

            sql = PCommon.ConcaT("SELECT id",
                    " FROM lexTbes",
                    " WHERE ", strWhereClause,
                    " LIMIT 1");

            c =  _db.rawQuery(sql, null);
            c.moveToFirst();

            hasDetail = !c.isAfterLast();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return hasDetail;
    }

    /***
     * Get lexAggr Id of verse ref or -1 if not found
     * @return id
     */
    int GetLexAggrIdOfVerse(final int bNumber, final int cNumber, final int vNumber)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        int id = -1;

        try
        {
            final String verseRef = this.GetLexAggrVerseRef(bNumber, cNumber, vNumber);
            sql = PCommon.ConcaT("SELECT id FROM lexAggr ",
                    " WHERE field0 like ", PCommon.AQ(verseRef), " OR field0 like ", PCommon.AQ(PCommon.ConcaT(verseRef, " %")),
                    " ORDER BY id ASC ",
                    " LIMIT 1");

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast()) {
                id = c.getInt(0);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null) {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return id;
    }

    /***
     * Get Lex verse ref
     * @return verse ref
     */
    String GetLexAggrVerseRef(final int bNumber, final int cNumber, final int vNumber)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        String verseRef = "";

        try
        {
            sql = PCommon.ConcaT("SELECT bsName FROM lexBook WHERE bNumber=", bNumber);

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            if (!c.isAfterLast())
            {
                final String bsName = c.getString(0);
                verseRef = PCommon.ConcaT(bsName, ".", cNumber, ".", vNumber);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null) {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return verseRef;
    }

    /***
     * Get All Lex Book
     * @return All books with bsName
     */
    private Map<String, Integer> GetAllLexBook()
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        Cursor c = null;
        Map<String, Integer> mapBook = new HashMap<>();

        try
        {
            int bNumber;
            String bsName;

            sql = "SELECT bNumber, bsName FROM lexBook";

            c = _db.rawQuery(sql, null);
            c.moveToFirst();

            while (!c.isAfterLast())
            {
                bNumber = c.getInt(0);
                bsName = c.getString(1);
                mapBook.put(bsName, bNumber);

                c.moveToNext();
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;

            if (c != null) {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return mapBook;
    }

    /***
     * Add all Lex refs to cache search
     * @param bbName
     * @param word
     * @param tabId
     */
    @SuppressWarnings("JavaDoc")
    void AddAllLexRefToCacheSearch(final String bbName, String word, final int tabId)
    {
        @SuppressWarnings("UnusedAssignment") String sql = null;
        @SuppressWarnings("UnusedAssignment") String sql2 = null;
        @SuppressWarnings("UnusedAssignment") String sql3 = null;
        Cursor c = null;
        Cursor c2 = null;

        try
        {
            final String expr;
            try
            {
                word = word.trim();
                String[] arrWord = word.split(" ");
                word = arrWord[0].trim();
                expr = PCommon.AQ(PCommon.ConcaT("%", PCommon.RQ(word), "%"));
                //TODO: LEX, exact expr (without %) ?
            }
            catch (Exception ex)
            {
                return;
            }

            int id, idFrom, idTo, bookNr, cNumber, vNumber;
            String bsName, refOrig, refOrigUpd, refCleaned;
            Map<Integer, String> mapRef = new HashMap<>();
            Map<String, Integer> mapBook = this.GetAllLexBook();

            sql = PCommon.ConcaT("SELECT DISTINCT id",
                    " FROM lexAggr",
                    " WHERE field0 like '%Word%'",
                    " AND (field1 like ", expr,
                    " OR field2 like ", expr,
                    " OR field3 like ", expr,
                    " OR field4 like ", expr,
                    " OR field5 like ", expr,
                    " OR field6 like ", expr,
                    " OR field7 like ", expr,
                    " OR field8 like ", expr,
                    " OR field9 like ", expr,
                    " OR field10 like ", expr, ")",
                    " ORDER BY id ASC");

            c =  _db.rawQuery(sql, null);
            c.moveToFirst();

            while (!c.isAfterLast())
            {
                //Get id
                id = c.getInt(0);

                //Get ref
                idTo = id;
                idFrom = idTo - 10;

                sql2 = PCommon.ConcaT("SELECT field0",
                        " FROM lexAggr",
                        " WHERE id >= ", idFrom, " AND id < ", idTo,
                        " AND (field0 not like '%Word%' AND field0 not like '%Trans%' AND field0 not like '%Sign%' )",
                        " ORDER BY id DESC",
                        " LIMIT 1");

                c2 = _db.rawQuery(sql2, null);
                c2.moveToFirst();
                if (!c2.isAfterLast())
                {
                    //Found
                    refOrig = c2.getString(0);
                    refOrigUpd = refOrig.trim().replaceAll("_", "");

                    String[] arrRefNumber = refOrigUpd.split("\\.");
                    if (arrRefNumber.length >= 3)
                    {
                        try
                        {
                            bsName = arrRefNumber[0];
                            cNumber = Integer.parseInt(arrRefNumber[1]);
                            vNumber = Integer.parseInt(arrRefNumber[2]);
                        } catch (Exception ignored) {
                            if (PCommon._isDebug) System.out.println(PCommon.ConcaT("!GET ALL REF: (", id, "): ", refOrig, " NOT CORRECT, SKIPPED!"));
                            c.moveToNext();
                            continue;
                        }

                        bookNr = mapBook.containsKey(bsName) ? mapBook.get(bsName) : -1;
                        if (bookNr >= 0)
                        {
                            refCleaned = PCommon.ConcaT(bookNr, ".", cNumber, ".", vNumber);
                            //TODO: LEX, SOLVE SOME LONG REF.
                            if (PCommon._isDebug) System.out.println(PCommon.ConcaT("GET ALL REF: (", id, "): ", refOrig, ", ", refCleaned));
                            if (!mapRef.containsValue(refCleaned))
                            {
                                mapRef.put(mapRef.size(), refCleaned);
                                ArrayList<VerseBO> arrVerse = this.GetVerse(bbName, bookNr, cNumber, vNumber);
                                if (arrVerse != null)
                                {
                                    for (VerseBO verse : arrVerse)
                                    {
                                        sql3 = PCommon.ConcaT("INSERT INTO cacheSearch (tabId, bibleId) VALUES (", tabId, ",", verse.id, ")");
                                        _db.execSQL(sql3);
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (PCommon._isDebug) System.out.println(PCommon.ConcaT("!GET ALL REF: (", id, "): ", refOrig, " BOOK NOT FOUND, SKIPPED!"));
                        }
                    }
                    else
                    {
                        if (PCommon._isDebug) System.out.println(PCommon.ConcaT("!GET ALL REF: (", id, "): ", refOrig, " NOT CORRECT, SKIPPED!"));
                    }
                    arrRefNumber = null;
                }
                c2.close();

                //Next
                c.moveToNext();
            }

            if (PCommon._isDebug) System.out.println(PCommon.ConcaT("GET ALL REF: ", c.getCount(), " refs before cleaning"));
            if (PCommon._isDebug) System.out.println(PCommon.ConcaT("GET ALL REF: ", mapRef.size(), " refs after cleaning"));
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            //noinspection UnusedAssignment
            sql = null;
            //noinspection UnusedAssignment
            sql2 = null;
            //noinspection UnusedAssignment
            sql3 = null;

            if (c != null)
            {
                if (!c.isClosed()) c.close();
                //noinspection UnusedAssignment
                c = null;
            }

            if (c2 != null)
            {
                if (!c2.isClosed()) c2.close();
                //noinspection UnusedAssignment
                c2 = null;
            }
        }
    }

    //</editor-fold>
}

//Works:
//select b1.bnumber, b1.cnumber, b1.vnumber, b1.vtext, (select vtext from bible b2 where b2.bbName='d' and b2.bnumber=b1.bnumber and b2.cnumber=b1.cnumber and b2.vnumber=b1.vnumber) vtext2 from bible b1 where b1.bnumber=1 and b1.cnumber=1 order by b1.bnumber asc, b1.cnumber asc, b1.vnumber asc, b1.bbname asc

/*

-- SEARCH H... G... (G5598: Omega)
select *
from lexAggr
where field0 like '%Word%' AND
(field1 like '%G5598%'
OR  field2 like '%G5598%'
OR  field3 like '%G5598%'
OR  field4 like '%G5598%'
OR  field5 like '%G5598%'
OR  field6 like '%G5598%'
OR  field7 like '%G5598%'
OR  field8 like '%G5598%'
OR  field9 like '%G5598%'
OR  field10 like '%G5598%')

--- SEARCH for others: omega
select id
from lexAggr
where field1 like '%omega%'
OR  field2 like '%omega%'
OR  field3 like '%omega%'
OR  field4 like '%omega%'
OR  field5 like '%omega%'
OR  field6 like '%omega%'
OR  field7 like '%omega%'
OR  field8 like '%omega%'
OR  field9 like '%omega%'
OR  field10 like '%omega%'

== Omega ids: 174261, 174288, 177680, 177972
=> With id: Find first not Word like, not translation like, not variant like
(keep only field0)
=> so it's a ref (Gen to Rev).
=> Remove possible leading underscore.
=> group refs to find book numbers and cnumber, vnumber
=> search in Bible with bNumber, cNumber, vNumber
=> display these verses as normal.

-- for 174261 => (-2) Rev.1.8
select *
from lexAggr
where id >= (174261 - 20) and id <= 174261
AND (field0 not like '%Word%' AND field0 not like '%Trans%' AND field0 not like '%Sign%' )
order by id DESC
limit 1

-- for 174288 => (-2) Rev.1.11
-- for 177680 => (-2) _Rev.21.6
-- for 177972 => (-2) Rev.22.13

=================================== Find all ref G3341 => 24 rec.
Dans lexAggr
field0 like '%Word%'
for all field1...field10
=> find ref of IDs
=> remove '_' from field0 of refs
=> unique

 */