
package com.appcoholic.bible;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.NotificationManager;
import android.app.UiModeManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.format.DateFormat;
import android.text.method.LinkMovementMethod;
import android.text.style.BackgroundColorSpan;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.UnderlineSpan;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.material.appbar.AppBarLayout;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

/**
 * Project Common Class
 */
@SuppressWarnings("JavaDoc")
final class PCommon implements IProject
{
    //<editor-fold defaultstate="collapsed" desc="-- Variables --">

    //The following variables should be false before putting on the Market and Debuggable=False in manifest
    static boolean _isDebug = false;
    static int _debugCounter = 0;
    //private final static boolean _useStrictMode = false;

    final static LayoutParams _layoutParamsWrap = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
    final static LayoutParams _layoutParamsMatchAndWrap = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
    final static LayoutParams _layoutParamsMatch = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

    @SuppressLint("StaticFieldLeak")
    private static SCommon _s = null;
    private static boolean _isShowMyArt = false;

    enum ARTICLE_ACTION
    {
        CREATE_ARTICLE,
        RENAME_ARTICLE,
        DELETE_ARTICLE
    }

    enum BROWSE_FILE_TYPE
    {
        PICTURE,
        DATABASE
    }

    enum LEX_DETAIL_SEARCH_TYPE {
        EXACT_SEARCH
        //ALL_SEARCH
    }

    enum LEX_DETAIL_SEARCH_FIELD {
        HG_WORD,
        HG_NR,
        ENGLISH
    }

    enum AUDIO_BIBLE_ACTION {
        LANG_CLICK,
        START_CLICK,
        BACK_CLICK,
        PLAY_CLICK,
        STOP_CLICK,
        FORWARD_CLICK,
        END_CLICK
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="-- Initializer --">

    /***
     * Initializer
     */
    private PCommon()
    {
    }

    //</editor-fold>

    /***
     * Check local instance
     * @param context
     */
    private static void CheckLocalInstance(final Context context)
    {
        try
        {
            if (_s == null) _s = SCommon.GetInstance(context);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) LogR(context, ex);
        }
    }

    /**
     * Convert stack trace to string
     * @param stackTrace    Stack trace
     * @return string
     */
    static String StackTraceToString(final StackTraceElement[] stackTrace)
    {
        final StringWriter sw = new StringWriter();
        PrintStackTrace(stackTrace, new PrintWriter(sw));

        return sw.toString();
    }

    private static void PrintStackTrace(final StackTraceElement[] stackTrace, final PrintWriter pw)
    {
        for(StackTraceElement stackTraceElement : stackTrace)
        {
            pw.println(stackTraceElement);
        }
    }

    /**
     * Concatenate objects (generic) with StringBuilder
     * @param args  Arguments
     * @return string
     */
    static String ConcaT(final Object... args)
    {
        final StringBuilder sb = new StringBuilder();

        for (final Object obj : args)
        {
            if (obj != null) sb.append(obj);
        }

        return sb.toString();
    }

    /***
     * Add quotes at start & Stop of string
     * @param value Field value
     * @return Quotated string
     */
    static String AQ(final String value)
    {
        return PCommon.ConcaT("'", value, "'");
    }

    /***
     * Replace quotes (') by double quotes in fields
     * @param value     Field value
     * @return Field value ready to be concatenated in sql query
     */
    static String RQ(final String value)
    {
        if (!value.contains("'"))
            return value;

        return value.replaceAll("'", "''");
    }

    /**
     * Get current date YYYYMMDD (E.G.: 20160818)
     * @return YYYYMMDD
     */
    static String NowYYYYMMDD()
    {
        return DateFormat.format("yyyyMMdd", new Date()).toString();
    }

    /**
     * Get current time (E.G.: 14:34:20)
     * @return NowFunc
     */
    private static String TimeFunc()
    {
        return DateFormat.format("kk:mm:ss", new Date()).toString();
    }

    /**
     * Get current time (E.G.: 143420)
     * @return NowFunc
     */
    static String TimeFuncShort()
    {
        return DateFormat.format("kkmmss", new Date()).toString();
    }

    /**
     * Save key in SharedPreferences
     * @param context
     * @param key
     * @param value
     */
    static void SavePref(final Context context, final APP_PREF_KEY key, final String value)
    {
        //SharedPreferences appPrefs = context.getSharedPreferences("task1", MODE_PRIVATE);
        final SharedPreferences appPrefs = PreferenceManager.getDefaultSharedPreferences(context);

        final SharedPreferences.Editor editor = appPrefs.edit();
        editor.putString(key.toString(), value);
        editor.apply();
    }

    /**
     * Save key in SharedPreferences
     * @param context
     * @param key   integer logs as String
     * @param value
     */
    static void SavePrefInt(final Context context, final APP_PREF_KEY key, final int value)
    {
        SavePref(context, key, ConcaT(value));

        //LogD(context, key);
    }

    /***
     * Get search string limit
     * @return limit
     */
    static int GetSearchFullQueryLimit()
    {
        return 3;
    }

    /**
     * Get key from SharedPreferences
     * @param context
     * @param key
     * @param defaultValue
     * @return
     */
    static String GetPref(final Context context, final APP_PREF_KEY key, final String defaultValue)
    {
        final SharedPreferences appPrefs = PreferenceManager.getDefaultSharedPreferences(context);

        return appPrefs.getString(key.toString(), defaultValue);
    }

    /***
     * Get BIBLE_APP_TYPE
     * @param context
     * @return 1 (simple) or M (multi)
     */
    static String GetPrefBibleAppType(final Context context)
    {
        String bibleAppType = PCommon.GetPref(context, APP_PREF_KEY.BIBLE_APP_TYPE, "1");
        if (bibleAppType == null || bibleAppType.isEmpty()) bibleAppType = "1";

        return bibleAppType;
    }

    /***
     * Get BIBLE_NAME
     * @param context
     */
    static String GetPrefBibleName(final Context context)
    {
        String bbName = PCommon.GetPref(context, APP_PREF_KEY.BIBLE_NAME, "");
        if (bbName == null || bbName.isEmpty()) bbName = "k";

        return bbName;
    }

    /***
     * Get TRAD_BIBLE_NAME
     * @param context
     * @param canReturnDefaultValue  False => its real value (can be empty), True => if empty: fill with a default value
     */
    static String GetPrefTradBibleName(final Context context, final boolean canReturnDefaultValue)
    {
        String trad = PCommon.GetPref(context, IProject.APP_PREF_KEY.TRAD_BIBLE_NAME, "");
        if (trad == null || trad.isEmpty())
        {
            if (canReturnDefaultValue)
            {
                trad = GetPrefBibleName(context);
            }
            else
            {
                trad = "";
            }
        }

        return trad;
    }

    /***
     * Get theme ID
     * @param context
     * @return theme ID
     */
    static int GetPrefThemeId(final Context context)
    {
        final String THEME_NAME = PCommon.GetPrefThemeName(context);
        final int themeId;

        switch (THEME_NAME)
        {
            case "DARK":
                themeId = R.style.AppThemeDark;
                break;

            case "BLACK":
                themeId = R.style.AppThemeBlack;
                break;

            case "NAVY":
                themeId = R.style.AppThemeNavy;
                break;

            case "LIGHT":
            default:
                themeId = R.style.AppThemeLight;
                break;
        }

        return themeId;
    }

    /**
     * Manage TRAD_BIBLE_NAME (set)
     * @param context
     * @param operation When = 0 then return lang stack, when > 0 then add else remove
     * @param bbName    bbName
     * @return String with selected language names
     */
    private static String ManageTradBibleName(final Context context, final int operation, final String bbName)
    {
        boolean valueChanged = false;
        String trad = PCommon.GetPrefTradBibleName(context, false);
        if (operation > 0)
        {
            //Add
            if (!trad.contains(bbName))
            {
                trad = PCommon.ConcaT(trad, bbName);
                valueChanged = true;
            }
        }
        else if (operation < 0)
        {
            //Remove
            if (trad.contains(bbName))
            {
                trad = trad.replace(bbName, "");
                valueChanged = true;
            }
        }
        //Save TRAD
        if (valueChanged) PCommon.SavePref(context, APP_PREF_KEY.TRAD_BIBLE_NAME, trad);

        //Returns lang stack
        final int size = trad.length();
        StringBuilder sb = new StringBuilder();
        String bb;
        for (int i = 0; i < size; i++)
        {
            bb = trad.substring(i, i + 1);
            if (bb.compareToIgnoreCase("k") == 0)
            {
                sb.append(context.getString(R.string.languageEnShort));
                sb.append(" ");
            }
            else if (bb.compareToIgnoreCase("v") == 0)
            {
                sb.append(context.getString(R.string.languageEsShort));
                sb.append(" ");
            }
            else if (bb.compareToIgnoreCase("l") == 0)
            {
                sb.append(context.getString(R.string.languageFrShort));
                sb.append(" ");
            }
            else if (bb.compareToIgnoreCase("d") == 0)
            {
                sb.append(context.getString(R.string.languageItShort));
                sb.append(" ");
            }
            else if (bb.compareToIgnoreCase("a") == 0)
            {
                sb.append(context.getString(R.string.languagePtShort));
                sb.append(" ");
            }
            else if (bb.compareToIgnoreCase("o") == 0)
            {
                sb.append(context.getString(R.string.languageFr2Short));
                sb.append(" ");
            }
            else if (bb.compareToIgnoreCase("2") == 0)
            {
                sb.append(context.getString(R.string.languageEn2Short));
                sb.append(" ");
            }
            else if (bb.compareToIgnoreCase("9") == 0)
            {
                sb.append(context.getString(R.string.languageEs2Short));
                sb.append(" ");
            }
            else if (bb.compareToIgnoreCase("1") == 0)
            {
                sb.append(context.getString(R.string.languageIt2Short));
                sb.append(" ");
            }
            else if (bb.compareToIgnoreCase("i") == 0)
            {
                sb.append(context.getString(R.string.languageInShort));
                sb.append(" ");
            }
            else if (bb.compareToIgnoreCase("y") == 0)
            {
                sb.append(context.getString(R.string.languageArShort));
                sb.append(" ");
            }
            else if (bb.compareToIgnoreCase("c") == 0)
            {
                sb.append(context.getString(R.string.languageCnShort));
                sb.append(" ");
            }
            else if (bb.compareToIgnoreCase("s") == 0)
            {
                sb.append(context.getString(R.string.languageDeShort));
                sb.append(" ");
            }
            else if (bb.compareToIgnoreCase("e") == 0)
            {
                sb.append(context.getString(R.string.languageDe2Short));
                sb.append(" ");
            }
            else if (bb.compareToIgnoreCase("j") == 0)
            {
                sb.append(context.getString(R.string.languageJpShort));
                sb.append(" ");
            }
            else if (bb.compareToIgnoreCase("u") == 0)
            {
                sb.append(context.getString(R.string.languageRoShort));
                sb.append(" ");
            }
            else if (bb.compareToIgnoreCase("z") == 0)
            {
                sb.append(context.getString(R.string.languagePlShort));
                sb.append(" ");
            }
            else if (bb.compareToIgnoreCase("r") == 0)
            {
                sb.append(context.getString(R.string.languageRuShort));
                sb.append(" ");
            }
            else if (bb.compareToIgnoreCase("t") == 0)
            {
                sb.append(context.getString(R.string.languageTrShort));
                sb.append(" ");
            }
            else if (bb.compareToIgnoreCase("b") == 0)
            {
                sb.append(context.getString(R.string.languageBdShort));
                sb.append(" ");
            }
            else if (bb.compareToIgnoreCase("h") == 0)
            {
                sb.append(context.getString(R.string.languageSwShort));
                sb.append(" ");
            }
            else if (bb.compareToIgnoreCase("w") == 0)
            {
                sb.append(context.getString(R.string.languageHeElShort));
                sb.append(" ");
            }
        }

        return sb.toString().trim().replaceAll(" ", ", ");
    }

    /**
     * Get THEME_NAME
     * @param context
     * @return LIGHT as default
     */
    static String GetPrefThemeName(final Context context)
    {
        String themeName = "LIGHT";

        try
        {
            themeName = PCommon.GetPref(context, APP_PREF_KEY.THEME_NAME, "LIGHT");

            switch (themeName)
            {
                case "DARK":
                case "BLACK":
                case "NAVY":
                case "LIGHT":
                    break;

                default:
                    themeName = "LIGHT";
                    break;
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }

        return themeName;
    }

    /**
     * Set THEME_NAME
     * @param context   Context
     * @param themeName Theme
     */
    static void SetThemeName(final Context context, String themeName)
    {
        try
        {
            switch (themeName)
            {
                case "DARK":
                case "BLACK":
                case "NAVY":
                case "LIGHT":
                    break;

                default:
                    themeName = "LIGHT";
                    break;
            }

            PCommon.SavePref(context, APP_PREF_KEY.THEME_NAME, themeName);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Get dynamic column count
     * @param context      Context
     * @param bibleCount   Number of Bibles to display
     * @return Preferred number of columns to use to display the info
     */
    static int GetDynamicColumnCount(final Context context, final int bibleCount)
    {
        try {
            switch (bibleCount)
            {
                case 1:
                    return Integer.parseInt(PCommon.GetPref(context, IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_1, "1"));
                case 2:
                    return Integer.parseInt(PCommon.GetPref(context, IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_2, "1"));
                case 3:
                    return Integer.parseInt(PCommon.GetPref(context, IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_3, "1"));
                case 4:
                    return Integer.parseInt(PCommon.GetPref(context, IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_4, "1"));
                case 5:
                    return Integer.parseInt(PCommon.GetPref(context, IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_5, "1"));
                case 6:
                    return Integer.parseInt(PCommon.GetPref(context, IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_6, "1"));
                default:
                    return 1;
            }
        }
        catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }

        return 1;
    }

    /**
     * Log (Release mode)
     * @param msg   Message
     */
    static void LogR(final Context context, final String msg)
    {
        try
        {
            final String newMsg = ConcaT(TimeFunc(), " ", msg);
            System.out.println(newMsg);

            //AddLog implementation
            CheckLocalInstance(context);
            _s.AddLog(newMsg);
        }
        catch(Exception ignored) {}
    }

    /**
     * Log (Release mode) stack trace
     * @param context   Context
     * @param ex    Exception
     */
    static void LogR(final Context context, final Exception ex)
    {
        final String msg = PCommon.ConcaT(ex.toString(), "\n>> ", StackTraceToString(ex.getStackTrace()));
        LogR(context, R.string.logErr, msg);
    }

    /**
     * Log (Release mode)
     * @param resId RessourceId
     * @param args  Arguments (for String.format)
     */
    private static void LogR(final Context context, @SuppressWarnings("SameParameterValue") final int resId, final Object... args)
    {
        LogR(context, String.format(context.getResources().getText(resId).toString(), args));
    }

    static void LogRMainVars(final Context context, final String titleBlock)
    {
        try
        {
            String msg = "";
            LogR(context, msg);
            LogR(context, PCommon.ConcaT("* ", titleBlock));

            msg = PCommon.ConcaT("BIBLE_NAME:", PCommon.GetPref(context, IProject.APP_PREF_KEY.BIBLE_NAME, ""));
            LogR(context, msg);

            msg = PCommon.ConcaT("ALT_LANGUAGE:", PCommon.GetPref(context, IProject.APP_PREF_KEY.ALT_LANGUAGE, ""));
            LogR(context, msg);

            msg = PCommon.ConcaT("BIBLE_NAME_DIALOG:", PCommon.GetPref(context, IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, ""));
            LogR(context, msg);

            msg = PCommon.ConcaT("TRAD_BIBLE_NAME:", PCommon.GetPref(context, IProject.APP_PREF_KEY.TRAD_BIBLE_NAME, ""));
            LogR(context, msg);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /*
    protected static void LogR(final Context context, final Exception ex, String addMsg)
    {
        String msg = null;
        String logs = null;
        String logsCut = null;

        try
        {
            //was LogR(context, R.string.logErr, StackTraceToString(ex.getStackTrace()));

            final String now = NowFunc();

            if (!_isDebugVersion)
            {
                //Release
                msg = ex.getMessage();
                logs = ConcaT(GetPref(context, APP_PREF_KEY.LOG_STATUS), "\n", now, "  ", msg);
            }
            else
            {
                //Debug
                if (addMsg == null || addMsg.length() == 0)
                {
                    addMsg = "?";
                }

                msg = StackTraceToString(ex.getStackTrace());
                logs = ConcaT(GetPref(context, APP_PREF_KEY.LOG_STATUS), "\n", now, "  ", addMsg, " <\n", msg);
            }

            logsCut = LogCut(logs);
            SavePref(context, APP_PREF_KEY.LOG_STATUS, logsCut);
        }
        catch(Exception ex2)
        {
            //Should do nothing
        }
        finally
        {
            msg = null;
            logs = null;
            logsCut = null;
        }
    }
*/

    /*
    protected static String LogCut(final String logs)
    {
        //TODO: external maxSize and put above
        final int size = logs.length();
        final int maxSize = 10000;

        if (size >= maxSize) {
            //Cut
            //TODO: begin after first /n, #### seems ok for end of string but check it.
            return logs.substring(size - maxSize);
        }

        return logs;
    }
    */

    /***
     * Get resource threadId
     * @param context   Context
     * @param resName   Resource name
     * @return Resource Id (-1 by default)
     */
    static int GetResId(final Context context, final String resName)
    {
        int id = -1;

        try
        {
            if (resName == null || resName.isEmpty()) return id;

            id = com.appcoholic.bible.R.string.class.getField( resName ).getInt(null);
        }
        catch(Exception ignored)
        {
            return id;
        }

        return id;
    }

    /***
     * Get drawable ciId
     * @param context   Context
     * @param resName   Resource name
     * @return Resource Id (-1 by default)
     */
    static int GetDrawableId(final Context context, final String resName)
    {
        int id = -1;

        try
        {
            if (resName == null || resName.isEmpty())
            {
                return id;
            }

            id = com.appcoholic.bible.R.drawable.class.getField( resName ).getInt(null);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }

        return id;
    }

    /***
     * Get drawable
     * @param context   Context
     * @param id    drawable Id
     * @return drawable
     */
    static Drawable GetDrawable(final Context context, @SuppressWarnings("SameParameterValue") final int id)
    {
        try
        {
            return (Build.VERSION.SDK_INT >= 22) ? ContextCompat.getDrawable(context, id) : context.getResources().getDrawable(id);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }

        return null;
    }

    /***
     * Get thread type running
     * @param context   Context
     * @param findThreadType    Thread to find (0=ANY, 1=INSTALL, 2=LISTEN)
     * @return Get thread type running (0=DEFAUT, 1=INSTALL, 2=LISTEN)
     */
    static int GetThreadTypeRunning(final Context context, final int findThreadType)
    {
        int threadType = 0;

        try
        {
            final String threadName = context.getString(R.string.threadNfoPrefix);
            final String threadNameInstall = context.getString(R.string.threadNfoInstall);
            final String threadNameListen = context.getString(R.string.threadNfoListen);

            Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
            final Thread[] threadArr = threadSet.toArray(new Thread[0]);
            for (Thread thread : threadArr)
            {
                //TODO: ThreadGroup! => list group to find it?
                if (thread.getName().startsWith(threadName))
                {
                    if (findThreadType == 0)
                    {
                        if (thread.getName().contains(threadNameInstall))
                        {
                            threadType = 1;
                            break;
                        }
                        else if (thread.getName().contains(threadNameListen))
                        {
                            threadType = 2;
                            break;
                        }
                    }
                    else if (findThreadType == 1)
                    {
                        if (thread.getName().contains(threadNameInstall))
                        {
                            threadType = findThreadType;
                            break;
                        }
                    }
                    else if (findThreadType == 2)
                    {
                        if (thread.getName().contains(threadNameListen))
                        {
                            threadType = findThreadType;
                            break;
                        }
                    }
                }
            }

            threadSet.clear();
            //noinspection UnusedAssignment
            threadSet = null;
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }

        return threadType;
    }

    /***
     * Try to quit application
     * @param context   Context
     * @return 0=quit application
     */
    static int TryQuitApplication(final Context context)
    {
        try
        {
            if (PCommon._isDebug) System.out.println("TryQuitApplication");

            MainActivity.Tab.SaveScrollPosY(context);

            final int threadType = PCommon.GetThreadTypeRunning(context, 0);
            if (threadType == 1)
            {
                ShowToast(context, R.string.installQuit, Toast.LENGTH_SHORT);
                return threadType;
            }
            else if (threadType == 2)
            {
                final Intent startMain = new Intent(Intent.ACTION_MAIN);
                startMain.addCategory(Intent.CATEGORY_HOME);
                startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(startMain);
                return threadType;
            }

            final NotificationManager notifManager= (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            Objects.requireNonNull(notifManager).cancelAll();

            QuitApplication(context);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }

        return 0;
    }

    /***
     * Quit application
     * @param context   context
     */
    private static void QuitApplication(final Context context)
    {
        //noinspection EmptyCatchBlock
        try
        {
/*
            PCommon.SavePrefInt(context, APP_PREF_KEY.EDIT_STATUS, 0);
            Thread.sleep(300);

            //if (_s == null) CheckLocalInstance(context);
            //_s.ShrinkDb(context);
            //if (PCommon._isDebugVersion) System.out.print("Shrunk");
*/
            CheckLocalInstance(context);
            _s.CloseDb();
        }
        catch (Exception ex) { }
        finally
        {
            _s = null;
        }

        /*
        try
        {
            //System.exit(0);
            android.os.Process.killProcess(android.os.Process.myPid());
        }
        catch (Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(context, ex);
        }
         */
    }

    /***
     * Show Toast
     * @param context   Context
     * @param message   Message
     * @param duration  Duration (ex: Toast.LENGTH_SHORT...)
     */
    static void ShowToast(final Context context, final int message, final int duration)
    {
        try
        {
            final Toast toast = Toast.makeText(context, message, duration);
            toast.show();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Show Toast
     * @param context   Context
     * @param message   Message
     * @param duration  Duration (ex: Toast.LENGTH_SHORT...)
     */
    static void ShowToast(final Context context, final String message, @SuppressWarnings("SameParameterValue") final int duration)
    {
        try
        {
            final Toast toast = Toast.makeText(context, message, duration);
            toast.show();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Copy text to clipboard
     * @param context   Context
     * @param label     Label
     * @param text      Text to copy
     */
    static void CopyTextToClipboard(final Context context, @SuppressWarnings("SameParameterValue") final String label, final String text, final boolean shouldShowToast)
    {
        try
        {
            final ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            if (clipboard != null)
            {
                final ClipData clip = ClipData.newPlainText(label, text);
                clipboard.setPrimaryClip(clip);
                if (shouldShowToast) PCommon.ShowToast(context, R.string.toastCopiedClipboard, Toast.LENGTH_SHORT);
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Send email
     * @param context   Context
     * @param toList    Array of email address
     * @param subject   Email subject
     * @param body      Email body
     */
    static void SendEmail(final Context context, final String[] toList, final String subject, @SuppressWarnings("SameParameterValue") final String body)
    {
        try
        {
            final Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("message/rfc822");
            intent.putExtra(Intent.EXTRA_EMAIL  , toList);
            intent.putExtra(Intent.EXTRA_SUBJECT, (subject == null) ? "" : subject);
            intent.putExtra(Intent.EXTRA_TEXT   , (body == null) ? "" : body);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

            context.startActivity(Intent.createChooser(intent, context.getResources().getString(R.string.emailChooser)));
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Share text
     * @param context   Context
     * @param text      Text to share
     */
    static void ShareText(final Context context, final String text)
    {
        try
        {
            final String mimeTypeTextPlain = "text/plain";
            final Intent intentQuery = new Intent();
            intentQuery.setAction(Intent.ACTION_SEND);
            intentQuery.setType(mimeTypeTextPlain);
            intentQuery.putExtra(Intent.EXTRA_TEXT, text);

            final List<ResolveInfo> resInfos = context.getPackageManager().queryIntentActivities(intentQuery, 0);
            if (!resInfos.isEmpty())
            {
                context.startActivity(intentQuery);
            }
            else
            {
                PCommon.ShowToast(context, R.string.toastNoAppsToShare, Toast.LENGTH_LONG);
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Open url
     * @param context   Context
     * @param url       Url
     */
    static void OpenUrl(final Context context, @SuppressWarnings("SameParameterValue") final String url)
    {
        try
        {
            if (url.toUpperCase().startsWith("COPY:"))
            {
                final String newUrl = url.substring(5);
                PCommon.CopyTextToClipboard(context, "", newUrl, true);
                return;
            }

            final Uri webpage = Uri.parse(url);
            final Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
            context.startActivity(intent);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Select bible language
     * Response in BIBLE_NAME_DIALOG
     * @param context
     * @param msg
     * @param desc
     * @param isCancelable
     * @param forceShowAllButtons  Force to show all buttons
     */
    static void SelectBibleLanguage(final AlertDialog builder, final Context context, final View view, final String msg, @SuppressWarnings("UnusedParameters") final String desc, @SuppressWarnings("SameParameterValue") final boolean isCancelable, @SuppressWarnings("SameParameterValue") final boolean forceShowAllButtons)
    {
        try
        {
            builder.setCancelable(isCancelable);
            if (isCancelable) {
                builder.setOnCancelListener(dialogInterface -> PCommon.SavePref(context, APP_PREF_KEY.BIBLE_NAME_DIALOG, ""));
            }
            builder.setTitle(msg);
            builder.setView(view);

            final int colorAccent = ContextCompat.getColor(context, R.color.colorAccent);
            final String bbName = PCommon.GetPref(context, APP_PREF_KEY.BIBLE_NAME, "");
            final int installStatus = (forceShowAllButtons) ? PCommon.GetInstallStatusShouldBe() : Integer.parseInt(PCommon.GetPref(context, APP_PREF_KEY.INSTALL_STATUS, "1"));

            final Button btnLanguageEN = view.findViewById(R.id.btnLanguageEN);
            if (installStatus < 1) btnLanguageEN.setVisibility(View.INVISIBLE);
            if (bbName.compareToIgnoreCase("k") == 0) btnLanguageEN.setTextColor(colorAccent);
            btnLanguageEN.setOnClickListener(v -> {
                PCommon.SavePref(context, APP_PREF_KEY.BIBLE_NAME_DIALOG, "k");
                PCommon.SavePref(context, APP_PREF_KEY.ALT_LANGUAGE, "en");
                builder.dismiss();
            });
            final Button btnLanguageES = view.findViewById(R.id.btnLanguageES);
            if (installStatus < 2) btnLanguageES.setVisibility(View.INVISIBLE);
            if (bbName.compareToIgnoreCase("v") == 0) btnLanguageES.setTextColor(colorAccent);
            btnLanguageES.setOnClickListener(v -> {
                PCommon.SavePref(context, APP_PREF_KEY.BIBLE_NAME_DIALOG, "v");
                PCommon.SavePref(context, APP_PREF_KEY.ALT_LANGUAGE, "es");
                builder.dismiss();
            });
            final Button btnLanguageFR = view.findViewById(R.id.btnLanguageFR);
            if (installStatus < 3) btnLanguageFR.setVisibility(View.INVISIBLE);
            if (bbName.compareToIgnoreCase("l") == 0) btnLanguageFR.setTextColor(colorAccent);
            btnLanguageFR.setOnClickListener(v -> {
                PCommon.SavePref(context, APP_PREF_KEY.BIBLE_NAME_DIALOG, "l");
                PCommon.SavePref(context, APP_PREF_KEY.ALT_LANGUAGE, "fr");
                builder.dismiss();
            });
            final Button btnLanguageIT = view.findViewById(R.id.btnLanguageIT);
            if (installStatus < 4) btnLanguageIT.setVisibility(View.INVISIBLE);
            if (bbName.compareToIgnoreCase("d") == 0) btnLanguageIT.setTextColor(colorAccent);
            btnLanguageIT.setOnClickListener(v -> {
                PCommon.SavePref(context, APP_PREF_KEY.BIBLE_NAME_DIALOG, "d");
                PCommon.SavePref(context, APP_PREF_KEY.ALT_LANGUAGE, "it");
                builder.dismiss();
            });
            final Button btnLanguagePT = view.findViewById(R.id.btnLanguagePT);
            if (installStatus < 5) btnLanguagePT.setVisibility(View.INVISIBLE);
            if (bbName.compareToIgnoreCase("a") == 0) btnLanguagePT.setTextColor(colorAccent);
            btnLanguagePT.setOnClickListener(v -> {
                PCommon.SavePref(context, APP_PREF_KEY.BIBLE_NAME_DIALOG, "a");
                PCommon.SavePref(context, APP_PREF_KEY.ALT_LANGUAGE, "pt");
                builder.dismiss();
            });
            final Button btnLanguageFR2 = view.findViewById(R.id.btnLanguageFR2);
            if (installStatus < 6) btnLanguageFR2.setVisibility(View.INVISIBLE);
            if (bbName.compareToIgnoreCase("o") == 0) btnLanguageFR2.setTextColor(colorAccent);
            btnLanguageFR2.setOnClickListener(v -> {
                PCommon.SavePref(context, APP_PREF_KEY.BIBLE_NAME_DIALOG, "o");
                PCommon.SavePref(context, APP_PREF_KEY.ALT_LANGUAGE, "fr");
                builder.dismiss();
            });
            final Button btnLanguageEN2 = view.findViewById(R.id.btnLanguageEN2);
            if (installStatus < 7) btnLanguageEN2.setVisibility(View.INVISIBLE);
            if (bbName.compareToIgnoreCase("2") == 0) btnLanguageEN2.setTextColor(colorAccent);
            btnLanguageEN2.setOnClickListener(v -> {
                PCommon.SavePref(context, APP_PREF_KEY.BIBLE_NAME_DIALOG, "2");
                PCommon.SavePref(context, APP_PREF_KEY.ALT_LANGUAGE, "en");
                builder.dismiss();
            });
            final Button btnLanguageES2 = view.findViewById(R.id.btnLanguageES2);
            if (installStatus < 8) btnLanguageES2.setVisibility(View.INVISIBLE);
            if (bbName.compareToIgnoreCase("9") == 0) btnLanguageES2.setTextColor(colorAccent);
            btnLanguageES2.setOnClickListener(v -> {
                PCommon.SavePref(context, APP_PREF_KEY.BIBLE_NAME_DIALOG, "9");
                PCommon.SavePref(context, APP_PREF_KEY.ALT_LANGUAGE, "es");
                builder.dismiss();
            });
            final Button btnLanguageIT2 = view.findViewById(R.id.btnLanguageIT2);
            if (installStatus < 9) btnLanguageIT2.setVisibility(View.INVISIBLE);
            if (bbName.compareToIgnoreCase("1") == 0) btnLanguageIT2.setTextColor(colorAccent);
            btnLanguageIT2.setOnClickListener(v -> {
                PCommon.SavePref(context, APP_PREF_KEY.BIBLE_NAME_DIALOG, "1");
                PCommon.SavePref(context, APP_PREF_KEY.ALT_LANGUAGE, "it");
                builder.dismiss();
            });
            final Button btnLanguageIN = view.findViewById(R.id.btnLanguageIN);
            if (installStatus < 10) btnLanguageIN.setVisibility(View.INVISIBLE);
            if (bbName.compareToIgnoreCase("i") == 0) btnLanguageIN.setTextColor(colorAccent);
            btnLanguageIN.setOnClickListener(v -> {
                PCommon.SavePref(context, APP_PREF_KEY.BIBLE_NAME_DIALOG, "i");
                PCommon.SavePref(context, APP_PREF_KEY.ALT_LANGUAGE, "hi");
                builder.dismiss();
            });
            final Button btnLanguageAR = view.findViewById(R.id.btnLanguageAR);
            if (installStatus < 11) btnLanguageAR.setVisibility(View.INVISIBLE);
            if (bbName.compareToIgnoreCase("y") == 0) btnLanguageAR.setTextColor(colorAccent);
            btnLanguageAR.setOnClickListener(v -> {
                PCommon.SavePref(context, APP_PREF_KEY.BIBLE_NAME_DIALOG, "y");
                PCommon.SavePref(context, APP_PREF_KEY.ALT_LANGUAGE, "en");
                builder.dismiss();
            });
            final Button btnLanguageCN = view.findViewById(R.id.btnLanguageCN);
            if (installStatus < 12) btnLanguageCN.setVisibility(View.INVISIBLE);
            if (bbName.compareToIgnoreCase("c") == 0) btnLanguageCN.setTextColor(colorAccent);
            btnLanguageCN.setOnClickListener(v -> {
                PCommon.SavePref(context, APP_PREF_KEY.BIBLE_NAME_DIALOG, "c");
                PCommon.SavePref(context, APP_PREF_KEY.ALT_LANGUAGE, "en");
                builder.dismiss();
            });
            final Button btnLanguageDE = view.findViewById(R.id.btnLanguageDE);
            if (installStatus < 13) btnLanguageDE.setVisibility(View.INVISIBLE);
            if (bbName.compareToIgnoreCase("s") == 0) btnLanguageDE.setTextColor(colorAccent);
            btnLanguageDE.setOnClickListener(v -> {
                PCommon.SavePref(context, APP_PREF_KEY.BIBLE_NAME_DIALOG, "s");
                PCommon.SavePref(context, APP_PREF_KEY.ALT_LANGUAGE, "en");
                builder.dismiss();
            });
            final Button btnLanguageJP = view.findViewById(R.id.btnLanguageJP);
            if (installStatus < 14) btnLanguageJP.setVisibility(View.INVISIBLE);
            if (bbName.compareToIgnoreCase("j") == 0) btnLanguageJP.setTextColor(colorAccent);
            btnLanguageJP.setOnClickListener(v -> {
                PCommon.SavePref(context, APP_PREF_KEY.BIBLE_NAME_DIALOG, "j");
                PCommon.SavePref(context, APP_PREF_KEY.ALT_LANGUAGE, "en");
                builder.dismiss();
            });
            final Button btnLanguageRU = view.findViewById(R.id.btnLanguageRU);
            if (installStatus < 15) btnLanguageRU.setVisibility(View.INVISIBLE);
            if (bbName.compareToIgnoreCase("r") == 0) btnLanguageRU.setTextColor(colorAccent);
            btnLanguageRU.setOnClickListener(v -> {
                PCommon.SavePref(context, APP_PREF_KEY.BIBLE_NAME_DIALOG, "r");
                PCommon.SavePref(context, APP_PREF_KEY.ALT_LANGUAGE, "en");
                builder.dismiss();
            });
            final Button btnLanguageHEEL = view.findViewById(R.id.btnLanguageHEEL);
            if (installStatus < 16) btnLanguageHEEL.setVisibility(View.INVISIBLE);
            if (bbName.compareToIgnoreCase("w") == 0) btnLanguageHEEL.setTextColor(colorAccent);
            btnLanguageHEEL.setOnClickListener(v -> {
                PCommon.SavePref(context, APP_PREF_KEY.BIBLE_NAME_DIALOG, "w");
                PCommon.SavePref(context, APP_PREF_KEY.ALT_LANGUAGE, "en");
                builder.dismiss();
            });
            final Button btnLanguageTR = view.findViewById(R.id.btnLanguageTR);
            if (installStatus < 17) btnLanguageTR.setVisibility(View.INVISIBLE);
            if (bbName.compareToIgnoreCase("t") == 0) btnLanguageTR.setTextColor(colorAccent);
            btnLanguageTR.setOnClickListener(v -> {
                PCommon.SavePref(context, APP_PREF_KEY.BIBLE_NAME_DIALOG, "t");
                PCommon.SavePref(context, APP_PREF_KEY.ALT_LANGUAGE, "en");
                builder.dismiss();
            });
            final Button btnLanguageBD = view.findViewById(R.id.btnLanguageBD);
            if (installStatus < 18) btnLanguageBD.setVisibility(View.INVISIBLE);
            if (bbName.compareToIgnoreCase("b") == 0) btnLanguageBD.setTextColor(colorAccent);
            btnLanguageBD.setOnClickListener(v -> {
                PCommon.SavePref(context, APP_PREF_KEY.BIBLE_NAME_DIALOG, "b");
                PCommon.SavePref(context, APP_PREF_KEY.ALT_LANGUAGE, "en");
                builder.dismiss();
            });
            final Button btnLanguageSW = view.findViewById(R.id.btnLanguageSW);
            if (installStatus < 19) btnLanguageSW.setVisibility(View.INVISIBLE);
            if (bbName.compareToIgnoreCase("h") == 0) btnLanguageSW.setTextColor(colorAccent);
            btnLanguageSW.setOnClickListener(v -> {
                PCommon.SavePref(context, APP_PREF_KEY.BIBLE_NAME_DIALOG, "h");
                PCommon.SavePref(context, APP_PREF_KEY.ALT_LANGUAGE, "en");
                builder.dismiss();
            });
            final Button btnLanguageDE2 = view.findViewById(R.id.btnLanguageDE2);
            if (installStatus < 20) btnLanguageDE2.setVisibility(View.INVISIBLE);
            if (bbName.compareToIgnoreCase("e") == 0) btnLanguageDE2.setTextColor(colorAccent);
            btnLanguageDE2.setOnClickListener(v -> {
                PCommon.SavePref(context, APP_PREF_KEY.BIBLE_NAME_DIALOG, "e");
                PCommon.SavePref(context, APP_PREF_KEY.ALT_LANGUAGE, "en");
                builder.dismiss();
            });
            final Button btnLanguageRO = view.findViewById(R.id.btnLanguageRO);
            if (installStatus < 21) btnLanguageRO.setVisibility(View.INVISIBLE);
            if (bbName.compareToIgnoreCase("u") == 0) btnLanguageRO.setTextColor(colorAccent);
            btnLanguageRO.setOnClickListener(v -> {
                PCommon.SavePref(context, APP_PREF_KEY.BIBLE_NAME_DIALOG, "u");
                PCommon.SavePref(context, APP_PREF_KEY.ALT_LANGUAGE, "en");
                builder.dismiss();
            });
            final Button btnLanguagePL = view.findViewById(R.id.btnLanguagePL);
            if (installStatus < 22) btnLanguagePL.setVisibility(View.INVISIBLE);
            if (bbName.compareToIgnoreCase("z") == 0) btnLanguagePL.setTextColor(colorAccent);
            btnLanguagePL.setOnClickListener(v -> {
                PCommon.SavePref(context, APP_PREF_KEY.BIBLE_NAME_DIALOG, "z");
                PCommon.SavePref(context, APP_PREF_KEY.ALT_LANGUAGE, "en");
                builder.dismiss();
            });
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Select multiple bible language
     * Response in TRAD_BIBLE_NAME
     * @param context
     * @param msg
     * @param desc
     * @param isCancelable
     * @param forceShowAllButtons  Force to show all buttons
     */
    static void SelectBibleLanguageMulti(final AlertDialog builder, final Context context, final View view, final String msg, @SuppressWarnings({"UnusedParameters", "SameParameterValue"}) final String desc, @SuppressWarnings("SameParameterValue") final boolean isCancelable, @SuppressWarnings("SameParameterValue") final boolean forceShowAllButtons)
    {
        try
        {
            final String bbName = PCommon.GetPrefBibleName(context);

            builder.setCancelable(isCancelable);
            if (isCancelable) {
                builder.setOnCancelListener(dialogInterface -> PCommon.SavePref(context, APP_PREF_KEY.BIBLE_NAME_DIALOG, ""));
            }
            builder.setTitle(msg);

            final String bibleAppType = PCommon.GetPrefBibleAppType(context);
            if (bibleAppType.compareTo("1") == 0) {
                builder.hide();
                PCommon.SavePref(context, APP_PREF_KEY.BIBLE_NAME_DIALOG, bbName);
                PCommon.SavePref(context, APP_PREF_KEY.TRAD_BIBLE_NAME, bbName);
                final Handler hdl = new Handler();
                hdl.post(builder::dismiss);
                return;
            } else {
                builder.setView(view);
            }

            final int colorAccent = ContextCompat.getColor(context, R.color.colorAccent);
            final int installStatus = (forceShowAllButtons) ? PCommon.GetInstallStatusShouldBe() : Integer.parseInt(PCommon.GetPref(context, APP_PREF_KEY.INSTALL_STATUS, "1"));

            final TextView tvTrad = view.findViewById(R.id.tvTrad);
            final ToggleButton btnLanguageEN = view.findViewById(R.id.btnLanguageEN);
            if (installStatus < 1) btnLanguageEN.setEnabled(false);
            if (bbName.compareToIgnoreCase("k") == 0) btnLanguageEN.setTextColor(colorAccent);
            btnLanguageEN.setOnCheckedChangeListener((compoundButton, b) -> {
                final int op = compoundButton.isChecked() ? 1 : -1;
                final String languageStack = PCommon.ConcaT(context.getString(R.string.tvTrad), " ", PCommon.ManageTradBibleName(context, op, "k"));
                tvTrad.setText(languageStack);
            });
            final ToggleButton btnLanguageES = view.findViewById(R.id.btnLanguageES);
            if (installStatus < 2) btnLanguageES.setEnabled(false);
            if (bbName.compareToIgnoreCase("v") == 0) btnLanguageES.setTextColor(colorAccent);
            btnLanguageES.setOnCheckedChangeListener((compoundButton, b) -> {
                final int op = compoundButton.isChecked() ? 1 : -1;
                final String languageStack = PCommon.ConcaT(context.getString(R.string.tvTrad), " ", PCommon.ManageTradBibleName(context, op, "v"));
                tvTrad.setText(languageStack);
            });
            final ToggleButton btnLanguageFR = view.findViewById(R.id.btnLanguageFR);
            if (installStatus < 3) btnLanguageFR.setEnabled(false);
            if (bbName.compareToIgnoreCase("l") == 0) btnLanguageFR.setTextColor(colorAccent);
            btnLanguageFR.setOnCheckedChangeListener((compoundButton, b) -> {
                final int op = compoundButton.isChecked() ? 1 : -1;
                final String languageStack = PCommon.ConcaT(context.getString(R.string.tvTrad), " ", PCommon.ManageTradBibleName(context, op, "l"));
                tvTrad.setText(languageStack);
            });
            final ToggleButton btnLanguageIT = view.findViewById(R.id.btnLanguageIT);
            if (installStatus < 4) btnLanguageIT.setEnabled(false);
            if (bbName.compareToIgnoreCase("d") == 0) btnLanguageIT.setTextColor(colorAccent);
            btnLanguageIT.setOnCheckedChangeListener((compoundButton, b) -> {
                final int op = compoundButton.isChecked() ? 1 : -1;
                final String languageStack = PCommon.ConcaT(context.getString(R.string.tvTrad), " ", PCommon.ManageTradBibleName(context, op, "d"));
                tvTrad.setText(languageStack);
            });
            final ToggleButton btnLanguagePT = view.findViewById(R.id.btnLanguagePT);
            if (installStatus < 5) btnLanguagePT.setEnabled(false);
            if (bbName.compareToIgnoreCase("a") == 0) btnLanguagePT.setTextColor(colorAccent);
            btnLanguagePT.setOnCheckedChangeListener((compoundButton, b) -> {
                final int op = compoundButton.isChecked() ? 1 : -1;
                final String languageStack = PCommon.ConcaT(context.getString(R.string.tvTrad), " ", PCommon.ManageTradBibleName(context, op, "a"));
                tvTrad.setText(languageStack);
            });
            final ToggleButton btnLanguageFR2 = view.findViewById(R.id.btnLanguageFR2);
            if (installStatus < 6) btnLanguageFR2.setEnabled(false);
            if (bbName.compareToIgnoreCase("o") == 0) btnLanguageFR2.setTextColor(colorAccent);
            btnLanguageFR2.setOnCheckedChangeListener((compoundButton, b) -> {
                final int op = compoundButton.isChecked() ? 1 : -1;
                final String languageStack = PCommon.ConcaT(context.getString(R.string.tvTrad), " ", PCommon.ManageTradBibleName(context, op, "o"));
                tvTrad.setText(languageStack);
            });
            final ToggleButton btnLanguageEN2 = view.findViewById(R.id.btnLanguageEN2);
            if (installStatus < 7) btnLanguageEN2.setEnabled(false);
            if (bbName.compareToIgnoreCase("2") == 0) btnLanguageEN2.setTextColor(colorAccent);
            btnLanguageEN2.setOnCheckedChangeListener((compoundButton, b) -> {
                final int op = compoundButton.isChecked() ? 1 : -1;
                final String languageStack = PCommon.ConcaT(context.getString(R.string.tvTrad), " ", PCommon.ManageTradBibleName(context, op, "2"));
                tvTrad.setText(languageStack);
            });
            final ToggleButton btnLanguageES2 = view.findViewById(R.id.btnLanguageES2);
            if (installStatus < 8) btnLanguageES2.setEnabled(false);
            if (bbName.compareToIgnoreCase("9") == 0) btnLanguageES2.setTextColor(colorAccent);
            btnLanguageES2.setOnCheckedChangeListener((compoundButton, b) -> {
                final int op = compoundButton.isChecked() ? 1 : -1;
                final String languageStack = PCommon.ConcaT(context.getString(R.string.tvTrad), " ", PCommon.ManageTradBibleName(context, op, "9"));
                tvTrad.setText(languageStack);
            });
            final ToggleButton btnLanguageIT2 = view.findViewById(R.id.btnLanguageIT2);
            if (installStatus < 9) btnLanguageIT2.setEnabled(false);
            if (bbName.compareToIgnoreCase("1") == 0) btnLanguageIT2.setTextColor(colorAccent);
            btnLanguageIT2.setOnCheckedChangeListener((compoundButton, b) -> {
                final int op = compoundButton.isChecked() ? 1 : -1;
                final String languageStack = PCommon.ConcaT(context.getString(R.string.tvTrad), " ", PCommon.ManageTradBibleName(context, op, "1"));
                tvTrad.setText(languageStack);
            });
            final ToggleButton btnLanguageIN = view.findViewById(R.id.btnLanguageIN);
            if (installStatus < 10) btnLanguageIN.setEnabled(false);
            if (bbName.compareToIgnoreCase("i") == 0) btnLanguageIN.setTextColor(colorAccent);
            btnLanguageIN.setOnCheckedChangeListener((compoundButton, b) -> {
                final int op = compoundButton.isChecked() ? 1 : -1;
                final String languageStack = PCommon.ConcaT(context.getString(R.string.tvTrad), " ", PCommon.ManageTradBibleName(context, op, "i"));
                tvTrad.setText(languageStack);
            });
            final ToggleButton btnLanguageAR = view.findViewById(R.id.btnLanguageAR);
            if (installStatus < 11) btnLanguageAR.setEnabled(false);
            if (bbName.compareToIgnoreCase("y") == 0) btnLanguageAR.setTextColor(colorAccent);
            btnLanguageAR.setOnCheckedChangeListener((compoundButton, b) -> {
                final int op = compoundButton.isChecked() ? 1 : -1;
                final String languageStack = PCommon.ConcaT(context.getString(R.string.tvTrad), " ", PCommon.ManageTradBibleName(context, op, "y"));
                tvTrad.setText(languageStack);
            });
            final ToggleButton btnLanguageCN = view.findViewById(R.id.btnLanguageCN);
            if (installStatus < 12) btnLanguageCN.setEnabled(false);
            if (bbName.compareToIgnoreCase("c") == 0) btnLanguageCN.setTextColor(colorAccent);
            btnLanguageCN.setOnCheckedChangeListener((compoundButton, b) -> {
                final int op = compoundButton.isChecked() ? 1 : -1;
                final String languageStack = PCommon.ConcaT(context.getString(R.string.tvTrad), " ", PCommon.ManageTradBibleName(context, op, "c"));
                tvTrad.setText(languageStack);
            });
            final ToggleButton btnLanguageDE = view.findViewById(R.id.btnLanguageDE);
            if (installStatus < 13) btnLanguageDE.setEnabled(false);
            if (bbName.compareToIgnoreCase("s") == 0) btnLanguageDE.setTextColor(colorAccent);
            btnLanguageDE.setOnCheckedChangeListener((compoundButton, b) -> {
                final int op = compoundButton.isChecked() ? 1 : -1;
                final String languageStack = PCommon.ConcaT(context.getString(R.string.tvTrad), " ", PCommon.ManageTradBibleName(context, op, "s"));
                tvTrad.setText(languageStack);
            });
            final ToggleButton btnLanguageJP = view.findViewById(R.id.btnLanguageJP);
            if (installStatus < 14) btnLanguageJP.setEnabled(false);
            if (bbName.compareToIgnoreCase("j") == 0) btnLanguageJP.setTextColor(colorAccent);
            btnLanguageJP.setOnCheckedChangeListener((compoundButton, b) -> {
                final int op = compoundButton.isChecked() ? 1 : -1;
                final String languageStack = PCommon.ConcaT(context.getString(R.string.tvTrad), " ", PCommon.ManageTradBibleName(context, op, "j"));
                tvTrad.setText(languageStack);
            });
            final ToggleButton btnLanguageRU = view.findViewById(R.id.btnLanguageRU);
            if (installStatus < 15) btnLanguageRU.setEnabled(false);
            if (bbName.compareToIgnoreCase("r") == 0) btnLanguageRU.setTextColor(colorAccent);
            btnLanguageRU.setOnCheckedChangeListener((compoundButton, b) -> {
                final int op = compoundButton.isChecked() ? 1 : -1;
                final String languageStack = PCommon.ConcaT(context.getString(R.string.tvTrad), " ", PCommon.ManageTradBibleName(context, op, "r"));
                tvTrad.setText(languageStack);
            });
            final ToggleButton btnLanguageHEEL = view.findViewById(R.id.btnLanguageHEEL);
            if (installStatus < 16) btnLanguageHEEL.setEnabled(false);
            if (bbName.compareToIgnoreCase("w") == 0) btnLanguageHEEL.setTextColor(colorAccent);
            btnLanguageHEEL.setOnCheckedChangeListener((compoundButton, b) -> {
                final int op = compoundButton.isChecked() ? 1 : -1;
                final String languageStack = PCommon.ConcaT(context.getString(R.string.tvTrad), " ", PCommon.ManageTradBibleName(context, op, "w"));
                tvTrad.setText(languageStack);
            });
            final ToggleButton btnLanguageTR = view.findViewById(R.id.btnLanguageTR);
            if (installStatus < 17) btnLanguageTR.setEnabled(false);
            if (bbName.compareToIgnoreCase("t") == 0) btnLanguageTR.setTextColor(colorAccent);
            btnLanguageTR.setOnCheckedChangeListener((compoundButton, b) -> {
                final int op = compoundButton.isChecked() ? 1 : -1;
                final String languageStack = PCommon.ConcaT(context.getString(R.string.tvTrad), " ", PCommon.ManageTradBibleName(context, op, "t"));
                tvTrad.setText(languageStack);
            });
            final ToggleButton btnLanguageBD = view.findViewById(R.id.btnLanguageBD);
            if (installStatus < 18) btnLanguageBD.setEnabled(false);
            if (bbName.compareToIgnoreCase("b") == 0) btnLanguageBD.setTextColor(colorAccent);
            btnLanguageBD.setOnCheckedChangeListener((compoundButton, b) -> {
                final int op = compoundButton.isChecked() ? 1 : -1;
                final String languageStack = PCommon.ConcaT(context.getString(R.string.tvTrad), " ", PCommon.ManageTradBibleName(context, op, "b"));
                tvTrad.setText(languageStack);
            });
            final ToggleButton btnLanguageSW = view.findViewById(R.id.btnLanguageSW);
            if (installStatus < 19) btnLanguageSW.setEnabled(false);
            if (bbName.compareToIgnoreCase("h") == 0) btnLanguageSW.setTextColor(colorAccent);
            btnLanguageSW.setOnCheckedChangeListener((compoundButton, b) -> {
                final int op = compoundButton.isChecked() ? 1 : -1;
                final String languageStack = PCommon.ConcaT(context.getString(R.string.tvTrad), " ", PCommon.ManageTradBibleName(context, op, "h"));
                tvTrad.setText(languageStack);
            });
            final ToggleButton btnLanguageDE2 = view.findViewById(R.id.btnLanguageDE2);
            if (installStatus < 20) btnLanguageDE2.setEnabled(false);
            if (bbName.compareToIgnoreCase("e") == 0) btnLanguageDE2.setTextColor(colorAccent);
            btnLanguageDE2.setOnCheckedChangeListener((compoundButton, b) -> {
                final int op = compoundButton.isChecked() ? 1 : -1;
                final String languageStack = PCommon.ConcaT(context.getString(R.string.tvTrad), " ", PCommon.ManageTradBibleName(context, op, "e"));
                tvTrad.setText(languageStack);
            });
            final ToggleButton btnLanguageRO = view.findViewById(R.id.btnLanguageRO);
            if (installStatus < 21) btnLanguageRO.setEnabled(false);
            if (bbName.compareToIgnoreCase("u") == 0) btnLanguageRO.setTextColor(colorAccent);
            btnLanguageRO.setOnCheckedChangeListener((compoundButton, b) -> {
                final int op = compoundButton.isChecked() ? 1 : -1;
                final String languageStack = PCommon.ConcaT(context.getString(R.string.tvTrad), " ", PCommon.ManageTradBibleName(context, op, "u"));
                tvTrad.setText(languageStack);
            });
            final ToggleButton btnLanguagePL = view.findViewById(R.id.btnLanguagePL);
            if (installStatus < 22) btnLanguagePL.setEnabled(false);
            if (bbName.compareToIgnoreCase("z") == 0) btnLanguagePL.setTextColor(colorAccent);
            btnLanguagePL.setOnCheckedChangeListener((compoundButton, b) -> {
                final int op = compoundButton.isChecked() ? 1 : -1;
                final String languageStack = PCommon.ConcaT(context.getString(R.string.tvTrad), " ", PCommon.ManageTradBibleName(context, op, "z"));
                tvTrad.setText(languageStack);
            });
            final Button btnLanguageClear = view.findViewById(R.id.btnLanguageClear);
            if (installStatus <= 0) btnLanguageClear.setEnabled(false);
            btnLanguageClear.setOnClickListener(v -> {
                //clear all & select default
                final String currentTrad = "";
                PCommon.SavePref(context, APP_PREF_KEY.TRAD_BIBLE_NAME, currentTrad);
                btnLanguageEN.setChecked(false);
                btnLanguageES.setChecked(false);
                btnLanguageFR.setChecked(false);
                btnLanguageIT.setChecked(false);
                btnLanguagePT.setChecked(false);
                btnLanguageFR2.setChecked(false);
                btnLanguageEN2.setChecked(false);
                btnLanguageES2.setChecked(false);
                btnLanguageIT2.setChecked(false);
                btnLanguageIN.setChecked(false);
                btnLanguageAR.setChecked(false);
                btnLanguageCN.setChecked(false);
                btnLanguageDE.setChecked(false);
                btnLanguageJP.setChecked(false);
                btnLanguageRU.setChecked(false);
                btnLanguageHEEL.setChecked(false);
                btnLanguageTR.setChecked(false);
                btnLanguageBD.setChecked(false);
                btnLanguageSW.setChecked(false);
                btnLanguageDE2.setChecked(false);
                btnLanguageRO.setChecked(false);
                btnLanguagePL.setChecked(false);
                final String languageStack = PCommon.ConcaT(context.getString(R.string.tvTrad), " ", PCommon.ManageTradBibleName(context, 0, ""));
                tvTrad.setText(languageStack);
                PCommon.SavePref(context, APP_PREF_KEY.BIBLE_NAME_DIALOG, "");
            });
            final Button btnLanguageContinue = view.findViewById(R.id.btnSearchContinue);
            if (installStatus <= 0) btnLanguageContinue.setEnabled(false);
            btnLanguageContinue.setOnClickListener(v -> {
                //all selected toggle
                final String currentTrad = PCommon.GetPrefTradBibleName(context, false);
                if (currentTrad.isEmpty()) return;
                PCommon.SavePref(context, APP_PREF_KEY.BIBLE_NAME_DIALOG, currentTrad.substring(0, 1));
                builder.dismiss();
            });

            final String tradInit = PCommon.GetPrefTradBibleName(context, false);
            if (tradInit.contains("k")) btnLanguageEN.setChecked(true);
            if (tradInit.contains("v")) btnLanguageES.setChecked(true);
            if (tradInit.contains("l")) btnLanguageFR.setChecked(true);
            if (tradInit.contains("d")) btnLanguageIT.setChecked(true);
            if (tradInit.contains("a")) btnLanguagePT.setChecked(true);
            if (tradInit.contains("o")) btnLanguageFR2.setChecked(true);
            if (tradInit.contains("2")) btnLanguageEN2.setChecked(true);
            if (tradInit.contains("9")) btnLanguageES2.setChecked(true);
            if (tradInit.contains("1")) btnLanguageIT2.setChecked(true);
            if (tradInit.contains("i")) btnLanguageIN.setChecked(true);
            if (tradInit.contains("y")) btnLanguageAR.setChecked(true);
            if (tradInit.contains("c")) btnLanguageCN.setChecked(true);
            if (tradInit.contains("s")) btnLanguageDE.setChecked(true);
            if (tradInit.contains("j")) btnLanguageJP.setChecked(true);
            if (tradInit.contains("r")) btnLanguageRU.setChecked(true);
            if (tradInit.contains("w")) btnLanguageHEEL.setChecked(true);
            if (tradInit.contains("t")) btnLanguageTR.setChecked(true);
            if (tradInit.contains("b")) btnLanguageBD.setChecked(true);
            if (tradInit.contains("h")) btnLanguageSW.setChecked(true);
            if (tradInit.contains("e")) btnLanguageDE2.setChecked(true);
            if (tradInit.contains("u")) btnLanguageRO.setChecked(true);
            if (tradInit.contains("z")) btnLanguagePL.setChecked(true);
            final String languageStack = PCommon.ConcaT(context.getString(R.string.tvTrad), " ", PCommon.ManageTradBibleName(context, 0, ""));
            tvTrad.setText(languageStack);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Select item
     * Response in BOOK_CHAPTER_DIALOG
     * @param context
     * @param title
     * @param fieldTitleId
     * @param desc
     * @param isCancelable
     * @param itemMax
     * @param shouldAddAllitem  Should add ALL item?
     */
    static void SelectItem(final AlertDialog builder, final Context context, final View view, final String title, @SuppressWarnings("SameParameterValue") final int fieldTitleId, @SuppressWarnings({"UnusedParameters", "SameParameterValue"}) final String desc, @SuppressWarnings("SameParameterValue") final boolean isCancelable, final int itemMax, final boolean shouldAddAllitem)
    {
        try
        {
            final Typeface typeface = PCommon.GetTypeface(context);
            final int fontSize = PCommon.GetFontSize(context);

            final TextView tvFieldTitle = view.findViewById(R.id.tvTitle);
            tvFieldTitle.setText(fieldTitleId);

            builder.setCancelable(isCancelable);
            if (isCancelable) {
                builder.setOnCancelListener(dialogInterface -> PCommon.SavePref(context, APP_PREF_KEY.BIBLE_NAME_DIALOG, ""));
            }
            builder.setTitle(title);
            builder.setView(view);

            final LinearLayout llItem = view.findViewById(R.id.llFile);
            llItem.setTag(0);
            final int itemMin = shouldAddAllitem ? 0 : 1;
            for (int i = itemMin; i <= itemMax; i++)
            {
                final TextView tvItem = new TextView(context);
                tvItem.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                tvItem.setPadding(10, 15, 10, 15);
                tvItem.setGravity(Gravity.CENTER);
                tvItem.setText( i != 0 ? String.valueOf(i) : context.getString(R.string.itemAll));
                tvItem.setTag( i );
                tvItem.setOnClickListener(v -> {
                    final int cnumber = (int) v.getTag();
                    PCommon.SavePref(context, APP_PREF_KEY.BOOK_CHAPTER_DIALOG, String.valueOf(cnumber));
                    builder.dismiss();
                });
                tvItem.setFocusable(true);
                tvItem.setBackground(PCommon.GetDrawable(context, R.drawable.focus_text));

                //Font
                if (typeface != null) { tvItem.setTypeface(typeface); }
                tvItem.setTextSize(fontSize);

                llItem.addView(tvItem);
            }
            llItem.requestFocus();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Show articles
     * @param context   Context
     */
    static void ShowArticles(final Context context)
    {
        PCommon.ShowArticles(context, _isShowMyArt, false);
    }

    /***
     * Show articles
     * @param context   Context
     * @param isMyArticleType   Is MyArt type?
     * @param isForSelection    Is for selection? (False=to open it)
     */
    static void ShowArticles(final Context context, final boolean isMyArticleType, final boolean isForSelection)
    {
        try
        {
            CheckLocalInstance(context);

            _isShowMyArt = isMyArticleType;

            final AlertDialog builder = new AlertDialog.Builder(context).create();                     //R.style.DialogStyleKaki
            final ScrollView sv = new ScrollView(context);
            sv.setLayoutParams(PCommon._layoutParamsMatchAndWrap);

            final LinearLayout llArt = new LinearLayout(context);
            llArt.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            llArt.setOrientation(LinearLayout.VERTICAL);
            llArt.setPadding(30, 20, 30,20);

            final Typeface typeface = PCommon.GetTypeface(context);
            final int fontSize = PCommon.GetFontSize(context);

            if (!isForSelection)
            {
                final Button btnSwitchArt = new Button(context);
                btnSwitchArt.setLayoutParams(PCommon._layoutParamsWrap);
                btnSwitchArt.setText(_isShowMyArt ? R.string.switchToArt : R.string.switchToMyArt);
                btnSwitchArt.setContentDescription(context.getString(_isShowMyArt ? R.string.switchToArt : R.string.switchToMyArt));
                btnSwitchArt.setOnClickListener(v -> {
                    _isShowMyArt = !_isShowMyArt;

                    final Handler handler = new Handler();
                    handler.postDelayed(() -> {
                        builder.dismiss();
                        ShowArticles(v.getContext());
                    }, 0);
                });
                btnSwitchArt.setFocusable(true);
                btnSwitchArt.setBackground(PCommon.GetDrawable(context, R.drawable.focus_button));
                llArt.addView(btnSwitchArt);
            }

            final TextView tvSep2 = new TextView(context);
            tvSep2.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            tvSep2.setText(R.string.mnuEmptyWithoutText);
            llArt.addView(tvSep2);

            final Button btnCreateArt = new Button(context);
            btnCreateArt.setLayoutParams(PCommon._layoutParamsWrap);
            btnCreateArt.setVisibility(_isShowMyArt ? View.VISIBLE : View.GONE);
            btnCreateArt.setText(R.string.btnCreate);
            btnCreateArt.setOnClickListener(v -> EditArticleDialog(builder, R.string.btnCreate, -1, ARTICLE_ACTION.CREATE_ARTICLE, isForSelection));
            btnCreateArt.setFocusable(true);
            btnCreateArt.setBackground(PCommon.GetDrawable(context, R.drawable.focus_button));
            llArt.addView(btnCreateArt);

            int resId;
            int nr = 0;
            TextView tvArt;
            TextView tvArtStatus;
            String text;

            final String[] arrArt = (_isShowMyArt) ? _s.GetListMyArticlesId() : context.getResources().getStringArray(R.array.ART_ARRAY);
            for (final String artRef : arrArt)
            {
                if (!_isShowMyArt)
                {
                    if (nr == 6 || nr == 15)
                    {
                        TextView tvSep = new TextView(context);
                        tvSep.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                        tvSep.setText(R.string.mnuEmptyWithoutText);
                        llArt.addView(tvSep);

                        final View vwSep = new View(context);
                        vwSep.setPadding(20, 0, 20, 0);
                        vwSep.setLayoutParams(new AppBarLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2));
                        vwSep.setBackgroundColor(tvSep.getCurrentTextColor());
                        llArt.addView(vwSep);

                        tvSep = new TextView(context);
                        tvSep.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                        tvSep.setText(R.string.mnuEmptyWithoutText);
                        llArt.addView(tvSep);
                    }

                    resId = PCommon.GetResId(context, artRef);
                    text = PCommon.ConcaT(context.getString(R.string.bulletDefault), Html.fromHtml("&nbsp;"), context.getString(resId));
                }
                else
                {
                    resId = Integer.parseInt(artRef);
                    text = PCommon.ConcaT(context.getString(R.string.bulletDefault), Html.fromHtml("&nbsp;"), _s.GetMyArticleName(resId));
                }

                tvArt = new TextView(context);
                tvArt.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                tvArt.setPadding(10, nr == 0 ? 30 : 20, 10, _isShowMyArt ? 0 : 20);
                tvArt.setMinHeight(48);
                tvArt.setText(text);
                tvArt.setTag( artRef );
                if (!_isShowMyArt && (nr == 2 || nr == 3 || nr == 4 | nr == 6)) tvArt.setTextColor(context.getResources().getColor(R.color.colorAccent));
                tvArt.setOnClickListener(v -> {
                    try
                    {
                        final String fullQuery = (String) v.getTag();
                        if (PCommon._isDebug) System.out.println(fullQuery);
                        if (isForSelection)
                        {
                            final int artId = Integer.parseInt(fullQuery.replace(v.getContext().getString(R.string.tabMyArtPrefix), ""));
                            if (artId < 0) return;
                            PCommon.SavePrefInt(v.getContext(), APP_PREF_KEY.EDIT_STATUS, 1);
                            PCommon.SavePrefInt(v.getContext(), APP_PREF_KEY.EDIT_ART_ID, artId);
                            PCommon.SavePref(v.getContext(), APP_PREF_KEY.EDIT_SELECTION, "");
                        }
                        else
                        {
                            if (isMyArticleType)
                            {
                                PCommon.ShowMyArticleMenu(builder, fullQuery);
                            }
                            else
                            {
                                ShowArticle(context, fullQuery);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        if (PCommon._isDebug) PCommon.LogR(v.getContext(), ex);
                    }
                    finally
                    {
                        builder.dismiss();
                    }
                });
                tvArt.setFocusable(true);
                tvArt.setBackground(PCommon.GetDrawable(context, R.drawable.focus_text));

                //Font
                if (typeface != null) tvArt.setTypeface(typeface);
                tvArt.setTextSize(fontSize);
                llArt.addView(tvArt);

                if (_isShowMyArt)
                {
                    text = PCommon.ConcaT("<blockquote>&nbsp;(", context.getString(R.string.tabMyArtPrefix), artRef,")</blockquote>");

                    final HorizontalScrollView hsv = new HorizontalScrollView(context);
                    hsv.setLayoutParams(PCommon._layoutParamsMatchAndWrap);

                    tvArtStatus = new TextView(context);
                    tvArtStatus.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                    tvArtStatus.setPadding(50, 0, 10, 0);
                    tvArtStatus.setMinHeight(48);
                    tvArtStatus.setText(Html.fromHtml(text));
                    tvArtStatus.setTextSize(fontSize);
                    if (typeface != null) tvArtStatus.setTypeface(typeface);

                    hsv.addView(tvArtStatus);
                    llArt.addView(hsv);
                }

                nr++;
            }
            llArt.requestFocus();
            sv.addView(llArt);

            builder.setTitle(R.string.mnuArticles);
            builder.setCancelable(true);
            builder.setView(sv);
            builder.show();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Show article
     * @param context   Context
     * @param artName   Article name
     */
    static void ShowArticle(final Context context, final String artName)
    {
        try
        {
            CheckLocalInstance(context);

            final int artNameTabId = _s.GetArticleTabId(artName);
            if (artNameTabId >= 0)
            {
                MainActivity.Tab.SelectTabByTabId(artNameTabId);
                return;
            }
            final String bbName = PCommon.GetPrefBibleName(context);
            MainActivity.Tab.AddTab(context, "A", bbName, artName, true);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Show MYART menu
     * @param dlgMyArticles  Dialog
     * @param artName     Article Name
     */
    private static void ShowMyArticleMenu(final AlertDialog dlgMyArticles, final String artName)
    {
        final Context context = dlgMyArticles.getContext();

        try
        {
            final int artId = Integer.parseInt(artName.replace(context.getString(R.string.tabMyArtPrefix),""));
            if (artId < 0) return;

            CheckLocalInstance(context);

            final LayoutInflater inflater = dlgMyArticles.getLayoutInflater();
            final View view = inflater.inflate(R.layout.fragment_myart_menu, dlgMyArticles.findViewById(R.id.svMyArtMenu));

            final String myartTitle = PCommon.ConcaT(context.getString(R.string.tabMyArtPrefix), artId);

            final AlertDialog builder = new AlertDialog.Builder(context).create();
            builder.setCancelable(true);
            builder.setTitle(myartTitle);
            builder.setView(view);

            final Button btnOpen = view.findViewById(R.id.btnOpen);
            btnOpen.setOnClickListener(v -> {
                final Handler handler = new Handler();
                handler.postDelayed(() -> {
                    builder.dismiss();
                    dlgMyArticles.dismiss();
                    ShowArticle(context, artName);
                }, 0);
            });
            final Button btnRename = view.findViewById(R.id.btnRename);
            btnRename.setOnClickListener(v -> {
                final Handler handler = new Handler();
                handler.postDelayed(() -> {
                    EditArticleDialog(builder, R.string.mnuRename, artId, ARTICLE_ACTION.RENAME_ARTICLE, false);
                    builder.dismiss();
                    dlgMyArticles.dismiss();
                }, 0);
            });
            final Button btnDelete = view.findViewById(R.id.btnDelete);
            btnDelete.setOnClickListener(v -> {
                final Handler handler = new Handler();
                handler.postDelayed(() -> {
                    EditArticleDialog(builder, R.string.mnuDelete, artId, ARTICLE_ACTION.DELETE_ARTICLE, false);
                    builder.dismiss();
                    dlgMyArticles.dismiss();
                }, 0);
            });
            final Button btnCopySourceToClipboard = view.findViewById(R.id.btnCopySourceToClipboard);
            btnCopySourceToClipboard.setOnClickListener(v -> {
                final Handler handler = new Handler();
                handler.postDelayed(() -> {
                    final String text = _s.GetMyArticleSource(artId);
                    PCommon.CopyTextToClipboard(context, "", text, true);
                    builder.dismiss();
                    dlgMyArticles.dismiss();
                }, 0);
            });
            final Button btnEmailSourceToDeveloper = view.findViewById(R.id.btnEmailSourceToDeveloper);
            btnEmailSourceToDeveloper.setOnClickListener(v -> {
                final Handler handler = new Handler();
                handler.postDelayed(() -> {
                    final String app =  PCommon.ConcaT("Bible Multi ", context.getString(R.string.appName));
                    final String devEmail = context.getString(R.string.devEmail).replaceAll("r", "");
                    final String text = _s.GetMyArticleSource(artId);
                    PCommon.SendEmail(context,
                            new String[]{ devEmail },
                            app,
                            text);
                    builder.dismiss();
                    dlgMyArticles.dismiss();
                }, 0);
            });
            builder.show();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Edit article dialog (globally for action)
     * @param dlg       Dialog of myarticles
     * @param titleId   Main title
     * @param artId     Article Id (-1 if not used)
     * @param action    Action
     * @param isForSelection    CREATE_ARTICLE can be called in 2 ways: during the selection of an article or to open an article
     */
    private static void EditArticleDialog(final AlertDialog dlg, final int titleId, final int artId, final ARTICLE_ACTION action, final boolean isForSelection)
    {
        final Context context = dlg.getContext();

        try
        {
            final Typeface typeface = PCommon.GetTypeface(context);
            final int fontSize = PCommon.GetFontSize(context);

            final LayoutInflater inflater = dlg.getLayoutInflater();
            final View view = inflater.inflate(R.layout.fragment_edit_dialog, dlg.findViewById(R.id.svEdition));
            final TextView tvTitle = view.findViewById(R.id.tvTitle);
            final EditText etEdition = view.findViewById(R.id.etLexSearch);
            final AlertDialog builder = new AlertDialog.Builder(context).create();
            builder.setCancelable(true);
            builder.setTitle(titleId);
            builder.setView(view);

            tvTitle.setText(titleId);

            //EditText
            final String artName = artId > 0 ? _s.GetMyArticleName(artId) : "";
            etEdition.setText(artName);
            etEdition.setTextSize(fontSize);
            if (typeface != null) etEdition.setTypeface(typeface);
            if (action == ARTICLE_ACTION.CREATE_ARTICLE || action == ARTICLE_ACTION.RENAME_ARTICLE) etEdition.setSingleLine(true);
            if (action == ARTICLE_ACTION.DELETE_ARTICLE) {
                etEdition.setEnabled(false);
                etEdition.setTextColor(ContextCompat.getColor(context, R.color.colorDisable));
            }

            //BtnClear
            final Button btnClear = view.findViewById(R.id.btnEditionClear);
            btnClear.setOnClickListener(v -> etEdition.setText(""));
            if (action == ARTICLE_ACTION.DELETE_ARTICLE) btnClear.setVisibility(View.GONE);

            //BtnContinue
            final Button btnContinue = view.findViewById(R.id.btnEditionContinue);
            btnContinue.setOnClickListener(v -> {
                final Handler handler = new Handler();
                handler.postDelayed(() -> {
                    CheckLocalInstance(context);

                    final String title = etEdition.getText().toString().replaceAll("\n", "").trim();
                    switch (action)
                    {
                        case RENAME_ARTICLE:
                        {
                            if (title.isEmpty()) return;

                            _s.UpdateMyArticleTitle(artId, title);
                            break;
                        }
                        case DELETE_ARTICLE:
                        {
                            _s.DeleteMyArticle(artId);

                            final int currentEditStatus = PCommon.GetEditStatus(context);
                            if (currentEditStatus == 0) break;
                            final int currentEditArtId = PCommon.GetEditArticleId(context);
                            if (currentEditArtId == artId)
                            {
                                //Stop editing
                                PCommon.SavePrefInt(context, APP_PREF_KEY.EDIT_STATUS, 0);
                                PCommon.SavePrefInt(context, APP_PREF_KEY.EDIT_ART_ID, -1);
                            }
                            break;
                        }
                        case CREATE_ARTICLE:
                        {
                            if (title.isEmpty()) return;

                            final ArtDescBO ad = new ArtDescBO();
                            ad.artId = _s.GetNewMyArticleId();
                            ad.artUpdatedDt = PCommon.NowYYYYMMDD();
                            ad.artTitle = title;
                            ad.artSrc = "...";

                            _s.AddMyArticle(ad);
                            break;
                        }
                    }

                    builder.dismiss();
                    dlg.dismiss();
                    ShowArticles(context, true, isForSelection);
                }, 0);
            });

            builder.show();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Show simple dialog
     * @param activity
     * @param titleId
     * @param isCancelable
     * @param msgIds
     */
    static void ShowDialog(final Activity activity, final int titleId, final boolean isCancelable,View.OnClickListener onClickListener, final int... msgIds)
    {
        try
        {
            final Context context = activity.getApplicationContext();
            final Typeface typeface = PCommon.GetTypeface(context);
            final int fontSize = PCommon.GetFontSize(context);
            final int textColor = PCommon.GetPrefThemeName(context).compareTo("LIGHT") == 0 ? Color.BLACK : Color.WHITE; //TODO: harcoded, need to find from attribute

            final LayoutInflater inflater = activity.getLayoutInflater();
            final View view = inflater.inflate(R.layout.fragment_show_dialog, activity.findViewById(R.id.llDialog));
            final LinearLayout llMsg = view.findViewById(R.id.llMsg);
            final AlertDialog builder = new AlertDialog.Builder(activity).create();
            builder.setCancelable(isCancelable);
            builder.setTitle(titleId);
            builder.setView(view);

            for (int msgId : msgIds)
            {
                final TextView tvMsg = new TextView(context);
                tvMsg.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                tvMsg.setText(msgId);
                if (typeface != null) { tvMsg.setTypeface(typeface); }
                tvMsg.setTextSize(fontSize);
                tvMsg.setFocusable(true);
                tvMsg.setBackground(PCommon.GetDrawable(context, R.drawable.focus_text));
                tvMsg.setTextColor(textColor);
                //tvMsg.setTextColor(Color.GRAY);    //Let this, was an issue on many Android versions
                //tvMsg.setTextColor(ContextCompat.getColor(context, R.color.colorDisable));
                llMsg.addView(tvMsg);
            }

            final Button btnClose = view.findViewById(R.id.btnClose);
            btnClose.setVisibility(isCancelable ? View.GONE : View.VISIBLE);
            // If onClickListener is null, use a default one that dismisses the dialog
            // Combining the custom listener with dismiss action
            btnClose.setOnClickListener(v -> {
                builder.dismiss();  // Dismiss the dialog after running the OnClickListener
                if (onClickListener != null) {
                    onClickListener.onClick(v);  // Execute the provided OnClickListener
                }
            });
            builder.show();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(activity.getApplicationContext(), ex);
        }
    }

    /***
     * Creates and shows a "ProgressDialog like"
     * @param currentActivity
     * @param titleId
     * @param msgId
     * @return "ProgressDialog" to be dismissed...
     */
    static AlertDialog ShowProgressDialog(final FragmentActivity currentActivity, final int titleId, final int msgId)
    {
        final Context context = currentActivity.getApplicationContext();
        AlertDialog pgr = null;

        try
        {
            final Typeface typeface = PCommon.GetTypeface(context);
            final int fontSize = PCommon.GetFontSize(context);

            final LayoutInflater inflater = currentActivity.getLayoutInflater();
            final View view = inflater.inflate(R.layout.fragment_show_progress_dialog, currentActivity.findViewById(R.id.llDialog));
            final TextView tvMsg = view.findViewById(R.id.tvMsg);

            tvMsg.setText(context.getString(msgId));
            if (typeface != null) { tvMsg.setTypeface(typeface); }
            tvMsg.setTextSize(fontSize);

            pgr = new AlertDialog.Builder(currentActivity).create();
            pgr.setCancelable(false);
            pgr.setTitle(titleId);
            pgr.setView(view);
            pgr.show();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }

        return pgr;
    }

    /***
     * Show custom menu
     * @param builder       Builder menu
     * @param mnuTitleId    Menu title
     * @param lstMnuItem    List of menu items
     * @param lstId         List of IDs
     * return Set MENU_DIALOG
     */
    static void ShowMenu(final AlertDialog builder, final int mnuTitleId, final ArrayList<String> lstMnuItem, final ArrayList<Integer> lstId)
    {
        final Context context = builder.getContext();

        try
        {
            final String mnuTitle = builder.getContext().getString(mnuTitleId);

            ShowMenu(builder, mnuTitle, lstMnuItem, lstId);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Show custom menu
     * @param builder       Builder menu
     * @param mnuTitle      Menu title
     * @param lstMnuItem    List of menu items
     * @param lstId         List of IDs
     * return Set MENU_DIALOG
     */
    static void ShowMenu(final AlertDialog builder, final String mnuTitle, final ArrayList<String> lstMnuItem, final ArrayList<Integer> lstId)
    {
        final Context context = builder.getContext();

        try
        {
            final Typeface typeface = PCommon.GetTypeface(context);
            final int fontSize = PCommon.GetFontSize(context) + 2;

            final ScrollView sv = new ScrollView(context);
            sv.setLayoutParams(PCommon._layoutParamsMatchAndWrap);

            //final LinearLayout llMnu = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.context_menu, (ViewGroup) getActivity().findViewById(R.id.llMnu));
            //TextView tvMnuItemGen = llMnu.findViewById(R.id.tv_menu_item);

            final LinearLayout llMnu = new LinearLayout(context);
            llMnu.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            llMnu.setOrientation(LinearLayout.VERTICAL);
            llMnu.setPadding(20, 20, 20, 20);

            int index = 0;
            for (final String itemRef : lstMnuItem)
            {
                final TextView tvMnuItem = new TextView(context);
                tvMnuItem.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                tvMnuItem.setPadding(20, 20, 20, 20);
                tvMnuItem.setMinHeight(48);
                tvMnuItem.setText( itemRef );
                tvMnuItem.setTag( lstId == null ? itemRef :  lstId.get(index));
                tvMnuItem.setOnClickListener(v -> {
                    PCommon.SavePref(v.getContext(), IProject.APP_PREF_KEY.MENU_DIALOG, v.getTag().toString());
                    builder.dismiss();
                });

                //TODO FAB: slow GetDrawable
                tvMnuItem.setFocusable(true);
                tvMnuItem.setBackground(PCommon.GetDrawable(context, R.drawable.focus_text));

                //Font
                if (typeface != null) tvMnuItem.setTypeface(typeface);
                tvMnuItem.setTextSize(fontSize);

                llMnu.addView(tvMnuItem);

                index += 1;
            }
            sv.addView(llMnu);

            builder.setOnCancelListener(dialogInterface -> PCommon.SavePref(context, IProject.APP_PREF_KEY.MENU_DIALOG, ""));
            builder.setTitle(mnuTitle);
            builder.setCancelable(true);
            builder.setView(sv);
            builder.show();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Select file from list of files and assign it to resIv if used
     * If resIv is null it will show only db files else it will show picture files
     * @param fullPathDir   Start path dir
     * @param searchText    Search filter
     * @param resIv         Result Imageview to set if used
     * @return Sets resIv if used
     */
    static void SelectFile(final Activity currentActivity, String fullPathDir, final String searchText, final ImageView resIv)
    {
        final Context context = currentActivity.getApplicationContext();

        try
        {
            final Typeface typeface = PCommon.GetTypeface(context);
            final int fontSize = PCommon.GetFontSize(context);
            final int textColor = PCommon.GetPrefThemeName(context).compareTo("LIGHT") == 0 ? Color.BLACK : Color.WHITE; //TODO: harcoded, need to find from attribute

            final LayoutInflater inflater = currentActivity.getLayoutInflater();
            final View view = inflater.inflate(R.layout.fragment_show_file_dialog, currentActivity.findViewById(R.id.llDialog));
            final EditText etSearchText = view.findViewById(R.id.etSearchText);
            if (!searchText.isEmpty()) etSearchText.setText(searchText);
            final LinearLayout llDialog = view.findViewById(R.id.llDialog);
            final AlertDialog builder = new AlertDialog.Builder(currentActivity).create();
            builder.setCancelable(true);
            builder.setTitle(context.getString(R.string.btnSelect));
            builder.setView(view);

            String finalFullPathDir = fullPathDir;
            etSearchText.setOnEditorActionListener((v, actionId, event) ->
            {
                final int size = v.getText().toString().length();
                if (size >= 1 && size < 3)
                {
                    PCommon.ShowToast(v.getContext(), R.string.toastEmpty3, Toast.LENGTH_SHORT);
                    return false;
                }

                builder.dismiss();
                SelectFile(currentActivity, finalFullPathDir, v.getText().toString(), resIv);
                return false;
            });

            final File fileTest = new File(fullPathDir);
            if (!fileTest.exists()) {
                fullPathDir = "/";
            }

            final BROWSE_FILE_TYPE browseFileType = (resIv == null) ? BROWSE_FILE_TYPE.DATABASE : BROWSE_FILE_TYPE.PICTURE;
            final ArrayList<String> lstFile = GetLstFiles(context, fullPathDir, browseFileType, searchText.toUpperCase());
            if (lstFile == null || lstFile.isEmpty()) {
                PCommon.ShowToast(context, R.string.toastEmpty, Toast.LENGTH_SHORT);
                return;
            }

            TextView tvFile;
            for (String item : lstFile)
            {
                tvFile = new TextView(context);
                tvFile.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                tvFile.setPadding(20, 20, 20, 20);
                tvFile.setMinHeight(48);
                tvFile.setText(PCommon.ConcaT("> ", item));
                if (typeface != null) { tvFile.setTypeface(typeface); }
                tvFile.setTextSize(fontSize);
                tvFile.setTag(R.id.tv1, item);
                tvFile.setTag(R.id.tv2, fullPathDir);
                tvFile.setFocusable(true);
                tvFile.setBackground(PCommon.GetDrawable(context, R.drawable.focus_text));
                tvFile.setTextColor(textColor);
                tvFile.setOnClickListener(v -> {
                    try
                    {
                        builder.dismiss();

                        final String tagFilename = (String) v.getTag(R.id.tv1);
                        final String tagFullPathDir = (String) v.getTag(R.id.tv2);
                        if (tagFilename == null || tagFilename.isEmpty()) return;

                        String newFullPathDir = PCommon.ConcaT(tagFullPathDir, "/", tagFilename);
                        if (tagFilename.equalsIgnoreCase("..")) {
                            newFullPathDir = newFullPathDir.replaceFirst("/", "").replaceFirst("/\\.\\.", "");
                            final String[] arrDir = newFullPathDir.split("/");
                            if ((arrDir.length - 2) >= 0) {
                                String redoPathDir = "";
                                for (int i = 0; i <= arrDir.length - 2; i++) {
                                    redoPathDir = PCommon.ConcaT(redoPathDir, "/", arrDir[i]);
                                }
                                newFullPathDir = redoPathDir;
                            }
                        }

                        final File fileSelected = new File(newFullPathDir);
                        if (fileSelected.exists() && (!fileSelected.isDirectory()) && fileSelected.canRead()
                                && (tagFilename.toUpperCase().endsWith(".JPG") || tagFilename.toUpperCase().endsWith(".JPEG") || tagFilename.toUpperCase().endsWith(".PNG") || tagFilename.toUpperCase().endsWith(".DB")) )
                        {
                            if (browseFileType == BROWSE_FILE_TYPE.PICTURE) {
                                resIv.setImageURI(Uri.fromFile(fileSelected));
                            } else {
                                PCommon.ImportDb(currentActivity, context, fileSelected.getAbsolutePath());
                            }
                            return;
                        }

                        SelectFile(currentActivity, newFullPathDir, searchText, resIv);
                    }
                    catch(Exception ex)
                    {
                        if (PCommon._isDebug) PCommon.LogR(context, ex);
                    }
                });

                llDialog.addView(tvFile);
            }

            llDialog.requestFocus();
            builder.show();
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Get list of files and dirs
     * @param context
     * @param fullPathDir
     * @param browseFileType
     * @param searchText
     * @return May returns empty list
     */
    private static ArrayList<String> GetLstFiles(final Context context, final String fullPathDir, final BROWSE_FILE_TYPE browseFileType, final String searchText)
    {
        ArrayList<String> lstDir = new ArrayList<>();
        ArrayList<String> lstFile = new ArrayList<>();
        ArrayList<String> lstMerge = new ArrayList<>();

        try
        {
            final File initDir = new File(fullPathDir);

            try
            {
                if (initDir.getParentFile().listFiles() != null) lstDir.add("..");
            }
            catch (Exception ignored)
            { }

            final File[] arrFiles = initDir.listFiles(fileItem -> {
                if (fileItem.isHidden() || (!fileItem.canRead())) return false;
                if (fileItem.isDirectory()) return true;

                final String filename = fileItem.getName().toUpperCase();
                final boolean fileCondition = browseFileType == BROWSE_FILE_TYPE.PICTURE
                        ? filename.endsWith(".JPG") || filename.endsWith(".JPEG") || filename.endsWith(".PNG")
                        : filename.endsWith(".DB");
                return (fileCondition && (searchText.isEmpty() || filename.contains(searchText)));
            });

            if ((!initDir.exists()) || (initDir.isHidden()) || (!initDir.isDirectory()) || (!initDir.canRead()) || arrFiles == null) return lstDir;

            for(File fileItem : Objects.requireNonNull(arrFiles)) {
                if (fileItem.isDirectory()) {
                    lstDir.add(fileItem.getName());
                } else {
                    lstFile.add(fileItem.getName());
                }
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }

        if (!lstDir.isEmpty()) lstMerge.addAll(lstDir);
        if (!lstFile.isEmpty()) lstMerge.addAll(lstFile);

        return lstMerge;
    }

    /***
     * Import/Export Db
     * @param context
     * @return fullPath to db exported
     */
    static String ExportDb(final Context context)
    {
        String resFullPathToDbTo = null;
        DbHelperCommon dbHelperTo = null;
        SQLiteDatabase dbTo = null;
        DbHelper dbHelperFrom = null;
        SQLiteDatabase dbFrom = null;

        try
        {
            final String fullPathToDbTo = PCommon.ConcaT(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "/thelight-", PCommon.NowYYYYMMDD(), "-", PCommon.TimeFuncShort(), ".db");
            resFullPathToDbTo = fullPathToDbTo;

            final InputStream from = context.getAssets().open("db/bible_export.db");
            final File to = new File(fullPathToDbTo);
            PCommon.CopyFile(from, to);
            from.close();

            //From: Internal (Orig)
            dbHelperFrom = new DbHelper(context);
            dbFrom = dbHelperFrom.getReadableDatabase();

            //To: External (Bak)
            final int dbVersionToDummy = 2000;   //Don't change the value!
            dbHelperTo = new DbHelperCommon(context, fullPathToDbTo, null, dbVersionToDummy);
            dbTo = dbHelperTo.getWritableDatabase();

            dbTo.execSQL("DELETE FROM setting");
            dbTo.execSQL(PCommon.ConcaT("INSERT INTO setting (k, v) VALUES('PRAGMA_USER_VERSION', ", dbHelperFrom.GetDbVersion(), ")"));

            //Common operations
            PCommon.ImportExportDbCommonExecSqlOperations(context, dbFrom, fullPathToDbTo);

            if (dbTo.isOpen()) dbTo.close();
            dbHelperTo.close();

            if (dbFrom.isOpen()) dbFrom.close();
            dbHelperFrom.close();

            PCommon.ShowToast(context, fullPathToDbTo, Toast.LENGTH_LONG);
        } catch (Exception ex) {
            resFullPathToDbTo = null;
            PCommon.ShowToast(context, "Export failure!", Toast.LENGTH_SHORT);
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        } finally {
            try { if (dbTo.isOpen()) dbTo.close(); } catch (Exception ignored) {}
            try { dbHelperTo.close(); } catch (Exception ignored) {}

            try { if (dbFrom.isOpen()) dbFrom.close(); } catch (Exception ignored) {}
            try { dbHelperFrom.close(); } catch (Exception ignored) {}
        }

        return resFullPathToDbTo;
    }

    /***
     * Import/Export Db
     * @param context
     * @param fullPathToDbFrom  Full path to external db file
     */
    static void ImportDb(final Activity currentActivity, final Context context, final String fullPathToDbFrom)
    {
        //TODO: enableOnBackInvokedCallback
        //TODO: EXPORT => A resource failed to call AbstractCursor.close.

        DbHelper dbHelperTo = null;
        SQLiteDatabase dbTo = null;
        DbHelperCommon dbHelperFrom = null;
        SQLiteDatabase dbFrom = null;
        Cursor c = null;
        boolean shouldRestart = false;

        try
        {
            if ((!fullPathToDbFrom.toUpperCase().contains("/THELIGHT")) || (!fullPathToDbFrom.toUpperCase().endsWith(".DB"))) {
                PCommon.ShowToast(context, "Import failure: filename should be 'thelight*.db'", Toast.LENGTH_LONG);
                return;
            }

            //To: Internal
            dbHelperTo = new DbHelper(context);
            dbTo = dbHelperTo.getWritableDatabase();

            final String fullPathToDbTo = dbTo.getPath();
            final int dbVersionToDummy = 2000;   //Don't change the value!

            //From: External
            dbHelperFrom = new DbHelperCommon(context, fullPathToDbFrom, null, dbVersionToDummy);
            dbFrom = dbHelperFrom.getWritableDatabase();

            //Checks
            int dbVersionFrom = -1;
            c = dbFrom.rawQuery("SELECT v FROM setting WHERE k='PRAGMA_USER_VERSION'", null);
            c.moveToFirst();
            if (!c.isAfterLast()) dbVersionFrom = c.getInt(0);
            if (c != null) {
                c.close();
                //noinspection UnusedAssignment
                c = null;
            }

            if (dbVersionFrom < 82) {
                PCommon.ShowToast(context, "Import failure: db version not supported!", Toast.LENGTH_LONG);
                return;
            }

            //Common operations
            PCommon.ImportExportDbCommonExecSqlOperations(context, dbFrom, fullPathToDbTo);

            if (dbTo.isOpen()) dbTo.close();
            dbHelperTo.close();

            if (dbFrom.isOpen()) dbFrom.close();
            dbHelperFrom.close();

            shouldRestart = true;
        } catch (Exception ex) {
            PCommon.ShowToast(context, "Import failure!", Toast.LENGTH_SHORT);
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        } finally {
            try {
                if (c != null) {
                    c.close();
                    //noinspection UnusedAssignment
                    c = null;
                }
            } catch (Exception ignored) {}

            try { if (dbTo.isOpen()) dbTo.close(); } catch (Exception ignored) {}
            try { dbHelperTo.close(); } catch (Exception ignored) {}

            try { if (dbFrom.isOpen()) dbFrom.close(); } catch (Exception ignored) {}
            try { dbHelperFrom.close(); } catch (Exception ignored) {}

            try {
                if (shouldRestart)
                {
                    if (currentActivity == null) {
                        //TODO: LEX, RESTART
                        if (PCommon._isDebug) System.out.println("Data imported successfully, please restart app.");
                    } else {
                        final Intent intent = new Intent(currentActivity.getApplicationContext(), MainActivity.class);
                        currentActivity.startActivityForResult(intent, Activity.RESULT_CANCELED);
                    }
                }
            } catch (Exception ignored) {}
        }
    }

    /***
     * Contains only common ExecSQL operations of import/export db
     * @param context
     * @param dbFrom
     * @param fullPathToDbTo
     */
    static void ImportExportDbCommonExecSqlOperations(final Context context, final SQLiteDatabase dbFrom, final String fullPathToDbTo)
    {
        try
        {
            dbFrom.execSQL(PCommon.ConcaT("ATTACH DATABASE '", fullPathToDbTo, "' AS BIBLETO"));
            dbFrom.execSQL("DELETE FROM BIBLETO.artDesc");
            dbFrom.execSQL("DELETE FROM BIBLETO.bibleNote");
            dbFrom.execSQL("DELETE FROM BIBLETO.bookmark");
            dbFrom.execSQL("DELETE FROM BIBLETO.cacheSearch");
            dbFrom.execSQL("DELETE FROM BIBLETO.cacheTab");
            dbFrom.execSQL("DELETE FROM BIBLETO.planCal");
            dbFrom.execSQL("DELETE FROM BIBLETO.planDesc");
            dbFrom.execSQL("DELETE FROM BIBLETO.td");

            dbFrom.execSQL("INSERT INTO BIBLETO.artDesc     SELECT * FROM artDesc");
            dbFrom.execSQL("INSERT INTO BIBLETO.bibleNote   SELECT * FROM bibleNote");
            dbFrom.execSQL("INSERT INTO BIBLETO.bookmark    SELECT * FROM bookmark");
            dbFrom.execSQL("INSERT INTO BIBLETO.cacheSearch SELECT * FROM cacheSearch");
            dbFrom.execSQL("INSERT INTO BIBLETO.cacheTab    SELECT * FROM cacheTab");
            dbFrom.execSQL("INSERT INTO BIBLETO.planCal     SELECT * FROM planCal");
            dbFrom.execSQL("INSERT INTO BIBLETO.planDesc    SELECT * FROM planDesc");
            dbFrom.execSQL("INSERT INTO BIBLETO.td          SELECT * FROM td");
        } catch (Exception ex) {
            throw ex;
        }
    }

    /***
     * Get db version from db (official db version)
     * @param context
     * @param db
     * @return PRAGMA user_version or -1
     */
    static int GetDbVersionDb(final Context context, final SQLiteDatabase db)
    {
        Cursor c = null;
        int version = -1;

        try
        {
            c = db.rawQuery("PRAGMA user_version", null);
            c.moveToFirst();

            if (!c.isAfterLast())
            {
                version = c.getInt(0);
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
        finally
        {
            if (c != null)
            {
                c.close();
                //noinspection UnusedAssignment
                c = null;
            }
        }

        return version;
    }

    /***
     * Get random int value in ranges [min, max]
     * @param context
     * @param minRange  Minimum
     * @param maxRange  Maximum
     * @return Random int
     */
    static int GetRandomInt(final Context context, final int minRange, final int maxRange)
    {
        int rndValue = minRange;

        try
        {
            Thread.sleep(10);

            if (minRange == maxRange) return rndValue;

            final int range = maxRange - minRange + 1;
            final Random randomGenerator = new Random(System.currentTimeMillis());
            rndValue = randomGenerator.nextInt(range);  //from 0..n-1
            rndValue += minRange;
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }

        return rndValue;
    }

    /***
     * Get typeface
     * @param context
     * @return null has default typeface, so don't set it. Roboto?
     */
    static Typeface GetTypeface(final Context context)
    {
        try
        {
            final String tfName = PCommon.GetPref(context, APP_PREF_KEY.FONT_NAME, "");

            return (tfName == null || tfName.isEmpty())
                    ? Typeface.defaultFromStyle(Typeface.NORMAL)
                    : Typeface.createFromAsset(context.getAssets(), PCommon.ConcaT("fonts/", tfName, ".ttf"));
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }

        return null;
    }

    /***
     * Get font size (verse)
     * @param context
     */
    static int GetFontSize(final Context context)
    {
        try
        {
            return Integer.parseInt(PCommon.GetPref(context, APP_PREF_KEY.FONT_SIZE, "14"));
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }

        return 14;
    }

    /***
     * Get install status
     * @param context
     * @return
     */
    static int GetInstallStatus(final Context context)
    {
        return Integer.parseInt(PCommon.GetPref(context, IProject.APP_PREF_KEY.INSTALL_STATUS, "1"));
    }

    /***
     * Get install status should be (= number total of bibles in the package)
     * @return
     */
    static int GetInstallStatusShouldBe()
    {
        return 22;
    }

    /***
     * Get edit status
     * @param context
     * @return
     */
    static int GetEditStatus(final Context context)
    {
        return Integer.parseInt(PCommon.GetPref(context, APP_PREF_KEY.EDIT_STATUS, "0"));
    }

    /***
     * Get edit article id
     * @param context
     * @return < 0 if not used
     */
    static int GetEditArticleId(final Context context)
    {
        return Integer.parseInt(PCommon.GetPref(context, APP_PREF_KEY.EDIT_ART_ID, "-1"));
    }

    /***
     * Get Fav Filter
     * @return 0..2
     */
    static int GetFavFilter(final Context context)
    {
        return Integer.parseInt(PCommon.GetPref(context, APP_PREF_KEY.FAV_FILTER, "0"));
    }

    /***
     * Get Fav Order
     * @return 1 or 2
     */
    static int GetFavOrder(final Context context)
    {
        final int orderBy = Integer.parseInt(PCommon.GetPref(context, IProject.APP_PREF_KEY.FAV_ORDER, "1"));

        return orderBy == 0 ? 1 : orderBy;
    }

    /***
     * Get listen status
     * @param context
     * @return (1=Active, 0=Inactive)
     */
    static int GetListenStatus(final Context context)
    {
        return PCommon.GetThreadTypeRunning(context, 2) > 0 ? 1 : 0;
    }

    /***
     * Get CLIPBOARD_IDS
     * @param context
     * @return List of String Id
     */
    static ArrayList<Integer> GetClipboardIds(final Context context)
    {
        final ArrayList<Integer> lstId = new ArrayList<>();

        try
        {
            final String strIds = PCommon.GetPref(context, APP_PREF_KEY.CLIPBOARD_IDS, "");
            if (strIds == null || strIds.equalsIgnoreCase("")) return lstId;

            final String[] arrStrId = strIds.replaceAll(" ", "").replace("[", "").replace("]", "").split(",");
            for (String strId: arrStrId) {
                lstId.add(Integer.parseInt(strId));
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }

        return lstId;
    }

    /**
     * Save CLIPBOARD_IDS
     * @param context   Context
     * @param lstId     List of String Id
     */
    static void SaveClipboardIds(final Context context, final ArrayList<Integer> lstId)
    {
        try
        {
            final String strLstId = (lstId == null || lstId.size() <= 0) ? "" : lstId.toString();
            SavePref(context, APP_PREF_KEY.CLIPBOARD_IDS, strLstId);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Is UI Television?
     * @param context
     * @return true/false
     */
    static boolean IsUiTelevision(final Context context)
    {
        boolean isUiTelevision = false;
        @SuppressWarnings("unused") final String logHeader = "org.hlwd.bible: ";

        try
        {
            //No check needed
            final String UI_LAYOUT = PCommon.GetPref(context, APP_PREF_KEY.UI_LAYOUT, "C");
            isUiTelevision = UI_LAYOUT.equalsIgnoreCase("T");
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) System.out.println( PCommon.ConcaT(logHeader, "IsUiTelevision (exception)=", ex ));
        }
        finally
        {
            //TODO FAB: bug of Scommon dbOpening => to review. Set isDebug=true to get errors when installing app on emulator.
            if (PCommon._isDebug) System.out.println( PCommon.ConcaT(logHeader, "isUiTelevision=", isUiTelevision ));
        }

        return isUiTelevision;
    }

    /***
     * Detect if it's running on a television
     * @param context
     * @return true/false
     */
    static boolean DetectIsUiTelevision(final Context context)
    {
        boolean isUiTelevision = false;
        final String logHeader = "org.hlwd.bible: ";

        try
        {
            try
            {
                final UiModeManager uiModeManager = (UiModeManager) context.getSystemService(Context.UI_MODE_SERVICE);
                final boolean isUiModeTypeTelevision = (Objects.requireNonNull(uiModeManager).getCurrentModeType() == Configuration.UI_MODE_TYPE_TELEVISION);
                System.out.println( PCommon.ConcaT(logHeader, "isUiModeTypeTelevision=", isUiModeTypeTelevision ));

                if (isUiModeTypeTelevision)
                {
                    isUiTelevision = true;
                    PCommon.SavePref(context, APP_PREF_KEY.UI_LAYOUT, "T");
                    return true;
                }
            }
            catch(Exception ex)
            {
                System.out.println( PCommon.ConcaT(logHeader, "DetectIsUiTelevision (exception)=", ex ));
            }

            final PackageManager pm = context.getPackageManager();

            try
            {
                //Always Build.VERSION.SDK_INT >= 21
                final boolean hasFeatureLeanback = pm.hasSystemFeature(PackageManager.FEATURE_LEANBACK);
                System.out.println( PCommon.ConcaT(logHeader, "hasFeatureLeanback=", hasFeatureLeanback ));
                if (hasFeatureLeanback)
                {
                    isUiTelevision = true;
                    PCommon.SavePref(context, APP_PREF_KEY.UI_LAYOUT, "T");
                    return true;
                }
            }
            catch (Exception ex)
            {
                System.out.println( PCommon.ConcaT(logHeader, "DetectIsUiTelevision (exception)=", ex ));
            }

            try
            {
                final boolean hasFeatureTelevision = pm.hasSystemFeature(PackageManager.FEATURE_TELEVISION);
                System.out.println( PCommon.ConcaT(logHeader, "hasFeatureTelevision=", hasFeatureTelevision ));
                if (hasFeatureTelevision)
                {
                    isUiTelevision = true;
                    PCommon.SavePref(context, APP_PREF_KEY.UI_LAYOUT, "T");
                    return true;
                }
            }
            catch (Exception ex)
            {
                System.out.println( PCommon.ConcaT(logHeader, "DetectIsUiTelevision (exception)=", ex ));
            }

            //Default
            PCommon.SavePref(context, APP_PREF_KEY.UI_LAYOUT, "C");
        }
        catch (Exception ex)
        {
            //TODO FAB: add check isDebug=true... (see TODO in finally)
            System.out.println( PCommon.ConcaT(logHeader, "DetectIsUiTelevision (exception)=", ex ));
        }
        finally
        {
            //TODO FAB: add check isDebug=true, but there is a bug of Scommon dbOpening => to review. Set isDebug=true to get errors when installing app on emulator.
            System.out.println( PCommon.ConcaT(logHeader, "DetectIsUiTelevision=", isUiTelevision ));
        }

        return isUiTelevision;
    }

    /***
     * Get TV borders
     * @param context   Context
     * @param key       Variable to use
     * @return String of 4 values delimited by ,
     */
    static String GetUiLayoutTVBorders(final Context context, final APP_PREF_KEY key)
    {
        String borders = "30,27,27,30";

        try
        {
            if (key == null) return borders;

            borders = PCommon.GetPref(context, key, borders);

            return borders;
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }

        return borders;
    }

    /***
     * Set UI layout
     * @param context
     * @param classicLayoutId
     * @param tvLayoutId
     * @return classic or tv layout
     */
    static int SetUILayout(final Context context, final int classicLayoutId, final int tvLayoutId)
    {
        final boolean isUiTelevision = PCommon.IsUiTelevision(context);

        return (isUiTelevision) ? tvLayoutId : classicLayoutId;
    }

    /***
     * Add icon to menuitem
     * @param context
     * @param menuItem
     * @param drawable
     */
    static void SetIconMenuItem(final Context context, final MenuItem menuItem, final int drawable) {
        try
        {
            final SpannableStringBuilder spanStringBuilder = new SpannableStringBuilder(PCommon.ConcaT("  ", menuItem.getTitle()));
            spanStringBuilder.setSpan(new ImageSpan(context, drawable),0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            menuItem.setTitle(spanStringBuilder);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Add icon to menuitem
     * @param context
     * @param tv
     * @param drawable
     */
    static void SetIconTextView(final Context context, final TextView tv, final int drawable) {
        try
        {
            final SpannableStringBuilder spanStringBuilder = new SpannableStringBuilder(tv.getText());
            spanStringBuilder.setSpan(new ImageSpan(context, drawable),spanStringBuilder.length() - 1, spanStringBuilder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            tv.setText(spanStringBuilder);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Set text appareance
     * @param tv
     * @param context
     * @param resId
     */
    static void SetTextAppareance(final TextView tv, final Context context, @SuppressWarnings("SameParameterValue") final int resId)
    {
        try
        {
            final int version = Build.VERSION.SDK_INT;
            if (version < 23) {
                tv.setTextAppearance(context, resId);
            } else {
                tv.setTextAppearance(resId);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    static UnderlineSpan GetUnderlineSpan()
    {
        return new UnderlineSpan();
    }

    static ForegroundColorSpan GetForegroundColorSpan(final String fgColor)
    {
        return new ForegroundColorSpan(Color.parseColor(fgColor));
    }

    static BackgroundColorSpan GetBackgroundColorSpan(final String bgColor)
    {
        return new BackgroundColorSpan(Color.parseColor(bgColor));
    }

    /***
     * Convert dp to px
     * @param context   Context
     * @param dp        dp
     * @return px       pixel
     */
    static int ConvertDpToPx(final Context context, final int dp)
    {
        int px = 0;

        try
        {
            final Resources r = context.getResources();
            px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }

        return px;
    }

    /*
     * Use strict mode
     * @param context   Context
     */
    /*
    @SuppressWarnings("unused")
    static void TryStrictMode(final Context context)
    {
        try
        {
            if (_useStrictMode) //was: && BuildConfig.DEBUG
            {
                StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                        //.detectDiskReads()
                        //.detectDiskWrites()
                        .detectNetwork()        //or .detectAll() for all detectable problems
                        .penaltyLog()
                        .penaltyFlashScreen()
                        .build());
                
                StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                        .detectLeakedSqlLiteObjects()
                        .detectLeakedClosableObjects()
                        .penaltyLog()
                        .penaltyDeath()
                        .build());
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(context, ex);
        }
    }
    */

    /***
     * Get locale
     * @param context   context
     * @param bbName    bbname
     * @param bNumber   bNumber (only used for "w")
     * @return default is English
     */
    static Locale GetLocale(final Context context, final String bbName, final int bNumber, final boolean shouldReturnRealLocale)
    {
        try
        {
            String bbname = bbName.toLowerCase();
            String altBBName = bbname;

            if (bbname.equalsIgnoreCase("i"))
            {
                if (shouldReturnRealLocale) return new Locale("hi", "IN");
                altBBName = GetAltBBName(context, bbname);
            }
            else if (bbname.equalsIgnoreCase("y"))
            {
                if (shouldReturnRealLocale) return new Locale("ar", "SA");
                altBBName = GetAltBBName(context, bbname);
            }
            else if (bbname.equalsIgnoreCase("c"))
            {
                if (shouldReturnRealLocale) return new Locale("zh", "CN");
                altBBName = GetAltBBName(context, bbname);
            }
            else if (bbname.equalsIgnoreCase("s"))
            {
                if (shouldReturnRealLocale) return new Locale("de", "DE");
                altBBName = GetAltBBName(context, bbname);
            }
            else if (bbname.equalsIgnoreCase("e"))
            {
                if (shouldReturnRealLocale) return new Locale("de", "DE");
                altBBName = GetAltBBName(context, bbname);
            }
            else if (bbname.equalsIgnoreCase("j"))
            {
                if (shouldReturnRealLocale) return new Locale("ja", "JP");
                altBBName = GetAltBBName(context, bbname);
            }
            else if (bbname.equalsIgnoreCase("u"))
            {
                if (shouldReturnRealLocale) return new Locale("ro", "RO");
                altBBName = GetAltBBName(context, bbname);
            }
            else if (bbname.equalsIgnoreCase("z"))
            {
                if (shouldReturnRealLocale) return new Locale("pl", "PL");
                altBBName = GetAltBBName(context, bbname);
            }
            else if (bbname.equalsIgnoreCase("r"))
            {
                if (shouldReturnRealLocale) return new Locale("ru", "RU");
                altBBName = GetAltBBName(context, bbname);
            }
            else if (bbname.equalsIgnoreCase("t"))
            {
                if (shouldReturnRealLocale) return new Locale("tr", "TR");
                altBBName = GetAltBBName(context, bbname);
            }
            else if (bbname.equalsIgnoreCase("b"))
            {
                if (shouldReturnRealLocale) return new Locale("bn", "BD");
                altBBName = GetAltBBName(context, bbname);
            }
            else if (bbname.equalsIgnoreCase("h"))
            {
                if (shouldReturnRealLocale) return new Locale("sw", "TZ");
                altBBName = GetAltBBName(context, bbname);
            }
            else if (bbname.equalsIgnoreCase("w"))
            {
                if (shouldReturnRealLocale) {
                    if (bNumber < 40) {
                        return new Locale("iw", "IL"); //was: new Locale("he", "IL");
                    } else {
                        return new Locale("el", "GR");
                    }
                }
                altBBName = GetAltBBName(context, bbname);
            }

            switch(altBBName) {
                case "k":
                case "2":
                    return new Locale("en", "GB");

                case "v":
                case "9":
                    return new Locale("es", "ES");

                case "l":
                case "o":
                    return new Locale("fr", "FR");

                case "d":
                case "1":
                    return new Locale("it", "IT");

                case "a":
                    return new Locale("pt", "BR");

                case "i":
                    return new Locale("hi", "IN");
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }

        return new Locale("en", "GB");
    }

    private static String GetAltBBName(final Context context, final String bbName)
    {
        try
        {
            final String altLanguage = PCommon.GetPref(context, APP_PREF_KEY.ALT_LANGUAGE, "en").toLowerCase();
            switch (altLanguage) {
                case "es":
                    return "v";
                case "fr":
                    return "l";
                case "it":
                    return "d";
                case "pt":
                    return "a";
                case "hi":
                    return "i";
                default:
                    return "k";
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }

        return "k";
    }

    /***
     * Set locale
     * @param activity  Activity
     * @param bNumber   bNumber (only used for hebrew/greek)
     * @param shouldReturnRealLocale Real or not
     */
    static void SetLocale(final Activity activity, final int bNumber, final boolean shouldReturnRealLocale)
    {
        try
        {
            final Context context = activity.getApplicationContext();
            final String bbName = PCommon.GetPrefBibleName(context);
            final Locale locale = PCommon.GetLocale(context, bbName, bNumber, shouldReturnRealLocale);
            Locale.setDefault(locale);

            final Configuration config = context.getResources().getConfiguration();
            config.setLocale(locale);

            context.createConfigurationContext(config);
            context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());

            final Resources res = activity.getResources();
            res.updateConfiguration(config, null);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(activity.getApplicationContext(), ex);
        }
    }

    static String GetLTR()
    {
        return "\u202A";
    }

    static String GetRTL()
    {
        return "\u202B";
    }

    static void CopyFile(final InputStream src, final File dst) throws IOException {
        try (InputStream in = src)
        {
            if (PCommon._isDebug) System.out.println(PCommon.ConcaT("To db: ", dst.toString()));

            try (OutputStream out = new FileOutputStream(dst)) {
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            }
        }
    }

    /***
     * Create a TextView
     * @param context
     * @param layoutParams
     * @param isFocusable
     * @param paddingLeft
     * @param paddingTop
     * @param paddingRight
     * @param paddingBottom
     * @param htmlText
     * @param typeface
     * @param fontSize
     * @param textColor
     * @param backgroundDrawableId
     * @return TextView
     */
    static TextView CreateTextView(final Context context, final android.view.ViewGroup.LayoutParams layoutParams, final boolean isFocusable, final int paddingLeft, final int paddingTop, final int paddingRight, final int paddingBottom, final String htmlText, final Typeface typeface, final int fontSize, final int textColor, final int backgroundDrawableId)
    {
        final TextView tv = new TextView(context);

        try
        {
            tv.setLayoutParams(layoutParams);
            tv.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
            tv.setFocusable(isFocusable);
            if (backgroundDrawableId >= 0) tv.setBackground(PCommon.GetDrawable(context, backgroundDrawableId));
            tv.setText(Html.fromHtml(htmlText));
            if (typeface != null) tv.setTypeface(typeface);
            tv.setTextSize(fontSize);
            tv.setTextColor(textColor);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }

        return tv;
    }

    /***
     * Create a view separator
     * @param context
     * @param paddingLeft
     * @param paddingTop
     * @param paddingRight
     * @param paddingBottom
     * @param fgColor
     * @return
     */
    static View CreateLineSep(final Context context, final int paddingLeft, final int paddingTop, final int paddingRight, final int paddingBottom, final int fgColor)
    {
        final View vwSep = new View(context);

        try
        {
            vwSep.setFocusable(false);
            vwSep.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
            vwSep.setLayoutParams(new AppBarLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2));
            vwSep.setBackgroundColor(fgColor);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }

        return vwSep;
    }

    /***
     * Load data of interlinear word detail
     * @param currentActivity   Activity
     * @param word  Word
     * @param lex_detail_search_type    Lex detail search type
     * @param builder   Builder
     * @param view  View of builder
     * @param shouldRequestFocusForBtnRight False:left, True=right
     * @param dlgStyle  Style (todo: this field can be removed if I find how to use current Dialog style)
     */
    private static void LoadDataInterlinearWordDetail(final FragmentActivity currentActivity, final String word, final PCommon.LEX_DETAIL_SEARCH_TYPE lex_detail_search_type, final AlertDialog builder, final View view, final boolean shouldRequestFocusForBtnRight, final StyleBO dlgStyle)
    {
        final Context context = currentActivity.getApplicationContext();

        try
        {
            final String wTrimmed = word.trim();
            final Pattern pHebrew = Pattern.compile("\\p{InHebrew}", Pattern.UNICODE_CASE);
            final Pattern pGreek = Pattern.compile("\\p{InGreek}", Pattern.UNICODE_CASE);
            final Matcher mHebrew = pHebrew.matcher(wTrimmed);
            final Matcher mGreek = pGreek.matcher(wTrimmed);

            //Check
            final ArrayList<LexTbesBO> lstLex = (mHebrew.find() || mGreek.find()) ? _s.GetLexDetailByWord(wTrimmed, lex_detail_search_type) : _s.GetLexDetailByHGNr(wTrimmed);
            if (lstLex == null || lstLex.isEmpty()) return;

            //UI
            final String titleMsg = PCommon.ConcaT(context.getString(R.string.mnuInterlinearWord), ": ", wTrimmed);
            builder.setTitle(titleMsg);
            final LinearLayout llInt = view.findViewById(R.id.llInt);
            llInt.removeAllViewsInLayout();
            final Button btnIntGetAll = view.findViewById(R.id.btnIntGetAll);
            btnIntGetAll.setVisibility(View.GONE);
            final Button btnIntBack = view.findViewById(R.id.btnIntBack);
            btnIntBack.setVisibility(View.GONE);
            final Button btnIntForward = view.findViewById(R.id.btnIntForward);
            btnIntForward.setVisibility(View.GONE);

            //Prepare data
            final Typeface typeface = PCommon.GetTypeface(context);
            final int fontSize = PCommon.GetFontSize(context);
            final int textColor = PCommon.GetPrefThemeName(context).compareTo("LIGHT") == 0 ? Color.BLACK : Color.WHITE; //TODO: harcoded, need to find from attribute
            final int paddingLeft = 20, paddingTop = 30, paddingRight = 20, paddingBottom = 30;

            final int size = lstLex.size();
            final String fieldEmpty = " ";
            String field;
            LexTbesBO lex = null;

            for (int row = 0; row < size; row++)
            {
                if (row >= 10)
                {
                    field = PCommon.ConcaT("* Display limited to 10 records (of ", size, ")");
                    final TextView tvLimited = PCommon.CreateTextView(context, PCommon._layoutParamsMatchAndWrap, false, paddingLeft, paddingTop, paddingRight, paddingBottom, field, typeface, fontSize, textColor,-1);
                    llInt.addView(tvLimited);

                    field = fieldEmpty;
                    final TextView tvEmpty = PCommon.CreateTextView(context, PCommon._layoutParamsMatchAndWrap, false, paddingLeft, paddingTop, paddingRight, paddingBottom, field, typeface, fontSize, textColor,-1);
                    llInt.addView(tvEmpty);

                    break;
                }

                lex = lstLex.get(row);

                field = PCommon.ConcaT("<u>eStrong</u>: ", lex.eStrong);
                final TextView tvEstrong = PCommon.CreateTextView(context, PCommon._layoutParamsMatchAndWrap, true, paddingLeft, paddingTop, paddingRight, paddingBottom, field, typeface, fontSize, textColor, R.drawable.focus_text);
                tvEstrong.setTag(lex.eStrong);
                tvEstrong.setOnClickListener(v -> {
                    final String w = v.getTag().toString();
                    if (w.isEmpty()) return;
                    ShowInterlinearWordDetail(currentActivity, w, PCommon.LEX_DETAIL_SEARCH_TYPE.EXACT_SEARCH, dlgStyle);
                });
                llInt.addView(tvEstrong);

                field = PCommon.ConcaT("<u>dStrong</u>: ", lex.dStrong);
                final TextView tvDstrong = PCommon.CreateTextView(context, PCommon._layoutParamsMatchAndWrap, true, paddingLeft, paddingTop, paddingRight, paddingBottom, field, typeface, fontSize, textColor, R.drawable.focus_text);
                tvDstrong.setTag(lex.dStrong);
                tvDstrong.setOnClickListener(v -> {
                    final String w = v.getTag().toString();
                    if (w.isEmpty()) return;
                    ShowInterlinearWordDetail(currentActivity, w, PCommon.LEX_DETAIL_SEARCH_TYPE.EXACT_SEARCH, dlgStyle);
                });
                llInt.addView(tvDstrong);

                field = PCommon.ConcaT("<u>uStrong</u>: ", lex.uStrong);
                final TextView tvUstrong = PCommon.CreateTextView(context, PCommon._layoutParamsMatchAndWrap, true, paddingLeft, paddingTop, paddingRight, paddingBottom, field, typeface, fontSize, textColor, R.drawable.focus_text);
                tvUstrong.setTag(lex.uStrong);
                tvUstrong.setOnClickListener(v -> {
                    final String w = v.getTag().toString();
                    if (w.isEmpty()) return;
                    ShowInterlinearWordDetail(currentActivity, w, PCommon.LEX_DETAIL_SEARCH_TYPE.EXACT_SEARCH, dlgStyle);
                });
                llInt.addView(tvUstrong);

                field = PCommon.ConcaT("<u>HG</u>: ", lex.hg);
                final TextView tvHG = PCommon.CreateTextView(context, PCommon._layoutParamsMatchAndWrap, true, paddingLeft, paddingTop, paddingRight, paddingBottom, fieldEmpty, typeface, fontSize, textColor, R.drawable.focus_text);
                final SpannableStringBuilder spanHG = GetBiggerHGText(context, field);
                tvHG.setText(spanHG);
                tvHG.setTag(lex.hg);
                tvHG.setOnClickListener(v -> {
                    final String w = v.getTag().toString();
                    if (w.isEmpty()) return;
                    ShowInterlinearWordDetail(currentActivity, w, PCommon.LEX_DETAIL_SEARCH_TYPE.EXACT_SEARCH, dlgStyle);
                });
                llInt.addView(tvHG);

                field = PCommon.ConcaT("<u>Transliteration</u>: ", lex.transliteration);
                final TextView tvTrans = PCommon.CreateTextView(context, PCommon._layoutParamsMatchAndWrap, true, paddingLeft, paddingTop, paddingRight, paddingBottom, field, typeface, fontSize, textColor, R.drawable.focus_text);
                llInt.addView(tvTrans);

                field = PCommon.ConcaT("<u>Grammar</u>: ", lex.grammar);
                final TextView tvGrammar = PCommon.CreateTextView(context, PCommon._layoutParamsMatchAndWrap, true, paddingLeft, paddingTop, paddingRight, paddingBottom, field, typeface, fontSize, textColor, R.drawable.focus_text);
                llInt.addView(tvGrammar);

                field = PCommon.ConcaT("<u>English</u>: ", lex.english);
                final TextView tvEnglish = PCommon.CreateTextView(context, PCommon._layoutParamsMatchAndWrap, true, paddingLeft, paddingTop, paddingRight, paddingBottom, field, typeface, fontSize, textColor, R.drawable.focus_text);
                llInt.addView(tvEnglish);

                field = PCommon.ConcaT("<u>Meaning</u>:<br>", lex.meaning);
                final TextView tvMeaning = PCommon.CreateTextView(context, PCommon._layoutParamsMatchAndWrap, true, paddingLeft, paddingTop, paddingRight, paddingBottom, fieldEmpty, typeface, fontSize, textColor, R.drawable.focus_text);
                final SpannableStringBuilder spanMeaning = InspectInterlinearText(currentActivity, field, dlgStyle);
                tvMeaning.setText(spanMeaning);
                tvMeaning.setMovementMethod(LinkMovementMethod.getInstance());
                llInt.addView(tvMeaning);

                field = fieldEmpty;
                final TextView tvEmpty = PCommon.CreateTextView(context, PCommon._layoutParamsMatchAndWrap, false, paddingLeft, paddingTop, paddingRight, paddingBottom, field, typeface, fontSize, textColor,-1);
                llInt.addView(tvEmpty);

                final View vwSep = PCommon.CreateLineSep(context, paddingLeft, paddingTop, paddingRight, paddingBottom, textColor);
                llInt.addView(vwSep);
            }

            if (size == 1)
            {
                final int lexId = lex.id;

                btnIntGetAll.setVisibility(View.VISIBLE);
                btnIntGetAll.setOnClickListener(v -> {
                    CheckLocalInstance(v.getContext());

                    final LexTbesBO lexDetail = _s.GetLexDetail(lexId);
                    if (lexDetail == null) return;

                    btnIntGetAll.setVisibility(View.GONE);

                    final AlertDialog pgr = PCommon.ShowProgressDialog(currentActivity, R.string.mnuSearchLexicons, R.string.planCreating);
                    final Handler handler = new Handler();
                    handler.postDelayed(() -> {
                        final String bbName = PCommon.GetPrefBibleName(context);
                        final AlertDialog builderLanguages = new AlertDialog.Builder(currentActivity).create();             //, R.style.DialogStyleKaki
                        final LayoutInflater inflater = currentActivity.getLayoutInflater();
                        final View vllLanguages = inflater.inflate(R.layout.fragment_languages_multi, currentActivity.findViewById(R.id.llLanguages));
                        PCommon.SelectBibleLanguageMulti(builderLanguages, v.getContext(), vllLanguages, titleMsg, "", true, false);
                        builderLanguages.setOnDismissListener(dialogInterface -> {
                            final String bbname = PCommon.GetPref(v.getContext(), IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, bbName);
                            if (bbname.isEmpty())
                            {
                                btnIntGetAll.setVisibility(View.VISIBLE);
                                if (pgr != null) pgr.dismiss();
                                return;
                            }
                            final String tbbName = PCommon.GetPrefTradBibleName(v.getContext(), true);
                            //---
                            final String fullquery = PCommon.GetHGNrCleaned(v.getContext(), lexDetail.dStrong);
                            MainActivity.Tab.AddTab(v.getContext(), "SL", tbbName, fullquery, false);
                            //---
                            if (pgr != null) pgr.dismiss();
                        });
                        builderLanguages.show();
                    }, 500);
                });

                btnIntBack.setVisibility(View.VISIBLE);
                btnIntBack.setOnClickListener(v -> {
                    CheckLocalInstance(v.getContext());

                    final int lexIdPrev = lexId - 1;
                    final LexTbesBO lexPrev = _s.GetLexDetail(lexIdPrev);
                    if (lexPrev == null) return;

                    LoadDataInterlinearWordDetail(currentActivity, lexPrev.dStrong, PCommon.LEX_DETAIL_SEARCH_TYPE.EXACT_SEARCH, builder, view, false, dlgStyle);
                });

                btnIntForward.setVisibility(View.VISIBLE);
                btnIntForward.setOnClickListener(v -> {
                    CheckLocalInstance(v.getContext());

                    final int lexIdNext = lexId + 1;
                    final LexTbesBO lexNext = _s.GetLexDetail(lexIdNext);
                    if (lexNext == null) return;

                    LoadDataInterlinearWordDetail(currentActivity, lexNext.dStrong, PCommon.LEX_DETAIL_SEARCH_TYPE.EXACT_SEARCH, builder, view, true, dlgStyle);
                });

                if (shouldRequestFocusForBtnRight) { btnIntForward.requestFocus(); } else { btnIntBack.requestFocus(); }
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Show interlinear detail
     * @param currentActivity  Activity
     * @param word      Word
     * @param lex_detail_search_type    Lex search type
     */
    static void ShowInterlinearWordDetail(final FragmentActivity currentActivity, String word, final PCommon.LEX_DETAIL_SEARCH_TYPE lex_detail_search_type, final StyleBO dlgStyle)
    {
        final Context context = currentActivity.getApplicationContext();

        try
        {
            CheckLocalInstance(context);

            final String wTrimmed = word.trim();

            //UI , R.style.AppThemeDark
            final LayoutInflater inflater = currentActivity.getLayoutInflater();
            final AlertDialog builder = new AlertDialog.Builder(currentActivity).create();
            final View view = inflater.inflate(R.layout.fragment_interlinear, currentActivity.findViewById(R.id.svLex));
            builder.setTitle(PCommon.ConcaT(context.getString(R.string.mnuInterlinearWord), ": ", wTrimmed));
            builder.setCancelable(true);
            builder.setView(view);
            builder.show();

            //Load data
            LoadDataInterlinearWordDetail(currentActivity, wTrimmed, lex_detail_search_type, builder, view, false, dlgStyle);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Show Audio Bible
     * @param currentActivity  Activity
     * @param verse     Selected verse
     */
    static void ShowAudioBible(final FragmentActivity currentActivity, VerseBO verse)
    {
        final Context context = currentActivity.getApplicationContext();

        try
        {
            CheckLocalInstance(context);

            //Prepare data
            final Typeface typeface = PCommon.GetTypeface(context);
            final int fontSize = PCommon.GetFontSize(context);

            //UI , R.style.AppThemeDark
            final LayoutInflater inflater = currentActivity.getLayoutInflater();
            final AlertDialog builder = new AlertDialog.Builder(currentActivity).create();
            final View view = inflater.inflate(R.layout.fragment_audio_bible, currentActivity.findViewById(R.id.svAudio));
            builder.setTitle(context.getString(R.string.mnuListen));
            builder.setCancelable(false);
            builder.setButton(DialogInterface.BUTTON_POSITIVE, context.getString(R.string.btnClose), (dialog, which) -> builder.dismiss());
            builder.setView(view);
            builder.show();

            //UI
            final TextView tvAudioRef = view.findViewById(R.id.tvAudioRef);
            final Button btnAudioStart = view.findViewById(R.id.btnAudioStart);
            final Button btnAudioBack = view.findViewById(R.id.btnAudioBack);
            final Button btnAudioPlay = view.findViewById(R.id.btnAudioPlay);
            final Button btnAudioStop = view.findViewById(R.id.btnAudioStop);
            final Button btnAudioForward = view.findViewById(R.id.btnAudioForward);
            final Button btnAudioEnd = view.findViewById(R.id.btnAudioEnd);
            final View tvAudioSep = view.findViewById(R.id.vwAudioSep);
            final TextView tvAudioCurrentChapterSelected = view.findViewById(R.id.tvAudioCurrentChapterSelected);

            if (typeface != null)
            {
                tvAudioRef.setTypeface(typeface);
                tvAudioCurrentChapterSelected.setTypeface(typeface);
            }
            tvAudioRef.setTextSize(fontSize);
            tvAudioCurrentChapterSelected.setTextSize(fontSize);

            class Inner
            {
                final int step = 2;

                void CommonAction(final Context ctx, final AUDIO_BIBLE_ACTION action)
                {
                    try
                    {
                        boolean isCalledFromPlay = false;

                        CheckLocalInstance(ctx);

                        final String listenPosition = _s.SayStop();
                        if (listenPosition == null) return;

                        final String[] arr =  listenPosition.split(",");
                        if (arr.length < 4) return;

                        String bbName = arr[0];
                        final int bNumber = Integer.parseInt(arr[1]);
                        int cNumber = Integer.parseInt(arr[2]);
                        int vNumber = Integer.parseInt(arr[3]);

                        //*** Get params
                        switch (action)
                        {
                            case STOP_CLICK:
                            {
                                break;
                            }
                            case PLAY_CLICK:
                            {
                                isCalledFromPlay = true;
                                _s.Say();
                                break;
                            }
                            case START_CLICK:
                            case BACK_CLICK:
                            case FORWARD_CLICK:
                            case END_CLICK:
                            case LANG_CLICK:
                            {
                                if (action == AUDIO_BIBLE_ACTION.START_CLICK)
                                {
                                    if (cNumber - 1 < 1)
                                    {
                                        cNumber = 1;
                                    }
                                    else
                                    {
                                        cNumber--;
                                    }

                                    vNumber = 1;

                                    _s.SaySetCurrentVerseNumber(vNumber);
                                }
                                else if (action == AUDIO_BIBLE_ACTION.BACK_CLICK)
                                {
                                    vNumber = vNumber - step;
                                    if (vNumber < 1) vNumber = 1;

                                    _s.SaySetCurrentVerseNumber(vNumber);
                                }
                                else if (action == AUDIO_BIBLE_ACTION.FORWARD_CLICK)
                                {
                                    vNumber = vNumber + step;

                                    final int vCount = _s.GetChapterVerseCount(bNumber, cNumber);

                                    if (vNumber > vCount)
                                    {
                                        vNumber = vCount;
                                    }

                                    _s.SaySetCurrentVerseNumber(vNumber);
                                }
                                else if (action == AUDIO_BIBLE_ACTION.LANG_CLICK)
                                {
                                    bbName = CommonWidget.RollBookName(ctx, bbName);
                                }
                                else    // AUDIO_BIBLE_ACTION.END_CLICK
                                {
                                    final int cNumberMax = _s.GetBookChapterMax(bNumber);

                                    if (cNumber + 1 > cNumberMax)
                                    {
                                        return;
                                    }
                                    else
                                    {
                                        cNumber++;
                                        vNumber = 1;

                                        _s.SaySetCurrentVerseNumber(vNumber);
                                    }
                                }

                                _s.SetListenPosition(ctx, bbName, bNumber, cNumber, vNumber);
                                break;
                            }
                        }

                        //*** Update widget
                        UpdateRef(isCalledFromPlay, ctx, bbName, bNumber, cNumber, vNumber);
                    }
                    catch(Exception ex)
                    {
                        if (PCommon._isDebug) PCommon.LogR(ctx, ex);
                    }
                }

                void UpdateRef(final boolean isCalledFromPlay, final Context ctx, final String bbName, final int bNumber, final int cNumber, final int vNumber)
                {
                    String verseRef = "~";

                    try
                    {
                        final ArrayList<VerseBO> arrVerse = _s.GetVerse(bbName, bNumber, cNumber, vNumber);
                        final VerseBO verse = arrVerse.isEmpty() ? null : arrVerse.get(0);

                        if (!isCalledFromPlay) {
                            verseRef = verse == null ? "~" : PCommon.ConcaT(verse.bName, " ", verse.cNumber, ".", verse.vNumber);
                        } else {
                            verseRef = verse == null ? "~" : verse.bName;
                        }
                    }
                    catch(Exception ex)
                    {
                        if (PCommon._isDebug) PCommon.LogR(ctx, ex);
                    }
                    finally
                    {
                        try
                        {
                            tvAudioRef.setText(verseRef);
                        }
                        catch (Exception ignored) { }
                    }
                }

                void LoadRef(final Context ctx)
                {
                    try
                    {
                        final String[] arr = _s.GetListenPosition(ctx);
                        if (! (arr == null || arr.length < 4))
                        {
                            final String bbName = arr[0];
                            final int bNumber = Integer.parseInt(arr[1]);
                            final int cNumber = Integer.parseInt(arr[2]);
                            final int vNumber = _s.GetListenVerseNumber();

                            UpdateRef(false, ctx, bbName, bNumber, cNumber, vNumber);

                            final Handler handler = new Handler();
                            handler.postDelayed(() -> {
                                UpdateRef(true, ctx, bbName, bNumber, cNumber, vNumber);
                            }, 2500);
                        }
                    }
                    catch (Exception ex)
                    {
                        if (PCommon._isDebug) PCommon.LogR(ctx, ex);
                    }
                }
            }

            //Events
            final Inner inner = new Inner();

            btnAudioStart.setOnClickListener(v -> {
                inner.CommonAction(v.getContext(), AUDIO_BIBLE_ACTION.START_CLICK);
            });

            btnAudioBack.setOnClickListener(v -> {
                inner.CommonAction(v.getContext(), AUDIO_BIBLE_ACTION.BACK_CLICK);
            });

            btnAudioPlay.setOnClickListener(v -> {
                inner.CommonAction(v.getContext(), AUDIO_BIBLE_ACTION.PLAY_CLICK);
            });

            btnAudioStop.setOnClickListener(v -> {
                inner.CommonAction(v.getContext(), AUDIO_BIBLE_ACTION.STOP_CLICK);
            });

            btnAudioForward.setOnClickListener(v -> {
                inner.CommonAction(v.getContext(), AUDIO_BIBLE_ACTION.FORWARD_CLICK);
            });

            btnAudioEnd.setOnClickListener(v -> {
                inner.CommonAction(v.getContext(), AUDIO_BIBLE_ACTION.END_CLICK);
            });

            tvAudioRef.setOnClickListener(v ->
            {
                inner.LoadRef(v.getContext());
            });

            tvAudioCurrentChapterSelected.setOnClickListener(v ->
            {
                if (verse == null) return;

                inner.UpdateRef(false, v.getContext(), verse.bbName, verse.bNumber, verse.cNumber, verse.vNumber);

                _s.Say(verse.bbName, verse.bNumber, verse.cNumber, verse.vNumber);
            });

            //Text, Styles
            final String currentChapterText = verse == null
                    ? null
                    : PCommon.ConcaT(context.getString(R.string.mnuListenCurrentChapter),
                        " (",
                        verse.bName, " ", verse.cNumber, ".", verse.vNumber,
                        ")");

            if (currentChapterText == null)
            {
                tvAudioSep.setVisibility(View.GONE);
                tvAudioCurrentChapterSelected.setVisibility(View.GONE);
            }
            else
            {
                tvAudioCurrentChapterSelected.setText(currentChapterText);
            }

            //Load
            inner.LoadRef(context);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    static void SetInterlinearSpan(final FragmentActivity currentActivity, final boolean hasDetail, final SpannableStringBuilder span, final String word, final int from, final int to, final StyleBO dlgStyle)
    {
        final Context context = currentActivity.getApplicationContext();

        try
        {
            @SuppressLint("ResourceType") final float hgRelativeSize = Float.parseFloat(context.getString(R.dimen.hgRelativeSize));

            if (hasDetail)
            {
                final ClickableSpan clickSpan = new ClickableSpan()
                {
                    @Override
                    public void onClick(@NonNull final View v) {
                        if (PCommon._isDebug) System.out.println(PCommon.ConcaT("Clicked: ", word));
                        ShowInterlinearWordDetail(currentActivity, word, PCommon.LEX_DETAIL_SEARCH_TYPE.EXACT_SEARCH, dlgStyle);
                    }
                };
                span.setSpan(clickSpan, from, to, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                //Bigger
                span.setSpan(new RelativeSizeSpan(hgRelativeSize), from, to, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                //Colors
                span.setSpan(new ForegroundColorSpan(Color.RED), from, to, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                final UnderlineSpan underlineSpan = new UnderlineSpan() {
                    @Override
                    public void updateDrawState(@NonNull TextPaint ds) {
                        ds.setUnderlineText(false);
                    }
                };
                span.setSpan(underlineSpan, from, to, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            else if (PCommon._isDebug)
            {
                //Bigger
                span.setSpan(new RelativeSizeSpan(hgRelativeSize), from, to, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                //Colors
                span.setSpan(new ForegroundColorSpan(Color.BLACK), from, to, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                span.setSpan(new BackgroundColorSpan(Color.RED), from, to, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                final UnderlineSpan underlineSpan = new UnderlineSpan() {
                    @Override
                    public void updateDrawState(@NonNull TextPaint ds) {
                        ds.setUnderlineText(false);
                    }
                };
                span.setSpan(underlineSpan, from, to, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Get HGNr regex (full or partial)
     * @param shouldStartWithHG   True=Full (default), False=To search partially in lexicons
     * @return regex
     */
    static String GetHGNrRegex(final boolean shouldStartWithHG) {
        return shouldStartWithHG ? "^[H|G|h|g][0-9]+[a-zA-Z]*" : "^[H|G|h|g]*[0-9]+[a-zA-Z]*";
    }

    /***
     * Clean HGNr expression
     * @param context
     * @param word  Long word (dStrong could contains "= meaning of"...)
     * @return  Cleaned, trimmed or the same expr if there is an error
     */
    static String GetHGNrCleaned(final Context context, final String word)
    {
        try
        {
            final String[] arrWord = word.trim().split(" ");
            return arrWord[0];
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }

        return word;
    }

    static SpannableStringBuilder InspectInterlinearText(final FragmentActivity currentActivity, final String htmlText, final StyleBO dlgStyle)
    {
        final Context context = currentActivity.getApplicationContext();
        final Spanned spannedText = Html.fromHtml(htmlText);
        final SpannableStringBuilder spanText = new SpannableStringBuilder(spannedText);
        Map<String, Boolean> mapLex = new HashMap<>();
        String[] words = null;
        int searchInTotal = 0, searchHGSkipped = 0, searchOther = 0;

        try
        {
            final String text = spanText.toString();

            words = text.split("\\b");
            if (words.length == 0) return spanText;

            boolean hasDetail;
            int realPos = 0, wRegionEnd = 0;

            final Pattern pHebrew = Pattern.compile("\\p{InHebrew}", Pattern.UNICODE_CASE);
            final Pattern pGreek = Pattern.compile("\\p{InGreek}", Pattern.UNICODE_CASE);
            final Pattern pHGNr = Pattern.compile(GetHGNrRegex(true), Pattern.UNICODE_CASE);
            for (String w : words)
            {
                final String wTrimmed = w.trim();
                final Matcher mHebrew = pHebrew.matcher(wTrimmed);
                final Matcher mGreek = pGreek.matcher(wTrimmed);
                final Matcher mHGNr = pHGNr.matcher(wTrimmed);

                searchInTotal += 1;

                if (mHebrew.find())
                {
                    if (PCommon._isDebug) System.out.println(PCommon.ConcaT("HEBREW YES: ", wTrimmed));
                    wRegionEnd = mHebrew.regionEnd();

                    if (mapLex.containsKey(wTrimmed))
                    {
                        searchHGSkipped += 1;
                        hasDetail = Boolean.TRUE.equals(mapLex.get(wTrimmed));
                    }
                    else
                    {
                        hasDetail = _s.HasLexDetailByWord(wTrimmed, PCommon.LEX_DETAIL_SEARCH_TYPE.EXACT_SEARCH);
                        mapLex.put(wTrimmed, hasDetail);
                    }

                    SetInterlinearSpan(currentActivity, hasDetail, spanText, wTrimmed, realPos,realPos + wRegionEnd, dlgStyle);
                }
                else if (mGreek.find())
                {
                    if (PCommon._isDebug) System.out.println(PCommon.ConcaT("GREEK YES: ", wTrimmed));
                    wRegionEnd = mGreek.regionEnd();

                    if (mapLex.containsKey(wTrimmed))
                    {
                        searchHGSkipped += 1;
                        hasDetail = Boolean.TRUE.equals(mapLex.get(wTrimmed));
                    }
                    else
                    {
                        hasDetail = _s.HasLexDetailByWord(wTrimmed, PCommon.LEX_DETAIL_SEARCH_TYPE.EXACT_SEARCH);
                        mapLex.put(wTrimmed, hasDetail);
                    }

                    SetInterlinearSpan(currentActivity, hasDetail, spanText, wTrimmed, realPos,realPos + wRegionEnd, dlgStyle);
                }
                else if (mHGNr.find())
                {
                    if (PCommon._isDebug) System.out.println(PCommon.ConcaT("HGNr YES: ", wTrimmed));
                    wRegionEnd = mHGNr.regionEnd();

                    if (mapLex.containsKey(wTrimmed))
                    {
                        searchHGSkipped += 1;
                        hasDetail = Boolean.TRUE.equals(mapLex.get(wTrimmed));
                    }
                    else
                    {
                        hasDetail = _s.HasLexDetailByHGNr(wTrimmed);
                        mapLex.put(wTrimmed, hasDetail);
                    }

                    SetInterlinearSpan(currentActivity, hasDetail, spanText, wTrimmed, realPos,realPos + wRegionEnd, dlgStyle);
                }
                else
                {
                    if (PCommon._isDebug) System.out.println(PCommon.ConcaT("Other: ", wTrimmed));
                    searchOther += 1;
                }

                realPos += w.length();
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
        finally
        {
            if (PCommon._isDebug) System.out.println(PCommon.ConcaT("Interlinear stats: words:", words.length, ", HGLex:", mapLex.size(), ", skipped: ", searchHGSkipped, "/", (searchInTotal - searchOther)));

            mapLex.clear();
            words = null;
        }

        return spanText;
    }

    static SpannableStringBuilder GetBiggerHGText(final Context context, final String htmlText)
    {
        final Spanned spannedText = Html.fromHtml(htmlText);
        final SpannableStringBuilder spanText = new SpannableStringBuilder(spannedText);

        try
        {
            final String text = spanText.toString();
            String[] words = text.split("\\b");
            if (words.length == 0) return spanText;

            @SuppressLint("ResourceType") final float hgRelativeSize = Float.parseFloat(context.getString(R.dimen.hgRelativeSize));
            int realPos = 0, wRegionEnd = 0;

            final Pattern pHebrew = Pattern.compile("\\p{InHebrew}", Pattern.UNICODE_CASE);
            final Pattern pGreek = Pattern.compile("\\p{InGreek}", Pattern.UNICODE_CASE);
            for (String w : words)
            {
                final String wTrimmed = w.trim();
                final Matcher mHebrew = pHebrew.matcher(wTrimmed);
                final Matcher mGreek = pGreek.matcher(wTrimmed);

                if (mHebrew.find())
                {
                    wRegionEnd = mHebrew.regionEnd();
                    spanText.setSpan(new RelativeSizeSpan(hgRelativeSize), realPos, realPos + wRegionEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
                else if (mGreek.find())
                {
                    wRegionEnd = mGreek.regionEnd();
                    spanText.setSpan(new RelativeSizeSpan(hgRelativeSize), realPos, realPos + wRegionEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                }

                realPos += w.length();
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }

        return spanText;
    }
}

 /* Works:

    if (!isUiTelevision)
    {
        final String contentMsg = getString(R.string.toastRestartLong);
        final int waitDuration = Integer.parseInt(getString(R.string.snackbarWaitVeryLong));
        final Snackbar snackbar = Snackbar.make(llMain, contentMsg, waitDuration);
        snackbar.show();
    }
    else
    {
        PCommon.ShowToast(getApplicationContext(), R.string.toastRestartShort, Toast.LENGTH_LONG);
    }
*/