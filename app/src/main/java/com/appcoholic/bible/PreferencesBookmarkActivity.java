
package com.appcoholic.bible;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class PreferencesBookmarkActivity extends AppCompatActivity
{
    private SCommon _s = null;
    private Context context = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        try
        {
            super.onCreate(savedInstanceState);

            context = getApplicationContext();
            CheckLocalInstance(context);

            PCommon.SetLocale(PreferencesBookmarkActivity.this, -1, false);

            final int themeId = PCommon.GetPrefThemeId(context);
            setTheme(themeId);
            //TODO: FAB, here
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }


    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    /***
     * Check local instance (to copy reader all activities that use it)
     */
    private void CheckLocalInstance(final Context context)
    {
        try
        {
            if (_s == null) _s = SCommon.GetInstance(context);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }
}
