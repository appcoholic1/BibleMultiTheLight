package com.appcoholic.bible;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.TypedValue;
import android.widget.RemoteViews;

import java.util.Objects;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

public class BibleWidgetVerseService extends Service
{
    private Context _context = null;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();

        _context = getApplicationContext();

        //Notify(_context); //TODO: Disabled since targeting Android 12
    }

    private void Notify(final Context context)
    {
        try
        {
            //Not necessary for old Android
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) return;

            final String longContentText = "Please turn off this unused notification channel, mandatory for foreground services!";
            final String channelId = "thelight_widget_channel_id";
            final NotificationManager notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);

            final Intent intentOpen = new Intent(context, MainActivity.class);
            intentOpen.putExtra("NOTIF_ID", 2);

            final PendingIntent pendingIntentOpen = PendingIntent.getActivity(
                    context,
                    0,
                    intentOpen,
                    PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_IMMUTABLE);

            final NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, channelId)
                    .setAutoCancel(true)
                    .setPriority(NotificationCompat.PRIORITY_LOW)
                    .setVisibility(NotificationCompat.VISIBILITY_SECRET)
                    .setSmallIcon(R.drawable.thelightnotif)
                    .setContentTitle(context.getString(R.string.channelWidgetBibleName))
                    .setCategory(NotificationCompat.CATEGORY_STATUS)
                    .setColorized(true)
                    .setWhen(System.currentTimeMillis())
                    .setShowWhen(false)
                    .setContentIntent(pendingIntentOpen)
                    .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL);

            notificationBuilder.setContentText(longContentText);
            notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(longContentText));

            final int channelImportance = NotificationManager.IMPORTANCE_LOW;
            final String channelName = context.getString(R.string.channelWidgetBibleName);
            final NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, channelImportance);
            notificationChannel.setDescription(channelName);
            //notificationChannel.setAllowBubbles(false);       //TODO NEXT: bug on my phone
            notificationChannel.setShowBadge(false);
            notificationChannel.enableLights(false);
            notificationChannel.enableVibration(false);

            Objects.requireNonNull(notificationManager).createNotificationChannel(notificationChannel);

            startForeground(2, notificationBuilder.build());

            final Handler hdl = new Handler(Looper.getMainLooper());
            hdl.postDelayed(() -> {
                try
                {
                    notificationManager.deleteNotificationChannel(channelId);
                }
                catch(Exception ex)
                {
                    if (PCommon._isDebug) PCommon.LogR(context, ex);
                }
            },500);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        try
        {
            this.startForegroundService(intent);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return START_NOT_STICKY;
    }

    @Override
    public ComponentName startForegroundService(Intent intent)
    {
        try
        {
            final SCommon _s = CommonWidget.CheckLocalInstance(_context);

            final String action = intent.getAction();
            switch (Objects.requireNonNull(action))
            {
                case "WIDGET_DOWN_CLICK":
                {
                    //*** Get params
                    final AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(_context);
                    final int appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);
                    final int id = intent.getIntExtra("ID", -1);
                    final String vText = intent.getStringExtra("VTEXT");

                    final String[] words = Objects.requireNonNull(vText).split("\\s");
                    if (words.length < 5) break;

                    int i = 0;
                    final StringBuilder sbText = new StringBuilder();
                    for (String word : words)
                    {
                        if (i >= 3)
                        {
                            sbText.append(word);
                            sbText.append(" ");
                        }
                        else i++;
                    }
                    final String newVText = sbText.toString();

                    //*** Update event
                    final Intent intent_DOWN_CLICK = new Intent(_context, BibleWidgetVerseService.class);
                    intent_DOWN_CLICK.setAction("WIDGET_DOWN_CLICK");
                    intent_DOWN_CLICK.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
                    intent_DOWN_CLICK.putExtra("ID", id);
                    intent_DOWN_CLICK.putExtra("VTEXT", newVText);

                    final int flags = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) ? PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE : PendingIntent.FLAG_UPDATE_CURRENT;
                    final PendingIntent pendingIntent_DOWN_CLICK;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                    {
                        pendingIntent_DOWN_CLICK = PendingIntent.getForegroundService(_context, appWidgetId, intent_DOWN_CLICK, flags);
                    }
                    else
                    {
                        pendingIntent_DOWN_CLICK = PendingIntent.getService(_context, appWidgetId, intent_DOWN_CLICK, flags);
                    }

                    //*** Set components
                    CommonWidget.SaveTheme(_context, CommonWidget.MODE.VERSE);

                    final RemoteViews views = new RemoteViews(_context.getPackageName(), CommonWidget.WIDGET_LAYOUT_ID);
                    views.setTextViewTextSize(R.id.widget_tv_text, TypedValue.COMPLEX_UNIT_SP, 18f);
                    views.setTextViewText(R.id.widget_tv_text, newVText);
                    views.setOnClickPendingIntent(R.id.widget_btn_down, pendingIntent_DOWN_CLICK);

                    //*** Update widget
                    appWidgetManager.updateAppWidget(appWidgetId, views);

                    break;
                }
                case "WIDGET_LANG_CLICK":
                {
                    //*** Get params
                    final int appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);

                    final String bbName = intent.getStringExtra("BBNAME");
                    final int bNumber = intent.getIntExtra("BNUMBER", -1);
                    final int cNumber = intent.getIntExtra("CNUMBER", -1);
                    final int vNumber = intent.getIntExtra("VNUMBER", -1);

                    //*** Update verse
                    final String rolledBBNAME = CommonWidget.RollBookName(_context, bbName);
                    final WidgetVerseBO widgetVerse = CommonWidget.GetWidgetVerse(_context, rolledBBNAME, bNumber, cNumber, vNumber);
                    if (PCommon._isDebug) System.out.println("onReceive => appWidgetId: " + appWidgetId + ", bbname: " + bbName + ", rolledbbname: " + rolledBBNAME);

                    //*** Update widget
                    CommonWidgetVerse.UpdateAppWidget(_context, appWidgetId, widgetVerse);

                    break;
                }
                case "WIDGET_REFRESH_CLICK":
                {
                    //*** Get params
                    final int appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);

                    //*** Update widget
                    CommonWidgetVerse.UpdateAppWidget(_context, appWidgetId, null);

                    break;
                }
                case "WIDGET_PREV_CLICK":
                {
                    //*** Get params
                    final int appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);
                    final int id = intent.getIntExtra("ID", -1) - 1;

                    //*** Update verse
                    final VerseBO verse = _s.GetVerse(id);
                    final WidgetVerseBO widgetVerse = (verse == null) ? null : CommonWidget.GetWidgetVerse(_context, verse.bbName, verse.bNumber, verse.cNumber, verse.vNumber);

                    //*** Update widget
                    CommonWidgetVerse.UpdateAppWidget(_context, appWidgetId, widgetVerse);

                    break;
                }
                case "WIDGET_FORWARD_CLICK":
                {
                    //*** Get params
                    final int appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);
                    final int id = intent.getIntExtra("ID", -1) + 1;

                    //*** Update verse
                    final VerseBO verse = _s.GetVerse(id);
                    final WidgetVerseBO widgetVerse = (verse == null) ? null : CommonWidget.GetWidgetVerse(_context, verse.bbName, verse.bNumber, verse.cNumber, verse.vNumber);

                    //*** Update widget
                    CommonWidgetVerse.UpdateAppWidget(_context, appWidgetId, widgetVerse);

                    break;
                }
                case "WIDGET_FAV_CLICK":
                {
                    //*** Get params
                    final int appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, -1);
                    final int id = intent.getIntExtra("ID", -1);

                    //*** Update verse
                    final VerseBO verse = _s.GetVerse(id);
                    if (verse == null) break;

                    if (verse.mark > 0)
                    {
                        _s.DeleteNote(verse.bNumber, verse.cNumber, verse.vNumber);
                    }
                    else
                    {
                        final String changeDt = PCommon.NowYYYYMMDD();
                        final NoteBO note = new NoteBO(verse.bNumber, verse.cNumber, verse.vNumber, changeDt, 1);
                        _s.SaveNote(note);
                    }
                    final WidgetVerseBO widgetVerse = CommonWidget.GetWidgetVerse(_context, verse.bbName, verse.bNumber, verse.cNumber, verse.vNumber);

                    //*** Update widget
                    CommonWidgetVerse.UpdateAppWidget(_context, appWidgetId, widgetVerse);

                    break;
                }
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return null;
    }
}
