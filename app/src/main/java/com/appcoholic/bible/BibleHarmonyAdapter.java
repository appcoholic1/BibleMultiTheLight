
package com.appcoholic.bible;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

class BibleHarmonyAdapter extends RecyclerView.Adapter<BibleHarmonyAdapter.ViewHolder>
{
    ArrayList<Object> lstVerse = null;
    private int[] harmScrollPosY = null;
    private int[] harmCount = null;
    private int indexHarmScrollPosY = -1;
    private Map<Integer, String> fav = null;
    private SCommon _s = null;
    private Context context = null;

    private void SetMark(final Context context)
    {
        //TODO: FAB, remove IProject.APP_PREF_KEY.FAV_SYMBOL
        this.fav = _s.GenerateBookmarkShortMap(context);
    }

    BibleHarmonyAdapter(final Context context, final int harmonyId, final String bbName, final TextView tvBookTitle, final TextView btnHarmMt, final TextView btnHarmMc, final TextView btnHarmLc, final TextView btnHarmJn)
    {
        try
        {
            CheckLocalInstance(context);
            SetMark(context);

            //final String bbName = tbbName.substring(0, 1);
            final String tbbName = bbName; //TODO: HARM, to change (simplify var used)
            final String[] harmTitleArr = context.getResources().getStringArray(R.array.HARMONY_TITLE_ARRAY);
            final String[] harmTitleRow = harmTitleArr[harmonyId - 1].split("\\|");
            final String harmTitleDisplayed = PCommon.ConcaT(harmonyId, ". ", harmTitleRow[1].toUpperCase(), " (", harmTitleRow[2].toUpperCase(), ")");
            final String[] harmDataArr = context.getResources().getStringArray(R.array.HARMONY_DATA_ARRAY);
            final String[] harmDataRow = harmDataArr[harmonyId - 1].split("\\|");
            final String harmMt = (harmDataRow.length >= 2) ? harmDataRow[1] : "";
            final String harmMc = (harmDataRow.length >= 3) ? harmDataRow[2] : "";
            final String harmLc = (harmDataRow.length >= 4) ? harmDataRow[3] : "";
            final String harmJn = (harmDataRow.length >= 5) ? harmDataRow[4] : "";

            final BibleRefBO harmMtRef = _s.GetBookRef(bbName, 40);
            final BibleRefBO harmMcRef = _s.GetBookRef(bbName, 41);
            final BibleRefBO harmLcRef = _s.GetBookRef(bbName, 42);
            final BibleRefBO harmJnRef = _s.GetBookRef(bbName, 43);

            this.lstVerse = new ArrayList<>();
            this.harmScrollPosY = new int[4];    //MT, MC, LC, JN
            this.harmCount = new int[4];    //MT, MC, LC, JN

            class InnerClass
            {
                private void SaveTitleScrollPosY()
                {
                    indexHarmScrollPosY++;
                    if (indexHarmScrollPosY > 3) return;
                    harmScrollPosY[indexHarmScrollPosY] = lstVerse.size();
                }

                private void AddBookTitleEmpty(final BibleRefBO harmBookRef)
                {
                    SaveTitleScrollPosY();
                    lstVerse.add(PCommon.ConcaT("<br><u>", harmBookRef.bName, "</u>: /<br>"));

                    final int additionalCell = tbbName.length() - 1;
                    for(int i = 1; i <= additionalCell; i++) lstVerse.add("");
                }

                private void AddBookTitleWithVerseLimit(final BibleRefBO harmBookRef, final int cNumber, final int vNumberFrom, final int vNumberTo)
                {
                    SaveTitleScrollPosY();
                    harmCount[harmBookRef.bNumber - 40]++;
                    lstVerse.add(PCommon.ConcaT("<br><u>", harmBookRef.bName, ": ", cNumber, ".", vNumberFrom, "-", vNumberTo, "</u><br>"));

                    final int additionalCell = tbbName.length() - 1;
                    for(int i = 1; i <= additionalCell; i++) lstVerse.add("");
                }

                void AddVersesOfGospel(final String harmFieldGospel, final BibleRefBO harmGospelRef)
                {
                    try
                    {
                        if (!harmFieldGospel.isEmpty())
                        {
                            final String[] refArr = harmFieldGospel.split("\\s");
                            final int bNumber = Integer.parseInt(refArr[0]);
                            final int cNumber = Integer.parseInt(refArr[1]);
                            final int vNumberFrom = Integer.parseInt(refArr[2]);
                            final int vNumberTo = (refArr.length <= 3) ? vNumberFrom : Integer.parseInt(refArr[3]);
                            AddBookTitleWithVerseLimit(harmGospelRef, cNumber, vNumberFrom, vNumberTo);

                            final ArrayList<VerseBO> lstHarmVerse = _s.GetVerses(tbbName, bNumber, cNumber, vNumberFrom, vNumberTo);
                            lstVerse.addAll(lstHarmVerse);
                        }
                        else
                        {
                            AddBookTitleEmpty(harmGospelRef);
                        }
                    }
                    catch (Exception ex)
                    {
                        if (PCommon._isDebug) PCommon.LogR(context, ex);
                    }
                }
            }
            final InnerClass innerClass = new InnerClass();
            innerClass.AddVersesOfGospel(harmMt, harmMtRef);
            innerClass.AddVersesOfGospel(harmMc, harmMcRef);
            innerClass.AddVersesOfGospel(harmLc, harmLcRef);
            innerClass.AddVersesOfGospel(harmJn, harmJnRef);

            this.SaveCacheSearch(context);

            tvBookTitle.setText(harmTitleDisplayed);
            btnHarmMt.setText(harmMtRef.bsName);
            btnHarmMc.setText(harmMcRef.bsName);
            btnHarmLc.setText(harmLcRef.bsName);
            btnHarmJn.setText(harmJnRef.bsName);

            btnHarmMt.setTag(harmScrollPosY[0]);
            btnHarmMc.setTag(harmScrollPosY[1]);
            btnHarmLc.setTag(harmScrollPosY[2]);
            btnHarmJn.setTag(harmScrollPosY[3]);

            btnHarmMt.setEnabled(harmCount[0] > 0);
            btnHarmMc.setEnabled(harmCount[1] > 0);
            btnHarmLc.setEnabled(harmCount[2] > 0);
            btnHarmJn.setEnabled(harmCount[3] > 0);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        private final TextView tv_title;
        private final CardView cv;
        private final TextView tv_ref;
        private final TextView tv_text;
        private final TextView tv_mark;
        private final TextView tv_cr;

        ViewHolder(View view)
        {
            super(view);

            context = view.getContext();
            tv_title = view.findViewById(R.id.tv_title);
            cv = view.findViewById(R.id.cv);
            tv_ref = view.findViewById(R.id.tv_ref);
            tv_text = view.findViewById(R.id.tv_text);
            tv_cr = view.findViewById(R.id.tv_cr);
            tv_mark = view.findViewById(R.id.tv_mark);

            final Typeface typeface = PCommon.GetTypeface(view.getContext());
            if (typeface != null)
            {
                tv_title.setTypeface(typeface, Typeface.BOLD);
                tv_ref.setTypeface(typeface, Typeface.BOLD);
                tv_text.setTypeface(typeface);
                tv_cr.setTypeface(typeface);
            }

            final int fontSize = PCommon.GetFontSize(view.getContext());
            tv_title.setTextSize(fontSize + 2);
            if (tv_ref != null) tv_ref.setTextSize(fontSize);
            tv_text.setTextSize(fontSize);
            tv_cr.setTextSize(fontSize);
            tv_mark.setTextSize(fontSize);
        }
    }

    @NonNull
    @Override
    public BibleHarmonyAdapter.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, final int viewType)
    {
        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.harmony_recipient, viewGroup, false);
        return new BibleHarmonyAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final BibleHarmonyAdapter.ViewHolder viewHolder, final int position)
    {
        try
        {
            //Current object
            final VerseBO verse;
            final Object obj = lstVerse.get(position);
            if (obj instanceof VerseBO)
            {
                verse = (VerseBO) obj;
            }
            else
            {
                verse = null;
            }

            if (verse == null)
            {
                viewHolder.cv.setVisibility(View.GONE);
                viewHolder.tv_title.setVisibility(View.VISIBLE);
                viewHolder.tv_title.setText(Html.fromHtml(obj.toString()));
            }
            else
            {
                viewHolder.tv_title.setVisibility(View.GONE);
                viewHolder.cv.setVisibility(View.VISIBLE);

                final String ref = PCommon.ConcaT(verse.bName, " ", verse.cNumber, ".", verse.vNumber);
                final String crCount = verse.crCount > 0 ? String.valueOf(verse.crCount) : "";

                //Mark
                if (verse.mark > 0)
                {
                    viewHolder.tv_mark.setPadding(10, 0, 5, 0);
                    viewHolder.tv_mark.setText( fav.get(verse.mark) );
                }

                //Ref
                if (viewHolder.tv_ref != null)
                {
                    viewHolder.tv_ref.setText(ref);
                    viewHolder.tv_ref.setId(verse.id);
                    viewHolder.tv_ref.setTag(position);
                }

                //Text
                viewHolder.tv_text.setText(verse.vText);
                viewHolder.tv_text.setId(verse.id);
                viewHolder.tv_text.setTag(position);

                viewHolder.tv_cr.setText(crCount);

                //Events
                if (viewHolder.tv_ref != null)
                {
                    viewHolder.tv_ref.setOnClickListener(v -> {
                        final TextView tvRef = (TextView) v;
                        if (tvRef == null) return;

                        final int bibleId = tvRef.getId();
                        final int position1 = Integer.parseInt(tvRef.getTag().toString());
                        PCommon.SavePrefInt(v.getContext(), IProject.APP_PREF_KEY.BIBLE_ID, bibleId);
                        PCommon.SavePrefInt(v.getContext(), IProject.APP_PREF_KEY.VIEW_POSITION, position1);

                        v.showContextMenu();
                    });
                }

                viewHolder.tv_text.setOnClickListener(v -> {
                    final TextView tvText = (TextView) v;
                    if (tvText == null) return;

                    final int bibleId = tvText.getId();
                    final int position12 = Integer.parseInt(tvText.getTag().toString());
                    PCommon.SavePrefInt(v.getContext(), IProject.APP_PREF_KEY.BIBLE_ID, bibleId);
                    PCommon.SavePrefInt(v.getContext(), IProject.APP_PREF_KEY.VIEW_POSITION, position12);

                    v.showContextMenu();
                });
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    @Override
    public int getItemCount()
    {
        return lstVerse == null ? 0 : lstVerse.size();
    }

    @Override
    public int getItemViewType(int position)
    {
        final Object obj = lstVerse.get(position);
        if (obj instanceof VerseBO)
        {
            final VerseBO verse = (VerseBO) obj;
            return verse.mark;
        }

        return 0;
    }

    private void CheckLocalInstance(final Context context)
    {
        try
        {
            if (_s == null)
            {
                _s = SCommon.GetInstance(context);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    private void SaveCacheSearch(final Context context)
    {
        try
        {
            ArrayList<Integer> lstId = new ArrayList<>();
            if (lstVerse != null)
            {
                VerseBO verse;
                for (Object obj : lstVerse)
                {
                    if (obj instanceof VerseBO)
                    {
                        verse = (VerseBO) obj;
                        lstId.add(verse.id);
                    }
                }
            }

            _s.SaveCacheSearch(lstId);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }
}

