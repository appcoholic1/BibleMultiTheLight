package com.appcoholic.bible;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.widget.RemoteViews;

final class CommonWidgetAudio
{
    static void UpdateAppWidget(final Context context, final int appWidgetId, final String bbName, final String vRef)
    {
        try
        {
            if (PCommon._isDebug) System.out.println("My UpdateAppWidget => " + appWidgetId);

            //*** Set params
            final Intent intent_LANG_CLICK = new Intent(context, BibleWidgetAudioService.class);
            intent_LANG_CLICK.setAction("WIDGET_LANG_CLICK");
            intent_LANG_CLICK.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);

            final Intent intent_START_CLICK = new Intent(context, BibleWidgetAudioService.class);
            intent_START_CLICK.setAction("WIDGET_START_CLICK");
            intent_START_CLICK.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);

            final Intent intent_BACK_CLICK = new Intent(context, BibleWidgetAudioService.class);
            intent_BACK_CLICK.setAction("WIDGET_BACK_CLICK");
            intent_BACK_CLICK.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);

            final Intent intent_PLAY_CLICK = new Intent(context, BibleWidgetAudioService.class);
            intent_PLAY_CLICK.setAction("WIDGET_PLAY_CLICK");
            intent_PLAY_CLICK.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);

            final Intent intent_STOP_CLICK = new Intent(context, BibleWidgetAudioService.class);
            intent_STOP_CLICK.setAction("WIDGET_STOP_CLICK");
            intent_STOP_CLICK.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);

            final Intent intent_FORWARD_CLICK = new Intent(context, BibleWidgetAudioService.class);
            intent_FORWARD_CLICK.setAction("WIDGET_FORWARD_CLICK");
            intent_FORWARD_CLICK.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);

            final Intent intent_END_CLICK = new Intent(context, BibleWidgetAudioService.class);
            intent_END_CLICK.setAction("WIDGET_END_CLICK");
            intent_END_CLICK.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);

            //*** Set events
            final PendingIntent pendingIntent_LANG_CLICK;
            final PendingIntent pendingIntent_START_CLICK;
            final PendingIntent pendingIntent_BACK_CLICK;
            final PendingIntent pendingIntent_PLAY_CLICK;
            final PendingIntent pendingIntent_STOP_CLICK;
            final PendingIntent pendingIntent_FORWARD_CLICK;
            final PendingIntent pendingIntent_END_CLICK;
            final int flags = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) ? PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE : PendingIntent.FLAG_UPDATE_CURRENT;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            {
                pendingIntent_LANG_CLICK = PendingIntent.getForegroundService(context, appWidgetId, intent_LANG_CLICK, flags);
                pendingIntent_START_CLICK = PendingIntent.getForegroundService(context, appWidgetId, intent_START_CLICK, flags);
                pendingIntent_BACK_CLICK = PendingIntent.getForegroundService(context, appWidgetId, intent_BACK_CLICK, flags);
                pendingIntent_PLAY_CLICK = PendingIntent.getForegroundService(context, appWidgetId, intent_PLAY_CLICK, flags);
                pendingIntent_STOP_CLICK = PendingIntent.getForegroundService(context, appWidgetId, intent_STOP_CLICK, flags);
                pendingIntent_FORWARD_CLICK = PendingIntent.getForegroundService(context, appWidgetId, intent_FORWARD_CLICK, flags);
                pendingIntent_END_CLICK = PendingIntent.getForegroundService(context, appWidgetId, intent_END_CLICK, flags);
            }
            else
            {
                pendingIntent_LANG_CLICK = PendingIntent.getService(context, appWidgetId, intent_LANG_CLICK, flags);
                pendingIntent_START_CLICK = PendingIntent.getService(context, appWidgetId, intent_START_CLICK, flags);
                pendingIntent_BACK_CLICK = PendingIntent.getService(context, appWidgetId, intent_BACK_CLICK, flags);
                pendingIntent_PLAY_CLICK = PendingIntent.getService(context, appWidgetId, intent_PLAY_CLICK, flags);
                pendingIntent_STOP_CLICK = PendingIntent.getService(context, appWidgetId, intent_STOP_CLICK, flags);
                pendingIntent_FORWARD_CLICK = PendingIntent.getService(context, appWidgetId, intent_FORWARD_CLICK, flags);
                pendingIntent_END_CLICK = PendingIntent.getService(context, appWidgetId, intent_END_CLICK, flags);
            }

            //*** Set components
            CommonWidget.SaveTheme(context, CommonWidget.MODE.AUDIO);

            final String lang = bbName.compareToIgnoreCase("k") == 0 ? "EN1611"
                    : bbName.compareToIgnoreCase("v") == 0 ? "ES"
                    : bbName.compareToIgnoreCase("l") == 0 ? "FRS"
                    : bbName.compareToIgnoreCase("o") == 0 ? "FRO"
                    : bbName.compareToIgnoreCase("d") == 0 ? "IT"
                    : bbName.compareToIgnoreCase("a") == 0 ? "PT"
                    : "EN2000";

            final RemoteViews views = new RemoteViews(context.getPackageName(), CommonWidget.WIDGET_LAYOUT_ID);
            views.setTextViewText(R.id.widget_play_tv_lang, lang);
            views.setTextViewText(R.id.widget_play_tv_ref, vRef);

            views.setOnClickPendingIntent(R.id.widget_play_tv_lang, pendingIntent_LANG_CLICK);
            views.setOnClickPendingIntent(R.id.widget_play_btn_start, pendingIntent_START_CLICK);
            views.setOnClickPendingIntent(R.id.widget_play_btn_back, pendingIntent_BACK_CLICK);
            views.setOnClickPendingIntent(R.id.widget_play_btn_play, pendingIntent_PLAY_CLICK);
            views.setOnClickPendingIntent(R.id.widget_play_btn_stop, pendingIntent_STOP_CLICK);
            views.setOnClickPendingIntent(R.id.widget_play_btn_forward, pendingIntent_FORWARD_CLICK);
            views.setOnClickPendingIntent(R.id.widget_play_btn_end, pendingIntent_END_CLICK);

            //*** Update widget
            final AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }
}
