
package com.appcoholic.bible;

public interface IProject
{
    enum APP_PREF_KEY
    {
        LOG_STATUS,

        INSTALL_STATUS,

        UPDATE_STATUS,

        EDIT_STATUS,

        EDIT_DIALOG,

        EDIT_ART_ID,

        EDIT_SELECTION,

        BIBLE_APP_TYPE,

        BIBLE_NAME,

        BIBLE_NAME_DIALOG,

        BOOK_CHAPTER_DIALOG,

        TRAD_BIBLE_NAME,

        BIBLE_ID,

        CLIPBOARD_IDS,

        LAYOUT_DYNAMIC_1,

        LAYOUT_DYNAMIC_2,

        LAYOUT_DYNAMIC_3,

        LAYOUT_DYNAMIC_4,

        LAYOUT_DYNAMIC_5,

        LAYOUT_DYNAMIC_6,

        LISTEN_POSITION,

        THEME_NAME,

        FONT_NAME,

        FONT_HINDI_NAME,

        FONT_SIZE,

        FAV_FILTER,

        FAV_ORDER,

        VIEW_POSITION,

        TAB_SELECTED,

        PLAN_ID,

        PLAN_PAGE,

        UI_LAYOUT,

        UI_LAYOUT_TV_BORDERS_DIALOG,

        UI_LAYOUT_TV_BORDERS,

        MENU_DIALOG,

        TODO_STATUS,

        STYLE_HIGHLIGHT_SEARCH,

        RANDOM_CATS_SELECTED,

        RANDOM_BOOKS_SELECTED,

        ALT_LANGUAGE
    }
}
