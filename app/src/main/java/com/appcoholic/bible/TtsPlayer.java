package com.appcoholic.bible;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.media3.common.AudioAttributes;
import androidx.media3.common.C;
import androidx.media3.common.MediaItem;
import androidx.media3.common.MediaMetadata;
import androidx.media3.common.Player;
import androidx.media3.common.SimpleBasePlayer;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

public class TtsPlayer extends SimpleBasePlayer
{
    private Context _context = null;
    private SCommon _s = null;
    private State state = null;
    private MediaMetadata mediaMetadata = null;

    protected TtsPlayer(Context context, Looper applicationLooper)
    {
        super(applicationLooper);

        _context = context;

        if (PCommon._isDebug) System.out.println("TtsPlayer()...");

        CheckLocalInstance(_context);

        state = new State.Builder()
                .setAvailableCommands(new Commands.Builder().addAll(
                        Player.COMMAND_PLAY_PAUSE,
                        Player.COMMAND_GET_METADATA)
                        .build())
                .setPlayWhenReady(false, Player.PLAY_WHEN_READY_CHANGE_REASON_USER_REQUEST)
                .setAudioAttributes(new AudioAttributes.Builder()
                        .setUsage(C.USAGE_MEDIA)
                        .setContentType(C.AUDIO_CONTENT_TYPE_SPEECH)
                        .build())
                .build();

        this.updatePlaybackState(STATE_READY, true);
    }

    @NonNull
    @Override
    protected State getState()
    {
        if (PCommon._isDebug) System.out.println("TtsPlayer.getState...");

        return state;
    }

    private void updatePlaybackState(int playbackState, Boolean playWhenReady)
    {
        if (PCommon._isDebug) System.out.println(PCommon.ConcaT( "TtsPlayer.updatePlaybackState: playbackState=", playbackState, ", playWhenReady=", playWhenReady));

        CheckLocalInstance(_context);

        if (mediaMetadata == null)
        {
            final Bitmap bitmap = BitmapFactory.decodeResource(_context.getResources(), R.drawable.thelightlogo_playing);
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0, outputStream);
            final byte[] artworkData = outputStream.toByteArray();

            mediaMetadata = new MediaMetadata.Builder()
                    .setArtworkData(artworkData, MediaMetadata.PICTURE_TYPE_OTHER)
                    .build();
        }

        final String notificationTitle = _s.TTSGetNotificationTitle();
        mediaMetadata = mediaMetadata.buildUpon()
                .setTitle(notificationTitle == null ? "..." : notificationTitle)
                .setMediaType(MediaMetadata.MEDIA_TYPE_AUDIO_BOOK_CHAPTER)
                .build();

        final MediaItem mediaItem = new MediaItem.Builder()
                .setMediaId("THELIGHT_MEDIA")
                .setMediaMetadata(mediaMetadata).build();

        final ArrayList<MediaItemData> lstMediaItemData = new ArrayList<>();
        lstMediaItemData.add(new MediaItemData.Builder("THELIGHT_MEDIAITEMDATA").setMediaItem(mediaItem).setMediaMetadata(mediaMetadata).build());

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(() ->
        {
            state = state.buildUpon()
                    .setPlaybackState(playbackState)
                    .setPlayWhenReady(playWhenReady, Player.PLAY_WHEN_READY_CHANGE_REASON_USER_REQUEST)
                    .setPlaylist(lstMediaItemData)
                    .build();

            invalidateState();
        });

        if (!playWhenReady) _s.SayStop();
    }

    @NonNull
    @Override
    protected ListenableFuture<?> handleSetPlayWhenReady(boolean playWhenReady)
    {
        if (PCommon._isDebug) System.out.println(PCommon.ConcaT( "TtsPlayer.handleSetPlayWhenReady: playWhenReady=", playWhenReady, ", state.playbackState=", getPlaybackState()));

        updatePlaybackState(playWhenReady ? STATE_READY : STATE_ENDED, playWhenReady);

        return Futures.immediateVoidFuture();
    }

    /***
     * Check local instance
     */
    private void CheckLocalInstance(final Context context)
    {
        try
        {
            if (_s == null) _s = SCommon.GetInstance(context);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }
}

//Source: https://github.com/androidx/media/issues/400

