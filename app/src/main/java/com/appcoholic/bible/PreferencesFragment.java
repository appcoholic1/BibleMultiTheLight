
package com.appcoholic.bible;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;

public class PreferencesFragment extends PreferenceFragmentCompat
{
    @Override
    public void onCreatePreferences(@Nullable Bundle savedInstanceState, @Nullable String rootKey)
    {
        try
        {
            setPreferencesFromResource(R.xml.preferences, rootKey);

            final PreferenceManager prefManager = getPreferenceManager();

            final String bbName = PCommon.GetPrefBibleName(getContext());
            final ListPreference prefBibleName = prefManager.findPreference("BIBLE_NAME");
            if (prefBibleName != null)
            {
                prefBibleName.setOnPreferenceChangeListener((preference, o) -> {
                    String altLanguage = "en";
                    final String bbname = o.toString();
                    PCommon.SavePref(preference.getContext(), IProject.APP_PREF_KEY.BIBLE_NAME, bbname);
                    switch(bbname) {
                        case "v":
                        case "9":
                            altLanguage = "es";
                            break;
                        case "l":
                        case "o":
                            altLanguage = "fr";
                            break;
                        case "d":
                        case "1":
                            altLanguage = "it";
                            break;
                        case "a":
                            altLanguage = "pt";
                            break;
                        case "i":
                            altLanguage = "hi";
                            break;
                        default:
                            altLanguage = "en";
                            break;
                    }
                    PCommon.SavePref(preference.getContext(), IProject.APP_PREF_KEY.ALT_LANGUAGE, altLanguage);

                    if (PCommon._isDebug) PCommon.LogRMainVars(preference.getContext(), "PreferencesFragment: prefBibleName change");

                    SetRestart(); //SetResultOk();
                    return true;
                });
            }

            final ListPreference prefAltLanguage = prefManager.findPreference("ALT_LANGUAGE");
            prefAltLanguage.setEnabled(bbName.equalsIgnoreCase("i") || bbName.equalsIgnoreCase("y") || bbName.equalsIgnoreCase("w") || bbName.equalsIgnoreCase("c") || bbName.equalsIgnoreCase("s") || bbName.equalsIgnoreCase("j") || bbName.equalsIgnoreCase("u") || bbName.equalsIgnoreCase("z") || bbName.equalsIgnoreCase("r") || bbName.equalsIgnoreCase("t") || bbName.equalsIgnoreCase("b") || bbName.equalsIgnoreCase("e") || bbName.equalsIgnoreCase("h"));
            if (prefAltLanguage != null)
            {
                prefAltLanguage.setOnPreferenceChangeListener((preference, o) -> {
                    PCommon.SavePref(preference.getContext(), IProject.APP_PREF_KEY.ALT_LANGUAGE, o.toString());

                    if (PCommon._isDebug) PCommon.LogRMainVars(preference.getContext(), "PreferencesFragment: prefAltLanguage change");

                    SetResultOk();
                    return true;
                });
            }

            final ListPreference prefThemeName = prefManager.findPreference("THEME_NAME");
            if (prefThemeName != null)
            {
                if (Build.VERSION.SDK_INT < 31) prefThemeName.setSummary(R.string.mnuGroupThemeUiLayoutSummary);
                prefThemeName.setOnPreferenceChangeListener((preference, o) -> {
                    final String themeName = o.toString();
                    PCommon.SetThemeName(preference.getContext(), themeName);
                    SetRestart();
                    return true;
                });
            }

            final ListPreference prefLayoutColumn = prefManager.findPreference("LAYOUT_COLUMN");
            if (prefLayoutColumn != null) {
                prefLayoutColumn.setOnPreferenceChangeListener((preference, o) -> {
                    SetResultOk();
                    return true;
                });
            }

            final ListPreference prefUiLayout = prefManager.findPreference("UI_LAYOUT");
            if (prefUiLayout != null) {
                prefUiLayout.setOnPreferenceChangeListener((preference, o) -> {
                    SetResultOk();
                    return true;
                });
            }

            final androidx.preference.Preference prefStyleHighlightSearch = prefManager.findPreference("STYLE_HIGHLIGHT_SEARCH");
            if (prefStyleHighlightSearch != null) {
                prefStyleHighlightSearch.setOnPreferenceChangeListener((preference, o) -> {
                    SetResultOk();
                    return true;
                });
            }

            final androidx.preference.Preference prefUiLayoutTvBordersWhenSave = prefManager.findPreference("UI_LAYOUT_TV_BORDERS_SAVE");
            if (prefUiLayoutTvBordersWhenSave != null) {
                prefUiLayoutTvBordersWhenSave.setOnPreferenceClickListener(preference -> {
                    final String borders = PCommon.GetUiLayoutTVBorders(preference.getContext(), PCommon.APP_PREF_KEY.UI_LAYOUT_TV_BORDERS_DIALOG);
                    PCommon.SavePref(preference.getContext(), IProject.APP_PREF_KEY.UI_LAYOUT_TV_BORDERS, borders);
                    SetResultOk();
                    return true;
                });
            }

            final Preference prefUiLayoutTvBordersWhenReset = prefManager.findPreference("UI_LAYOUT_TV_BORDERS_RESET");
            if (prefUiLayoutTvBordersWhenReset != null) {
                prefUiLayoutTvBordersWhenReset.setOnPreferenceClickListener(preference -> {
                    final String borders = PCommon.GetUiLayoutTVBorders(preference.getContext(),null);
                    PCommon.SavePref(preference.getContext(), IProject.APP_PREF_KEY.UI_LAYOUT_TV_BORDERS, borders);
                    SetResultOk();
                    return true;
                });
            }

            if (Build.VERSION.SDK_INT < 31) {
                SetResultOk();
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(getContext(), ex);
        }
    }

    private void SetResultOk() {
        if (Build.VERSION.SDK_INT >= 31) {
            SetRestart();
            return;
        }

        final Intent returnIntent = new Intent();
        getActivity().setResult(Activity.RESULT_OK, returnIntent);
    }

    private void SetRestart() {
        final Intent intent = new Intent(getActivity().getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        getActivity().startActivity(intent);
    }
}