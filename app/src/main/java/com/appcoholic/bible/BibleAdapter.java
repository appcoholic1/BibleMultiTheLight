
package com.appcoholic.bible;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

class BibleAdapter extends RecyclerView.Adapter<BibleAdapter.ViewHolder>
{
    @SuppressWarnings("UnusedAssignment")
    ArrayList<VerseBO> lstVerse = null;

    TreeMap<Integer, Integer> mapBookJump = null;

    private Map<Integer, String> fav = null;
    private final int prefix = 0;               //TODO NEXT: get prefix
    private SCommon _s = null;
    final private int cvMoveIncrement = 40;   //Px

    private String tbbNameFirst = "";
    private int tbbNameCount = 1;
    private int columnCount = 1;

    //Used for cross_references
    private int bNumber;
    private int cNumber;
    private int vNumber;
    private SEARCH_TYPE searchType;
    final private String searchStringExpr;
    final private String[] arrSearchStringExpr; //Used for coloring
    private String styleName = "";
    private String styleFgColor = null;
    private String styleBgColor = null;

    private enum SEARCH_TYPE
    {
        CROSS_REFERENCE,
        OTHER
    }

    private void SetColumnParams(final Context context, final String tbbName)
    {
        if (!tbbName.isEmpty()) {
            this.tbbNameFirst = tbbName.substring(0, 1);
            this.tbbNameCount = tbbName.length();
            this.columnCount = PCommon.GetDynamicColumnCount(context, tbbNameCount);
        }
    }

    private void SetMark(final Context context)
    {
        //TODO: FAB, remove IProject.APP_PREF_KEY.FAV_SYMBOL
        this.fav = _s.GenerateBookmarkShortMap(context);
    }

    void SetBibleStyle(final Context context)
    {
        this.styleName = PCommon.GetPref(context, IProject.APP_PREF_KEY.STYLE_HIGHLIGHT_SEARCH, context.getString(R.string.highlightSearchStyleDefault));
        final BibleStyleBO bibleStyle = new BibleStyleBO(context);
        final Map<String, String> mapProp = bibleStyle.GetStylePropertiesFromId(styleName);
        if (mapProp == null) {
            styleFgColor = null;
            styleBgColor = null;
        } else {
            styleFgColor = mapProp.get("fg");
            styleBgColor = mapProp.get("bg");
        }
    }

    BibleAdapter(final Context context)
    {
        SetColumnParams(context, "");

        this.lstVerse = null;
        this.mapBookJump = null;
        this.searchStringExpr = null;
        this.arrSearchStringExpr = null;
    }

    BibleAdapter(final Context context, final String tbbName, final int bNumber, final int cNumber, final int vNumber)
    {
        CheckLocalInstance(context);
        SetMark(context);
        SetColumnParams(context, tbbName);

        this.searchType = SEARCH_TYPE.OTHER;
        this.searchStringExpr = null;
        this.arrSearchStringExpr = null;
        this.lstVerse = _s.GetVerse(tbbName, bNumber, cNumber, vNumber);
        SetMapBookJumpFastInit(context);
    }

    BibleAdapter(final Context context, final String tbbName, final int bNumber, final int cNumber, final int vNumberFrom, final int vNumberTo)
    {
        CheckLocalInstance(context);
        SetMark(context);
        SetColumnParams(context, tbbName);

        this.searchType = SEARCH_TYPE.OTHER;
        this.searchStringExpr = null;
        this.arrSearchStringExpr = null;
        this.lstVerse = _s.GetVerses(tbbName, bNumber, cNumber, vNumberFrom, vNumberTo);
        SetMapBookJumpFastInit(context);

        this.SaveCacheSearch(context);
    }

    BibleAdapter(final Context context, final String tbbName, final int bNumber, final int cNumber, final int vNumber, final String searchStringExpr)
    {
        CheckLocalInstance(context);
        SetMark(context);
        SetColumnParams(context, tbbName);

        this.bNumber = bNumber;
        this.cNumber = cNumber;
        this.vNumber = vNumber;
        this.searchType = SEARCH_TYPE.CROSS_REFERENCE;
        this.searchStringExpr = null; //Not used
        this.arrSearchStringExpr = null;

        final Map<String, Object> mapSearchBible = _s.GetCrossReferences(tbbName, bNumber, cNumber, vNumber);
        this.lstVerse = mapSearchBible != null && mapSearchBible.containsKey("LSTVERSE") ? (ArrayList<VerseBO>) mapSearchBible.get("LSTVERSE") : null;
        this.mapBookJump = mapSearchBible != null && mapSearchBible.containsKey("BOOKJUMP") ? (TreeMap<Integer, Integer>) mapSearchBible.get("BOOKJUMP") : null;
    }

    BibleAdapter(final Context context, final String bbName, final int bNumber, final int cNumber, final String searchStringExpr)
    {
        CheckLocalInstance(context);
        SetMark(context);
        SetColumnParams(context, bbName);

        this.searchType = SEARCH_TYPE.OTHER;
        this.searchStringExpr = searchStringExpr;
        this.arrSearchStringExpr = GetArraySearchStringExpr(context, bbName, PCommon.ConcaT(bNumber, " ", cNumber, " ", searchStringExpr));
        this.lstVerse = _s.SearchBible(bbName, bNumber, cNumber, searchStringExpr);
        SetMapBookJumpFastInit(context);

        this.SetBibleStyle(context);
        this.SaveCacheSearch(context);
    }

    BibleAdapter(final Context context, final String tbbName, final int bNumber, final int cNumber)
    {
        CheckLocalInstance(context);
        SetMark(context);
        SetColumnParams(context, tbbName);

        this.searchType = SEARCH_TYPE.OTHER;
        this.searchStringExpr = null;
        this.arrSearchStringExpr = null;
        this.lstVerse = _s.GetChapter(tbbName, bNumber, cNumber);
        SetMapBookJumpFastInit(context);
    }

    BibleAdapter(final Context context, final String bbName, final int bNumber, final String searchStringExpr)
    {
        CheckLocalInstance(context);
        SetMark(context);
        SetColumnParams(context, bbName);

        this.searchType = SEARCH_TYPE.OTHER;
        this.searchStringExpr = searchStringExpr;
        this.arrSearchStringExpr = GetArraySearchStringExpr(context, bbName, PCommon.ConcaT(bNumber, " ", searchStringExpr));
        this.lstVerse = _s.SearchBible(bbName, bNumber, searchStringExpr);
        SetMapBookJumpFastInit(context);

        this.SetBibleStyle(context);
        this.SaveCacheSearch(context);
    }

    void SetMapBookJumpFastInit(final Context context)
    {
        try
        {
            if (lstVerse == null || lstVerse.isEmpty())
            {
                this.mapBookJump = new TreeMap<>();
                return;
            }

            final VerseBO verse = lstVerse.get(0);
            this.mapBookJump = new TreeMap<>();
            this.mapBookJump.put(verse.bNumber, 0);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    BibleAdapter(final Context context, final String bbName, final String booksSelection, final int quantity)
    {
        CheckLocalInstance(context);
        SetMark(context);
        SetColumnParams(context, bbName);

        this.searchType = SEARCH_TYPE.OTHER;
        this.searchStringExpr = null;
        this.arrSearchStringExpr = null;
        this.lstVerse = _s.GetListRandomVerses(context, bbName, booksSelection, quantity);
        SetMapBookJumpFastInit(context);

        this.SetBibleStyle(context);
        this.SaveCacheSearch(context);
    }

    BibleAdapter(final Context context, final String bbName, final String searchStringExpr)
    {
        CheckLocalInstance(context);
        SetMark(context);
        SetColumnParams(context, bbName);

        this.searchType = SEARCH_TYPE.OTHER;
        this.searchStringExpr = searchStringExpr;
        this.arrSearchStringExpr = GetArraySearchStringExpr(context, bbName, searchStringExpr);

        final Map<String, Object> mapSearchBible = _s.SearchBible(bbName, searchStringExpr);
        this.lstVerse = mapSearchBible != null && mapSearchBible.containsKey("LSTVERSE") ? (ArrayList<VerseBO>) mapSearchBible.get("LSTVERSE") : null;
        this.mapBookJump = mapSearchBible != null && mapSearchBible.containsKey("BOOKJUMP") ? (TreeMap<Integer, Integer>) mapSearchBible.get("BOOKJUMP") : null;

        this.SetBibleStyle(context);
        this.SaveCacheSearch(context);
    }

    BibleAdapter(final Context context, final int tabId)
    {
        CheckLocalInstance(context);
        SetMark(context);

        this.searchType = SEARCH_TYPE.OTHER;
        final CacheTabBO cacheTab = _s.GetCacheTab(tabId);
        if (cacheTab != null)
        {
            final String tbbName = cacheTab.trad == null || cacheTab.trad.isEmpty() ? cacheTab.bbName : cacheTab.trad;
            SetColumnParams(context, tbbName);
            if (cacheTab.tabType.compareToIgnoreCase("P") != 0) {
                this.searchStringExpr = cacheTab.fullQuery;
                this.arrSearchStringExpr = GetArraySearchStringExpr(context, cacheTab.bbName, this.searchStringExpr);
                this.SetBibleStyle(context);
            } else {
                this.searchStringExpr = null;
                this.arrSearchStringExpr = null;
            }
        } else {
            this.searchStringExpr = null;
            this.arrSearchStringExpr = null;
        }

        final Map<String, Object> mapSearchBible = _s.SearchBible(tabId);
        this.lstVerse = mapSearchBible != null && mapSearchBible.containsKey("LSTVERSE") ? (ArrayList<VerseBO>) mapSearchBible.get("LSTVERSE") : null;
        this.mapBookJump = mapSearchBible != null && mapSearchBible.containsKey("BOOKJUMP") ? (TreeMap<Integer, Integer>) mapSearchBible.get("BOOKJUMP") : null;
    }

    /***
     * Get all favorites
     * @param context
     * @param bbName
     * @param searchStringExpr  Give NULL to get all notes
     * @param orderBy       Order by
     * @param markType      Mark type
     */
    @SuppressWarnings("JavaDoc")
    BibleAdapter(final Context context, final String bbName, final String searchStringExpr, final int orderBy, final int markType)
    {
        CheckLocalInstance(context);
        SetMark(context);
        SetColumnParams(context, bbName);

        this.searchType = SEARCH_TYPE.OTHER;
        this.searchStringExpr = searchStringExpr;
        this.arrSearchStringExpr = GetArraySearchStringExpr(context, bbName, searchStringExpr);
        this.lstVerse = _s.SearchFav(bbName, searchStringExpr, orderBy, markType);
        SetMapBookJumpFastInit(context);

        this.SetBibleStyle(context);
    }

    static class ViewHolder extends RecyclerView.ViewHolder
    {
        //private LinearLayout card_recipient;
        private final CardView cv;
        private final TextView tv_ref;
        private final TextView tv_text;
        private final TextView tv_mark;
        private final TextView tv_cr;

        ViewHolder(View view)
        {
            super(view);

            cv = view.findViewById(R.id.cv);
            tv_ref = view.findViewById(R.id.tv_ref);
            tv_text = view.findViewById(R.id.tv_text);
            tv_cr = view.findViewById(R.id.tv_cr);
            tv_mark = view.findViewById(R.id.tv_mark);

            final Typeface typeface = PCommon.GetTypeface(view.getContext());
            if (typeface != null)
            {
                tv_ref.setTypeface(typeface, Typeface.BOLD);
                tv_text.setTypeface(typeface);
                tv_cr.setTypeface(typeface);
            }
            final int fontSize = PCommon.GetFontSize(view.getContext());
            if (tv_ref != null) tv_ref.setTextSize(fontSize);
            tv_text.setTextSize(fontSize);
            tv_cr.setTextSize(fontSize);
            tv_mark.setTextSize(fontSize);
        }
    }

    @NonNull
    @Override
    public BibleAdapter.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, final int viewType)
    {
        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(prefix == 0
                                                ? R.layout.card_recipient
                                                : R.layout.card_recipient1,
                                                viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final BibleAdapter.ViewHolder viewHolder, final int position)
    {
        //Current verse
        final VerseBO verse = lstVerse.get(position);
        final String ref = prefix == 0
                ? PCommon.ConcaT(verse.bName, " ", verse.cNumber, ".", verse.vNumber)
                : PCommon.ConcaT(verse.vNumber);
        final String crCount = verse.crCount > 0 ? String.valueOf(verse.crCount) : "";
        final boolean isLanguageRTL = verse.bbName.equalsIgnoreCase("y") || (verse.bbName.equalsIgnoreCase("w") && verse.bNumber < 40);
        boolean isTbbNameFirstLanguageRTL = tbbNameFirst.equalsIgnoreCase("y") || (tbbNameFirst.equalsIgnoreCase("w") && verse.bNumber < 40);

        //Decal: only when 1 column
        if (columnCount == 1)
        {
            final ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) viewHolder.cv.getLayoutParams();
            mlp.leftMargin = mlp.rightMargin = 0;

            if ((!tbbNameFirst.isEmpty()) && (!verse.bbName.equalsIgnoreCase(tbbNameFirst))) {
                if (isTbbNameFirstLanguageRTL) {
                    mlp.rightMargin = cvMoveIncrement;
                } else {
                    mlp.leftMargin = cvMoveIncrement;
                }
            }

            viewHolder.cv.setLayoutParams(mlp);
        }

        //Mark
        if (verse.mark > 0)
        {
            viewHolder.tv_mark.setPadding(10, 0, 5, 0);
            viewHolder.tv_mark.setText( fav.get(verse.mark) );
        }

        //Ref
        if (viewHolder.tv_ref != null)
        {
            viewHolder.tv_ref.setText(ref);
            viewHolder.tv_ref.setId(verse.id);
            viewHolder.tv_ref.setTag(position);
            viewHolder.tv_ref.setTextDirection(isLanguageRTL ? View.TEXT_DIRECTION_RTL : View.TEXT_DIRECTION_LTR);
            viewHolder.tv_ref.setGravity(isLanguageRTL ? Gravity.RIGHT : Gravity.LEFT);
        }

        //Text
        if (prefix == 0)
        {
            if (arrSearchStringExpr != null && this.searchStringExpr != null && !this.searchStringExpr.isEmpty())
            {
                try
                {
                    final Spannable span = new SpannableString(verse.vText);
                    for (String searchExpr : arrSearchStringExpr) {
                        final Pattern p = Pattern.compile(searchExpr.replaceAll("%", ".*"), Pattern.CASE_INSENSITIVE);
                        final Matcher m = p.matcher(verse.vText);
                        while (m.find()) SetSpan(span, m.start(), m.end(), styleName);
                    }
                    viewHolder.tv_text.setText(span);
                }
                catch(Exception ex)
                {
                    if (PCommon._isDebug) PCommon.LogR(viewHolder.tv_text.getContext(), ex);
                }
            } else {
                viewHolder.tv_text.setText(verse.vText);
            }
        }
        else if (prefix == 1)
        {
            viewHolder.tv_text.setText(PCommon.ConcaT(verse.vNumber, ". ", verse.vText));
        }
        viewHolder.tv_text.setId(verse.id);
        viewHolder.tv_text.setTag(position);
        viewHolder.tv_text.setTextDirection(isLanguageRTL ? View.TEXT_DIRECTION_RTL : View.TEXT_DIRECTION_LTR);
        viewHolder.tv_text.setGravity(isLanguageRTL ? Gravity.RIGHT : Gravity.LEFT);
        viewHolder.tv_cr.setText(crCount);

        //CR
        if (this.searchType == SEARCH_TYPE.CROSS_REFERENCE && verse.bNumber == this.bNumber && verse.cNumber == this.cNumber && verse.vNumber == this.vNumber)
        {
            final int fgColor = viewHolder.tv_text.getResources().getColor(R.color.white);
            viewHolder.cv.setCardBackgroundColor(viewHolder.cv.getResources().getColor(R.color.blueDark));
            viewHolder.tv_mark.setTextColor(fgColor);
            if (viewHolder.tv_ref != null) viewHolder.tv_ref.setTextColor(fgColor);
            viewHolder.tv_text.setTextColor(fgColor);
            viewHolder.tv_cr.setTextColor(fgColor);
            viewHolder.setIsRecyclable(false);
        }

        //Events
        if (viewHolder.tv_ref != null)
        {
            viewHolder.tv_ref.setOnClickListener(v -> {
                final TextView tvRef = (TextView) v;
                if (tvRef == null) return;

                final int bibleId = tvRef.getId();
                final int position1 = Integer.parseInt( tvRef.getTag().toString() );
                PCommon.SavePrefInt(v.getContext(), IProject.APP_PREF_KEY.BIBLE_ID, bibleId);
                PCommon.SavePrefInt(v.getContext(), IProject.APP_PREF_KEY.VIEW_POSITION, position1);

                v.showContextMenu();
            });
        }
        viewHolder.tv_text.setOnClickListener(v -> {
            final TextView tvText = (TextView) v;
            if (tvText == null) return;

            final int bibleId = tvText.getId();
            final int position12 = Integer.parseInt( tvText.getTag().toString() );
            PCommon.SavePrefInt(v.getContext(), IProject.APP_PREF_KEY.BIBLE_ID, bibleId);
            PCommon.SavePrefInt(v.getContext(), IProject.APP_PREF_KEY.VIEW_POSITION, position12);

            v.showContextMenu();
        });
    }

    @Override
    public int getItemCount()
    {
        return lstVerse == null ? 0 : lstVerse.size();
    }

    @Override
    public int getItemViewType(int position)
    {
        final VerseBO verse = lstVerse.get(position);

        return verse.mark;
    }

    private void SetSpan(final Spannable span, final int start, final int end, final String styleName)
    {
        if (styleName == null || styleName.isEmpty() || styleFgColor == null || styleBgColor == null) {
            span.setSpan(PCommon.GetUnderlineSpan(), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else {
            span.setSpan(PCommon.GetBackgroundColorSpan(styleBgColor), start, end, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
            span.setSpan(PCommon.GetForegroundColorSpan(styleFgColor), start, end, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        }
    }

    private void CheckLocalInstance(final Context context)
    {
        try
        {
            if (_s == null)
            {
                _s = SCommon.GetInstance(context);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    private void SaveCacheSearch(final Context context)
    {
        try
        {
            ArrayList<Integer> lstId = new ArrayList<>();

            if (lstVerse != null)
            {
                for (VerseBO verse : lstVerse) {
                    lstId.add(verse.id);
                }
            }

            _s.SaveCacheSearch(lstId);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    private String[] GetArraySearchStringExpr(final Context context, final String bbName, final String searchFullQuery)
    {
        try
        {
            String[] arrExpr;
            final String patternDigit = "\\d+";
            final String[] testWords = searchFullQuery.split("\\s");
            final int testWcount = testWords.length;
            String testNumbersExpr = "";

            if (testWcount >= 2) {
                final int testBnumber = testWords[0].matches(patternDigit) ? Integer.parseInt(testWords[0]) : _s.GetBookNumberByName(bbName, testWords[0]);
                if (testBnumber > 0) {
                    testNumbersExpr = PCommon.ConcaT(testWords[0], " ");

                    final int testCnumber = testWords[1].matches(patternDigit) ? Integer.parseInt(testWords[1]) : -1;
                    if (testCnumber > 0) {
                        testNumbersExpr = PCommon.ConcaT(testNumbersExpr, testCnumber, " ");
                    }
                }
            }

            final String testCommaExpr = searchFullQuery.substring(testNumbersExpr.length());
            arrExpr = testCommaExpr.toLowerCase().replaceAll("%+", "%").split(",");
            for (int i = 0; i < arrExpr.length; i++) {
                arrExpr[ i ] = arrExpr[ i ].replaceAll("^%","").replaceAll("%$", "");
                if (arrExpr[ i ].length() < PCommon.GetSearchFullQueryLimit()) {
                    if (PCommon._isDebug) System.out.println("LIMITED: " + i + " > '" + arrExpr[ i ] + "'");
                    //PCommon.ShowToast(context, R.string.toastEmpty4, Toast.LENGTH_SHORT);
                }
            }
            return arrExpr;
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }

        return null;
    }
}
