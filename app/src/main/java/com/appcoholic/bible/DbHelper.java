
package com.appcoholic.bible;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Handler;
import android.widget.Toast;

import com.ximpleware.AutoPilot;
import com.ximpleware.VTDGen;
import com.ximpleware.VTDNav;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;

//<editor-fold defaultstate="collapsed" desc="-- History --">
// PROD: Bible 3.87    DbVersion: 92 (39)2024-06-30
// PROD: Bible 3.86    DbVersion: 91 (38)2024-06-20
// PROD: Bible 3.85    DbVersion: 90 (37)2024-06-10
// PROD: Bible 3.84    DbVersion: 89 (36)2024-06-05
// PROD: Bible 3.83    DbVersion: 88 (35)2024-05-29
// PROD: Bible 3.82    DbVersion: 86 (34)2024-04-25
// PROD: Bible 3.81    DbVersion: 86 (34)2024-04-07
// PROD: Bible 3.80    DbVersion: 86 (34)2024-03-27
// PROD: Bible 3.79    DbVersion: 86 (34)2024-01-31
// PROD: Bible 3.78,   DbVersion: 86 (34)2024-01-24
// PROD: Bible 3.77,   DbVersion: 86 (34)2024-01-15
// PROD: Bible 3.76,   DbVersion: 86 (34)2023-12-27
// PROD: Bible 3.75,   DbVersion: 86 (34)2023-12-20
// PROD: Bible 3.74,   DbVersion: 86 (34)2023-12-06
// PROD: Bible 3.73,   DbVersion: 85 (34)2023-11-21
// PROD: Bible 3.72,   DbVersion: 84 (33)2023-11-01
// PROD: Bible 3.71,   DbVersion: 83 (32)2023-08-20
// PROD: Bible 3.70,   DbVersion: 82 (31)2023-08-10 (align tables between apps, based on The Life (9))
// PROD: Bible 3.69,   DbVersion: 81 (30)2023-07-21
// PROD: Bible 3.68,   DbVersion: 81 (30)2023-05-26
// PROD: Bible 3.67,   DbVersion: 80 (29)2023-05-20
// PROD: Bible 3.66,   DbVersion: 79 (28)2023-05-13
// PROD: Bible 3.65,   DbVersion: 78 (27)2023-05-09
// PROD: Bible 3.64,   DbVersion: 77 (26)2023-05-   (test only)
// PROD: Bible 3.63,   DbVersion: 76 (25)2023-04-29
// PROD: Bible 3.62,   DbVersion: 75 (24)2023-03-15
// PROD: Bible 3.61,   DbVersion: 74 (23)2023-03-09
// PROD: Bible 3.60,   DbVersion: 73 (22)2023-01-14
// PROD: Bible 3.58,   DbVersion: 73 (22)2022-11-28
// PROD: Bible 3.57,   DbVersion: 72 (22)2022-11-07
// PROD: Bible 3.56,   DbVersion: 71 (22)2022-10-31
// PROD: Bible 3.55,   DbVersion: 71 (22)2022-10-20
// PROD: Bible 3.54,   DbVersion: 71 (22)2022-08-30
// PROD: Bible 3.53,   DbVersion: 71 (22)2022-05-26
// PROD: Bible 3.52,   DbVersion: 71 (22)2021-12-15 (Try to solve Android TV rejection) Open
// PROD: Bible 3.51,   DbVersion: 71 (22)2021-12-14 (Try to solve Android TV rejection) Internal
// PROD: Bible 3.50,   DbVersion: 71 (22)2021-12-13 (Try to solve Android TV rejection) Internal
// PROD: Bible 3.49,   DbVersion: 71 (22)2021-12-12 (Try to solve Android TV rejection) Internal
// PROD: Bible 3.48,   DbVersion: 70 (22)2021-12-06 (db copy for new)
// PROD: Bible 3.47,   DbVersion: 68 (22)2021-11-05
// PROD: Bible 3.46,   DbVersion: 67 (22)2021-08-25 retry
// PROD: Bible 3.45,   DbVersion: 66 (22)2021-08-24 retry-not-published
// PROD: Bible 3.44,   DbVersion: 65 (22)2021-08-23 (bug-not-published)
// PROD: Bible 3.43,   DbVersion: 64 (21)2021-08-14
// PROD: Bible 3.42,   DbVersion: 63 (21)2021-07-26
// PROD: Bible 3.41,   DbVersion: 62 (21)2021-05-18
// PROD: Bible 3.40,   DbVersion: 61 (21)2021-05-03
// PROD: Bible 3.39,   DbVersion: 60 (21)2021-04-07
// PROD: Bible 3.38,   DbVersion: 59 (21)2021-03-23
// PROD: Bible 3.37,   DbVersion: 58 (20)2021-02-20
// PROD: Bible 3.36,   DbVersion: 57 (19)2021-01-26
// PROD: Bible 3.35,   DbVersion: 56 (19)2021-01-19
// PROD: Bible 3.34,   DbVersion: 55 (19)2020-12-07 (fix 3.32)
// PROD: Bible 3.33,   DbVersion: 54 (19)2020-12-06 (rollback 3.31)
// PROD: Bible 3.32,   DbVersion: 53 (19)2020-12-05 (bug)
// PROD: Bible 3.31,   DbVersion: 52 (19)2020-10-04
// PROD: Bible 3.30,   DbVersion: 51 (19)2020-08-26
// PROD: Bible 3.29,   DbVersion: 50 (18)2020-06-28
// PROD: Bible 3.28,   DbVersion: 49 (18)2020-04-02
// PROD: Bible 3.27,   DbVersion: 48 (18)2020-02-06
// PROD: Bible 3.26,   DbVersion: 47 (17)2019-12-29
// PROD: Bible 3,25,   DbVersion: 46 (17)2019-11-02
// PROD: Bible 3,24,   DbVersion: 45 (16)2019-10-20
// PROD: Bible 3.23,   DbVersion: 44 (15)2019-10-13
// PROD: Bible 3.22,   DbVersion: 43 (15)2019-10-09
// PROD: Bible 3.21,   DbVersion: 42 (15)2019-09-22
// PROD: Bible 3.20,   DbVersion: 41 (15)2019-09-18
// PROD: Bible 3.19,   DbVersion: 40 (15)2019-09-08
// PROD: Bible 3.18,   DbVersion: 39 (14)2019-08-25
// PROD: Bible 3.17,   DbVersion: 38 (14)2019-08-16 (me only)
// PROD: Bible 3.16,   DbVersion: 37 (14)2019-08-15
// PROD: Bible 3.15,   DbVersion: 36 (13)2019-07-24
// PROD: Bible 3.14,   DbVersion: 35 (13)2019-07-17
// PROD: Bible 3.13,   DbVersion: 34 (12)2019-07-07
// PROD: Bible 3.12,   DbVersion: 33 (12)2019-06-23
// PROD: Bible 3.11,   DbVersion: 32 (12)2019-06-16
// PROD: Bible 3.11,   DbVersion: 31 (12)2019-06-12 (me only)
// PROD: Bible 3.11,   DbVersion: 30 (11)2019-06-10 (me only)
// PROD: Bible 3.10,   DbVersion: 29 (11)2019-05-11
// PROD: Bible 3.10,   DbVersion: 29 (11)2019-04-28 (me only)
// PROD: Bible 3.9,    DbVersion: 28 (10)2019-01-27
// PROD: Bible 3.8,    DbVersion: 27 (10)2018-12-16
// PROD: Bible 3.7,    DbVersion: 26 (10)2018-11-25
// PROD: Bible 3.6,    DbVersion: 25 (10)2018-11-18
// PROD: Bible 3.5,    DbVersion: 24 (10)2018-11-10
// PROD: Bible 3.4,    DbVersion: 23 (9) 2018-10-14
// PROD: Bible 3.3,    DbVersion: 22 (8) 2018-06-10
// PROD: Bible 3.2,    DbVersion: 21 (8) 2018-05-05
// PROD: Bible 3.1,    DbVersion: 20 (8) 2018-05-01
// PROD: Bible 3.0,    DbVersion: 19 (8) 2018-04-22
// PROD: Bible 2.13,   DbVersion: 18 (8) 2018-03-04
// PROD: Bible 2.12,   DbVersion: 17 (8) 2018-02-03
// PROD: Bible 2.11,   DbVersion: 16 (8) 2018-01-14
// PROD: Bible 2.10,   DbVersion: 15 (8) 2018-01-07
// PROD: Bible 2.9,    DbVersion: 14 (8) 2017-12-12
// PROD: Bible 2.8,    DbVersion: 13 (8) 2017-10-10
// PROD: Bible 2.7,    DbVersion: 12 (8) 2017-09-05
// PROD: Bible 2.6,    DbVersion: 11 (8) 2017-08-27
// PROD: Bible 2.5,    DbVersion: 10 (7) 2017-08-15
// PROD: Bible 2.4,    DbVersion: 9  (6) 2017-07-09
// PROD: Bible 2.3,    DbVersion: 8  (6) 2017-07-02
// PROD: Bible 2.2,    DbVersion: 7  (6) 2017-06-20
// PROD: Bible 2.1,    DbVersion: 6      2017-04-17
// PROD: Bible 2.0,    DbVersion: 5      2017-03-21
// PROD: Bible 1.9,    DbVersion: 4      2017-02-26
// PROD: Bible 1.7,    DbVersion: 3      2017-02...
// PROD: Bible 1.6,    DbVersion; 3      2017-01-04
// PROD: Bible 1.5,    DbVersion: 2      2016-10-xx
// PROD: Bible 1.0,    DbVersion: 1      2016-10-07
//-------------------------------------------------

//</editor-fold>

/***
 * Use only LogD and LogE for logs, all is "hidden"
 */
class DbHelper extends SQLiteOpenHelper
{
    //<editor-fold defaultstate="collapsed" desc="-- Variables --">

    @SuppressWarnings("UnusedAssignment")
    private Context _context = null;
    private SQLiteDatabase _db = null;
    private static final int _version = 92;     //db version code

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="-- Constructor --">

    DbHelper(final Context context)
    {
        super(context, "bible.db", null, _version);
        _context = context;
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="-- Events --">

    @Override
    public void onCreate(final SQLiteDatabase database)
    {
        try
        {
            _db = database;

            //Upgrade to latest version
            onUpgrade(database, -1, _version);
        }
        catch (Exception ex)
        {
            LogE(ex);
        }
    }

    @Override
    public void onUpgrade(final SQLiteDatabase database, final int oldVersion, final int newVersion)
    {
        try
        {
            String sql;

            if (PCommon._isDebug)
            {
                LogD(PCommon.ConcaT("onUpgrade: from ", oldVersion, " => ", newVersion, "\nDbHelper version: ", _version, ", Db GetVersion: ", database.getVersion()));
            }

            _db = database;

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            if (oldVersion < 1)
            {
                //This should be the latest version (in case of 1st installation)

                SetGlobalSettings();
                PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, PCommon.GetInstallStatusShouldBe());

                //<editor-fold defaultstate="collapsed" desc="-- After DbVersion 70 --">
                _db.endTransaction();
                _db.close();

                final InputStream from = _context.getAssets().open("db/bible.db");
                final File to = new File(_context.getDatabasePath("bible.db").toString());
                PCommon.CopyFile(from, to);
                from.close();
                //</editor-fold>

                //<editor-fold defaultstate="collapsed" desc="-- Before DbVersion 70 --">
                //
                // sql = DropTable("log");
                // _db.execSQL(sql);
                //
                // sql = "CREATE TABLE log (msg TEXT)";
                // _db.execSQL(sql);
                //
                // sql = "DROP INDEX IF EXISTS cacheSearch_ndx";
                // _db.execSQL(sql);
                //
                // sql = DropTable("cacheSearch");
                // _db.execSQL(sql);
                //
                // sql = "DROP INDEX IF EXISTS cacheTab0_ndx";
                // _db.execSQL(sql);
                //
                // sql = "DROP INDEX IF EXISTS cacheTab1_ndx";
                // _db.execSQL(sql);
                //
                // sql = DropTable("cacheTab");
                // _db.execSQL(sql);
                //
                // sql = DropTable("bibleNote");
                // _db.execSQL(sql);
                //
                // sql = DropTable("bibleRef");
                // _db.execSQL(sql);
                //
                // sql = "DROP INDEX IF EXISTS bible_ndx";
                // _db.execSQL(sql);
                //
                // sql = "DROP INDEX IF EXISTS bibleNumber_ndx";
                // _db.execSQL(sql);
                //
                // sql = DropTable("bible");
                // _db.execSQL(sql);
                //
                // //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                //
                // sql = "CREATE TABLE bibleRef (bbName TEXT NOT NULL, bNumber INTEGER NOT NULL, bName TEXT NOT NULL, bsName TEXT NOT NULL, PRIMARY KEY (bbName, bNumber))";
                // _db.execSQL(sql);
                //
                // sql = "CREATE TABLE bible (id INTEGER NOT NULL, bbName TEXT NOT NULL, bNumber INTEGER NOT NULL, cNumber INTEGER NOT NULL, vNumber INTEGER NOT NULL, vText TEXT NOT NULL, PRIMARY KEY (bbName, bNumber, cNumber, vNumber))";
                // _db.execSQL(sql);
                //
                // sql = "CREATE UNIQUE INDEX bible_ndx on bible (id)";
                // _db.execSQL(sql);
                //
                // //23
                // sql = "CREATE UNIQUE INDEX bibleNumber_ndx on bible (bbName, bNumber, cNumber, vNumber)";
                // _db.execSQL(sql);
                //
                // //Mark: 1=Fav, 2=Bookmark | rating=0..5 | note=personal note | links? //was (never used): sql = "CREATE TABLE bibleNote (bNumber INTEGER, cNumber INTEGER, vNumber INTEGER, changeDt TEXT, mark INTEGER, rating INTEGER, note TEXT, PRIMARY KEY (bNumber, cNumber, vNumber))";
                // sql = "CREATE TABLE bibleNote (bNumber INTEGER NOT NULL, cNumber INTEGER NOT NULL, vNumber INTEGER NOT NULL, changeDt TEXT NOT NULL, mark INTEGER CHECK(mark >= 1 AND mark <= 2), note TEXT NOT NULL, PRIMARY KEY (bNumber, cNumber, vNumber))";
                // _db.execSQL(sql);
                //
                // sql = "CREATE TABLE cacheTab (tabId INTEGER NOT NULL, tabType TEXT CHECK(tabType='S' OR tabType='F' or tabType='A' or tabType='P'), tabTitle TEXT NOT NULL, fullQuery TEXT NOT NULL, scrollPosY INTEGER NOT NULL, bbName TEXT NOT NULL, isBook INTEGER NOT NULL, isChapter INTEGER NOT NULL, isVerse INTEGER NOT NULL, bNumber INTEGER NOT NULL, cNumber INTEGER NOT NULL, vNumber INTEGER NOT NULL, trad TEXT, PRIMARY KEY (tabId))";
                // _db.execSQL(sql);
                //
                // sql = "CREATE UNIQUE INDEX cacheTab0_ndx on cacheTab (tabId)";
                // _db.execSQL(sql);
                //
                // sql = "CREATE INDEX cacheTab1_ndx on cacheTab (tabType)";
                // _db.execSQL(sql);
                //
                // sql = "CREATE TABLE cacheSearch (tabId INTEGER NOT NULL, bibleId INTEGER NOT NULL)";
                // _db.execSQL(sql);
                //
                // sql = "CREATE INDEX cacheSearch_ndx on cacheSearch (tabId)";
                // _db.execSQL(sql);
                //
                // //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                // //Load Genesis
                // sql = PCommon.ConcaT("INSERT INTO cacheTab VALUES (", 0, ",", PCommon.AQ("S"), ",", PCommon.AQ("Gen1"), ",", PCommon.AQ("1 1"), ",", 0, ",", PCommon.AQ("k"), ",", 1, ",", 1, ",", 0, ",", 1, ",", 1, ",", 0, ",", PCommon.AQ("k"), ")");
                // _db.execSQL(sql);
                //
                // sql = PCommon.ConcaT("INSERT INTO bibleNote VALUES (", 1, ",", 1, ",", 1, ",", PCommon.AQ(PCommon.NowYYYYMMDD()), ",", 2, ",", PCommon.AQ(""), ")");
                // _db.execSQL(sql);
                //
                // sql = PCommon.ConcaT("INSERT INTO bibleNote VALUES (", 1, ",", 1, ",", 5, ",", PCommon.AQ(PCommon.NowYYYYMMDD()), ",", 1, ",", PCommon.AQ(""), ")");
                // _db.execSQL(sql);
                //
                // //10
                // sql = "CREATE TABLE bibleCi (ciId INTEGER NOT NULL, bNumber INTEGER NOT NULL, cNumber INTEGER NOT NULL, vCount INTEGER NOT NULL, PRIMARY KEY (bNumber, cNumber))";
                // _db.execSQL(sql);
                //
                // sql = "CREATE UNIQUE INDEX bibleCi_ndx on bibleCi (ciId)";
                // _db.execSQL(sql);
                //
                // sql = "CREATE TABLE planDesc (planId INTEGER NOT NULL, planRef TEXT NOT NULL, startDt TEXT NOT NULL, endDt TEXT NOT NULL, bCount INTEGER NOT NULL, cCount INTEGER NOT NULL, vCount INTEGER NOT NULL, vDayCount INTEGER NOT NULL, dayCount INTEGER NOT NULL, PRIMARY KEY (planRef))";
                // _db.execSQL(sql);
                //
                // sql = "CREATE TABLE planCal (planId INTEGER NOT NULL, dayNumber INTEGER NOT NULL, dayDt TEXT NOT NULL, isRead INTEGER NOT NULL, bNumberStart INTEGER NOT NULL, cNumberStart INTEGER NOT NULL, vNumberStart INTEGER NOT NULL, bNumberEnd INTEGER NOT NULL, cNumberEnd INTEGER NOT NULL, vNumberEnd INTEGER NOT NULL, PRIMARY KEY (planId, bNumberStart, cNumberStart, vNumberStart))";
                // _db.execSQL(sql);
                //
                // sql = "CREATE INDEX planCal0_ndx on planCal (planId, dayNumber)";
                // _db.execSQL(sql);
                //
                // sql = "CREATE TABLE artDesc (artId INTEGER NOT NULL, artUpdatedDt TEXT NOT NULL, artTitle TEXT NOT NULL, artSrc TEXT NOT NULL, PRIMARY KEY (artTitle))";
                // _db.execSQL(sql);
                //
                // sql = "CREATE UNIQUE INDEX artDesc0_ndx on artDesc (artId)";
                // _db.execSQL(sql);
                //
                // //37
                // sql = DropTable("bibleCrossRef");
                // _db.execSQL(sql);
                //
                // sql = "CREATE TABLE bibleCrossRef (crId INTEGER NOT NULL, bNumberFrom INTEGER NOT NULL, cNumberFrom INTEGER NOT NULL, vNumberFrom INTEGER NOT NULL, bNumberTo INTEGER NOT NULL, cNumberTo INTEGER NOT NULL, vNumberTo INTEGER NOT NULL)";
                // _db.execSQL(sql);
                //
                // sql = "DROP INDEX IF EXISTS bibleCrossRef0_ndx";
                // _db.execSQL(sql);
                //
                // sql = "DROP INDEX IF EXISTS bibleCrossRef1_ndx";
                // _db.execSQL(sql);
                //
                // sql = "CREATE INDEX bibleCrossRef0_ndx on bibleCrossRef (bNumberFrom, cNumberFrom, vNumberFrom)";
                // _db.execSQL(sql);
                //
                // sql = "CREATE INDEX bibleCrossRef1_ndx on bibleCrossRef (bNumberTo, cNumberTo, vNumberTo)";
                // _db.execSQL(sql);
                //
                // sql = DropTable("bibleCrossRefi");
                // _db.execSQL(sql);
                //
                // sql = "CREATE TABLE bibleCrossRefi (bNumber integer not null, cNumber integer not null, vNumber integer not null, tot integer not null);";
                // _db.execSQL(sql);
                //
                // sql = "DROP INDEX IF EXISTS bibleCrossRefi0_ndx";
                // _db.execSQL(sql);
                //
                // sql = "CREATE UNIQUE INDEX bibleCrossRefi0_ndx on bibleCrossRefi (bNumber, cNumber, vNumber)";
                // _db.execSQL(sql);
                //
                // //58
                // sql = "DROP INDEX IF EXISTS td0_ndx";
                // _db.execSQL(sql);
                //
                // sql = "DROP INDEX IF EXISTS td1_ndx";
                // _db.execSQL(sql);
                //
                // sql = "DROP INDEX IF EXISTS td2_ndx";
                // _db.execSQL(sql);
                //
                // sql = "DROP INDEX IF EXISTS td3_ndx";
                // _db.execSQL(sql);
                //
                // sql = DropTable("td");
                // _db.execSQL(sql);
                //
                // sql = "CREATE TABLE td (tdId INTEGER not null, tdStatus TEXT not null, tdPriority TEXT not null, tdDesc TEXT not null, tdCommentIssues TEXT not null);";
                // _db.execSQL(sql);
                //
                // sql = "CREATE UNIQUE INDEX td0_ndx on td (tdId)";
                // _db.execSQL(sql);
                //
                // sql = "CREATE INDEX td1_ndx on td (tdStatus)";
                // _db.execSQL(sql);
                //
                // sql = "CREATE INDEX td2_ndx on td (tdPriority)";
                // _db.execSQL(sql);
                //
                // sql = "CREATE INDEX td3_ndx on td (tdDesc)";
                // _db.execSQL(sql);
                //
                // FillDbWithAll();
                //
                //</editor-fold>

                //Let return here (all has been done)
                return;
            }
            if (oldVersion < 3)    //1, 2 => 3
            {
                sql = "DROP INDEX IF EXISTS cacheTab0_ndx";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS cacheTab1_ndx";
                _db.execSQL(sql);

                sql = "ALTER TABLE cacheTab RENAME TO temp_cacheTab";
                _db.execSQL(sql);

                sql = "CREATE TABLE cacheTab (tabId INTEGER NOT NULL, tabType TEXT CHECK(tabType='S' OR tabType='F' or tabType='A'), tabTitle TEXT NOT NULL, fullQuery TEXT NOT NULL, scrollPosY INTEGER NOT NULL, bbName TEXT NOT NULL, isBook INTEGER NOT NULL, isChapter INTEGER NOT NULL, isVerse INTEGER NOT NULL, bNumber INTEGER NOT NULL, cNumber INTEGER NOT NULL, vNumber INTEGER NOT NULL, PRIMARY KEY (tabId))";
                _db.execSQL(sql);

                sql = "CREATE UNIQUE INDEX cacheTab0_ndx on cacheTab (tabId)";
                _db.execSQL(sql);

                sql = "CREATE INDEX cacheTab1_ndx on cacheTab (tabType)";
                _db.execSQL(sql);

                sql = "INSERT INTO cacheTab SELECT tabId, tabType, tabTitle, fullQuery, scrollPosY, bbName, isBook, isChapter, isVerse, bNumber, cNumber, vNumber FROM temp_cacheTab";
                _db.execSQL(sql);

                sql = DropTable("temp_cacheTab");
                _db.execSQL(sql);
            }
            //noinspection StatementWithEmptyBody
            if (oldVersion < 4)    //1..3 => 4
            {
                //Code REMOVED: PCommon.SavePref(_context, IProject.APP_PREF_KEY.LAYOUT_COLUMN, "1");
            }
            if (oldVersion < 5)    //1..4 => 5
            {
                //--- New settings
                PCommon.SavePref(_context, IProject.APP_PREF_KEY.TRAD_BIBLE_NAME, "");
                PCommon.SavePref(_context, IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_1, "1");
                PCommon.SavePref(_context, IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_2, "2");
                PCommon.SavePref(_context, IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_3, "3");
                PCommon.SavePref(_context, IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_4, "2");

                sql = "DROP INDEX IF EXISTS cacheTab0_ndx";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS cacheTab1_ndx";
                _db.execSQL(sql);

                sql = "ALTER TABLE cacheTab RENAME TO temp_cacheTab";
                _db.execSQL(sql);

                sql = "CREATE TABLE cacheTab (tabId INTEGER NOT NULL, tabType TEXT CHECK(tabType='S' OR tabType='F' or tabType='A'), tabTitle TEXT NOT NULL, fullQuery TEXT NOT NULL, scrollPosY INTEGER NOT NULL, bbName TEXT NOT NULL, isBook INTEGER NOT NULL, isChapter INTEGER NOT NULL, isVerse INTEGER NOT NULL, bNumber INTEGER NOT NULL, cNumber INTEGER NOT NULL, vNumber INTEGER NOT NULL, trad TEXT, PRIMARY KEY (tabId))";
                _db.execSQL(sql);

                sql = "CREATE UNIQUE INDEX cacheTab0_ndx on cacheTab (tabId)";
                _db.execSQL(sql);

                sql = "CREATE INDEX cacheTab1_ndx on cacheTab (tabType)";
                _db.execSQL(sql);

                sql = "INSERT INTO cacheTab SELECT *, bbName FROM temp_cacheTab";
                _db.execSQL(sql);

                sql = DropTable("temp_cacheTab");
                _db.execSQL(sql);
            }
            if (oldVersion < 6)
            {
                PCommon.SavePref(_context, IProject.APP_PREF_KEY.FONT_NAME, "");
                PCommon.SavePref(_context, IProject.APP_PREF_KEY.FONT_SIZE, "14");
            }
            if (oldVersion < 7)
            {
                PCommon.SavePref(_context, IProject.APP_PREF_KEY.BOOK_CHAPTER_DIALOG, "1");
            }
            if (oldVersion < 10)     //1..9 => 10
            {
                //--- New setting
                PCommon.SavePref(_context, IProject.APP_PREF_KEY.PLAN_ID, "-1");
                PCommon.SavePref(_context, IProject.APP_PREF_KEY.PLAN_PAGE, "-1");

                //10 new tables & index
                sql = "CREATE TABLE bibleCi (ciId INTEGER NOT NULL, bNumber INTEGER NOT NULL, cNumber INTEGER NOT NULL, vCount INTEGER NOT NULL, PRIMARY KEY (bNumber, cNumber))";
                _db.execSQL(sql);

                sql = "CREATE UNIQUE INDEX bibleCi_ndx on bibleCi (ciId)";
                _db.execSQL(sql);

                sql = "CREATE TABLE planDesc (planId INTEGER NOT NULL, planRef TEXT NOT NULL, startDt TEXT NOT NULL, endDt TEXT NOT NULL, bCount INTEGER NOT NULL, cCount INTEGER NOT NULL, vCount INTEGER NOT NULL, vDayCount INTEGER NOT NULL, dayCount INTEGER NOT NULL, dayRead INTEGER NOT NULL, progressPerc INTEGER NOT NULL, PRIMARY KEY (planRef))";
                _db.execSQL(sql);

                sql = "CREATE TABLE planCal (planId INTEGER NOT NULL, dayNumber INTEGER NOT NULL, dayDt TEXT NOT NULL, isRead INTEGER NOT NULL, bNumberStart INTEGER NOT NULL, cNumberStart INTEGER NOT NULL, vNumberStart INTEGER NOT NULL, bNumberEnd INTEGER NOT NULL, cNumberEnd INTEGER NOT NULL, vNumberEnd INTEGER NOT NULL, PRIMARY KEY (planId, bNumberStart, cNumberStart, vNumberStart))";
                _db.execSQL(sql);

                sql = "CREATE INDEX planCal0_ndx on planCal (planId, dayNumber)";
                _db.execSQL(sql);

                //10 alter table
                sql = "DROP INDEX IF EXISTS cacheTab0_ndx";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS cacheTab1_ndx";
                _db.execSQL(sql);

                sql = "ALTER TABLE cacheTab RENAME TO temp_cacheTab";
                _db.execSQL(sql);

                sql = "CREATE TABLE cacheTab (tabId INTEGER NOT NULL, tabType TEXT CHECK(tabType='S' OR tabType='F' or tabType='A' or tabType='P'), tabTitle TEXT NOT NULL, fullQuery TEXT NOT NULL, scrollPosY INTEGER NOT NULL, bbName TEXT NOT NULL, isBook INTEGER NOT NULL, isChapter INTEGER NOT NULL, isVerse INTEGER NOT NULL, bNumber INTEGER NOT NULL, cNumber INTEGER NOT NULL, vNumber INTEGER NOT NULL, trad TEXT, PRIMARY KEY (tabId))";
                _db.execSQL(sql);

                sql = "CREATE UNIQUE INDEX cacheTab0_ndx on cacheTab (tabId)";
                _db.execSQL(sql);

                sql = "CREATE INDEX cacheTab1_ndx on cacheTab (tabType)";
                _db.execSQL(sql);

                sql = "INSERT INTO cacheTab SELECT tabId, tabType, tabTitle, fullQuery, scrollPosY, bbName, isBook, isChapter, isVerse, bNumber, cNumber, vNumber, trad FROM temp_cacheTab";
                _db.execSQL(sql);

                sql = DropTable("temp_cacheTab");
                _db.execSQL(sql);

//sql = "CREATE TABLE planHistory (startDt TEXT NOT NULL, endDt TEXT NOT NULL, desc TEXT NOT NULL)";
//_db.execSQL(sql);

                ImportCi();
            }
            if (oldVersion < 11)    //1..10 => 11
            {
                //11 alter table
                sql = "ALTER TABLE planDesc RENAME TO temp_planDesc";
                _db.execSQL(sql);

                sql = "CREATE TABLE planDesc (planId INTEGER NOT NULL, planRef TEXT NOT NULL, startDt TEXT NOT NULL, endDt TEXT NOT NULL, bCount INTEGER NOT NULL, cCount INTEGER NOT NULL, vCount INTEGER NOT NULL, vDayCount INTEGER NOT NULL, dayCount INTEGER NOT NULL, PRIMARY KEY (planRef))";
                _db.execSQL(sql);

                sql = "INSERT INTO planDesc SELECT planId, planRef, startDt, endDt, bCount, cCount, vCount, vDayCount, dayCount FROM temp_planDesc";
                _db.execSQL(sql);

                sql = DropTable("temp_planDesc");
                _db.execSQL(sql);
            }
            if (oldVersion < 18)    //1..17 => 18
            {
                final String themeName = PCommon.GetPref(_context, IProject.APP_PREF_KEY.THEME_NAME, "LIGHT");
                if (themeName.equalsIgnoreCase("LIGHT_AND_BLUE"))
                {
                    PCommon.SavePref(_context, IProject.APP_PREF_KEY.THEME_NAME, "LIGHT");
                }
            }
            if (oldVersion < 20)    //1..19 => 20
            {
                PCommon.SavePref(_context, IProject.APP_PREF_KEY.UI_LAYOUT, "C");
            }
            if (oldVersion < 23)    //1..22 => 23
            {
                sql = "DROP INDEX IF EXISTS bibleNumber_ndx";
                _db.execSQL(sql);

                sql = "CREATE UNIQUE INDEX bibleNumber_ndx on bible (bbName, bNumber, cNumber, vNumber)";
                _db.execSQL(sql);
            }
            if (oldVersion < 24)   //1..23 ==> 24
            {
                //--- New setting
                PCommon.SavePref(_context, IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_5, "1");
            }
            if (oldVersion < 29)   //1..28 => 29
            {
                PCommon.SavePref(_context, IProject.APP_PREF_KEY.EDIT_DIALOG, "");
                PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.EDIT_STATUS, 0);
                PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.EDIT_ART_ID, -1);
                PCommon.SavePref(_context, IProject.APP_PREF_KEY.EDIT_SELECTION, "");

                sql = DropTable("artDesc");
                _db.execSQL(sql);

                sql = "CREATE TABLE artDesc (artId INTEGER NOT NULL, artUpdatedDt TEXT NOT NULL, artTitle TEXT NOT NULL, artSrc TEXT NOT NULL, PRIMARY KEY (artTitle))";
                _db.execSQL(sql);

                sql = "CREATE UNIQUE INDEX artDesc0_ndx on artDesc (artId)";
                _db.execSQL(sql);
            }
            if (oldVersion < 32)    //1..31 => 32
            {
                PCommon.SavePref(_context, IProject.APP_PREF_KEY.LISTEN_POSITION, "k,1,1");
            }
            if (oldVersion < 40)    //1..39 ==> 40
            {
                PCommon.SavePref(_context, IProject.APP_PREF_KEY.UI_LAYOUT_TV_BORDERS, "");
                PCommon.SavePref(_context, IProject.APP_PREF_KEY.UI_LAYOUT_TV_BORDERS_DIALOG, "");

                final String borders = PCommon.GetUiLayoutTVBorders(_context,null);
                PCommon.SavePref(_context, IProject.APP_PREF_KEY.UI_LAYOUT_TV_BORDERS, borders);
                PCommon.SavePref(_context, IProject.APP_PREF_KEY.UI_LAYOUT_TV_BORDERS_DIALOG, borders);
            }
            if (oldVersion < 45)    //1..44 => 45
            {
                String listenPosition = PCommon.GetPref(_context, IProject.APP_PREF_KEY.LISTEN_POSITION, "k,1,1");
                final String[] arr = listenPosition.split(",");
                if (arr.length < 4)
                {
                    final String bbName = arr[0];
                    final int bNumber = Integer.parseInt(arr[1]);
                    final int cNumber = Integer.parseInt(arr[2]);
                    final int vNumber = 1;

                    listenPosition = PCommon.ConcaT(bbName, ",", bNumber, ",", cNumber, ",", vNumber);
                    PCommon.SavePref(_context, IProject.APP_PREF_KEY.LISTEN_POSITION, listenPosition);
                }
            }
            if (oldVersion < 46)    //1..45 => 46
            {
                //--- New setting
                PCommon.SavePref(_context, IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_6, "3");
            }
            if (oldVersion < 48)    //1..47 => 48
            {
                //--- New setting
                PCommon.SavePref(_context, IProject.APP_PREF_KEY.MENU_DIALOG, "");
            }
            if (oldVersion < 51)    //1..50 => 51
            {
                //--- New setting
                PCommon.SavePref(_context, IProject.APP_PREF_KEY.CLIPBOARD_IDS, "");
            }
            if (oldVersion < 58)    //1..57 => 58
            {
                //--- New setting
                PCommon.SavePref(_context, IProject.APP_PREF_KEY.TODO_STATUS, "TODO");

                sql = "DROP INDEX IF EXISTS td0_ndx";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS td1_ndx";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS td2_ndx";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS td3_ndx";
                _db.execSQL(sql);

                sql = DropTable("td");
                _db.execSQL(sql);

                sql = "CREATE TABLE td (tdId INTEGER not null, tdStatus TEXT not null, tdPriority TEXT not null, tdDesc TEXT not null, tdCommentIssues TEXT not null);";
                _db.execSQL(sql);

                sql = "CREATE UNIQUE INDEX td0_ndx on td (tdId)";
                _db.execSQL(sql);

                sql = "CREATE INDEX td1_ndx on td (tdStatus)";
                _db.execSQL(sql);

                sql = "CREATE INDEX td2_ndx on td (tdPriority)";
                _db.execSQL(sql);

                sql = "CREATE INDEX td3_ndx on td (tdDesc)";
                _db.execSQL(sql);
            }
            if (oldVersion < 59)    //1..58 => 59
            {
                //--- New setting
                PCommon.SavePref(_context, IProject.APP_PREF_KEY.BIBLE_APP_TYPE, "M");
            }
            if (oldVersion < 63)    //1..62 => 63
            {
                //--- New setting
                PCommon.SavePref(_context, IProject.APP_PREF_KEY.STYLE_HIGHLIGHT_SEARCH, _context.getString(R.string.highlightSearchStyleDefault));
            }
            if (oldVersion < 72)    //1..71 => 72
            {
                //--- New settings
                PCommon.SavePref(_context, IProject.APP_PREF_KEY.RANDOM_CATS_SELECTED, "");
                PCommon.SavePref(_context, IProject.APP_PREF_KEY.RANDOM_BOOKS_SELECTED, "");
            }
            if (oldVersion < 78)    //1..77 => 78
            {
                //--- New settings
                PCommon.SavePref(_context, IProject.APP_PREF_KEY.FONT_HINDI_NAME, "");

                String altLanguage;
                final String bbName = PCommon.GetPrefBibleName(_context);
                switch(bbName) {
                    case "v":
                    case "9":
                        altLanguage = "es";
                        break;
                    case "l":
                    case "o":
                        altLanguage = "fr";
                        break;
                    case "d":
                    case "1":
                        altLanguage = "it";
                        break;
                    case "a":
                        altLanguage = "pt";
                        break;
                    case "i":
                        altLanguage = "hi";
                        break;
                    default:
                        altLanguage = "en";
                        break;
                }
                PCommon.SavePref(_context, IProject.APP_PREF_KEY.ALT_LANGUAGE, altLanguage);
            }
            if (oldVersion < 82)    //1..81 => 82
            {
                //--- CACHETAB
                sql = "DROP INDEX IF EXISTS cacheTab0_ndx";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS cacheTab1_ndx";
                _db.execSQL(sql);

                sql = "ALTER TABLE cacheTab RENAME TO temp_cacheTab";
                _db.execSQL(sql);

                sql = "CREATE TABLE cacheTab (tabId INTEGER NOT NULL, tabType TEXT CHECK(tabType='S' OR tabType='F' or tabType='A' or tabType='P'), tabTitle TEXT NOT NULL, fullQuery TEXT NOT NULL, scrollPosY INTEGER NOT NULL, bbName TEXT NOT NULL, isBook INTEGER NOT NULL, isChapter INTEGER NOT NULL, isVerse INTEGER NOT NULL, bNumber INTEGER NOT NULL, cNumber INTEGER NOT NULL, vNumber INTEGER NOT NULL, trad TEXT, orderBy INTEGER DEFAULT 0, favFilter INTEGER DEFAULT 0, PRIMARY KEY (tabId))";
                _db.execSQL(sql);

                sql = "CREATE UNIQUE INDEX cacheTab0_ndx on cacheTab (tabId)";
                _db.execSQL(sql);

                sql = "CREATE INDEX cacheTab1_ndx on cacheTab (tabType)";
                _db.execSQL(sql);

                sql = "INSERT INTO cacheTab SELECT tabId, tabType, tabTitle, fullQuery, scrollPosY, bbName, isBook, isChapter, isVerse, bNumber, cNumber, vNumber, trad, 0 AS orderBy, 0 AS favFilter FROM temp_cacheTab";
                _db.execSQL(sql);

                sql = DropTable("temp_cacheTab");
                _db.execSQL(sql);

                //--- BIBLENOTE
                sql = "ALTER TABLE bibleNote RENAME TO temp_bibleNote";
                _db.execSQL(sql);

                sql = "CREATE TABLE bibleNote (bNumber INTEGER NOT NULL, cNumber INTEGER NOT NULL, vNumber INTEGER NOT NULL, changeDt TEXT NOT NULL, mark INTEGER CHECK(mark >= 1), note TEXT NOT NULL, PRIMARY KEY (bNumber, cNumber, vNumber))";
                _db.execSQL(sql);

                sql = "INSERT INTO bibleNote SELECT bNumber, cNumber, vNumber, changeDt, mark, note FROM temp_bibleNote";
                _db.execSQL(sql);

                sql = DropTable("temp_bibleNote");
                _db.execSQL(sql);

                //--- BOOKMARK
                sql = DropTable("bookmark");
                _db.execSQL(sql);

                sql = "CREATE TABLE bookmark (bmId INTEGER NOT NULL, bmCurrent TEXT NOT NULL, bmDesc TEXT NOT NULL, bmDev1 TEXT NOT NULL, bmDev2 TEXT DEFAULT '', bmDev3 TEXT DEFAULT '', bmPrev1 TEXT DEFAULT '', bmPrev2 TEXT DEFAULT '', bmPrev3 TEXT DEFAULT '', bmPrev4 TEXT DEFAULT '', bmPrev5 TEXT DEFAULT '', PRIMARY KEY(bmId))";
                _db.execSQL(sql);

                sql = "INSERT INTO bookmark ('bmId','bmCurrent','bmDesc','bmDev1','bmDev2','bmDev3','bmPrev1','bmPrev2','bmPrev3','bmPrev4','bmPrev5') VALUES (0,'☆','Internal','☆','','','','','','','')";
                _db.execSQL(sql);

                sql = "INSERT INTO bookmark ('bmId','bmCurrent','bmDesc','bmDev1','bmDev2','bmDev3','bmPrev1','bmPrev2','bmPrev3','bmPrev4','bmPrev5') VALUES (1,'✞','Favorite','✞','✝','✝️','','','','','')";
                _db.execSQL(sql);

                sql = "INSERT INTO bookmark ('bmId','bmCurrent','bmDesc','bmDev1','bmDev2','bmDev3','bmPrev1','bmPrev2','bmPrev3','bmPrev4','bmPrev5') VALUES (2,'📖','Reading','️📖','','','','','','','')";
                _db.execSQL(sql);

                sql = "INSERT INTO bookmark ('bmId','bmCurrent','bmDesc','bmDev1','bmDev2','bmDev3','bmPrev1','bmPrev2','bmPrev3','bmPrev4','bmPrev5') VALUES (10,'💕️','Love','💕️','❤️','♥️','','','','','')";
                _db.execSQL(sql);

                sql = "INSERT INTO bookmark ('bmId','bmCurrent','bmDesc','bmDev1','bmDev2','bmDev3','bmPrev1','bmPrev2','bmPrev3','bmPrev4','bmPrev5') VALUES (20,'💯','Top 100','️💯','☝️','👍','','','','','')";
                _db.execSQL(sql);

                sql = "INSERT INTO bookmark ('bmId','bmCurrent','bmDesc','bmDev1','bmDev2','bmDev3','bmPrev1','bmPrev2','bmPrev3','bmPrev4','bmPrev5') VALUES (23,'📚','To read','📚','📓','📔','','','','','')";
                _db.execSQL(sql);

                sql = "INSERT INTO bookmark ('bmId','bmCurrent','bmDesc','bmDev1','bmDev2','bmDev3','bmPrev1','bmPrev2','bmPrev3','bmPrev4','bmPrev5') VALUES (30,'✏️','To study','️✏️','🖋️','🖍️️','','','','','')";
                _db.execSQL(sql);

                sql = "INSERT INTO bookmark ('bmId','bmCurrent','bmDesc','bmDev1','bmDev2','bmDev3','bmPrev1','bmPrev2','bmPrev3','bmPrev4','bmPrev5') VALUES (40,'🤔','To solve','🤔️','🤨','💡','','','','','')";
                _db.execSQL(sql);

                sql = "INSERT INTO bookmark ('bmId','bmCurrent','bmDesc','bmDev1','bmDev2','bmDev3','bmPrev1','bmPrev2','bmPrev3','bmPrev4','bmPrev5') VALUES (50,'✅','Done','✅','☑️','🆗','','','','','')";
                _db.execSQL(sql);

                sql = "INSERT INTO bookmark ('bmId','bmCurrent','bmDesc','bmDev1','bmDev2','bmDev3','bmPrev1','bmPrev2','bmPrev3','bmPrev4','bmPrev5') VALUES (60,'❗','Exclamation','❗️','❕','','','','','','')";
                _db.execSQL(sql);

                sql = "INSERT INTO bookmark ('bmId','bmCurrent','bmDesc','bmDev1','bmDev2','bmDev3','bmPrev1','bmPrev2','bmPrev3','bmPrev4','bmPrev5') VALUES (70,'❓','Question','️❓','❔','','','','','','')";
                _db.execSQL(sql);

                sql = "INSERT INTO bookmark ('bmId','bmCurrent','bmDesc','bmDev1','bmDev2','bmDev3','bmPrev1','bmPrev2','bmPrev3','bmPrev4','bmPrev5') VALUES (80,'🔥','Important','️🔥','🔑','🗝️','','','','','')";
                _db.execSQL(sql);

                sql = "INSERT INTO bookmark ('bmId','bmCurrent','bmDesc','bmDev1','bmDev2','bmDev3','bmPrev1','bmPrev2','bmPrev3','bmPrev4','bmPrev5') VALUES (90,'⚡','Danger','️⚡','💥','⚠️️','','','','','')";
                _db.execSQL(sql);

                sql = "INSERT INTO bookmark ('bmId','bmCurrent','bmDesc','bmDev1','bmDev2','bmDev3','bmPrev1','bmPrev2','bmPrev3','bmPrev4','bmPrev5') VALUES (100,'💀','Death','️💀','☠️','👽','','','','','')";
                _db.execSQL(sql);

                sql = "INSERT INTO bookmark ('bmId','bmCurrent','bmDesc','bmDev1','bmDev2','bmDev3','bmPrev1','bmPrev2','bmPrev3','bmPrev4','bmPrev5') VALUES (105,'🌱','Life','️🌱','🌼','🌸','','','','','')";
                _db.execSQL(sql);

                sql = "INSERT INTO bookmark ('bmId','bmCurrent','bmDesc','bmDev1','bmDev2','bmDev3','bmPrev1','bmPrev2','bmPrev3','bmPrev4','bmPrev5') VALUES (110,'✨','Prophecy','️✨','🌟','☄️','','','','','')";
                _db.execSQL(sql);
            }
            if (oldVersion < 83)    //1..82 => 83
            {
                sql = "UPDATE bookmark SET bmCurrent='\uD83D\uDCA1' WHERE bmId=40";
                _db.execSQL(sql);
            }
            if (oldVersion < 85)    //1..84 => 85
            {
                //LexBook
                sql = DropTable("lexBook");
                _db.execSQL(sql);

                sql = "CREATE TABLE lexBook (bNumber INTEGER NOT NULL, bsName TEXT NOT NULL)";
                _db.execSQL(sql);

                //TBES
                sql = DropTable("lexTbes");
                _db.execSQL(sql);

                sql = "CREATE TABLE lexTbes (id INTEGER NOT NULL, eStrong TEXT, dStrong TEXT, uStrong TEXT, hg TEXT, transliteration TEXT, grammar TEXT, english TEXT, meaning TEXT)";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS lexTbes0_ndx";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS lexTbes1_ndx";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS lexTbes2_ndx";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS lexTbes3_ndx";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS lexTbes4_ndx";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS lexTbes5_ndx";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS lexTbes6_ndx";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS lexTbes7_ndx";
                _db.execSQL(sql);

                //LexAggr
                sql = DropTable("lexAggr");
                _db.execSQL(sql);

                sql = "CREATE TABLE lexAggr (id INTEGER NOT NULL, field0 TEXT, field1 TEXT, field2 TEXT, field3 TEXT, field4 TEXT, field5 TEXT, field6 TEXT, field7 TEXT, field8 TEXT, field9 TEXT, field10 TEXT)";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS lexAggr0_ndx";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS lexAggr1_ndx";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS lexAggr2_ndx";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS lexAggr3_ndx";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS lexAggr4_ndx";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS lexAggr5_ndx";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS lexAggr6_ndx";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS lexAggr7_ndx";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS lexAggr8_ndx";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS lexAggr9_ndx";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS lexAggr10_ndx";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS lexAggr11_ndx";
                _db.execSQL(sql);

                //Import Lex
                ImportLex();
            }

            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //Last
            int fillDbUpdPoint = 0; //When 0 ==> No additional install
            if (oldVersion < 92)    //1..91 => 92
            {
                fillDbUpdPoint = 17;
            }
            if (oldVersion < 91)    //1..90 => 91
            {
                fillDbUpdPoint = 16;
            }
            if (oldVersion < 90)    //1..89 => 90
            {
                fillDbUpdPoint = 15;
            }
            if (oldVersion < 89)    //1..88 => 89
            {
                fillDbUpdPoint = 14;
            }
            if (oldVersion < 88)    //1..87 => 88
            {
                fillDbUpdPoint = 13;
            }
            if (oldVersion < 84)    //1..83 => 84
            {
                fillDbUpdPoint = 12;
            }
            if (oldVersion < 81)    //1..80 => 81
            {
                fillDbUpdPoint = 11;
            }
            if (oldVersion < 80)    //1..79 => 80
            {
                fillDbUpdPoint = 9;
            }
            if (oldVersion < 79)    //1..78 => 79
            {
                fillDbUpdPoint = 8;
            }
            if (oldVersion < 78)    //1..77 => 78
            {
                fillDbUpdPoint = 7;
            }
            if (oldVersion < 76)    //1..75 => 76
            {
                fillDbUpdPoint = 6;
            }
            if (oldVersion < 75)    //1..74 => 75
            {
                fillDbUpdPoint = 5;
            }
            if (oldVersion < 74)    //1..73 ==> 74
            {
                fillDbUpdPoint = 4;
            }
            if (oldVersion < 67)    //1..66  ==> 67
            {
                fillDbUpdPoint = 3;
            }
            if (oldVersion < 46)    //1..45  ==> 46
            {
                fillDbUpdPoint = 2;
            }
            if (oldVersion < 24)    //1..23  ==> 24
            {
                fillDbUpdPoint = 1;
            }
            if (fillDbUpdPoint > 0)
            {
                FillDbWithAdditional(fillDbUpdPoint);
            }

            //Very last
            if (oldVersion < 37)
            {
                //--- REF
                sql = "UPDATE bibleRef SET bsName='2Chr' WHERE bbName='k' AND bNumber=14";
                _db.execSQL(sql);

                sql = "UPDATE bibleRef SET bName='Jacques' WHERE bbName='l' AND bNumber=59";
                _db.execSQL(sql);

                //--- CR
                sql = DropTable("bibleCrossRef");
                _db.execSQL(sql);

                sql = "CREATE TABLE bibleCrossRef (crId INTEGER NOT NULL, bNumberFrom INTEGER NOT NULL, cNumberFrom INTEGER NOT NULL, vNumberFrom INTEGER NOT NULL, bNumberTo INTEGER NOT NULL, cNumberTo INTEGER NOT NULL, vNumberTo INTEGER NOT NULL)";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS bibleCrossRef0_ndx";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS bibleCrossRef1_ndx";
                _db.execSQL(sql);

                sql = "CREATE INDEX bibleCrossRef0_ndx on bibleCrossRef (bNumberFrom, cNumberFrom, vNumberFrom)";
                _db.execSQL(sql);

                sql = "CREATE INDEX bibleCrossRef1_ndx on bibleCrossRef (bNumberTo, cNumberTo, vNumberTo)";
                _db.execSQL(sql);

                sql = DropTable("bibleCrossRefi");
                _db.execSQL(sql);

                sql = "CREATE TABLE bibleCrossRefi (bNumber integer not null, cNumber integer not null, vNumber integer not null, tot integer not null);";
                _db.execSQL(sql);

                sql = "DROP INDEX IF EXISTS bibleCrossRefi0_ndx";
                _db.execSQL(sql);

                sql = "CREATE UNIQUE INDEX bibleCrossRefi0_ndx on bibleCrossRefi (bNumber, cNumber, vNumber)";
                _db.execSQL(sql);

                ImportCr();
            }
            if (oldVersion < _version)    //1..(last-1) => last
            {
                //=== FOR LAST VERSION
                //PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.UPDATE_STATUS, 0);

                if (PCommon._isDebug) PrintGlobalSettings();

                //if (fillDbUpdPoint == 0)
                //{
                //    PCommon.ShowToast(_context, R.string.installFinish, Toast.LENGTH_LONG);
                //}

                _db.setVersion(_version);
                //=== END FOR LAST VERSION
            }
        }
        catch (Exception ex)
        {
            LogE(ex);
        }
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="-- Db Methods --">

    private void LogD(final String msg)
    {
        if (PCommon._isDebug) System.out.println(PCommon.ConcaT("DB ", msg));
        //Log.d("DB", msg);
    }

    /***
     * Log error
     * @param ex    Exception
     */
    private void LogE(Exception ex)
    {
        if (PCommon._isDebug) System.out.println(PCommon.ConcaT("DB, Failure ", PCommon.StackTraceToString(ex.getStackTrace())));
        //was Log.e("DB", "Failure", ex);
    }

    private void SetGlobalSettings()
    {
        PCommon.SavePref(_context,      IProject.APP_PREF_KEY.UI_LAYOUT, "C");
        final boolean isUiTelevision = PCommon.DetectIsUiTelevision(_context);

        PCommon.SavePref(_context, IProject.APP_PREF_KEY.UI_LAYOUT_TV_BORDERS, "");
        PCommon.SavePref(_context, IProject.APP_PREF_KEY.UI_LAYOUT_TV_BORDERS_DIALOG, "");

        final String borders = PCommon.GetUiLayoutTVBorders(_context,null);
        PCommon.SavePref(_context,      IProject.APP_PREF_KEY.UI_LAYOUT_TV_BORDERS, borders);
        PCommon.SavePref(_context,      IProject.APP_PREF_KEY.UI_LAYOUT_TV_BORDERS_DIALOG, borders);

        PCommon.SavePrefInt(_context,   IProject.APP_PREF_KEY.INSTALL_STATUS, 1);                   //Will be updated
        //PCommon.SavePrefInt(_context,   IProject.APP_PREF_KEY.UPDATE_STATUS, 1);
        PCommon.SavePref(_context,      IProject.APP_PREF_KEY.LOG_STATUS, "");
        PCommon.SavePref(_context,      IProject.APP_PREF_KEY.EDIT_DIALOG, "");
        PCommon.SavePrefInt(_context,   IProject.APP_PREF_KEY.EDIT_STATUS, 0);
        PCommon.SavePrefInt(_context,   IProject.APP_PREF_KEY.EDIT_ART_ID, -1);
        PCommon.SavePref(_context,      IProject.APP_PREF_KEY.EDIT_SELECTION, "");
        PCommon.SavePref(_context,      IProject.APP_PREF_KEY.BIBLE_APP_TYPE, "1");
        PCommon.SavePref(_context,      IProject.APP_PREF_KEY.BIBLE_NAME, "");
        PCommon.SavePref(_context,      IProject.APP_PREF_KEY.ALT_LANGUAGE, "");
        PCommon.SavePref(_context,      IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, "k");
        PCommon.SavePref(_context,      IProject.APP_PREF_KEY.BOOK_CHAPTER_DIALOG, "1");
        PCommon.SavePref(_context,      IProject.APP_PREF_KEY.CLIPBOARD_IDS, "");
        PCommon.SavePref(_context,      IProject.APP_PREF_KEY.TRAD_BIBLE_NAME, "");
        PCommon.SavePrefInt(_context,   IProject.APP_PREF_KEY.BIBLE_ID, 0);
        PCommon.SavePref(_context,      IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_1, "1");
        PCommon.SavePref(_context,      IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_2, "2");
        PCommon.SavePref(_context,      IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_3, "3");
        PCommon.SavePref(_context,      IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_4, "2");
        PCommon.SavePref(_context,      IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_5, "1");
        PCommon.SavePref(_context,      IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_6, "3");
        PCommon.SavePref(_context,      IProject.APP_PREF_KEY.LISTEN_POSITION, "k,1,1,1");
        PCommon.SavePref(_context,      IProject.APP_PREF_KEY.THEME_NAME, "LIGHT");
        PCommon.SavePref(_context,      IProject.APP_PREF_KEY.FONT_NAME, isUiTelevision ? "RobotoCondensed.regular" : "");
        PCommon.SavePref(_context,      IProject.APP_PREF_KEY.FONT_HINDI_NAME, "");
        PCommon.SavePref(_context,      IProject.APP_PREF_KEY.FONT_SIZE,"20");
        PCommon.SavePref(_context,      IProject.APP_PREF_KEY.MENU_DIALOG, "");
        PCommon.SavePref(_context,      IProject.APP_PREF_KEY.TODO_STATUS, "TODO");
        PCommon.SavePrefInt(_context,   IProject.APP_PREF_KEY.FAV_FILTER, 0);
        PCommon.SavePrefInt(_context,   IProject.APP_PREF_KEY.FAV_ORDER, 1);
        PCommon.SavePrefInt(_context,   IProject.APP_PREF_KEY.VIEW_POSITION, 0);
        PCommon.SavePrefInt(_context,   IProject.APP_PREF_KEY.PLAN_ID, -1);
        PCommon.SavePrefInt(_context,   IProject.APP_PREF_KEY.PLAN_PAGE, -1);
        PCommon.SavePref(_context,      IProject.APP_PREF_KEY.STYLE_HIGHLIGHT_SEARCH, _context.getString(R.string.highlightSearchStyleDefault));
        PCommon.SavePref(_context,      IProject.APP_PREF_KEY.RANDOM_CATS_SELECTED, "");
        PCommon.SavePref(_context,      IProject.APP_PREF_KEY.RANDOM_BOOKS_SELECTED, "");
    }

    private void PrintGlobalSettings()
    {
        try
        {
            if (!PCommon._isDebug) return;

            System.out.println(PCommon.ConcaT("INSTALL_STATUS:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, "")));
            //System.out.println(PCommon.ConcaT("UPDATE_STATUS:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.UPDATE_STATUS, "")));
            System.out.println(PCommon.ConcaT("LOG_STATUS:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.LOG_STATUS, "")));
            System.out.println(PCommon.ConcaT("EDIT_DIALOG:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.EDIT_DIALOG, "")));
            System.out.println(PCommon.ConcaT("EDIT_STATUS:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.EDIT_STATUS, "")));
            System.out.println(PCommon.ConcaT("EDIT_ART_ID:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.EDIT_ART_ID, "")));
            System.out.println(PCommon.ConcaT("EDIT_SELECTION:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.EDIT_SELECTION, "")));
            System.out.println(PCommon.ConcaT("BIBLE_APP_TYPE:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.BIBLE_APP_TYPE, "")));
            System.out.println(PCommon.ConcaT("BIBLE_NAME:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.BIBLE_NAME, "")));
            System.out.println(PCommon.ConcaT("ALT_LANGUAGE:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.ALT_LANGUAGE, "")));
            System.out.println(PCommon.ConcaT("BIBLE_NAME_DIALOG:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, "")));
            System.out.println(PCommon.ConcaT("TRAD_BIBLE_NAME:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.TRAD_BIBLE_NAME, "")));
            System.out.println(PCommon.ConcaT("BIBLE_ID:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.BIBLE_ID, "")));
            System.out.println(PCommon.ConcaT("CLIPBOARD_IDS:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.CLIPBOARD_IDS, "")));
            System.out.println(PCommon.ConcaT("LAYOUT_DYNAMIC_1:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_1, "")));
            System.out.println(PCommon.ConcaT("LAYOUT_DYNAMIC_2:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_2, "")));
            System.out.println(PCommon.ConcaT("LAYOUT_DYNAMIC_3:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_3, "")));
            System.out.println(PCommon.ConcaT("LAYOUT_DYNAMIC_4:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_4, "")));
            System.out.println(PCommon.ConcaT("LAYOUT_DYNAMIC_5:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_5, "")));
            System.out.println(PCommon.ConcaT("LAYOUT_DYNAMIC_6:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.LAYOUT_DYNAMIC_6, "")));
            System.out.println(PCommon.ConcaT("LISTEN_POSITION:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.LISTEN_POSITION, "")));
            System.out.println(PCommon.ConcaT("THEME_NAME:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.THEME_NAME, "")));
            System.out.println(PCommon.ConcaT("FONT_NAME:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.FONT_NAME, "")));
            System.out.println(PCommon.ConcaT("FONT_HINDI_NAME:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.FONT_HINDI_NAME, "")));
            System.out.println(PCommon.ConcaT("FONT_SIZE:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.FONT_SIZE, "")));
            System.out.println(PCommon.ConcaT("STYLE_HIGHLIGHT_SEARCH", PCommon.GetPref(_context, IProject.APP_PREF_KEY.STYLE_HIGHLIGHT_SEARCH, "")));
            System.out.println(PCommon.ConcaT("FAV_FILTER:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.FAV_FILTER, "")));
            System.out.println(PCommon.ConcaT("FAV_ORDER:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.FAV_ORDER, "")));
            System.out.println(PCommon.ConcaT("VIEW_POSITION:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.VIEW_POSITION, "")));
            System.out.println(PCommon.ConcaT("PLAN_ID:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.PLAN_ID, "")));
            System.out.println(PCommon.ConcaT("PLAN_PAGE:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.PLAN_PAGE, "")));
            System.out.println(PCommon.ConcaT("UI_LAYOUT:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.UI_LAYOUT, "")));
            System.out.println(PCommon.ConcaT("UI_LAYOUT_TV_BORDERS:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.UI_LAYOUT_TV_BORDERS, "")));
            System.out.println(PCommon.ConcaT("MENU_DIALOG:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.MENU_DIALOG, "")));
            System.out.println(PCommon.ConcaT("TODO_STATUS:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.TODO_STATUS, "")));
            System.out.println(PCommon.ConcaT("RANDOM_CATS_SELECTED:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.RANDOM_CATS_SELECTED, "")));
            System.out.println(PCommon.ConcaT("RANDOM_BOOKS_SELECTED:", PCommon.GetPref(_context, IProject.APP_PREF_KEY.RANDOM_BOOKS_SELECTED, "")));
        }
        catch (Exception ex)
        {
            LogE(ex);
        }
    }

    private String DropTable(final String tblName)
    {
        try
        {
            return PCommon.ConcaT("DROP TABLE IF EXISTS ", tblName);
        }
        catch(Exception ex)
        {
            return null;
        }
    }

    /***
     * Get db version of this code, not the official db version installed
     */
    public int GetDbVersion()
    {
        return _version;
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="-- Bible Methods --">

    /***
     * Fill db
     */
    private void FillDbWithAdditional(final int step)
    {
        try
        {
            final ThreadGroup threadGroup = new ThreadGroup(_context.getString(R.string.threadNfoGroup));
            final String threadName = PCommon.ConcaT(_context.getString(R.string.threadNfoPrefix), PCommon.TimeFuncShort(), _context.getString(R.string.threadNfoInstall));
            //noinspection SameParameterValue
            final Thread thread = new Thread(threadGroup, threadName)
            {
                final int limit = 31102;
                int id = limit * (step + 3);
                final Handler handler = new Handler();

                @Override
                public void run()
                {
                    FillDbTask();
                }

                private void FillDbTask()
                {
                    try
                    {
                        if (step == 1)
                        {
                            ImportBibleRef("a");
                            ImportXmlBible("a");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 5);

                            ImportBibleRef("o");
                            ImportXmlBible("o");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 6);

                            ImportBibleRef("2");
                            ImportXmlBible("2");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 7);

                            ImportBibleRef("9");
                            ImportXmlBible("9");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 8);

                            ImportBibleRef("1");
                            ImportXmlBible("1");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 9);

                            ImportBibleRef("i");
                            ImportXmlBible("i");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 10);

                            ImportBibleRef("y");
                            ImportXmlBible("y");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 11);

                            ImportBibleRef("c");
                            ImportXmlBible("c");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 12);

                            ImportBibleRef("s");
                            ImportXmlBible("s");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 13);

                            ImportBibleRef("j");
                            ImportXmlBible("j");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 14);

                            ImportBibleRef("r");
                            ImportXmlBible("r");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 15);

                            ImportBibleRef("w");
                            ImportXmlBible("w");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 16);

                            ImportBibleRef("t");
                            ImportXmlBible("t");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 17);

                            ImportBibleRef("b");
                            ImportXmlBible("b");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 18);

                            ImportBibleRef("h");
                            ImportXmlBible("h");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 19);

                            ImportBibleRef("e");
                            ImportXmlBible("e");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 20);

                            ImportBibleRef("u");
                            ImportXmlBible("u");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 21);

                            ImportBibleRef("z");
                            ImportXmlBible("z");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 22);
                        }
                        else if (step == 2)
                        {
                            ImportBibleRef("o");
                            ImportXmlBible("o");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 6);

                            ImportBibleRef("2");
                            ImportXmlBible("2");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 7);

                            ImportBibleRef("9");
                            ImportXmlBible("9");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 8);

                            ImportBibleRef("1");
                            ImportXmlBible("1");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 9);

                            ImportBibleRef("i");
                            ImportXmlBible("i");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 10);

                            ImportBibleRef("y");
                            ImportXmlBible("y");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 11);

                            ImportBibleRef("c");
                            ImportXmlBible("c");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 12);

                            ImportBibleRef("s");
                            ImportXmlBible("s");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 13);

                            ImportBibleRef("j");
                            ImportXmlBible("j");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 14);

                            ImportBibleRef("r");
                            ImportXmlBible("r");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 15);

                            ImportBibleRef("w");
                            ImportXmlBible("w");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 16);

                            ImportBibleRef("t");
                            ImportXmlBible("t");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 17);

                            ImportBibleRef("b");
                            ImportXmlBible("b");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 18);

                            ImportBibleRef("h");
                            ImportXmlBible("h");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 19);

                            ImportBibleRef("e");
                            ImportXmlBible("e");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 20);

                            ImportBibleRef("u");
                            ImportXmlBible("u");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 21);

                            ImportBibleRef("z");
                            ImportXmlBible("z");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 22);
                        }
                        else if (step == 3)
                        {
                            ImportBibleRef("2");
                            ImportXmlBible("2");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 7);

                            ImportBibleRef("9");
                            ImportXmlBible("9");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 8);

                            ImportBibleRef("1");
                            ImportXmlBible("1");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 9);

                            ImportBibleRef("i");
                            ImportXmlBible("i");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 10);

                            ImportBibleRef("y");
                            ImportXmlBible("y");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 11);

                            ImportBibleRef("c");
                            ImportXmlBible("c");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 12);

                            ImportBibleRef("s");
                            ImportXmlBible("s");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 13);

                            ImportBibleRef("j");
                            ImportXmlBible("j");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 14);

                            ImportBibleRef("r");
                            ImportXmlBible("r");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 15);

                            ImportBibleRef("w");
                            ImportXmlBible("w");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 16);

                            ImportBibleRef("t");
                            ImportXmlBible("t");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 17);

                            ImportBibleRef("b");
                            ImportXmlBible("b");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 18);

                            ImportBibleRef("h");
                            ImportXmlBible("h");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 19);

                            ImportBibleRef("e");
                            ImportXmlBible("e");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 20);

                            ImportBibleRef("u");
                            ImportXmlBible("u");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 21);

                            ImportBibleRef("z");
                            ImportXmlBible("z");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 22);
                        }
                        else if (step == 4)
                        {
                            ImportBibleRef("9");
                            ImportXmlBible("9");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 8);

                            ImportBibleRef("1");
                            ImportXmlBible("1");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 9);

                            ImportBibleRef("i");
                            ImportXmlBible("i");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 10);

                            ImportBibleRef("y");
                            ImportXmlBible("y");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 11);

                            ImportBibleRef("c");
                            ImportXmlBible("c");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 12);

                            ImportBibleRef("s");
                            ImportXmlBible("s");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 13);

                            ImportBibleRef("j");
                            ImportXmlBible("j");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 14);

                            ImportBibleRef("r");
                            ImportXmlBible("r");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 15);

                            ImportBibleRef("w");
                            ImportXmlBible("w");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 16);

                            ImportBibleRef("t");
                            ImportXmlBible("t");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 17);

                            ImportBibleRef("b");
                            ImportXmlBible("b");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 18);

                            ImportBibleRef("h");
                            ImportXmlBible("h");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 19);

                            ImportBibleRef("e");
                            ImportXmlBible("e");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 20);

                            ImportBibleRef("u");
                            ImportXmlBible("u");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 21);

                            ImportBibleRef("z");
                            ImportXmlBible("z");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 22);
                        }
                        else if (step == 5)
                        {
                            ImportBibleRef("1");
                            ImportXmlBible("1");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 9);

                            ImportBibleRef("i");
                            ImportXmlBible("i");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 10);

                            ImportBibleRef("y");
                            ImportXmlBible("y");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 11);

                            ImportBibleRef("c");
                            ImportXmlBible("c");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 12);

                            ImportBibleRef("s");
                            ImportXmlBible("s");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 13);

                            ImportBibleRef("j");
                            ImportXmlBible("j");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 14);

                            ImportBibleRef("r");
                            ImportXmlBible("r");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 15);

                            ImportBibleRef("w");
                            ImportXmlBible("w");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 16);

                            ImportBibleRef("t");
                            ImportXmlBible("t");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 17);

                            ImportBibleRef("b");
                            ImportXmlBible("b");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 18);

                            ImportBibleRef("h");
                            ImportXmlBible("h");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 19);

                            ImportBibleRef("e");
                            ImportXmlBible("e");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 20);

                            ImportBibleRef("u");
                            ImportXmlBible("u");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 21);

                            ImportBibleRef("z");
                            ImportXmlBible("z");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 22);
                        }
                        else if (step == 6)
                        {
                            ImportBibleRef("i");
                            ImportXmlBible("i");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 10);

                            ImportBibleRef("y");
                            ImportXmlBible("y");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 11);

                            ImportBibleRef("c");
                            ImportXmlBible("c");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 12);

                            ImportBibleRef("s");
                            ImportXmlBible("s");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 13);

                            ImportBibleRef("j");
                            ImportXmlBible("j");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 14);

                            ImportBibleRef("r");
                            ImportXmlBible("r");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 15);

                            ImportBibleRef("w");
                            ImportXmlBible("w");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 16);

                            ImportBibleRef("t");
                            ImportXmlBible("t");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 17);

                            ImportBibleRef("b");
                            ImportXmlBible("b");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 18);

                            ImportBibleRef("h");
                            ImportXmlBible("h");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 19);

                            ImportBibleRef("e");
                            ImportXmlBible("e");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 20);

                            ImportBibleRef("u");
                            ImportXmlBible("u");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 21);

                            ImportBibleRef("z");
                            ImportXmlBible("z");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 22);
                        }
                        else if (step == 7)
                        {
                            ImportBibleRef("y");
                            ImportXmlBible("y");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 11);

                            ImportBibleRef("c");
                            ImportXmlBible("c");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 12);

                            ImportBibleRef("s");
                            ImportXmlBible("s");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 13);

                            ImportBibleRef("j");
                            ImportXmlBible("j");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 14);

                            ImportBibleRef("r");
                            ImportXmlBible("r");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 15);

                            ImportBibleRef("w");
                            ImportXmlBible("w");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 16);

                            ImportBibleRef("t");
                            ImportXmlBible("t");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 17);

                            ImportBibleRef("b");
                            ImportXmlBible("b");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 18);

                            ImportBibleRef("h");
                            ImportXmlBible("h");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 19);

                            ImportBibleRef("e");
                            ImportXmlBible("e");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 20);

                            ImportBibleRef("u");
                            ImportXmlBible("u");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 21);

                            ImportBibleRef("z");
                            ImportXmlBible("z");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 22);
                        }
                        else if (step == 8)
                        {
                            ImportBibleRef("c");
                            ImportXmlBible("c");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 12);

                            ImportBibleRef("s");
                            ImportXmlBible("s");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 13);

                            ImportBibleRef("j");
                            ImportXmlBible("j");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 14);

                            ImportBibleRef("r");
                            ImportXmlBible("r");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 15);

                            ImportBibleRef("w");
                            ImportXmlBible("w");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 16);

                            ImportBibleRef("t");
                            ImportXmlBible("t");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 17);

                            ImportBibleRef("b");
                            ImportXmlBible("b");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 18);

                            ImportBibleRef("h");
                            ImportXmlBible("h");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 19);

                            ImportBibleRef("e");
                            ImportXmlBible("e");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 20);

                            ImportBibleRef("u");
                            ImportXmlBible("u");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 21);

                            ImportBibleRef("z");
                            ImportXmlBible("z");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 22);
                        }
                        else if (step == 9)
                        {
                            ImportBibleRef("s");
                            ImportXmlBible("s");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 13);

                            ImportBibleRef("j");
                            ImportXmlBible("j");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 14);

                            ImportBibleRef("r");
                            ImportXmlBible("r");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 15);

                            ImportBibleRef("w");
                            ImportXmlBible("w");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 16);

                            ImportBibleRef("t");
                            ImportXmlBible("t");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 17);

                            ImportBibleRef("b");
                            ImportXmlBible("b");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 18);

                            ImportBibleRef("h");
                            ImportXmlBible("h");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 19);

                            ImportBibleRef("e");
                            ImportXmlBible("e");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 20);

                            ImportBibleRef("u");
                            ImportXmlBible("u");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 21);

                            ImportBibleRef("z");
                            ImportXmlBible("z");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 22);
                        }
                        else if (step == 11)
                        {
                            ImportBibleRef("r");
                            ImportXmlBible("r");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 15);

                            ImportBibleRef("w");
                            ImportXmlBible("w");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 16);

                            ImportBibleRef("t");
                            ImportXmlBible("t");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 17);

                            ImportBibleRef("b");
                            ImportXmlBible("b");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 18);

                            ImportBibleRef("h");
                            ImportXmlBible("h");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 19);

                            ImportBibleRef("e");
                            ImportXmlBible("e");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 20);

                            ImportBibleRef("u");
                            ImportXmlBible("u");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 21);

                            ImportBibleRef("z");
                            ImportXmlBible("z");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 22);
                        }
                        else if (step == 12)
                        {
                            ImportBibleRef("w");
                            ImportXmlBible("w");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 16);

                            ImportBibleRef("t");
                            ImportXmlBible("t");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 17);

                            ImportBibleRef("b");
                            ImportXmlBible("b");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 18);

                            ImportBibleRef("h");
                            ImportXmlBible("h");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 19);

                            ImportBibleRef("e");
                            ImportXmlBible("e");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 20);

                            ImportBibleRef("u");
                            ImportXmlBible("u");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 21);

                            ImportBibleRef("z");
                            ImportXmlBible("z");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 22);
                        }
                        else if (step == 13)
                        {
                            ImportBibleRef("t");
                            ImportXmlBible("t");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 17);

                            ImportBibleRef("b");
                            ImportXmlBible("b");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 18);

                            ImportBibleRef("h");
                            ImportXmlBible("h");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 19);

                            ImportBibleRef("e");
                            ImportXmlBible("e");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 20);

                            ImportBibleRef("u");
                            ImportXmlBible("u");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 21);

                            ImportBibleRef("z");
                            ImportXmlBible("z");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 22);
                        }
                        else if (step == 14)
                        {
                            ImportBibleRef("b");
                            ImportXmlBible("b");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 18);

                            ImportBibleRef("h");
                            ImportXmlBible("h");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 19);

                            ImportBibleRef("e");
                            ImportXmlBible("e");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 20);

                            ImportBibleRef("u");
                            ImportXmlBible("u");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 21);

                            ImportBibleRef("z");
                            ImportXmlBible("z");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 22);
                        }
                        else if (step == 15)
                        {
                            ImportBibleRef("h");
                            ImportXmlBible("h");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 19);

                            ImportBibleRef("e");
                            ImportXmlBible("e");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 20);

                            ImportBibleRef("u");
                            ImportXmlBible("u");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 21);

                            ImportBibleRef("z");
                            ImportXmlBible("z");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 22);
                        }
                        else if (step == 16)
                        {
                            ImportBibleRef("e");
                            ImportXmlBible("e");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 20);

                            ImportBibleRef("u");
                            ImportXmlBible("u");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 21);

                            ImportBibleRef("z");
                            ImportXmlBible("z");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 22);
                        }
                        else if (step == 17)
                        {
                            ImportBibleRef("u");
                            ImportXmlBible("u");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 21);

                            ImportBibleRef("z");
                            ImportXmlBible("z");
                            PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.INSTALL_STATUS, 22);
                        }
                    }
                    catch(Exception ex)
                    {
                        LogE(ex);
                    }
                    finally
                    {
                        handler.post(() -> PCommon.ShowToast(_context, R.string.installFinish, Toast.LENGTH_LONG));
                    }
                }

                @SuppressWarnings("SameParameterValue")
                private void ImportXmlBible(final String bbName) throws Exception
                {
                    _db.execSQL(PCommon.ConcaT("DELETE FROM bible where bbName=", PCommon.AQ(bbName)));

                    @SuppressWarnings("UnusedAssignment") int size = 0;
                    int tot = 0;
                    final String xmlName = bbName + ".xml";

                    AssetManager am = _context.getAssets();
                    InputStream is = am.open(xmlName);

                    do
                    {
                        size = is.read();
                        if (size >= 0) tot++;
                    }
                    while(size >= 0);

                    is.close();
                    is = am.open(xmlName);

                    VTDGen vg = null;
                    {
                        byte[] b = new byte[tot];
                        if (is.read(b, 0, tot) != -1)
                        {
                            vg = new VTDGen();
                            vg.setDoc(b);
                            vg.parse(false);
                        }
                        //noinspection UnusedAssignment
                        b = null;
                    }
                    is.close();

                    //Chapter infos
                    is = am.open("ci.txt");
                    BufferedReader br = new BufferedReader(new InputStreamReader(is));
                    String row;
                    String[] cols;

                    //Verses
                    @SuppressWarnings("ConstantConditions") VTDNav vn = vg.getNav();
                    AutoPilot apVerses = new AutoPilot();
                    apVerses.bind(vn);

                    String xpath, sql, vText;
                    final String bbname = PCommon.AQ(bbName);
                    final String insertSql = "INSERT INTO bible(id,bbName,bNumber,cNumber,vNumber,vText)VALUES(";

                    xpath = PCommon.ConcaT("//BIBLEBOOK/CHAPTER/VERS");
                    apVerses.selectXPath( xpath );

                    int b, c, v, vCount, i;
                    while ((row = br.readLine()) != null)
                    {
                        cols = row.split("\\|");
                        if (cols.length != 3) break;

                        b = Integer.parseInt( cols[0] );
                        c = Integer.parseInt( cols[1] );
                        vCount = Integer.parseInt( cols[2] );
                        v = 0;

                        for (i = 0; i < vCount; i++)
                        {
                            apVerses.evalXPath();

                            v++;
                            id++;
                            vText = PCommon.AQ(PCommon.RQ(vn.getXPathStringVal()));
                            sql = PCommon.ConcaT(insertSql, id, ",", bbname, ",", b, ",", c, ",", v, ",", vText, ")");

                            //_db.beginTransaction();
                            //if (PCommon._isDebugVersion) System.out.println(sql); //DEBUG ONLY
                            _db.execSQL(sql);
                            //_db.setTransactionSuccessful();

                            //noinspection UnusedAssignment
                            sql = null;
                            //noinspection UnusedAssignment
                            vText = null;
                        }

                        //noinspection UnusedAssignment
                        cols = null;
                        //noinspection UnusedAssignment
                        row = null;
                    }

                    apVerses.resetXPath();
                    //noinspection UnusedAssignment
                    apVerses = null;
                    //noinspection UnusedAssignment
                    vn = null;

                    br.close();
                    is.close();
                }

                private void ImportBibleRef(final String bbName) throws Exception
                {
                    final String fileName = PCommon.ConcaT(bbName, ".txt");
                    _db.execSQL(PCommon.ConcaT("DELETE FROM bibleRef where bbName=", PCommon.AQ(bbName)));

                    String row;
                    String[] cols;
                    BibleRefBO ref = new BibleRefBO();

                    AssetManager am = _context.getAssets();
                    InputStream is = am.open(fileName);
                    BufferedReader br = new BufferedReader(new InputStreamReader(is));

                    while ((row = br.readLine()) != null)
                    {
                        cols = row.split("\\|");
                        if (cols.length != 4) break;

                        ref.bbName = cols[0];
                        ref.bNumber = Integer.parseInt(cols[1]);
                        ref.bName = cols[2];
                        ref.bsName = cols[3];

                        AddBibleRef(ref);
                    }

                    br.close();
                    is.close();
                }

                /***
                 * Add bible reference
                 * @param r Reference
                 */
                private void AddBibleRef(final BibleRefBO r)
                {
                    final String sql = PCommon.ConcaT("INSERT INTO bibleRef(bbName,bNumber,bName,bsName)VALUES(",
                            PCommon.AQ(r.bbName), ",",
                            r.bNumber, ",",
                            PCommon.AQ(r.bName), ",",
                            PCommon.AQ(r.bsName), ")");

                    _db.execSQL(sql);
                }
            };
            thread.setPriority(Thread.NORM_PRIORITY);
            thread.start();
        }
        catch(Exception ex)
        {
            LogE(ex);
        }
    }

    private void ImportCi()
    {
        try
        {
            final ThreadGroup threadGroup = new ThreadGroup(_context.getString(R.string.threadNfoGroup));
            final String threadName = PCommon.ConcaT(_context.getString(R.string.threadNfoPrefix), PCommon.TimeFuncShort(), _context.getString(R.string.threadNfoInstall));
            final Thread thread = new Thread(threadGroup, threadName)
            {
                @Override
                public void run()
                {
                    ImportCi();
                }

                private void ImportCi()
                {
                    try
                    {
                        final String fileName = "ci.txt";
                        final String insertSql = "INSERT INTO bibleCi(ciId,bNumber,cNumber,vCount)VALUES(";
                        String row, sql;
                        String[] cols;
                        int b, c, vCount, id = 0;

                        AssetManager am = _context.getAssets();
                        InputStream is = am.open(fileName);
                        BufferedReader br = new BufferedReader(new InputStreamReader(is));

                        while ((row = br.readLine()) != null)
                        {
                            cols = row.split("\\|");
                            if (cols.length != 3)
                                break;

                            id++;
                            b = Integer.parseInt( cols[0] );
                            c = Integer.parseInt( cols[1] );
                            vCount = Integer.parseInt( cols[2] );
                            sql = PCommon.ConcaT(insertSql, id, ",", b, ",", c, ",", vCount, ")");

                            _db.execSQL(sql);

                            //noinspection UnusedAssignment
                            sql = null;
                            //noinspection UnusedAssignment
                            row = null;
                        }

                        br.close();
                        is.close();
                    }
                    catch (Exception ex)
                    {
                        LogE(ex);
                    }
                }
            };
            thread.setPriority(Thread.NORM_PRIORITY);
            thread.start();
        }
        catch(Exception ex)
        {
            LogE(ex);
        }
    }

    private void ImportCr()
    {
        try
        {
            final ThreadGroup threadGroup = new ThreadGroup(_context.getString(R.string.threadNfoGroup));
            final String threadName = PCommon.ConcaT(_context.getString(R.string.threadNfoPrefix), PCommon.TimeFuncShort(), _context.getString(R.string.threadNfoInstall));
            final Thread thread = new Thread(threadGroup, threadName)
            {
                @Override
                public void run()
                {
                    ImportCr();
                    ImportCri();
                }

                private void ImportCr()
                {
                    try
                    {
                        final String fileName = "cr.txt";
                        final String insertSql = "INSERT INTO bibleCrossRef(crId,bNumberFrom,cNumberFrom,vNumberFrom,bNumberTo,cNumberTo,vNumberTo)VALUES(";
                        String row, sql;
                        String[] cols;
                        int bFrom, cFrom, vFrom, bTo, cTo, vTo, id = 0;

                        AssetManager am = _context.getAssets();
                        InputStream is = am.open(fileName);
                        BufferedReader br = new BufferedReader(new InputStreamReader(is));

                        while ((row = br.readLine()) != null)
                        {
                            cols = row.split(" ");
                            if (cols.length != 6)
                                break;

                            id++;
                            bFrom = Integer.parseInt( cols[0] );
                            cFrom = Integer.parseInt( cols[1] );
                            vFrom = Integer.parseInt( cols[2] );
                            bTo = Integer.parseInt( cols[3] );
                            cTo = Integer.parseInt( cols[4] );
                            vTo = Integer.parseInt( cols[5] );
                            sql = PCommon.ConcaT(insertSql, id, ",", bFrom, ",", cFrom, ",", vFrom, ",", bTo, ",", cTo, ",", vTo, ")");
                            _db.execSQL(sql);

                            //noinspection UnusedAssignment
                            sql = null;
                            //noinspection UnusedAssignment
                            row = null;
                        }

                        br.close();
                        is.close();
                    }
                    catch (Exception ex)
                    {
                        LogE(ex);
                    }
                }

                @SuppressWarnings("UnusedAssignment")
                private void ImportCri()
                {
                    try
                    {
                        final String fileName = "cri.txt";
                        final String insertSql = "INSERT INTO bibleCrossRefi(bNumber,cNumber,vNumber,tot)VALUES(";
                        String row, sql;
                        String[] cols;
                        int b, c, v, tot = 0;

                        AssetManager am = _context.getAssets();
                        InputStream is = am.open(fileName);
                        BufferedReader br = new BufferedReader(new InputStreamReader(is));

                        while ((row = br.readLine()) != null)
                        {
                            cols = row.split("\\|");
                            if (cols.length != 4)
                                break;

                            b = Integer.parseInt( cols[0] );
                            c = Integer.parseInt( cols[1] );
                            v = Integer.parseInt( cols[2] );
                            tot = Integer.parseInt( cols[3] );
                            sql = PCommon.ConcaT(insertSql, b, ",", c, ",", v, ",", tot, ")");
                            _db.execSQL(sql);

                            //noinspection UnusedAssignment
                            sql = null;
                            //noinspection UnusedAssignment
                            row = null;
                        }

                        br.close();
                        is.close();
                    }
                    catch (Exception ex)
                    {
                        LogE(ex);
                    }
                }
            };
            thread.setPriority(Thread.NORM_PRIORITY);
            thread.start();
        }
        catch(Exception ex)
        {
            LogE(ex);
        }
    }

    private void ImportLex()
    {
        try
        {
            final ThreadGroup threadGroup = new ThreadGroup(_context.getString(R.string.threadNfoGroup));
            final String threadName = PCommon.ConcaT(_context.getString(R.string.threadNfoPrefix), PCommon.TimeFuncShort(), _context.getString(R.string.threadNfoInstall));
            final Thread thread = new Thread(threadGroup, threadName)
            {
                @Override
                public void run()
                {
                    if (PCommon._isDebug) System.out.println("ImportLex started");

                    ImportLexBook();
                    ImportLexTbes();
                    ImportLexAggr();
                    LexCreateIndexes();

                    if (PCommon._isDebug) System.out.println("ImportLex finished");
                }

                private void ImportLexTbes()
                {
                    try
                    {
                        final String fileName = "lextbes.tsv";
                        final String insertSql = "INSERT INTO lexTbes(id, eStrong, dStrong, uStrong, hg, transliteration, grammar, english, meaning) VALUES(";
                        String row, sql;
                        String[] cols;
                        String eStrong, dStrong, uStrong, hg, transliteration, grammar, english, meaning;
                        final String emptyStr = "''";
                        int id = 0;

                        AssetManager am = _context.getAssets();
                        InputStream is = am.open(fileName);
                        BufferedReader br = new BufferedReader(new InputStreamReader(is));

                        while ((row = br.readLine()) != null)
                        {
                            id++;

                            cols = row.split("\t");
                            eStrong = cols.length < 1 ? emptyStr : cols[0] == null ? emptyStr : PCommon.AQ(PCommon.RQ( cols[0] ));
                            dStrong = cols.length < 2 ? emptyStr : cols[1] == null ? emptyStr : PCommon.AQ(PCommon.RQ( cols[1] ));
                            uStrong = cols.length < 3 ? emptyStr : cols[2] == null ? emptyStr : PCommon.AQ(PCommon.RQ( cols[2] ));
                            hg = cols.length < 4 ? emptyStr : cols[3] == null ? emptyStr : PCommon.AQ(PCommon.RQ( cols[3] ));
                            transliteration = cols.length < 5 ? emptyStr : cols[4] == null ? emptyStr : PCommon.AQ(PCommon.RQ( cols[4] ));
                            grammar = cols.length < 6 ? emptyStr : cols[5] == null ? emptyStr : PCommon.AQ(PCommon.RQ( cols[5] ));
                            english = cols.length < 7 ? emptyStr : cols[6] == null ? emptyStr : PCommon.AQ(PCommon.RQ( cols[6] ));
                            meaning = cols.length < 8 ? emptyStr : cols[7] == null ? emptyStr : PCommon.AQ(PCommon.RQ( cols[7] ));

                            sql = PCommon.ConcaT(insertSql, id, ",", eStrong, ",", dStrong, ",", uStrong, ",", hg, ",", transliteration, ",", grammar, ",", english, ",", meaning, ")");
                            _db.execSQL(sql);
                        }

                        br.close();
                        is.close();
                    }
                    catch (Exception ex)
                    {
                        LogE(ex);
                    }
                    finally
                    {
                        if (PCommon._isDebug) System.out.println("Exit ImportLexTbes");
                    }
                }

                private void ImportLexBook()
                {
                    try
                    {
                        final String fileName = "lexbook.csv";
                        final String insertSql = "INSERT INTO lexBook(bNumber, bsName) VALUES(";
                        String row, sql;
                        String[] cols;
                        String bsName;
                        int bNumber;

                        AssetManager am = _context.getAssets();
                        InputStream is = am.open(fileName);
                        BufferedReader br = new BufferedReader(new InputStreamReader(is));

                        while ((row = br.readLine()) != null)
                        {
                            cols = row.split("\\$");
                            bNumber = Integer.parseInt(cols[0]);
                            bsName = PCommon.AQ(PCommon.RQ( cols[1] ));

                            sql = PCommon.ConcaT(insertSql, bNumber, ",", bsName, ")");
                            _db.execSQL(sql);
                        }

                        br.close();
                        is.close();
                    }
                    catch (Exception ex)
                    {
                        LogE(ex);
                    }
                    finally
                    {
                        if (PCommon._isDebug) System.out.println("Exit ImportLexBook");
                    }
                }

                private void ImportLexAggr()
                {
                    try
                    {
                        final String fileName = "lexaggr.csv";
                        final String insertSql = "INSERT INTO lexAggr(id, field0, field1, field2, field3, field4, field5, field6, field7, field8, field9, field10) VALUES(";
                        String row, sql;
                        String[] cols;
                        String field0, field1, field2, field3, field4, field5, field6, field7, field8, field9, field10;
                        final String emptyStr = "''";
                        int id = 0;

                        AssetManager am = _context.getAssets();
                        InputStream is = am.open(fileName);
                        BufferedReader br = new BufferedReader(new InputStreamReader(is));

                        while ((row = br.readLine()) != null)
                        {
                            id++;

                            cols = row.split("\\$");
                            field0 = cols.length < 1 ? emptyStr : cols[0] == null ? emptyStr : PCommon.AQ(PCommon.RQ( cols[0] ));
                            field1 = cols.length < 2 ? emptyStr : cols[1] == null ? emptyStr : PCommon.AQ(PCommon.RQ( cols[1] ));
                            field2 = cols.length < 3 ? emptyStr : cols[2] == null ? emptyStr : PCommon.AQ(PCommon.RQ( cols[2] ));
                            field3 = cols.length < 4 ? emptyStr : cols[3] == null ? emptyStr : PCommon.AQ(PCommon.RQ( cols[3] ));
                            field4 = cols.length < 5 ? emptyStr : cols[4] == null ? emptyStr : PCommon.AQ(PCommon.RQ( cols[4] ));
                            field5 = cols.length < 6 ? emptyStr : cols[5] == null ? emptyStr : PCommon.AQ(PCommon.RQ( cols[5] ));
                            field6 = cols.length < 7 ? emptyStr : cols[6] == null ? emptyStr : PCommon.AQ(PCommon.RQ( cols[6] ));
                            field7 = cols.length < 8 ? emptyStr : cols[7] == null ? emptyStr : PCommon.AQ(PCommon.RQ( cols[7] ));
                            field8 = cols.length < 9 ? emptyStr : cols[8] == null ? emptyStr : PCommon.AQ(PCommon.RQ( cols[8] ));
                            field9 = cols.length < 10 ? emptyStr : cols[9] == null ? emptyStr : PCommon.AQ(PCommon.RQ( cols[9] ));
                            field10 = cols.length < 11 ? emptyStr : cols[10] == null ? emptyStr : PCommon.AQ(PCommon.RQ( cols[10] ));

                            sql = PCommon.ConcaT(insertSql, id, ",", field0, ",", field1, ",", field2, ",", field3, ",", field4, ",", field5, ",", field6, ",", field7, ",", field8, ",", field9, ",", field10, ")");
                            _db.execSQL(sql);
                        }

                        br.close();
                        is.close();
                    }
                    catch (Exception ex)
                    {
                        LogE(ex);
                    }
                    finally
                    {
                        if (PCommon._isDebug) System.out.println("Exit ImportLexAggr");
                    }
                }

                private void LexCreateIndexes()
                {
                    try
                    {
                        String sql;

                        //---
                        sql = "CREATE UNIQUE INDEX lexBook0_ndx on lexBook (bNumber)";
                        _db.execSQL(sql);

                        //---
                        sql = "CREATE UNIQUE INDEX lexTbes0_ndx on lexTbes (id)";
                        _db.execSQL(sql);

                        sql = "CREATE INDEX lexTbes1_ndx on lexTbes (eStrong)";
                        _db.execSQL(sql);

                        sql = "CREATE INDEX lexTbes2_ndx on lexTbes (dStrong)";
                        _db.execSQL(sql);

                        sql = "CREATE INDEX lexTbes3_ndx on lexTbes (uStrong)";
                        _db.execSQL(sql);

                        sql = "CREATE INDEX lexTbes4_ndx on lexTbes (hg)";
                        _db.execSQL(sql);

                        sql = "CREATE INDEX lexTbes5_ndx on lexTbes (transliteration)";
                        _db.execSQL(sql);

                        sql = "CREATE INDEX lexTbes6_ndx on lexTbes (english)";
                        _db.execSQL(sql);

                        sql = "CREATE INDEX lexTbes7_ndx on lexTbes (meaning)";
                        _db.execSQL(sql);

                        //---
                        sql = "CREATE UNIQUE INDEX lexAggr0_ndx on lexAggr (id)";
                        _db.execSQL(sql);

                        sql = "CREATE INDEX lexAggr11_ndx on lexAggr (field0)";
                        _db.execSQL(sql);

                        sql = "CREATE INDEX lexAggr1_ndx on lexAggr (field1)";
                        _db.execSQL(sql);

                        sql = "CREATE INDEX lexAggr2_ndx on lexAggr (field2)";
                        _db.execSQL(sql);

                        sql = "CREATE INDEX lexAggr3_ndx on lexAggr (field3)";
                        _db.execSQL(sql);

                        sql = "CREATE INDEX lexAggr4_ndx on lexAggr (field4)";
                        _db.execSQL(sql);

                        sql = "CREATE INDEX lexAggr5_ndx on lexAggr (field5)";
                        _db.execSQL(sql);

                        sql = "CREATE INDEX lexAggr6_ndx on lexAggr (field6)";
                        _db.execSQL(sql);

                        sql = "CREATE INDEX lexAggr7_ndx on lexAggr (field7)";
                        _db.execSQL(sql);

                        sql = "CREATE INDEX lexAggr8_ndx on lexAggr (field8)";
                        _db.execSQL(sql);

                        sql = "CREATE INDEX lexAggr9_ndx on lexAggr (field9)";
                        _db.execSQL(sql);

                        sql = "CREATE INDEX lexAggr10_ndx on lexAggr (field10)";
                        _db.execSQL(sql);
                    }
                    catch (Exception ex)
                    {
                        LogE(ex);
                    }
                    finally
                    {
                        if (PCommon._isDebug) System.out.println("Exit LexCreateIndexes");
                    }
                }
            };
            thread.setPriority(Thread.NORM_PRIORITY);
            thread.start();
        }
        catch(Exception ex)
        {
            LogE(ex);
        }
    }

    //</editor-fold>
}
