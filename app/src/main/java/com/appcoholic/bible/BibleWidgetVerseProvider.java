package com.appcoholic.bible;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;

public class BibleWidgetVerseProvider extends AppWidgetProvider
{
    @Override
    public void onUpdate(final Context context, final AppWidgetManager appWidgetManager, final int[] appWidgetIds)
    {
        try
        {
            if (PCommon._isDebug) System.out.println("onUpdate => count: " + appWidgetIds.length);

            for (int appWidgetId : appWidgetIds)
            {
                final WidgetVerseBO widgetVerse = CommonWidget.GetWidgetRandomVerse(context);
                CommonWidgetVerse.UpdateAppWidget(context, appWidgetId, widgetVerse);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }
}
