
package com.appcoholic.bible;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

public class PreferencesActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        try
        {
            super.onCreate(savedInstanceState);

            PCommon.SetLocale(PreferencesActivity.this, -1,false);

            final int themeId = PCommon.GetPrefThemeId(getApplicationContext());
            setTheme(themeId);

            getSupportFragmentManager()
                .beginTransaction()
                .replace(android.R.id.content, new PreferencesFragment(), "PA")
                .commit();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }
}
