package com.appcoholic.bible;

import android.content.Context;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

class ClipboardBO
{
    private SCommon _s = null;
    private final Context context;

    /***
     * Clipboard
     * @param cmContext Context
     */
    public ClipboardBO(final Context cmContext)
    {
        context = cmContext;
        CheckLocalInstance(context);
    }

    void ClearClipboard()
    {
        try {
            PCommon.SaveClipboardIds(context, new ArrayList<>());
            PCommon.CopyTextToClipboard(context, "", "", false);
            PCommon.ShowToast(context, R.string.toastEmpty, Toast.LENGTH_SHORT);
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Add to clipboard
     * @param bibleId   Bible ID
     * @param addType   Type of add
     * @param lstAllId  Used only with type ALL
     */
    void AddToClipboard(final int bibleId, final SearchFragment.CLIPBOARD_ADD_TYPE addType, final ArrayList<Integer> lstAllId)
    {
        try {
            //Manage IDs
            ArrayList<Integer> lstIdGen = PCommon.GetClipboardIds(context);
            ArrayList<Integer> lstVersesId;
            if (addType == SearchFragment.CLIPBOARD_ADD_TYPE.VERSE || addType == SearchFragment.CLIPBOARD_ADD_TYPE.CHAPTER) {
                final VerseBO currentVerse = _s.GetVerse(bibleId);
                if (currentVerse == null) return;
                final String bbname = currentVerse.bbName;
                final int bnumber = currentVerse.bNumber;
                final int cnumber = currentVerse.cNumber;
                final int vnumber = currentVerse.vNumber;

                if (addType == SearchFragment.CLIPBOARD_ADD_TYPE.VERSE) {
                    lstVersesId = _s.GetVersesId(bbname, bnumber, cnumber, vnumber, vnumber);
                } else {
                    lstVersesId = _s.GetVersesId(bbname, bnumber, cnumber, 1, 0);
                }
            } else {
                lstVersesId = lstAllId;
            }
            for (Integer verseId : lstVersesId) {
                if (!lstIdGen.contains(verseId)) lstIdGen.add(verseId);
            }
            Collections.sort(lstIdGen);
            PCommon.SaveClipboardIds(context, lstIdGen);
            GenerateTextForClipboard(true);
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    ///Returns text generated
    String GenerateTextForClipboard(final boolean shouldShowToast)
    {
        ArrayList<Integer> lstIdGen;
        String textToClipboard = "";

        try {
            lstIdGen = PCommon.GetClipboardIds(context);
            int size = lstIdGen.size();
            if (size <= 0) {
                ClearClipboard();
                return "";
            }

            VerseBO currentVerse, nextVerse;
            int idIntCurrent, idIntNext, bNumberCurrent, bNumberNext, cNumberCurrent, cNumberNext, vNumberCurrent;
            String dir, vTextCurrent, bNameCurrent, bbNameCurrent, bbNameNext, bbNames = "";

            //Get bbNames
            for (int index = 0; index < size; index++) {
                idIntCurrent = lstIdGen.get(index);
                currentVerse = _s.GetVerse(idIntCurrent);
                bbNameCurrent = currentVerse.bbName;
                if (!bbNames.contains(bbNameCurrent)) bbNames = PCommon.ConcaT(bbNames, bbNameCurrent);
            }

            //Check KJV2000...
            final String bbNamesBeforeCheck = bbNames;
            for (int bib = 0; bib < bbNamesBeforeCheck.length(); bib++)
            {
                final String bbNameCurrentBeforeCheck = bbNames.substring(bib, bib+1);
                if (bbNameCurrentBeforeCheck.equalsIgnoreCase("2") || bbNameCurrentBeforeCheck.equalsIgnoreCase("9") || bbNameCurrentBeforeCheck.equalsIgnoreCase("1") || bbNameCurrentBeforeCheck.equalsIgnoreCase("i") || bbNameCurrentBeforeCheck.equalsIgnoreCase("j"))
                {
                    final Map<Integer, Integer> mapBookCount = new HashMap<>();
                    for (int bNumber = 1; bNumber <= 66; bNumber++) mapBookCount.put(bNumber, 0);

                    //Counts
                    Integer content;
                    int vCountOther = 0;
                    for (int i = 0; i < size; i++) {
                        idIntCurrent = lstIdGen.get(i);
                        currentVerse = _s.GetVerse(idIntCurrent);
                        bNumberCurrent = currentVerse.bNumber;
                        bbNameCurrent = currentVerse.bbName;
                        if (bbNameCurrent.equalsIgnoreCase(bbNameCurrentBeforeCheck)) {
                            content = mapBookCount.get(bNumberCurrent);
                            if (content == null)
                                vCountOther++;
                            else
                                mapBookCount.put(bNumberCurrent, content + 1);
                        }
                    }

                    //Check all book count and total
                    int warnType = 0;
                    int sumBooks = vCountOther;
                    Integer[] ci;
                    for (int bNumber : mapBookCount.keySet()) {
                        content = mapBookCount.get(bNumber);
                        if (content == null) continue;

                        ci = _s.GetBibleCiByBook(bNumber);
                        if (content >= ci[1]) {
                            warnType = 1;
                            break;
                        }

                        sumBooks += content;
                    }
                    if (warnType <= 0) if (sumBooks > 500) warnType = 2;

                    //Cleaning
                    mapBookCount.clear();

                    //Remove incorrect
                    if (warnType > 0) {
                        bbNames = bbNames.replace(bbNameCurrentBeforeCheck, "");
                        PCommon.ShowToast(context, warnType == 1 ? R.string.toastWarnKJV2000LimitFullBook : R.string.toastWarnKJV2000Limit500, Toast.LENGTH_SHORT);
                        for (int i = size - 1; i >= 0; i--)
                        {
                            idIntCurrent = lstIdGen.get(i);
                            currentVerse = _s.GetVerse(idIntCurrent);
                            bbNameCurrent = currentVerse.bbName;
                            if (bbNameCurrent.equalsIgnoreCase(bbNameCurrentBeforeCheck)) {
                                lstIdGen.remove(i);
                            }
                        }

                        //Reset
                        size = lstIdGen.size();
                        if (size <= 0) {
                            ClearClipboard();
                            return "";
                        }
                        PCommon.SaveClipboardIds(context, lstIdGen);
                    }
                }
            }

            //Gen
            int prevId = -1;
            final StringBuilder sb = new StringBuilder();
            for (int index = 0; index < size; index++) {
                //Current
                idIntCurrent = lstIdGen.get(index);
                currentVerse = _s.GetVerse(idIntCurrent);
                bNumberCurrent = currentVerse.bNumber;
                cNumberCurrent = currentVerse.cNumber;
                vNumberCurrent = currentVerse.vNumber;
                vTextCurrent = currentVerse.vText;
                bNameCurrent = currentVerse.bName;
                bbNameCurrent = currentVerse.bbName;
                dir = bbNameCurrent.equalsIgnoreCase("y") || (bbNameCurrent.equalsIgnoreCase("w") && bNumberCurrent < 40) ? PCommon.GetRTL() : PCommon.GetLTR();

                if (prevId == -1) {
                    sb.append(PCommon.ConcaT( "\n\n", dir, bNameCurrent, " ", cNumberCurrent, "\n"));
                } else if (prevId + 1 != idIntCurrent) {
                    sb.append("\n");
                }
                sb.append(PCommon.ConcaT(dir, vNumberCurrent, ": ", vTextCurrent, "\n"));
                prevId = idIntCurrent;

                //Next
                if ((index + 1) < size) {
                    idIntNext = lstIdGen.get(index + 1);
                    nextVerse = _s.GetVerse(idIntNext);
                    bNumberNext = nextVerse.bNumber;
                    cNumberNext = nextVerse.cNumber;
                    bbNameNext = nextVerse.bbName;

                    //(bNameCurrent.compareTo(bNameNext) != 0
                    if ((bNumberCurrent != bNumberNext) || (cNumberCurrent != cNumberNext) ||  (bbNameCurrent.compareToIgnoreCase(bbNameNext) != 0)) {
                        prevId = -1;
                    }
                }
            }

            //Sources
            String bbNamesVerbose = "";
            for (int i = 0; i < bbNames.length(); i++) {
                final String bbName = bbNames.substring(i, i+1);
                if (bbName.equalsIgnoreCase("k")) bbNamesVerbose = PCommon.ConcaT(bbNamesVerbose, ", KJV1611");
                if (bbName.equalsIgnoreCase("l")) bbNamesVerbose = PCommon.ConcaT(bbNamesVerbose, ", Louis Segond");
                if (bbName.equalsIgnoreCase("o")) bbNamesVerbose = PCommon.ConcaT(bbNamesVerbose, ", Ostervald");
                if (bbName.equalsIgnoreCase("v")) bbNamesVerbose = PCommon.ConcaT(bbNamesVerbose, ", Reina Valera 1909");
                if (bbName.equalsIgnoreCase("a")) bbNamesVerbose = PCommon.ConcaT(bbNamesVerbose, ", Almeida");
                if (bbName.equalsIgnoreCase("d")) bbNamesVerbose = PCommon.ConcaT(bbNamesVerbose, ", DIO1649");
                if (bbName.equalsIgnoreCase("2")) bbNamesVerbose = PCommon.ConcaT(bbNamesVerbose, ", KJ2K");
                if (bbName.equalsIgnoreCase("9")) bbNamesVerbose = PCommon.ConcaT(bbNamesVerbose, ", RVA1989");
                if (bbName.equalsIgnoreCase("1")) bbNamesVerbose = PCommon.ConcaT(bbNamesVerbose, ", LND1991");
                if (bbName.equalsIgnoreCase("i")) bbNamesVerbose = PCommon.ConcaT(bbNamesVerbose, ", IRV2017");
                if (bbName.equalsIgnoreCase("y")) bbNamesVerbose = PCommon.ConcaT(bbNamesVerbose, ", SVDA");
                if (bbName.equalsIgnoreCase("c")) bbNamesVerbose = PCommon.ConcaT(bbNamesVerbose, ", CUVS");
                if (bbName.equalsIgnoreCase("s")) bbNamesVerbose = PCommon.ConcaT(bbNamesVerbose, ", SCH1951");
                if (bbName.equalsIgnoreCase("e")) bbNamesVerbose = PCommon.ConcaT(bbNamesVerbose, ", ELB1932");
                if (bbName.equalsIgnoreCase("j")) bbNamesVerbose = PCommon.ConcaT(bbNamesVerbose, ", NJB1973");
                if (bbName.equalsIgnoreCase("r")) bbNamesVerbose = PCommon.ConcaT(bbNamesVerbose, ", RST1876");
                if (bbName.equalsIgnoreCase("t")) bbNamesVerbose = PCommon.ConcaT(bbNamesVerbose, ", NTB2001");
                if (bbName.equalsIgnoreCase("b")) bbNamesVerbose = PCommon.ConcaT(bbNamesVerbose, ", BCL2016");
                if (bbName.equalsIgnoreCase("h")) bbNamesVerbose = PCommon.ConcaT(bbNamesVerbose, ", SUV1997");
                if (bbName.equalsIgnoreCase("u")) bbNamesVerbose = PCommon.ConcaT(bbNamesVerbose, ", RUC1928");
                if (bbName.equalsIgnoreCase("z")) bbNamesVerbose = PCommon.ConcaT(bbNamesVerbose, ", BW1975");
                if (bbName.equalsIgnoreCase("w")) bbNamesVerbose = PCommon.ConcaT(bbNamesVerbose, ", WLC/TR");
            }
            if (!bbNamesVerbose.isEmpty()) bbNamesVerbose = bbNamesVerbose.substring(2);
            sb.append(PCommon.ConcaT(PCommon.GetLTR(),"\n(", bbNamesVerbose, ")"));

            //Finally
            textToClipboard = sb.toString().trim();
            PCommon.CopyTextToClipboard(context, "", textToClipboard, shouldShowToast);
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        } finally {
            lstIdGen = null;
        }
        return textToClipboard;
    }

    /***
     * Check local instance (to copy reader all activities that use it)
     */
    private void CheckLocalInstance(final Context context)
    {
        try
        {
            if (_s == null) _s = SCommon.GetInstance(context);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }
}
