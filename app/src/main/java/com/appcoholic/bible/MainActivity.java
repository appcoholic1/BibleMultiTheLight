
package com.appcoholic.bible;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.Html;
import android.text.Spanned;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.appsflyer.AppsFlyerLib;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.google.android.play.core.review.ReviewInfo;
import com.google.android.play.core.review.ReviewManager;
import com.google.android.play.core.review.ReviewManagerFactory;
import com.google.firebase.crashlytics.FirebaseCrashlytics;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.TreeMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.app.ShareCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
public class MainActivity extends AppCompatActivity {
    @SuppressLint("StaticFieldLeak")
    private static TabLayout tabLayout;
    private View llMain;
    private View slideViewMenuTop;
    private View slideViewMenu;
    private View slideViewMenuHandle;
    private View slideViewTab;
    private View slideViewTabHandleMain;
    private View slideViewTabHandle;
    private boolean isUiTelevision = false;
    private static boolean isPlanSelectAlreadyWarned = false;
    private static boolean isTodoSelectAlreadyWarned = false;
    private SCommon _s = null;

    private SharedPrefHelper sharedPrefHelper;
    private long startTime;
    private static final int RATING_THRESHOLD = 3;
    private static final long USAGE_THRESHOLD = 10 * 60 * 1000; // 10 minutes in milliseconds
    private FloatingActionButton fabChat;
    private Toolbar toolbar;

    private static final String KEY_TAP_TARGET_SHOWN = "TapTargetShown";

    @Override
    protected void onStart() {
        try {
            super.onStart();

            CheckLocalInstance(getApplicationContext());

            if (PCommon._isDebug) System.out.println("Main: onStart");
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);

            CheckLocalInstance(getApplicationContext());


            AppsFlyerLib.getInstance().init("fbNaMFATPwTjTNUBXT6kWS", null, this);
            AppsFlyerLib.getInstance().start(this);


            if (PCommon._isDebug) System.out.println("Main: onCreate");

            Create(true);

            sharedPrefHelper = new SharedPrefHelper(this);
            startTime = SystemClock.elapsedRealtime();
            int openCount = sharedPrefHelper.getOpenCount();
            boolean isOnboardingCompleted = sharedPrefHelper.isOnboardingCompleted();

            // Create Notification Channel for Android O and above
            NotificationChannel channel = new NotificationChannel(
                    "channel_id",
                    "BibleGPT Notifications",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            channel.setDescription("Channel for general notifications");
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }

            // Check if notifications are enabled and prompt user if necessary
            if (!NotificationManagerCompat.from(this).areNotificationsEnabled() && openCount >= 1 && !isOnboardingCompleted) {
                showEnableNotificationsDialog();
                sharedPrefHelper.setOnboardingCompleted(true);
            }

            sharedPrefHelper.saveOpenCount(openCount + 1);


        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
        String userId = UniqueIDGenerator.getUniqueID(this);

        FirebaseCrashlytics.getInstance().setUserId(userId);
    }

    private void showEnableNotificationsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Enable Notifications")
                .setMessage("For the app to run smoothly, notifications should be turned on. Do you want to enable notifications?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User agreed to enable notifications
                        Intent intent = new Intent(Settings.ACTION_APP_NOTIFICATION_SETTINGS)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                .putExtra(Settings.EXTRA_APP_PACKAGE, getPackageName());
                        startActivity(intent);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User declined to enable notifications
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

//    @Override
//    protected void onStop() {
//        super.onStop();
//        Intent intent = new Intent(this, NotificationService.class);
//        startService(intent);
//    }

    private void Create(final boolean isOnCreate) {
        try {
            CheckLocalInstance(getApplicationContext());

            _s.DeleteAllLogs(false);

            if (PCommon._isDebug)
            {
                PCommon.LogRMainVars(getApplicationContext(), "Main: Create");
                PCommon.LogR(getApplicationContext(), PCommon.ConcaT("isOnCreate:", isOnCreate ? "true" : "false"));
            }

            PCommon.SetLocale(MainActivity.this, -1, false);

            isUiTelevision = PCommon.IsUiTelevision(getApplicationContext());

            if (isOnCreate)
            {
                final int themeId = PCommon.GetPrefThemeId(getApplicationContext());
                Tab.isThemeWhite = PCommon.GetPrefThemeName(getApplicationContext()).compareTo("LIGHT") == 0;
                setTheme(themeId);
            }

            setContentView(PCommon.SetUILayout(getApplicationContext(), R.layout.activity_main, R.layout.activity_main_tv));

            llMain = findViewById(isUiTelevision ? R.id.slideViewTab : R.id.llMain);
            slideViewMenuTop = (isUiTelevision) ? findViewById(R.id.slideViewMenuTop) : null;
            slideViewMenu = (isUiTelevision) ? findViewById(R.id.svSlideViewMenu) : null;
            slideViewMenuHandle = (isUiTelevision) ? findViewById(R.id.mnuTvHandle) : null;
            slideViewTab = (isUiTelevision) ? findViewById(R.id.slideViewTab) : null;
            slideViewTabHandleMain = (isUiTelevision) ? findViewById(R.id.slideViewTabHandleMain) : null;
            slideViewTabHandle = (isUiTelevision) ? findViewById(R.id.slideViewTabHandle) : null;

            fabChat = findViewById(R.id.fab_chat);
            fabChat.setOnClickListener(view -> {
                Log.i("MainActivity", "Chat button clicked");
                // Handle the click event here
                // For example, you can open the chat activity
                DefaultMessagesActivity.open(MainActivity.this);
            });

            if (!isUiTelevision) {
                toolbar = findViewById(R.id.toolbar);
                if (toolbar != null) {
                    setSupportActionBar(toolbar);

                    if (PCommon.GetPref(getApplicationContext(), IProject.APP_PREF_KEY.UI_LAYOUT, "C").equalsIgnoreCase("L")) {
                        final AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
                        params.setScrollFlags(0);
                    }
                }
            } else {
                final Context context = getApplicationContext();
                final Typeface typeface = PCommon.GetTypeface(context);
                final int fontSize = PCommon.GetFontSize(context);
                final String bordersDialog = PCommon.GetUiLayoutTVBorders(getApplicationContext(), IProject.APP_PREF_KEY.UI_LAYOUT_TV_BORDERS);
                String[] borders = bordersDialog.split(",");
                if (borders.length != 4)
                    borders = PCommon.GetUiLayoutTVBorders(getApplicationContext(), null).split(",");

                int left = Integer.parseInt(borders[0]);
                left = PCommon.ConvertDpToPx(context, left);
                int top = Integer.parseInt(borders[1]);
                top = PCommon.ConvertDpToPx(context, top);
                int right = Integer.parseInt(borders[2]);
                right = PCommon.ConvertDpToPx(context, right);
                int bottom = Integer.parseInt(borders[3]);
                bottom = PCommon.ConvertDpToPx(context, bottom);

                final RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                layoutParams.setMargins(left, top, right, bottom);

                final RelativeLayout rlOverscan = findViewById(R.id.rlOverscan);
                rlOverscan.setLayoutParams(layoutParams);

                if (slideViewMenuHandle != null) {
                    slideViewMenuHandle.setOnClickListener(v -> Slide(false));
                }
                if (slideViewTabHandle != null) {
                    slideViewTabHandle.setOnClickListener(v -> Slide(true));
                }
                final View slideViewTabSearch = findViewById(R.id.slideViewTabSearch);
                slideViewTabSearch.setOnClickListener(v -> SearchDialog(v.getContext(), true));
                final View slideViewTabFilter = findViewById(R.id.slideViewTabFilter);
                slideViewTabFilter.setOnClickListener(v -> SearchDialog(v.getContext(), false));
                final View slideViewTabBooks = findViewById(R.id.slideViewTabBooks);
                slideViewTabBooks.setOnClickListener(v -> ShowBooks(v.getContext()));
                final View slideViewTabLex = findViewById(R.id.slideViewTabLex);
                slideViewTabLex.setOnClickListener(v -> SearchLexicons(v.getContext()));
                final View slideViewTabPrbl = findViewById(R.id.slideViewTabPrbl);
                slideViewTabPrbl.setOnClickListener(v -> ShowPrbl(v.getContext()));
                final View slideViewTabArticles = findViewById(R.id.slideViewTabArticles);
                slideViewTabArticles.setOnClickListener(v -> PCommon.ShowArticles(v.getContext()));
                final View slideViewTabHarmony = findViewById(R.id.slideViewTabHarmony);
                slideViewTabHarmony.setOnClickListener(v -> ShowHarmony(v.getContext()));
                final View slideViewTabRnds = findViewById(R.id.slideViewTabRnds);
                slideViewTabRnds.setOnClickListener(v -> ShowRandomVerses(v.getContext()));
                final View slideViewTabListen = findViewById(R.id.slideViewTabListen);
                slideViewTabListen.setOnClickListener(v -> PCommon.ShowAudioBible(this, null));

                final TextView mnuTvArticles = findViewById(R.id.mnuTvArticles);
                mnuTvArticles.setTextSize(fontSize);
                if (typeface != null) mnuTvArticles.setTypeface(typeface);
                mnuTvArticles.setOnClickListener(v -> {
                    Slide(false);
                    PCommon.ShowArticles(v.getContext());
                });
                final TextView mnuTvListen = findViewById(R.id.mnuTvListen);
                mnuTvListen.setTextSize(fontSize);
                if (typeface != null) mnuTvListen.setTypeface(typeface);
                mnuTvListen.setOnClickListener(v -> {
                    Slide(false);
                    PCommon.ShowAudioBible(this, null);
                });
                final TextView mnuTvBooks = findViewById(R.id.mnuTvBooks);
                mnuTvBooks.setTextSize(fontSize);
                if (typeface != null) mnuTvBooks.setTypeface(typeface);
                mnuTvBooks.setOnClickListener(v -> {
                    Slide(false);
                    ShowBooks(v.getContext());
                });
                final TextView mnuTvFav = findViewById(R.id.mnuTvFav);
                mnuTvFav.setTextSize(fontSize);
                if (typeface != null) mnuTvFav.setTypeface(typeface);
                mnuTvFav.setOnClickListener(v -> {
                    Slide(false);
                    ShowFav();
                });
                final TextView mnuTvHelp = findViewById(R.id.mnuTvHelp);
                mnuTvHelp.setTextSize(fontSize);
                if (typeface != null) mnuTvHelp.setTypeface(typeface);
                mnuTvHelp.setOnClickListener(v -> {
                    Slide(false);
                    PCommon.ShowArticle(v.getContext(), "ART_APP_HELP");
                });
                final TextView mnuTvTools = findViewById(R.id.mnuTvTools);
                mnuTvTools.setTextSize(fontSize);
                if (typeface != null) mnuTvTools.setTypeface(typeface);
                mnuTvTools.setOnClickListener(v -> {
                    Slide(false);
                    ShowTools(v.getContext());
                });
                final TextView mnuTvTodos = findViewById(R.id.mnuTvTodos);
                mnuTvTodos.setTextSize(fontSize);
                if (typeface != null) mnuTvTodos.setTypeface(typeface);
                mnuTvTodos.setOnClickListener(v -> {
                    Slide(false);
                    ShowTodos();
                });
                final TextView mnuTvPlans = findViewById(R.id.mnuTvPlans);
                mnuTvPlans.setTextSize(fontSize);
                if (typeface != null) mnuTvPlans.setTypeface(typeface);
                mnuTvPlans.setOnClickListener(v -> {
                    Slide(false);
                    ShowPlans();
                });
                final TextView mnuTvHarmony = findViewById(R.id.mnuTvHarmony);
                mnuTvHarmony.setTextSize(fontSize);
                if (typeface != null) mnuTvHarmony.setTypeface(typeface);
                mnuTvHarmony.setOnClickListener(v -> {
                    Slide(false);
                    ShowHarmony(v.getContext());
                });
                final TextView mnuTvRnds = findViewById(R.id.mnuTvRnds);
                mnuTvRnds.setTextSize(fontSize);
                if (typeface != null) mnuTvRnds.setTypeface(typeface);
                mnuTvRnds.setOnClickListener(v -> {
                    Slide(false);
                    ShowRandomVerses(v.getContext());
                });
                final TextView mnuTvSharePic = findViewById(R.id.mnuTvSharePic);
                mnuTvSharePic.setTextSize(fontSize);
                if (typeface != null) mnuTvSharePic.setTypeface(typeface);
                mnuTvSharePic.setOnClickListener(v -> {
                    Slide(false);
                    ShowSharePic(v.getContext());
                });
                final TextView mnuTvLex = findViewById(R.id.mnuTvLex);
                mnuTvLex.setTextSize(fontSize);
                if (typeface != null) mnuTvLex.setTypeface(typeface);
                mnuTvLex.setOnClickListener(v -> {
                    Slide(false);
                    SearchLexicons(v.getContext());
                });
                final TextView mnuTvPrbl = findViewById(R.id.mnuTvPrbl);
                mnuTvPrbl.setTextSize(fontSize);
                if (typeface != null) mnuTvPrbl.setTypeface(typeface);
                mnuTvPrbl.setOnClickListener(v -> {
                    Slide(false);
                    ShowPrbl(v.getContext());
                });
                final TextView mnuTvSettings = findViewById(R.id.mnuTvSettings);
                mnuTvSettings.setTextSize(fontSize);
                if (typeface != null) mnuTvSettings.setTypeface(typeface);
                mnuTvSettings.setOnClickListener(v -> {
                    Slide(false);
                    final Intent intent = new Intent(getApplicationContext(), PreferencesActivity.class);
                    startActivityForResult(intent, 1);
                });
                final TextView mnuTvInvite = findViewById(R.id.mnuTvInvite);
                mnuTvInvite.setTextSize(fontSize);
                if (typeface != null) mnuTvInvite.setTypeface(typeface);
                mnuTvInvite.setOnClickListener(v -> InviteFriend());
                final TextView mnuTvAbout = findViewById(R.id.mnuTvAbout);
                mnuTvAbout.setTextSize(fontSize);
                if (typeface != null) mnuTvAbout.setTypeface(typeface);
                mnuTvAbout.setOnClickListener(v -> ShowAbout(v.getContext()));
                final TextView mnuTvQuit = findViewById(R.id.mnuTvQuit);
                mnuTvQuit.setTextSize(fontSize);
                if (typeface != null) mnuTvQuit.setTypeface(typeface);
                mnuTvQuit.setOnClickListener(v -> {
                    final int status = PCommon.TryQuitApplication(getApplicationContext());
                    if (status == 0) finishAffinity();
                });
            }

            tabLayout = findViewById(R.id.tabLayout);
            tabLayout.removeAllTabs();
            tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
            tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(final TabLayout.Tab tab) {
                    try {
                        final int tabId = tab.getPosition();
                        if (PCommon._isDebug) System.out.println("TabSelected: " + tabId);

                        final CacheTabBO cacheTab = _s.GetCacheTab(tabId);
                        final SearchFragment.FRAGMENT_TYPE fragmentType;

                        final FragmentManager fm = getSupportFragmentManager();
                        final FragmentTransaction ft = fm.beginTransaction();

                        if (cacheTab == null) {
                            fragmentType = SearchFragment.FRAGMENT_TYPE.SEARCH_TYPE;
                        } else {
                            if (cacheTab.tabType.compareTo("S") == 0) {
                                fragmentType = SearchFragment.FRAGMENT_TYPE.SEARCH_TYPE;
                            } else if (cacheTab.tabType.compareTo("F") == 0) {
                                fragmentType = SearchFragment.FRAGMENT_TYPE.FAV_TYPE;
                            } else if (cacheTab.tabType.compareTo("P") == 0) {
                                fragmentType = SearchFragment.FRAGMENT_TYPE.PLAN_TYPE;
                            } else {
                                fragmentType = SearchFragment.FRAGMENT_TYPE.ARTICLE_TYPE;
                            }
                        }
                        final Fragment frag = new SearchFragment(fragmentType);
                        ft.replace(R.id.content_frame, frag, Integer.toString(tabId));
                            /* Gives exceptions in Android Q
                            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                            */

                        try
                        {
                            ft.commit();
                        }
                        catch (Exception ex2)
                        {
                            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex2);
                        }

                        if (isUiTelevision)
                        {
                            final int threadType = PCommon.GetThreadTypeRunning(getApplicationContext(), 1);
                            if (threadType == 1)
                            {
                                final int perc = GetInstallStatusPerc();
                                if (perc < 10) return;
                                final String contentMsg = GetInstallStatusMsg();
                                PCommon.ShowToast(getApplicationContext(), contentMsg, Toast.LENGTH_SHORT);
                            }
                        }
                    } catch (Exception ex) {
                        if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
                    } finally {
                        Tab.LongPress(tabLayout.getContext());
                    }
                }

                @Override
                public void onTabUnselected(final TabLayout.Tab tab) {
                    try {
                        if (tab == null)
                            return;

                        final int tabId = tab.getPosition();
                        if (tabId < 0)
                            return;

                        final FragmentManager fm = getSupportFragmentManager();
                        final Fragment frag = fm.findFragmentByTag(Integer.toString(tabId));
                        if (frag == null)
                            return;
                        if (!(frag instanceof SearchFragment))
                            return;

                        final int posY = SearchFragment.GetScrollPosY();
                        final CacheTabBO t = _s.GetCacheTab(tabId);
                        if (t == null)
                            return;

                        t.scrollPosY = posY;
                        _s.SaveCacheTab(t);
                    } catch (Exception ex) {
                        if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
                    }
                }

                @Override
                public void onTabReselected(final TabLayout.Tab tab) {
                }
            });

            tabLayout.post(() ->
            {
                try
                {
                    CheckLocalInstance(getApplicationContext());

                    tabLayout.removeAllTabs();

                    TabLayout.Tab tab;
                    String tabTitle;
                    final int tabCount = _s.GetCacheTabCount();
                    for (int i = 0; i < tabCount; i++)
                    {
                        tab = tabLayout.newTab().setText(Integer.toString(i));
                        tabLayout.addTab(tab);

                        tabTitle = _s.GetCacheTabTitle(i);
                        if (tabTitle == null) tabTitle = getString(R.string.tabTitleDefault);
                        tab.setText(tabTitle);
                    }

                    final int restoreTabSelected = Integer.parseInt(PCommon.GetPref(getApplicationContext(), IProject.APP_PREF_KEY.TAB_SELECTED, "0"));
                    if (restoreTabSelected >= 0)
                    {
                        if (tabLayout != null)
                        {
                            if (tabCount > 0 && restoreTabSelected < tabCount) {
                                tabLayout.post(() ->
                                {
                                    //noinspection EmptyCatchBlock
                                    try
                                    {
                                        //noinspection ConstantConditions
                                        tabLayout.getTabAt(restoreTabSelected).select();
                                    } catch (Exception ignored) { }
                                });
                            }
                        }
                    }

                /* Could fail ??
                final int UPDATE_STATUS = Integer.parseInt(PCommon.GetPref(getApplicationContext(), IProject.APP_PREF_KEY.UPDATE_STATUS, "1"));
                if (UPDATE_STATUS != 1) {
                    PCommon.SavePrefInt(getApplicationContext(), IProject.APP_PREF_KEY.UPDATE_STATUS, 1);
                    PCommon.ShowArticle(getApplicationContext(), "ART_APP_LOG");
                }
                */
                }
                catch (Exception ex)
                {
                    if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
                }
            });


        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    @Override
    protected void onPostResume() {
        try {
            super.onPostResume();

            if (PCommon._isDebug) System.out.println("Main: onPostResume");

            final String BIBLE_NAME = PCommon.GetPref(getApplicationContext(), IProject.APP_PREF_KEY.BIBLE_NAME, "");
            if (BIBLE_NAME.isEmpty())
            {
                //Forced temporary
                PCommon.SavePref(getApplicationContext(), IProject.APP_PREF_KEY.BIBLE_NAME, "k");
                PCommon.SavePref(getApplicationContext(), IProject.APP_PREF_KEY.ALT_LANGUAGE, "en");
                PCommon.SavePref(getApplicationContext(), IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, "k");

                final LayoutInflater inflater = getLayoutInflater();
                final View view = inflater.inflate(R.layout.fragment_languages, findViewById(R.id.llLanguages));
                final String msg = getString(R.string.mnuLanguage);
                final String desc = "";
                final AlertDialog builder = new AlertDialog.Builder(MainActivity.this).create();
                builder.setOnDismissListener(dialogInterface -> {
                    final Handler hdl = new Handler();
                    hdl.postDelayed(() -> {
                        final String bbName = PCommon.GetPref(getApplicationContext(), IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, "k");
                        PCommon.SavePref(getApplicationContext(), IProject.APP_PREF_KEY.BIBLE_NAME, bbName);

                        PCommon.SetLocale(MainActivity.this, -1,false);

                        MainActivity.Tab.AddTab(getApplicationContext(), bbName, 1, 1, "1 1", 0); //was: if (!bbName.equalsIgnoreCase("k"))

                        MainActivity.Tab.AddTab(getApplicationContext(), "A", bbName, "ART_APP_HELP", true);

                        PCommon.SavePrefInt(getApplicationContext(), IProject.APP_PREF_KEY.TAB_SELECTED, 0); //Was: 1

                        PCommon.ShowDialog(MainActivity.this, R.string.languageInstalling, false, v -> {
                            showTapTargetSequenceIfNeeded(MainActivity.this, toolbar, false);
                        }, R.string.installMsg);

                        Recreate();
                    }, 0);
                });

                PCommon.SelectBibleLanguage(builder, getApplicationContext(), view, msg, desc, false, true);
                builder.show();
            }
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }


    private void showTapTargetSequenceIfNeeded(Activity activity, Toolbar toolbar, boolean forceShow) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);

        if (!prefs.getBoolean(KEY_TAP_TARGET_SHOWN, false) || forceShow) {
            new Handler(Looper.getMainLooper()).postDelayed(() -> {
                new TapTargetSequence(activity)
                        .targets(
                                TapTarget.forToolbarMenuItem(toolbar, R.id.mnu_sub_menu,
                                                getString(R.string.taptargetview_add),
                                                getString(R.string.taptargetview_add_desc))
                                        .cancelable(false),
                                TapTarget.forView(activity.findViewById(R.id.fab_chat),
                                                getString(R.string.taptargetview_fap_biblegpt_title),
                                        getString(R.string.taptargetview_fap_biblegpt_desc))
                                        .cancelable(false)
                                        .transparentTarget(true))
                        .listener(new TapTargetSequence.Listener() {
                            @Override
                            public void onSequenceFinish() {
                                prefs.edit().putBoolean(KEY_TAP_TARGET_SHOWN, true).apply();
                            }

                            @Override
                            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
                                // Handle each step if needed
                            }

                            @Override
                            public void onSequenceCanceled(TapTarget lastTarget) {
                                prefs.edit().putBoolean(KEY_TAP_TARGET_SHOWN, true).apply();
                            }
                        }).start();
            }, 1000); // 1-second delay
        }
    }

    @Override
    public void onPause() {
        try {
            super.onPause();

            Tab.SaveScrollPosY(getApplicationContext());
            long usageTime = SystemClock.elapsedRealtime() - startTime;
            sharedPrefHelper.saveUsageTime(sharedPrefHelper.getUsageTime() + usageTime);
            Log.d("MainActivity", "onPause called");
            Log.d("MainActivity", "Usage time: " + sharedPrefHelper.getUsageTime() + " milliseconds");
            Log.d("MainActivity", "Open count: " + sharedPrefHelper.getOpenCount());
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        startTime = SystemClock.elapsedRealtime();
        long totalUsageTime = sharedPrefHelper.getUsageTime();
        int openCount = sharedPrefHelper.getOpenCount();
        if (openCount >= RATING_THRESHOLD && totalUsageTime >= USAGE_THRESHOLD) {
            showRatingDialog();
        }
    }

    private void showRatingDialog() {
        // Code to show Google Play rating dialog
        ReviewManager manager = ReviewManagerFactory.create(this);
        Task<ReviewInfo> request = manager.requestReviewFlow();
        request.addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                // We got the ReviewInfo object
                ReviewInfo reviewInfo = task.getResult();
                Task<Void> flow = manager.launchReviewFlow(this, reviewInfo);
                flow.addOnCompleteListener(task1 -> {
                });
            } else {
            }
            sharedPrefHelper.saveOpenCount(0);
            sharedPrefHelper.saveUsageTime(0);
        });
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        try {
            getMenuInflater().inflate(R.menu.menu_bible, menu);

            if (!isUiTelevision) {
                final int threadType = PCommon.GetThreadTypeRunning(getApplicationContext(), 1);
                if (threadType == 1) {
                    menu.findItem(R.id.mnu_listen).setEnabled(false);
                    menu.findItem(R.id.mnu_lexicons).setEnabled(false);
                    menu.findItem(R.id.mnu_prbl).setEnabled(false);
                    menu.findItem(R.id.mnu_articles).setEnabled(false);
                    menu.findItem(R.id.mnu_plans).setEnabled(false);
//                    menu.findItem(R.id.mnu_tools).setEnabled(false);
//                    menu.findItem(R.id.mnu_invite).setEnabled(false);
                    menu.findItem(R.id.mnu_rnds).setEnabled(false);
                    menu.findItem(R.id.mnu_harmony).setEnabled(false);
                    menu.findItem(R.id.mnu_group_settings).setEnabled(false);

                    final MenuItem mnu_search_prbl = menu.findItem(R.id.mnu_search_prbl);
                    if (mnu_search_prbl != null) mnu_search_prbl.setEnabled(false);
                    final MenuItem mnu_search_harmony = menu.findItem(R.id.mnu_search_harmony);
                    if (mnu_search_harmony != null) mnu_search_harmony.setEnabled(false);
                    final MenuItem mnu_search_lex = menu.findItem(R.id.mnu_search_lex);
                    if (mnu_search_lex != null) mnu_search_lex.setEnabled(false);

                    final String contentMsg = GetInstallStatusMsg();
                    final Snackbar snackbar = Snackbar.make(llMain, contentMsg, Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull final MenuItem item) {
        try {
            final int id = item.getItemId();
//            if (id == R.id.mnu_add_tab) {
//                SearchDialog(this, true);
//                return true;
//            } else
            if (id == R.id.mnu_remove_tab) {
                Tab.RemoveCurrentTab(getApplicationContext());
                return true;
            } else if (id == R.id.mnu_listen) {
                PCommon.ShowAudioBible(this, null);
                return true;
            } else if (id == R.id.mnu_lexicons || id == R.id.mnu_search_lex) {
                SearchLexicons(this);
                return true;
            } else if (id == R.id.mnu_show_fav) {
                ShowFav();
                return true;
            } else if (id == R.id.mnu_plan) {
                final int planId = Integer.parseInt(PCommon.GetPref(getApplicationContext(), IProject.APP_PREF_KEY.PLAN_ID, "-1"));
                final int planPageNumber = Integer.parseInt(PCommon.GetPref(getApplicationContext(), IProject.APP_PREF_KEY.PLAN_PAGE, "-1"));
                ShowPlan(planId, planPageNumber);
                return true;
            } else if (id == R.id.mnu_books) {
                ShowBooks(this);
                return true;
            } else if (id == R.id.mnu_plans) {
                ShowPlans();
                return true;
            }
            // else if (id == R.id.mnu_todos) {
            //     ShowTodos();
            //     return true;
            // }
            else if (id == R.id.mnu_prbl || id == R.id.mnu_search_prbl) {
                ShowPrbl(this);
                return true;
            } else if (id == R.id.mnu_articles) {
                PCommon.ShowArticles(this);
                return true;
            } else if (id == R.id.mnu_harmony || id == R.id.mnu_search_harmony) {
                ShowHarmony(this);
                return true;
            } else if (id == R.id.mnu_rnds) {
                ShowRandomVerses(this);
                return true;
            }
//            else if (id == R.id.mnu_tools) {
//                ShowTools(this);
//                return true;
//            }
            else if (id == R.id.mnu_group_settings) {
                final Intent intent = new Intent(getApplicationContext(), PreferencesActivity.class);
                startActivityForResult(intent, 1);
                return true;
            } else if (id == R.id.mnu_help) {
                PCommon.ShowArticle(this, "ART_APP_HELP");
                return true;
            }
            else if (id == R.id.mnu_support) {
                sendSupportEmail();
                return true;
            }
            else if (id == R.id.mnu_tutorial){
                showTapTargetSequenceIfNeeded(this, toolbar, true);
                return true;
            }
//            else if (id == R.id.mnu_invite) {
//                InviteFriend();
//                return true;
//            }
//            else if (id == R.id.mnu_about) {
//                ShowAbout(this);
//                return true;
//            }
//            else if (id == R.id.mnu_quit) {
//                final int status = PCommon.TryQuitApplication(getApplicationContext());
//                if (status == 0) finishAffinity();
//                return true;
//            }
//             else if (id == R.id.mnu_share_pic) {
//                 ShowSharePic(this);
//                 return true;
//             }
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }

        return super.onOptionsItemSelected(item);
    }


    private void sendSupportEmail() {
        // Create and show the AlertDialog
        new AlertDialog.Builder(this)
                .setTitle("Contact Support")
                .setMessage("You can reach out to our support team via email \n(support@appcoholic.com). \n\nWould you like to proceed?")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // User clicked OK button, proceed to show the email chooser
                        proceedToSendEmail();
                    }
                })
                .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // User clicked Close button, dismiss the dialog
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private void proceedToSendEmail() {
        Intent emailIntent = ShareCompat.IntentBuilder.from(this)
                .setType("message/rfc822")
                .setEmailTo(new String[]{"support@appcoholic.com"})
                .setSubject("Support Request")
                .setText("Dear Support Team, \n\n")
                .setChooserTitle("Choose an email client:")
                .createChooserIntent();

        PackageManager packageManager = getPackageManager();
        List<ResolveInfo> emailApps = packageManager.queryIntentActivities(emailIntent, 0);

        Log.d("EmailClientCheck", "Number of email apps found: " + emailApps.size());
        for (ResolveInfo resolveInfo : emailApps) {
            Log.d("EmailClientCheck", "Email app found: " + resolveInfo.activityInfo.packageName);
        }

        if (!emailApps.isEmpty()) {
            Log.d("EmailClientCheck", "Email clients available, showing chooser");
            startActivity(emailIntent);
        } else {
            Log.d("EmailClientCheck", "No email clients installed.");
            Toast.makeText(this, "No email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }




    @Override
    protected void onDestroy() {
        try {
            super.onDestroy();

            if (tabLayout != null) {
                tabLayout.removeAllTabs();
                tabLayout.removeAllViews();
                tabLayout = null;
            }

            _s = null;
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        try {
            super.onConfigurationChanged(newConfig);

            final Handler handler = new Handler();
            handler.postDelayed(() -> {
                Tab.SaveScrollPosY(getApplicationContext());

                Recreate();
            }, 0);
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);

            if (resultCode == Activity.RESULT_OK) {
                if (PCommon._isDebug) System.out.println(PCommon.ConcaT("resultCode:", resultCode));
                Recreate();
            } else {
                final Handler handler = new Handler(Looper.getMainLooper());
                handler.postDelayed(() -> {
                    PCommon.SetLocale(MainActivity.this, -1,false);
                    if (!isUiTelevision) {
                        final String contentMsg = getString(R.string.toastRestartLong);
                        final int waitDuration = Integer.parseInt(getString(R.string.snackbarWaitVeryLong));
                        final Snackbar snackbar = Snackbar.make(llMain, contentMsg, waitDuration);
                        snackbar.show();
                    } else {
                        PCommon.ShowToast(getApplicationContext(), R.string.toastRestartShort, Toast.LENGTH_LONG);
                    }

                    final Handler handlerQuit = new Handler(Looper.getMainLooper());
                    handlerQuit.postDelayed(() -> {
                        final int status = PCommon.TryQuitApplication(getApplicationContext());
                        if (status == 0) finishAffinity();
                    }, 3000);
                }, 500);
            }
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    /***
     * Simulate recreate()
     */
    private void Recreate() {
        try {
            final Handler handler = new Handler();
            handler.postDelayed(() ->
            {
                if (PCommon._isDebug) System.out.println("Main: ReCreate");

                Create(false);

                final int tabId = Tab.GetCurrentTabPosition();
                if (tabId >= 0) {
                    Objects.requireNonNull(tabLayout.getTabAt(tabId)).select();
                }
            }, 0);
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    /***
     * Check local instance (to copy reader all activities that use it)
     */
    private void CheckLocalInstance(final Context context) {
        try {
            if (_s == null) {
                _s = SCommon.GetInstance(context);
            }
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Get install status percentage
     * @return percentage
     */
    private int GetInstallStatusPerc() {
        final float posBible = _s.GetBibleIdCount();
        final float posAggr = _s.GetLexAggrCount();
        final int totAll = (PCommon.GetInstallStatusShouldBe() * 31102) + 178055;

        return Math.round(((posBible + posAggr) * 100) / totAll);
    }

    /***
     * Get install status msg
     * @return Content msg
     */
    private String GetInstallStatusMsg() {
        final int perc = GetInstallStatusPerc();

        return PCommon.ConcaT(getString(R.string.languageInstalling), " (", perc == 0 ? 1 : perc, "%)");
    }

    /***
     * Show Fav (open Fav or goto it)
     */
    private void ShowFav() {
        try {
            if (tabLayout == null) return;

            final int tabCount = tabLayout.getTabCount();
            @SuppressWarnings("UnusedAssignment") boolean isFavShow = false;

            CacheTabBO cacheTabFav = _s.GetCacheTabFav();
            if (cacheTabFav == null) {
                //noinspection ConstantConditions
                isFavShow = false;

                cacheTabFav = new CacheTabBO();
                cacheTabFav.tabNumber = -1;
                cacheTabFav.tabType = "F";
                cacheTabFav.tabTitle = getString(R.string.favHeader);

                _s.SaveCacheTabFav(cacheTabFav);
            } else {
                isFavShow = (cacheTabFav.tabNumber >= 0);
            }

            isFavShow = !isFavShow;
            if (isFavShow) {
                //Show fav tab
                //############
                for (int i = tabCount - 1; i >= 0; i--) {
                    _s.UpdateCacheId(i, i + 1);
                }

                cacheTabFav.tabNumber = 0;
                _s.SaveCacheTabFav(cacheTabFav);

                final TabLayout.Tab tab = tabLayout.newTab().setText(R.string.favHeader);
                tabLayout.addTab(tab, 0);
            }
            Tab.FullScrollTab(getApplicationContext(), HorizontalScrollView.FOCUS_LEFT);
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    private void ShowBooks(final Context context) {
        try {
            CheckLocalInstance(context);

            final int INSTALL_STATUS = PCommon.GetInstallStatus(context);
            if (INSTALL_STATUS < 1) return;

            final Typeface typeface = PCommon.GetTypeface(context);
            final int fontSize = PCommon.GetFontSize(context);
            final String bbnm = PCommon.GetPrefBibleName(context);

            final AlertDialog builderBook = new AlertDialog.Builder(context).create();
            final LayoutInflater inflater = getLayoutInflater();

            final ScrollView sv = new ScrollView(context);
            sv.setLayoutParams(PCommon._layoutParamsMatchAndWrap);

            final ArrayList<BibleRefBO> lstRef = _s.GetListAllBookByName(bbnm);
            final LinearLayout llBooks = new LinearLayout(context);
            llBooks.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            llBooks.setOrientation(LinearLayout.VERTICAL);
            llBooks.setPadding(20, 20, 20, 20);

            final AlertDialog builderChapter = new AlertDialog.Builder(context).create();             //, R.style.DialogStyleKaki
            final View vwSvSelection = inflater.inflate(R.layout.fragment_selection_items, findViewById(R.id.svSelection));

            final AlertDialog builderLanguages = new AlertDialog.Builder(context).create();
            final View vllLanguages = inflater.inflate(R.layout.fragment_languages_multi, findViewById(R.id.llLanguages));

            int bNumber;
            String refText;
            String refNr;
            boolean isBookExist;
            int bNumberParam;
            boolean shouldWarn = false;

            for (BibleRefBO ref : lstRef) {
                bNumber = ref.bNumber;
                refNr = String.format(Locale.US, "%2d", bNumber);
                if (bbnm.equalsIgnoreCase("y") || bbnm.equalsIgnoreCase("w") || bbnm.equalsIgnoreCase("c") || bbnm.equalsIgnoreCase("j") || bbnm.equalsIgnoreCase("r") || bbnm.equalsIgnoreCase("t") || bbnm.equalsIgnoreCase("b")) {
                    refText = PCommon.ConcaT(refNr, Html.fromHtml("&nbsp;"), ref.bName);
                } else {
                    refText = PCommon.ConcaT(refNr, Html.fromHtml("&nbsp;"), "(", ref.bsName, ") ", ref.bName);
                }

                final TextView tvBook = new TextView(context);
                tvBook.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                tvBook.setPadding(10, 20, 10, 20);
                tvBook.setMinHeight(48);
                tvBook.setText(refText);
                tvBook.setTag(bNumber);

                bNumberParam = (bNumber != 66) ? bNumber + 1 : 66;
                isBookExist = (INSTALL_STATUS == PCommon.GetInstallStatusShouldBe()) || _s.IsBookExist(bNumberParam);
                if (isBookExist) {
                    tvBook.setOnClickListener(v -> {
                        try {
                            final int bNumber1 = (int) v.getTag();
                            if (PCommon._isDebug) System.out.println(bNumber1);

                            final int chapterMax = _s.GetBookChapterMax(bNumber1);
                            if (chapterMax < 1) {
                                PCommon.ShowToast(v.getContext(), R.string.toastBookNotInstalled, Toast.LENGTH_SHORT);
                                return;
                            }
                            final String[] titleArr = ((TextView) v).getText().toString().substring(3).split("\\(");
                            final String title = PCommon.ConcaT(context.getString(R.string.mnuBook), ": ", titleArr[0]);

                            PCommon.SelectItem(builderChapter, v.getContext(), vwSvSelection, title, R.string.tvChapter, "", true, chapterMax, false);
                            builderChapter.setOnDismissListener(dialogInterface -> {
                                PCommon.SelectBibleLanguageMulti(builderLanguages, v.getContext(), vllLanguages, title, "", true, false);
                                builderLanguages.setOnDismissListener(dialogInterface1 -> {
                                    final String bbName = PCommon.GetPrefBibleName(context);
                                    final String bbname = PCommon.GetPref(v.getContext(), IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, bbName);
                                    if (bbname.isEmpty()) return;
                                    final String tbbName = PCommon.GetPrefTradBibleName(v.getContext(), true);
                                    final int cNumber = Integer.parseInt(PCommon.GetPref(v.getContext(), IProject.APP_PREF_KEY.BOOK_CHAPTER_DIALOG, "1"));
                                    final String fullQuery = PCommon.ConcaT(bNumber1, " ", cNumber);
                                    Tab.AddTab(v.getContext(), "S", tbbName, fullQuery, true);
                                });
                                builderLanguages.show();
                            });
                            builderChapter.show();
                        } catch (Exception ex) {
                            if (PCommon._isDebug) PCommon.LogR(v.getContext(), ex);
                        } finally {
                            builderBook.dismiss();
                        }
                    });
                } else {
                    if (!shouldWarn) shouldWarn = true;
                    tvBook.setEnabled(false);
                }
                //TODO FAB: slow GetDrawable
                tvBook.setFocusable(true);
                tvBook.setBackground(PCommon.GetDrawable(context, R.drawable.focus_text));

                //Font
                if (typeface != null) tvBook.setTypeface(typeface);
                tvBook.setTextSize(fontSize);

                llBooks.addView(tvBook);
            }

            final TextView tvNT = new TextView(context);
            tvNT.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            tvNT.setPadding(20, 60, 20, 20);
            tvNT.setGravity(Gravity.CENTER_HORIZONTAL);
            tvNT.setText(Html.fromHtml(context.getString(R.string.tvBookNT)));
            PCommon.SetTextAppareance(tvNT, context, R.style.TextAppearance_AppCompat_Headline);
            if (typeface != null) tvNT.setTypeface(typeface);
            llBooks.addView(tvNT, 39);

            final TextView tvOT = new TextView(context);
            tvOT.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            tvOT.setPadding(20, 20, 20, 20);
            tvOT.setGravity(Gravity.CENTER_HORIZONTAL);
            tvOT.setText(Html.fromHtml(context.getString(R.string.tvBookOT)));
            PCommon.SetTextAppareance(tvOT, context, R.style.TextAppearance_AppCompat_Headline);
            if (typeface != null) tvOT.setTypeface(typeface);

            llBooks.addView(tvOT, 0);

            if (shouldWarn) {
                final TextView tvWarn = new TextView(context);
                tvWarn.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                tvWarn.setPadding(10, 10, 10, 20);
                tvWarn.setGravity(Gravity.CENTER_HORIZONTAL);
                tvWarn.setText(R.string.tvBookInstall);
                tvWarn.setTextSize(fontSize);
                llBooks.addView(tvWarn, 0);
            }
            llBooks.requestFocus();
            sv.addView(llBooks);

            builderBook.setTitle(R.string.mnuBooks);
            builderBook.setCancelable(true);
            builderBook.setView(sv);
            builderBook.show();
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    private void ShowHarmony(final Context context)
    {
        try
        {
            CheckLocalInstance(context);

            final AlertDialog builder = new AlertDialog.Builder(context).create();                     //R.style.DialogStyleKaki
            final ScrollView sv = new ScrollView(context);
            sv.setLayoutParams(PCommon._layoutParamsMatchAndWrap);

            final LinearLayout llHarm = new LinearLayout(context);
            llHarm.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            llHarm.setOrientation(LinearLayout.VERTICAL);
            llHarm.setPadding(20, 20, 20, 20);

            final Typeface typeface = PCommon.GetTypeface(context);
            final int fontSize = PCommon.GetFontSize(context);

            String harmId;
            String[] harmArr;
            String harmTitle;
            TextView tvHarm;
            String text;

            final String bbName = PCommon.GetPrefBibleName(context);
            final AlertDialog builderLanguages = new AlertDialog.Builder(context).create();             //, R.style.DialogStyleKaki
            final LayoutInflater inflater = getLayoutInflater();
            final View vllLanguages = inflater.inflate(R.layout.fragment_languages_multi, findViewById(R.id.llLanguages));
            final Spanned htmlSpace = Html.fromHtml("&nbsp;");

            for (String harmRow : context.getResources().getStringArray(R.array.HARMONY_TITLE_ARRAY)) {
                harmArr = harmRow.split("\\|");
                harmId = harmArr[0];
                harmTitle = harmArr[1]; //.replaceAll(" ", htmlSpace.toString());
                text = PCommon.ConcaT(harmId, htmlSpace, getString(R.string.bulletDefault), htmlSpace, harmTitle);

                tvHarm = new TextView(context);
                tvHarm.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                tvHarm.setPadding(20, 20, 20, 20);
                tvHarm.setMinHeight(48);
                tvHarm.setText(text);
                tvHarm.setTag(harmId);
                tvHarm.setOnClickListener(v -> {
                    /* TODO: HARM, multi languages (NOT USED now)
                    try {
                        final String fullQuery = PCommon.ConcaT((String) v.getTag(), " HARMONY:");
                        if (PCommon._isDebugVersion) System.out.println(fullQuery);

                        final String msg = ((TextView) v).getText().toString();
                        PCommon.SelectBibleLanguageMulti(builderLanguages, v.getContext(), vllLanguages, msg, "", true, false);
                        builderLanguages.setOnDismissListener(dialogInterface -> {
                            final String bbname = PCommon.GetPref(v.getContext(), IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, bbName);
                            if (bbname.equals("")) return;
                            final String tbbName = PCommon.GetPrefTradBibleName(v.getContext(), true);
                            Tab.AddTab(v.getContext(), "S", tbbName, fullQuery);
                        });
                        builderLanguages.show();
                    } catch (Exception ex) {
                        if (PCommon._isDebugVersion) PCommon.LogR(v.getContext(), ex);
                    } finally {
                        builder.dismiss();
                    }
                    */

                    final String fullQuery = PCommon.ConcaT((String) v.getTag(), " HARMONY:");
                    if (PCommon._isDebug) System.out.println(fullQuery);
                    Tab.AddTab(v.getContext(), "S", bbName, fullQuery, true);

                    builder.dismiss();
                });
                tvHarm.setFocusable(true);
                tvHarm.setBackground(PCommon.GetDrawable(context, R.drawable.focus_text));

                //Font
                if (typeface != null) tvHarm.setTypeface(typeface);
                tvHarm.setTextSize(fontSize);

                llHarm.addView(tvHarm);
            }
            llHarm.requestFocus();
            sv.addView(llHarm);

            builder.setTitle(R.string.mnuHarmony);
            builder.setCancelable(true);
            builder.setView(sv);
            builder.show();
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    private void ShowKeyboard(final EditText editText)
    {
        final Context context = editText.getContext();

        try
        {
            final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_FORCED);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    private void HideKeyboard(final EditText editText)
    {
        final Context context = editText.getContext();

        try
        {
            final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    private void SearchLexiconsAction(final Context context, final EditText etLexSearch, final RecyclerView lexRecyclerView, final RecyclerView.LayoutManager lexRecyclerViewLayoutManager, final StyleBO dlgStyle) {
        try
        {
            HideKeyboard(etLexSearch);

            final RecyclerView.Adapter<BibleLexAdapter.ViewHolder> lexRecyclerViewAdapterUpd = new BibleLexAdapter(MainActivity.this, etLexSearch.getText().toString(), dlgStyle);
            lexRecyclerView.setLayoutManager(lexRecyclerViewLayoutManager);
            lexRecyclerView.setAdapter(lexRecyclerViewAdapterUpd);
            lexRecyclerView.setHasFixedSize(true);
            lexRecyclerView.scrollToPosition(0);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Search lexicons
     * @param context   Context
     */
    private void SearchLexicons(final Context context) {
        try
        {
            CheckLocalInstance(context);

            //UI
            final LayoutInflater inflater = this.getLayoutInflater();
            final View view = inflater.inflate(R.layout.fragment_search_lexicons, findViewById(R.id.svLex));
            final AlertDialog builder = new AlertDialog.Builder(context).create();
            builder.setTitle(getString(R.string.mnuSearchLexicons));
            builder.setCancelable(true);
            builder.setView(view);
            builder.show();

            final TextView tvDummy = new TextView(context);
            final StyleBO dlgStyle = new StyleBO(tvDummy.getCurrentTextColor());

            //Load data
            final EditText etLexSearch = view.findViewById(R.id.etLexSearch);
            final RecyclerView lexRecyclerView = view.findViewById(R.id.lexRecyclerView);
            final RecyclerView.LayoutManager lexRecyclerViewLayoutManager = new LinearLayoutManager(context);
            final Button btnLexSearch = view.findViewById(R.id.btnLexSearch);
            btnLexSearch.setOnClickListener(v -> {
                SearchLexiconsAction(context, etLexSearch, lexRecyclerView, lexRecyclerViewLayoutManager, dlgStyle);
            });

            etLexSearch.setOnEditorActionListener((v, actionId, event) -> {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    SearchLexiconsAction(context, etLexSearch, lexRecyclerView, lexRecyclerViewLayoutManager, dlgStyle);
                    return true;
                }
                return false;
            });

            //etLexSearch.requestFocus();
            //ShowKeyboard(etLexSearch);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    private void ShowPrbl(final Context context) {
        try {
            CheckLocalInstance(context);

            final AlertDialog builder = new AlertDialog.Builder(context).create();                     //R.style.DialogStyleKaki
            final ScrollView sv = new ScrollView(context);
            sv.setLayoutParams(PCommon._layoutParamsMatchAndWrap);

            final LinearLayout llPrbl = new LinearLayout(context);
            llPrbl.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            llPrbl.setOrientation(LinearLayout.VERTICAL);
            llPrbl.setPadding(20, 20, 20, 20);

            final Typeface typeface = PCommon.GetTypeface(context);
            final int fontSize = PCommon.GetFontSize(context);

            int resId;
            String[] prblValue;
            String[] prblBookValue;
            BibleRefBO bookRef;
            TextView tvPrbl;
            String text;

            final String bbName = PCommon.GetPrefBibleName(context);
            final AlertDialog builderLanguages = new AlertDialog.Builder(context).create();             //, R.style.DialogStyleKaki
            final LayoutInflater inflater = getLayoutInflater();
            final View vllLanguages = inflater.inflate(R.layout.fragment_languages_multi, findViewById(R.id.llLanguages));
            final Spanned htmlSpace = Html.fromHtml("&nbsp;");

            for (String prblRef : context.getResources().getStringArray(R.array.PRBL_ARRAY)) {
                prblValue = prblRef.split("\\|");
                prblBookValue = prblValue[1].split(" ");
                bookRef = _s.GetBookRef(bbName, Integer.parseInt(prblBookValue[0]));

                resId = PCommon.GetResId(context, prblValue[0]);
                text = PCommon.ConcaT(getString(R.string.bulletDefault), htmlSpace, "(", bookRef.bsName, ")", htmlSpace, getString(resId));

                tvPrbl = new TextView(context);
                tvPrbl.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                tvPrbl.setPadding(20, 20, 20, 20);
                tvPrbl.setMinHeight(48);
                tvPrbl.setText(text);
                tvPrbl.setTag(prblValue[1]);
                tvPrbl.setOnClickListener(v -> {
                    try {
                        final String fullQuery = (String) v.getTag();
                        if (PCommon._isDebug) System.out.println(fullQuery);

                        final String msg = ((TextView) v).getText().toString().substring(2);
                        PCommon.SelectBibleLanguageMulti(builderLanguages, v.getContext(), vllLanguages, msg, "", true, false);
                        builderLanguages.setOnDismissListener(dialogInterface -> {
                            final String bbname = PCommon.GetPref(v.getContext(), IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, bbName);
                            if (bbname.isEmpty()) return;
                            final String tbbName = PCommon.GetPrefTradBibleName(v.getContext(), true);
                            Tab.AddTab(v.getContext(), "S", tbbName, fullQuery, true);
                        });
                        builderLanguages.show();
                    } catch (Exception ex) {
                        if (PCommon._isDebug) PCommon.LogR(v.getContext(), ex);
                    } finally {
                        builder.dismiss();
                    }
                });
                tvPrbl.setFocusable(true);
                tvPrbl.setBackground(PCommon.GetDrawable(context, R.drawable.focus_text));

                //Font
                if (typeface != null) tvPrbl.setTypeface(typeface);
                tvPrbl.setTextSize(fontSize);

                llPrbl.addView(tvPrbl);
            }
            llPrbl.requestFocus();
            sv.addView(llPrbl);

            builder.setTitle(R.string.mnuPrbl);
            builder.setCancelable(true);
            builder.setView(sv);
            builder.show();
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    private TextView CreateTvTitle(final Context context, final int titleId, final Typeface typeface) {
        try {
            final TextView tvTitle = new TextView(context);
            tvTitle.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            tvTitle.setPadding(20, 100, 20, 20);
            tvTitle.setGravity(Gravity.CENTER_HORIZONTAL);
            tvTitle.setText(Html.fromHtml(context.getString(titleId)));
            PCommon.SetTextAppareance(tvTitle, context, R.style.TextAppearance_AppCompat_Headline);
            if (typeface != null) tvTitle.setTypeface(typeface);

            return tvTitle;
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }

        return null;
    }

    private void ShowPlans() {
        try {
            CheckLocalInstance(this);

            final AlertDialog builder = new AlertDialog.Builder(this).create();
            final ScrollView sv = new ScrollView(this);
            sv.setLayoutParams(PCommon._layoutParamsMatchAndWrap);

            final LinearLayout llPlans = new LinearLayout(this);
            llPlans.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            llPlans.setOrientation(LinearLayout.VERTICAL);
            llPlans.setPadding(20, 20, 20, 20);

            final Typeface typeface = PCommon.GetTypeface(this);
            final int fontSize = PCommon.GetFontSize(this);

            TextView tvPlan, tvTitle;

            int resId, idx;
            String[] cols;
            String text;

            idx = 0;
            for (String plan : this.getResources().getStringArray(R.array.PLAN_ARRAY)) {
                cols = plan.split("\\|");
                final String planRef = cols[0];
                final boolean planExist = _s.IsPlanDescExist(planRef);

                resId = PCommon.GetResId(this, planRef);
                text = PCommon.ConcaT(getString(R.string.bulletDefault), Html.fromHtml("&nbsp;"), getString(resId));

                tvPlan = new TextView(this);
                tvPlan.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                tvPlan.setPadding(20, 20, 20, 20);
                tvPlan.setMinHeight(48);
                tvPlan.setText(text);
                tvPlan.setTag(idx);
                tvPlan.setOnClickListener(v -> {
                    try {
                        final int planIdx = Integer.parseInt(v.getTag().toString());
                        ShowPlan(true, planRef, planIdx, -1, -1);
                    } catch (Exception ex) {
                        if (PCommon._isDebug) PCommon.LogR(v.getContext(), ex);
                    } finally {
                        builder.dismiss();
                    }
                });
                if (planExist) {
                    tvPlan.setEnabled(false);
                    tvPlan.setTextColor(ContextCompat.getColor(this, R.color.colorDisable));
                }
                tvPlan.setFocusable(true);
                tvPlan.setBackground(PCommon.GetDrawable(getApplicationContext(), R.drawable.focus_text));

                //Font
                if (typeface != null) tvPlan.setTypeface(typeface);
                tvPlan.setTextSize(fontSize);

                llPlans.addView(tvPlan);
                idx++;
            }
            tvTitle = CreateTvTitle(this, R.string.tvPlanThemes, typeface);
            if (tvTitle != null) llPlans.addView(tvTitle, 0);
            tvTitle = CreateTvTitle(this, R.string.tvPlanBooks, typeface);
            if (tvTitle != null) {
                tvTitle.setPadding(tvTitle.getPaddingLeft(), tvTitle.getPaddingTop() + 40, tvTitle.getPaddingRight(), tvTitle.getPaddingBottom());
                llPlans.addView(tvTitle, 10);
            }

            //~~~
            final int planDescIdMax = _s.GetPlanDescIdMax();
            if (planDescIdMax <= 0) {
                if (!isPlanSelectAlreadyWarned) {
                    final Handler handler = new Handler();
                    handler.postDelayed(() -> {
                        isPlanSelectAlreadyWarned = true;
                        PCommon.ShowDialog(MainActivity.this, R.string.planSelect, false, null, R.string.planSelectMsg);
                    }, 500);
                }
            } else {
                final GridLayout glYourPlans = new GridLayout(this);
                glYourPlans.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                glYourPlans.setColumnCount(1);
                llPlans.addView(glYourPlans, 0);

                ArrayList<PlanDescBO> lstPd = _s.GetAllPlanDesc();
                for (PlanDescBO pd : lstPd) {
                    idx = 0;
                    @SuppressWarnings("UnusedAssignment") String plan = null;

                    for (String planToFind : this.getResources().getStringArray(R.array.PLAN_ARRAY)) {
                        cols = planToFind.split("\\|");
                        if (cols[0].compareTo(pd.planRef) == 0) {
                            final int planId = pd.planId;
                            plan = planToFind;

                            cols = plan.split("\\|");
                            final String planRef = cols[0];
                            resId = PCommon.GetResId(this, planRef);
                            text = PCommon.ConcaT(getString(R.string.bulletDefault),  Html.fromHtml("&nbsp;"), getString(resId));

                            final HorizontalScrollView hsv = new HorizontalScrollView(this);
                            hsv.setLayoutParams(PCommon._layoutParamsMatchAndWrap);

                            tvPlan = new TextView(this);
                            tvPlan.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                            tvPlan.setPadding(20, 10, 20, 0);
                            tvPlan.setText(text);
                            tvPlan.setTag(idx);
                            tvPlan.setOnClickListener(v -> {
                                try {
                                    final int planIdx = Integer.parseInt(v.getTag().toString());
                                    if (PCommon._isDebug) System.out.println(planIdx);
                                    ShowPlan(false, planRef, planIdx, planId, -1);
                                } catch (Exception ex) {
                                    if (PCommon._isDebug)
                                        PCommon.LogR(v.getContext(), ex);
                                } finally {
                                    builder.dismiss();
                                }
                            });
                            tvPlan.setFocusable(true);
                            tvPlan.setBackground(PCommon.GetDrawable(getApplicationContext(), R.drawable.focus_text));

                            text = _s.GetPlanCalProgressStatus(planId);
                            final TextView tvStatus = new TextView(this);
                            tvStatus.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                            tvStatus.setPadding(50, 10, 20, 0);
                            tvStatus.setText(Html.fromHtml(text));
                            tvStatus.setTag(idx);
                            tvStatus.setFocusable(true);
                            tvStatus.setBackground(PCommon.GetDrawable(getApplicationContext(), R.drawable.focus_text));
                            tvStatus.setOnClickListener(v -> {
                                try {
                                    final int planIdx = Integer.parseInt(v.getTag().toString());
                                    if (PCommon._isDebug) System.out.println(planIdx);
                                    ShowPlan(false, planRef, planIdx, planId, -1);
                                } catch (Exception ex) {
                                    if (PCommon._isDebug)
                                        PCommon.LogR(v.getContext(), ex);
                                } finally {
                                    builder.dismiss();
                                }
                            });
                            tvStatus.setOnLongClickListener(v -> {
                                ShowPlansMenu(builder, planId);
                                return false;
                            });
                            //Font
                            if (typeface != null) {
                                tvPlan.setTypeface(typeface);
                                tvStatus.setTypeface(typeface);
                            }
                            tvPlan.setTextSize(fontSize);
                            tvStatus.setTextSize(fontSize);

                            hsv.addView(tvStatus);
                            glYourPlans.addView(hsv, 0);
                            glYourPlans.addView(tvPlan, 0);
                        }
                        idx++;
                    }
                }
                if (!lstPd.isEmpty()) lstPd.clear();
            }
            tvTitle = CreateTvTitle(this, R.string.tvPlanYourPlans, typeface);
            if (tvTitle != null) llPlans.addView(tvTitle, 0);

            //~~~
            llPlans.requestFocus();
            sv.addView(llPlans);

            builder.setTitle(R.string.mnuPlansReading);
            builder.setCancelable(true);
            builder.setView(sv);
            builder.show();
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    /***
     * Show plans context menu
     * @param dlgPlans  Parent dialog
     * @param planId    Plan Id
     */
    private void ShowPlansMenu(final AlertDialog dlgPlans, final int planId) {
        try {
            CheckLocalInstance(this);

            final PlanDescBO pd = _s.GetPlanDesc(planId);
            if (pd == null) return;

            final Typeface typeface = PCommon.GetTypeface(this);
            final int fontSize = PCommon.GetFontSize(this);

            final LayoutInflater inflater = this.getLayoutInflater();
            final View view = inflater.inflate(R.layout.fragment_plan_delete_menu, this.findViewById(R.id.llToolsMenu));

            final AlertDialog builder = new AlertDialog.Builder(this).create();
            builder.setCancelable(true);
            builder.setTitle(R.string.mnuPlanReading);
            builder.setView(view);

            final int resId = PCommon.GetResId(getApplicationContext(), pd.planRef);
            final String planTitle = PCommon.ConcaT("<b>", getString(resId), ":</b>");
            final TextView tvPlanTitle = view.findViewById(R.id.tvToolsTitle);
            tvPlanTitle.setText(Html.fromHtml(planTitle));
            if (typeface != null) tvPlanTitle.setTypeface(typeface);
            tvPlanTitle.setTextSize(fontSize);

            final Button btnDelete = view.findViewById(R.id.btnDelete);
            btnDelete.setOnClickListener(v -> {
                btnDelete.setEnabled(false);

                final Handler handler = new Handler();
                handler.postDelayed(() -> {
                    _s.DeletePlan(planId);
                    builder.dismiss();
                    dlgPlans.dismiss();
                    ShowPlans();
                }, 500);
            });
            builder.show();
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    /***
     * Show plan
     * @param planId        Plan Id
     * @param pageNumber    Page number (should be >= 0 (0 = first page))
     */
    private void ShowPlan(final int planId, final int pageNumber) {
        try {
            CheckLocalInstance(getApplicationContext());

            final PlanDescBO pd = _s.GetPlanDesc(planId);
            final boolean isNewPlan = pd == null;
            if (isNewPlan) return;

            int idx = 0, planIdx = -1;
            String[] cols;

            for (String planToFind : this.getResources().getStringArray(R.array.PLAN_ARRAY)) {
                cols = planToFind.split("\\|");
                if (cols[0].compareTo(pd.planRef) == 0) {
                    planIdx = idx;
                    break;
                }

                idx++;
            }
            if (planIdx < 0) return;

            //noinspection ConstantConditions
            ShowPlan(isNewPlan, pd.planRef, planIdx, planId, pageNumber);
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    /***
     * Divide a / b
     * @param a (nomin)
     * @param b (denomin)
     * @return result or result + 1
     */
    private int Div(final int a, final int b) {
        try
        {
            if (b == 0) return a;

            return a % b == 0 ? a / b : (a / b) + 1;
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }

        return a;
    }

    /***
     * Show plan
     * @param isNewPlan     True: new plan, False: Update
     * @param planRef       Plan reference
     * @param planIdx       Position in array of templates
     * @param planId        Plan Id
     * @param pageNumber    Page number (should be >= 0 (0 = first page), -1 = auto)
     */
    @SuppressWarnings("UnusedAssignment")
    private void ShowPlan(final boolean isNewPlan, final String planRef, final int planIdx, final int planId, final int pageNumber) {
        try {
            CheckLocalInstance(this);

            final Typeface typeface = PCommon.GetTypeface(this);
            final int fontSize = PCommon.GetFontSize(this);

            //Check PageNumber
            final String bbname = PCommon.GetPrefBibleName(this);
            final int planCalRowCount = _s.GetPlanCalRowCount(bbname, planId);
            final int pageSize = 31;
            final int pageCount = ((planCalRowCount / pageSize) + 1);
            final int fpageNumber;
            if (pageNumber <= 0) {
                //TODO find dayNumber
                fpageNumber = 0;
            } else if (pageNumber >= pageCount) {
                fpageNumber = pageCount - 1;
            } else {
                fpageNumber = pageNumber;
            }

            final String plan = getApplicationContext().getResources().getStringArray(R.array.PLAN_ARRAY)[planIdx];
            final String[] cols = plan.split("\\|");
            final int bCount, cCount, vCount;
            int bNumber;
            final PlanDescBO pd;
            final String dtFormat = "yyyyMMdd";

            if (isNewPlan) {
                Integer[] ciTot = {0, 0};
                @SuppressWarnings("UnusedAssignment") Integer[] ci = null;

                //for book: get nb chapters & nb verses
                final String[] books = cols[1].split(",");
                for (String bookNumber : books) {
                    bNumber = Integer.parseInt(bookNumber);
                    ci = _s.GetBibleCiByBook(bNumber);

                    ciTot[0] += ci[0];
                    ciTot[1] += ci[1];
                }
                bCount = books.length;
                cCount = ciTot[0];
                vCount = ciTot[1];

                pd = new PlanDescBO();
                pd.planId = _s.GetPlanDescIdMax() + 1;
                pd.planRef = planRef;
                pd.bCount = bCount;
                pd.cCount = cCount;
                pd.vCount = vCount;
                pd.origVCount = vCount;

                //noinspection UnusedAssignment
                ci = null;
                //noinspection UnusedAssignment
                ciTot = null;
            } else {
                pd = _s.GetPlanDesc(planId);
                if (pd == null) return;

                bCount = pd.bCount;
                cCount = pd.cCount;
                vCount = pd.vCount;
            }

            //Dialog
            final LayoutInflater inflater = this.getLayoutInflater();
            final View view = inflater.inflate(R.layout.fragment_plan, this.findViewById(R.id.llPlan));

            final int planRefResId = PCommon.GetResId(getApplicationContext(), planRef);
            final String builderTitle = PCommon.ConcaT(getString(R.string.mnuPlan), ": ", getString(planRefResId));
            final AlertDialog builder = new AlertDialog.Builder(this).create();
            builder.setCancelable(true);
            builder.setTitle(builderTitle);
            builder.setView(view);

            final String strPlanDesc = PCommon.ConcaT(getResources().getString(R.string.planBookCount), ": ", bCount, "\n",
                    getResources().getString(R.string.planChapterCount), ": ", cCount, "\n",
                    getResources().getString(R.string.planVerseCount), ": ", vCount, "\n");
            final TextView tvPlanDesc = view.findViewById(R.id.tvPlanDesc);
            tvPlanDesc.setText(strPlanDesc);
            tvPlanDesc.setTextSize(fontSize);
            if (typeface != null) tvPlanDesc.setTypeface(typeface);

            final Button btnDelete = view.findViewById(R.id.btnDelete);
            final Button btnGotoPlans = view.findViewById(R.id.btnGotoPlans);
            btnGotoPlans.setOnClickListener(v -> {
                builder.dismiss();
                ShowPlans();
            });

            if (isNewPlan) {
                final int defaultVdayCount = 40;
                final int maxVerses = vCount; //4000
                final int maxChapters = cCount; //1189;

                btnDelete.setVisibility(View.GONE);
                final ToggleButton btnPlanCalVerseMode = view.findViewById(R.id.btnPlanCalVerseMode);
                final ToggleButton btnPlanCalChapterMode = view.findViewById(R.id.btnPlanCalChapterMode);
                pd.planType = btnPlanCalVerseMode.isChecked() ? PlanDescBO.PLAN_TYPE.VERSE_TYPE : PlanDescBO.PLAN_TYPE.CHAPTER_TYPE;

                final TextView tvTitlePlannedMode = view.findViewById(R.id.tvTitlePlannedMode);
                final TextView tvTitleVerseCount = view.findViewById(R.id.tvTitleVerseCount);
                final TextView tvTitleDayCount = view.findViewById(R.id.tvTitleDayCount);
                final TextView tvTitleStartDate = view.findViewById(R.id.tvTitleStartDate);
                PCommon.SetTextAppareance(tvTitlePlannedMode, this, R.style.TextAppearance_AppCompat_Headline);
                PCommon.SetTextAppareance(tvTitleVerseCount, this, R.style.TextAppearance_AppCompat_Headline);
                PCommon.SetTextAppareance(tvTitleDayCount, this, R.style.TextAppearance_AppCompat_Headline);
                PCommon.SetTextAppareance(tvTitleStartDate, this, R.style.TextAppearance_AppCompat_Headline);
                if (typeface != null) {
                    tvTitlePlannedMode.setTypeface(typeface);
                    tvTitleVerseCount.setTypeface(typeface);
                    tvTitleDayCount.setTypeface(typeface);
                    tvTitleStartDate.setTypeface(typeface);
                }

                final NumberPicker npVerseChapterCount = view.findViewById(R.id.npVerseChapterCount);
                final NumberPicker npDayCount = view.findViewById(R.id.npDayCount);

                npVerseChapterCount.setMinValue(1);
                npVerseChapterCount.setMaxValue(maxVerses); //setMaxValue(Math.min(pd.vCount, maxVerses));
                npVerseChapterCount.setValue(defaultVdayCount);
                npVerseChapterCount.setOnValueChangedListener((picker, oldVal, newVal) -> {
                    pd.vDayCount = npVerseChapterCount.getValue();
                    pd.dayCount = Div(pd.vCount, pd.vDayCount);
                    npDayCount.setValue(pd.dayCount);
                });

                npDayCount.setMinValue(1);
                npDayCount.setMaxValue(maxVerses); //setMaxValue(Math.min(pd.vCount, maxDays));
                npDayCount.setValue(Div(pd.vCount, defaultVdayCount));
                npDayCount.setOnValueChangedListener((picker, oldVal, newVal) -> {
                    pd.dayCount = npDayCount.getValue();
                    pd.vDayCount = Div(pd.vCount, pd.dayCount);
                    npVerseChapterCount.setValue(pd.vDayCount);
                });

                final View glPlanCalMeasures = view.findViewById(R.id.glPlanCalMeasures);
                glPlanCalMeasures.setVisibility(View.VISIBLE);
                final View clPlanCalMode = view.findViewById(R.id.clPlanCalMode);
                clPlanCalMode.setVisibility(View.VISIBLE);

                btnPlanCalVerseMode.setChecked(true);
                btnPlanCalChapterMode.setChecked(false);
                btnPlanCalVerseMode.setOnCheckedChangeListener((v, isChecked) -> {
                    if (isChecked) {
                        btnPlanCalVerseMode.setChecked(true);
                        btnPlanCalChapterMode.setChecked(false);

                        tvTitleVerseCount.setText(v.getContext().getString(R.string.planCalTitleVerseCount));
                        PCommon.SetTextAppareance(tvTitleVerseCount, v.getContext(), R.style.TextAppearance_AppCompat_Headline);
                        if (typeface != null) tvTitleVerseCount.setTypeface(typeface);

                        pd.planType = PlanDescBO.PLAN_TYPE.VERSE_TYPE;
                        pd.vCount = vCount;

                        npVerseChapterCount.setMaxValue(maxVerses); //setMaxValue(Math.min(pd.vCount, maxVerses));
                        npVerseChapterCount.setValue(npVerseChapterCount.getValue());

                        npDayCount.setMaxValue(maxVerses); //setMaxValue(Math.min(pd.vCount, maxDays));
                        npDayCount.setValue(Div(pd.vCount, npVerseChapterCount.getValue()));

                    } else {
                        btnPlanCalVerseMode.setChecked(false);
                        btnPlanCalChapterMode.setChecked(true);

                        tvTitleVerseCount.setText(v.getContext().getString(R.string.planCalTitleChapterCount));
                        PCommon.SetTextAppareance(tvTitleVerseCount, v.getContext(), R.style.TextAppearance_AppCompat_Headline);
                        if (typeface != null) tvTitleVerseCount.setTypeface(typeface);

                        pd.planType = PlanDescBO.PLAN_TYPE.CHAPTER_TYPE;
                        pd.vCount = cCount;

                        npVerseChapterCount.setMaxValue(maxChapters); //setMaxValue(Math.min(pd.vCount, maxChapters));
                        npVerseChapterCount.setValue(npVerseChapterCount.getValue());

                        npDayCount.setMaxValue(maxChapters); //setMaxValue(pd.vCount < maxChapters ? pd.vCount : maxDays);
                        npDayCount.setValue(Div(pd.vCount, npVerseChapterCount.getValue()));
                    }
                });
                btnPlanCalChapterMode.setOnCheckedChangeListener((v, isChecked) -> {
                    if (isChecked) {
                        btnPlanCalVerseMode.setChecked(false);
                        btnPlanCalChapterMode.setChecked(true);

                        tvTitleVerseCount.setText(v.getContext().getString(R.string.planCalTitleChapterCount));
                        PCommon.SetTextAppareance(tvTitleVerseCount, v.getContext(), R.style.TextAppearance_AppCompat_Headline);
                        if (typeface != null) tvTitleVerseCount.setTypeface(typeface);

                        pd.planType = PlanDescBO.PLAN_TYPE.CHAPTER_TYPE;
                        pd.vCount = cCount;

                        npVerseChapterCount.setMaxValue(maxChapters); //setMaxValue(Math.min(pd.vCount, maxChapters));
                        npVerseChapterCount.setValue(npVerseChapterCount.getValue());

                        npDayCount.setMaxValue(maxChapters); //setMaxValue(pd.vCount < maxChapters ? pd.vCount : maxDays);
                        npDayCount.setValue(Div(pd.vCount, npVerseChapterCount.getValue()));

                    } else {
                        btnPlanCalVerseMode.setChecked(true);
                        btnPlanCalChapterMode.setChecked(false);

                        tvTitleVerseCount.setText(v.getContext().getString(R.string.planCalTitleVerseCount));
                        PCommon.SetTextAppareance(tvTitleVerseCount, v.getContext(), R.style.TextAppearance_AppCompat_Headline);
                        if (typeface != null) tvTitleVerseCount.setTypeface(typeface);

                        pd.planType = PlanDescBO.PLAN_TYPE.VERSE_TYPE;
                        pd.vCount = vCount;

                        npVerseChapterCount.setMaxValue(maxVerses); //setMaxValue(Math.min(pd.vCount, maxVerses));
                        npVerseChapterCount.setValue(npVerseChapterCount.getValue());

                        npDayCount.setMaxValue(maxVerses); //setMaxValue(Math.min(pd.vCount, maxDays));
                        npDayCount.setValue(Div(pd.vCount, npVerseChapterCount.getValue()));
                    }
                });

                final Calendar nowCal = Calendar.getInstance();
                pd.startDt = DateFormat.format(dtFormat, nowCal).toString();
                final Button btnPlanSetStartDt = view.findViewById(R.id.btnPlanSetStartDt);
                btnPlanSetStartDt.setText(pd.startDt);
                btnPlanSetStartDt.setOnClickListener(v -> {
                    final DatePickerDialog.OnDateSetListener onDateSetListener = (view1, year, month, dayOfMonth) -> {
                        final Calendar startDt = Calendar.getInstance();
                        startDt.set(year, month, dayOfMonth);
                        final String startDtStr = DateFormat.format(dtFormat, startDt).toString();
                        btnPlanSetStartDt.setText(startDtStr);
                        pd.startDt = startDtStr;
                    };
                    final DatePickerDialog datePickerDialog = new DatePickerDialog(v.getContext(), onDateSetListener, nowCal.get(Calendar.YEAR), nowCal.get(Calendar.MONTH), nowCal.get(Calendar.DAY_OF_MONTH));
                    datePickerDialog.show();
                });

                final Button btnPlanCreate = view.findViewById(R.id.btnPlanCreate);
                btnPlanCreate.setVisibility(View.VISIBLE);
                btnPlanCreate.setOnClickListener(v -> {
                    btnPlanCalVerseMode.setEnabled(false);
                    btnPlanCalChapterMode.setEnabled(false);
                    btnPlanCreate.setEnabled(false);
                    btnGotoPlans.setEnabled(false);

                    pd.planType = btnPlanCalChapterMode.isChecked() ? PlanDescBO.PLAN_TYPE.CHAPTER_TYPE : PlanDescBO.PLAN_TYPE.VERSE_TYPE;
                    pd.vDayCount = npVerseChapterCount.getValue();
                    pd.dayCount = Div(pd.vCount, pd.vDayCount);

                    if (pd.planType == PlanDescBO.PLAN_TYPE.VERSE_TYPE) {
                        if (pd.dayCount > maxVerses || pd.vDayCount > maxVerses) {
                            final int msgId = pd.dayCount > maxVerses ? R.string.seeMat24_32 : R.string.seeGen2_2;
                            PCommon.ShowToast(getApplicationContext(), msgId, Toast.LENGTH_LONG);
                            builder.dismiss();
                            ShowPlans();
                            return;
                        }
                    } else {
                        if (pd.dayCount > maxChapters || pd.vDayCount > maxChapters) {
                            final int msgId = pd.dayCount > maxChapters ? R.string.seeMat24_32 : R.string.seeGen2_2;
                            PCommon.ShowToast(getApplicationContext(), msgId, Toast.LENGTH_LONG);
                            builder.dismiss();
                            ShowPlans();
                            return;
                        }
                    }

                    final Calendar cal = Calendar.getInstance();
                    cal.set(Integer.parseInt(pd.startDt.substring(0, 4)),
                            Integer.parseInt(pd.startDt.substring(4, 6)),
                            Integer.parseInt(pd.startDt.substring(6, 8)));
                    cal.add(Calendar.DAY_OF_MONTH, pd.dayCount);
                    pd.endDt = DateFormat.format(dtFormat, cal).toString();

                    final AlertDialog pgr = PCommon.ShowProgressDialog(MainActivity.this, R.string.mnuPlan, R.string.planCreating);
                    final Handler handler = new Handler();
                    handler.postDelayed(() -> {
                        pd.planId = _s.GetPlanDescIdMax() + 1;
                        _s.AddPlan(pd, cols[1]);

                        builder.dismiss();
                        ShowPlans();

                        if (pgr != null) pgr.dismiss();
                    }, 500);
                });
            } else {
                btnDelete.setVisibility(View.VISIBLE);
                btnDelete.setOnClickListener(v -> ShowPlansMenu(builder, planId));
                final Button btnBack = view.findViewById(R.id.btnBack);
                btnBack.setVisibility(View.VISIBLE);
                btnBack.setOnClickListener(v -> {
                    builder.dismiss();
                    ShowPlan(planId, fpageNumber - 1);
                });
                final Button btnForward = view.findViewById(R.id.btnForward);
                btnForward.setVisibility(View.VISIBLE);
                btnForward.setOnClickListener(v -> {
                    builder.dismiss();
                    ShowPlan(planId, fpageNumber + 1);
                });
                if (fpageNumber == 0) btnBack.setEnabled(false);
                if (fpageNumber == pageCount - 1) btnForward.setEnabled(false);

                final GridLayout glCal = view.findViewById(R.id.glCal);
                glCal.setVisibility(View.VISIBLE);

                final ArrayList<PlanCalBO> lstCal = _s.GetPlanCal(bbname, pd.planId, fpageNumber);
                TextView tvDay, tvUntil, tvTitleIsRead;
                CheckBox chkIsRead;
                String strDay, strUntil;

                tvDay = new TextView(this);
                tvDay.setLayoutParams(PCommon._layoutParamsWrap);
                tvDay.setPadding(10, 10, 10, 10);
                PCommon.SetTextAppareance(tvDay, this, R.style.TextAppearance_AppCompat_Headline);
                tvDay.setText(Html.fromHtml(PCommon.ConcaT("<b>", getString(R.string.planCalTitleDt).replaceFirst("\n", "<br><u>"), "</b>")));
                if (typeface != null) tvDay.setTypeface(typeface);

                tvUntil = new TextView(this);
                tvUntil.setLayoutParams(PCommon._layoutParamsWrap);
                tvUntil.setPadding(10, 10, 10, 10);
                PCommon.SetTextAppareance(tvUntil, this, R.style.TextAppearance_AppCompat_Headline);
                tvUntil.setText(Html.fromHtml(PCommon.ConcaT("<b>", getString(R.string.planCalTitleUntil).replaceFirst("\n", "<br><u>"), "</b>")));
                if (typeface != null) tvUntil.setTypeface(typeface);

                tvTitleIsRead = new TextView(this);
                tvTitleIsRead.setLayoutParams(PCommon._layoutParamsWrap);
                tvTitleIsRead.setPadding(10, 10, 10, 10);
                PCommon.SetTextAppareance(tvTitleIsRead, this, R.style.TextAppearance_AppCompat_Headline);
                tvTitleIsRead.setText(Html.fromHtml(PCommon.ConcaT("<b>", getString(R.string.planCalTitleIsRead).replaceFirst("\n", "<br><u>"), "</b>")));
                if (typeface != null) tvTitleIsRead.setTypeface(typeface);

                glCal.addView(tvTitleIsRead);
                glCal.addView(tvDay);
                glCal.addView(tvUntil);

                final int nowDayNumber = _s.GetCurrentDayNumberOfPlanCal(planId);
                for (PlanCalBO pc : lstCal) {
                    chkIsRead = new CheckBox(this);
                    chkIsRead.setLayoutParams(PCommon._layoutParamsWrap);
                    chkIsRead.setPadding(10, 10, 10, 10);
                    chkIsRead.setEnabled(true);
                    chkIsRead.setTag(R.id.tv1, pc.planId);
                    chkIsRead.setTag(R.id.tv2, pc.dayNumber);
                    chkIsRead.setOnCheckedChangeListener((v, isChecked) -> {
                        final int planId13 = (int) v.getTag(R.id.tv1);
                        final int dayNumber = (int) v.getTag(R.id.tv2);
                        final int isRead = isChecked ? 1 : 0;
                        _s.MarkPlanCal(planId13, dayNumber, isRead);
                    });
                    chkIsRead.setChecked(pc.isRead == 1);

                    tvDay = new TextView(this);
                    tvDay.setLayoutParams(PCommon._layoutParamsWrap);
                    tvDay.setPadding(10, 10, 10, 10);
                    strDay = PCommon.ConcaT(pc.dayNumber, "\n", pc.dayDt);
                    tvDay.setText(strDay);
                    tvDay.setTextSize(fontSize);
                    if (typeface != null) {
                        if (pc.dayNumber == nowDayNumber) {
                            tvDay.setTypeface(typeface, Typeface.BOLD);
                        } else {
                            tvDay.setTypeface(typeface);
                        }
                    }
                    tvDay.setTag(R.id.tv1, pc.planId);
                    tvDay.setTag(R.id.tv2, pc.dayNumber);
                    tvDay.setOnClickListener(v -> {
                        final int planId1 = (int) v.getTag(R.id.tv1);
                        final int dayNumber = (int) v.getTag(R.id.tv2);
                        ShowPlanMenu(builder, planId1, dayNumber, fpageNumber);
                    });
                    tvDay.setFocusable(true);
                    tvDay.setBackground(PCommon.GetDrawable(getApplicationContext(), R.drawable.focus_text));

                    tvUntil = new TextView(this);
                    tvUntil.setLayoutParams(PCommon._layoutParamsWrap);
                    tvUntil.setPadding(10, 10, 10, 10);
                    strUntil = PCommon.ConcaT(pc.bsNameStart, " ", pc.cNumberStart, ".", pc.vNumberStart, "\n", pc.bsNameEnd, " ", pc.cNumberEnd, ".", pc.vNumberEnd);
                    tvUntil.setText(strUntil);
                    if (typeface != null) tvUntil.setTypeface(typeface);
                    tvUntil.setTextSize(fontSize);
                    tvUntil.setTag(R.id.tv1, pc.planId);
                    tvUntil.setTag(R.id.tv2, pc.dayNumber);
                    tvUntil.setOnClickListener(v -> {
                        final int planId12 = (int) v.getTag(R.id.tv1);
                        final int dayNumber = (int) v.getTag(R.id.tv2);
                        ShowPlanMenu(builder, planId12, dayNumber, fpageNumber);
                    });
                    tvUntil.setFocusable(true);
                    tvUntil.setBackground(PCommon.GetDrawable(getApplicationContext(), R.drawable.focus_text));

                    glCal.addView(chkIsRead);
                    glCal.addView(tvDay);
                    glCal.addView(tvUntil);
                }
                btnGotoPlans.requestFocus();
            }

            builder.show();
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    /***
     * Show plan context menu
     * @param dlgPlan       Parent dialog
     * @param planId        Plan Id
     * @param dayNumber     Day number
     * @param pageNumber    Page number
     */
    private void ShowPlanMenu(final AlertDialog dlgPlan, final int planId, final int dayNumber, final int pageNumber) {
        try {
            CheckLocalInstance(this);

            final PlanDescBO pd = _s.GetPlanDesc(planId);
            if (pd == null) return;

            final Typeface typeface = PCommon.GetTypeface(this);
            final int fontSize = PCommon.GetFontSize(this);

            final LayoutInflater inflater = this.getLayoutInflater();
            final View view = inflater.inflate(R.layout.fragment_plan_menu, this.findViewById(R.id.llPlanMenu));

            final AlertDialog builder = new AlertDialog.Builder(this).create();
            builder.setCancelable(true);
            builder.setTitle(R.string.mnuPlanReading);
            builder.setView(view);

            final int resId = PCommon.GetResId(getApplicationContext(), pd.planRef);
            final String planTitle = PCommon.ConcaT("<b>", getString(resId), ":</b>");
            final TextView tvPlanTitle = view.findViewById(R.id.tvToolsTitle);
            tvPlanTitle.setText(Html.fromHtml(planTitle));
            if (typeface != null) tvPlanTitle.setTypeface(typeface);
            tvPlanTitle.setTextSize(fontSize);

            final Button btnOpen = view.findViewById(R.id.btnOpen);
            btnOpen.setOnClickListener(v -> {
                final Handler handler = new Handler();
                handler.post(() -> {
                    final String bbname = PCommon.GetPrefBibleName(getApplicationContext());
                    final String fullQuery = PCommon.ConcaT(planId, " ", dayNumber, " ", pageNumber);

                    final AlertDialog builderLanguages = new AlertDialog.Builder(view.getContext()).create();             //, R.style.DialogStyleKaki
                    final LayoutInflater inflater1 = getLayoutInflater();
                    final View vllLanguages = inflater1.inflate(R.layout.fragment_languages_multi, findViewById(R.id.llLanguages));
                    final String msg = getString(R.string.mnuPlanReading);
                    PCommon.SelectBibleLanguageMulti(builderLanguages, view.getContext(), vllLanguages, msg, "", true, false);
                    builderLanguages.setOnDismissListener(dialogInterface -> {
                        final String bbnamed = PCommon.GetPref(view.getContext(), IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, bbname);
                        if (bbnamed.isEmpty()) return;
                        final String tbbName = PCommon.GetPrefTradBibleName(view.getContext(), true);
                        Tab.AddTab(getApplicationContext(), "P", tbbName, fullQuery, true);
                        builder.dismiss();
                        dlgPlan.dismiss();
                    });
                    builderLanguages.show();
                });
            });
            final Button btnMarkAllAboveAsRead = view.findViewById(R.id.btnMarkAllAboveAsRead);
            btnMarkAllAboveAsRead.setOnClickListener(v -> {
                final Handler handler = new Handler();
                handler.post(() -> {
                    _s.MarkAllAbovePlanCal(planId, dayNumber, 1);
                    builder.dismiss();
                    dlgPlan.dismiss();
                    ShowPlan(planId, pageNumber);
                });
            });
            final Button btnUnmarkAllAboveAsRead = view.findViewById(R.id.btnUnmarkAllAboveAsRead);
            btnUnmarkAllAboveAsRead.setOnClickListener(v -> {
                final Handler handler = new Handler();
                handler.post(() -> {
                    _s.MarkAllAbovePlanCal(planId, dayNumber, 0);
                    builder.dismiss();
                    dlgPlan.dismiss();
                    ShowPlan(planId, pageNumber);
                });
            });
            builder.show();
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    private void ShowRandomVerses(final Context context) {
        try {
            CheckLocalInstance(context);

            final String bbName = PCommon.GetPrefBibleName(context);
            Tab.AddTab(context, "S", bbName, "RAND:", true);
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    private void ShowSharePic(final Context context) {
        try {
            CheckLocalInstance(context);

            final Intent intent = new Intent(context, SharePictureActivity.class);
            startActivity(intent);
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    private void ShowTodos() {
        try {
            CheckLocalInstance(this);

            final Typeface typeface = PCommon.GetTypeface(this);
            final int fontSize = PCommon.GetFontSize(this);

            //Dialog
            final LayoutInflater inflater = this.getLayoutInflater();
            final View view = inflater.inflate(R.layout.fragment_todos, this.findViewById(R.id.llTodos));
            final Button btnTdAdd = view.findViewById(R.id.btnTdAdd);
            final Button btnTdResetStatus = view.findViewById(R.id.btnTdResetStatus);
            final ToggleButton btnTdTodoStatusType = view.findViewById(R.id.btnTdTodoStatusType);
            final ToggleButton btnTdDoneStatusType = view.findViewById(R.id.btnTdDoneStatusType);
            final ToggleButton btnTdWaitStatusType = view.findViewById(R.id.btnTdWaitStatusType);
            final GridLayout glTd = view.findViewById(R.id.glTodos);

            final String builderTitle = getString(R.string.mnuTodos);
            final AlertDialog builder = new AlertDialog.Builder(this).create();
            builder.setCancelable(true);
            builder.setTitle(builderTitle);
            builder.setView(view);

            final String tdTitleTodoStatusType = PCommon.ConcaT(getString(R.string.tdTitleTodoStatusType), "\n(", _s.GetTodoCountByStatus("TODO"), ")");
            btnTdTodoStatusType.setTextOn(tdTitleTodoStatusType);
            btnTdTodoStatusType.setTextOff(tdTitleTodoStatusType);
            final String tdTitleDoneStatusType = PCommon.ConcaT(getString(R.string.tdTitleDoneStatusType), "\n(", _s.GetTodoCountByStatus("DONE"), ")");
            btnTdDoneStatusType.setTextOn(tdTitleDoneStatusType);
            btnTdDoneStatusType.setTextOff(tdTitleDoneStatusType);
            final String tdTitleWaitStatusType = PCommon.ConcaT(getString(R.string.tdTitleWaitStatusType), "\n(", _s.GetTodoCountByStatus("WAIT"), ")");
            btnTdWaitStatusType.setTextOn(tdTitleWaitStatusType);
            btnTdWaitStatusType.setTextOff(tdTitleWaitStatusType);

            btnTdTodoStatusType.setChecked(false);
            btnTdDoneStatusType.setChecked(false);
            btnTdWaitStatusType.setChecked(false);

            final String todoType = PCommon.GetPref(this, IProject.APP_PREF_KEY.TODO_STATUS, "TODO");
            if (todoType.compareTo("DONE") == 0) {
                btnTdDoneStatusType.setChecked(true);
            } else if (todoType.compareTo("WAIT") == 0) {
                btnTdWaitStatusType.setChecked(true);
            } else {
                btnTdTodoStatusType.setChecked(true);
            }

            final ArrayList<TodoBO> lstTd = _s.GetListTodoByStatus(todoType);
            for (final TodoBO td : lstTd) {
                if (td != null) {
                    final String tdPriority = td.tdPriority.compareTo("HIGH") == 0 ? getString(R.string.priorityHigh) : td.tdPriority.compareTo("LOW") == 0 ? getString(R.string.priorityLow) : getString(R.string.bulletDefault);
                    final String tdDesc = PCommon.ConcaT(Html.fromHtml(PCommon.ConcaT(tdPriority, "&nbsp;")), td.tdDesc);
                    final TextView tvDesc = new TextView(this);
                    tvDesc.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                    tvDesc.setPadding(20, 20, 20, 20);
                    tvDesc.setMinHeight(48);
                    tvDesc.setText(tdDesc);
                    tvDesc.setTextSize(fontSize);
                    if (typeface != null) {
                        tvDesc.setTypeface(typeface);
                    }
                    tvDesc.setTag(R.id.tv1, td.tdId);
                    tvDesc.setOnClickListener(v -> {
                        final Handler handler = new Handler();
                        handler.postDelayed(() -> {
                            builder.dismiss();
                            final int tdId = (int) v.getTag(R.id.tv1);
                            ShowTodo(false, tdId);
                        }, 0);
                    });
                    tvDesc.setOnLongClickListener(v -> {
                        final Handler handler = new Handler();
                        handler.postDelayed(() -> {
                            final int tdId = (int) v.getTag(R.id.tv1);
                            ShowTodoMenu(builder, 0, tdId);
                        }, 0);
                        return false;
                    });
                    tvDesc.setFocusable(true);
                    tvDesc.setBackground(PCommon.GetDrawable(getApplicationContext(), R.drawable.focus_text));
                    glTd.addView(tvDesc);
                }
            }
            btnTdAdd.setOnClickListener(v -> {
                final Handler handler = new Handler();
                handler.postDelayed(() -> {
                    PCommon.SavePref(v.getContext(), IProject.APP_PREF_KEY.TODO_STATUS, "TODO");

                    builder.dismiss();
                    ShowTodo(true, -1);
                }, 0);
            });
            btnTdResetStatus.setOnClickListener(v -> {
                final Handler handler = new Handler();
                handler.postDelayed(() -> ShowTodoMenu(builder, 1, -1), 0);
            });
            btnTdTodoStatusType.setOnClickListener(v -> {
                final Handler handler = new Handler();
                handler.postDelayed(() -> {
                    PCommon.SavePref(v.getContext(), IProject.APP_PREF_KEY.TODO_STATUS, "TODO");
                    btnTdTodoStatusType.setChecked(true);
                    btnTdDoneStatusType.setChecked(false);
                    btnTdWaitStatusType.setChecked(false);
                    builder.dismiss();
                    ShowTodos();
                }, 0);
            });
            btnTdDoneStatusType.setOnClickListener(v -> {
                final Handler handler = new Handler();
                handler.postDelayed(() -> {
                    PCommon.SavePref(v.getContext(), IProject.APP_PREF_KEY.TODO_STATUS, "DONE");
                    btnTdTodoStatusType.setChecked(false);
                    btnTdDoneStatusType.setChecked(true);
                    btnTdWaitStatusType.setChecked(false);
                    builder.dismiss();
                    ShowTodos();
                }, 0);
            });
            btnTdWaitStatusType.setOnClickListener(v -> {
                final Handler handler = new Handler();
                handler.postDelayed(() -> {
                    PCommon.SavePref(v.getContext(), IProject.APP_PREF_KEY.TODO_STATUS, "WAIT");
                    btnTdTodoStatusType.setChecked(false);
                    btnTdDoneStatusType.setChecked(false);
                    btnTdWaitStatusType.setChecked(true);
                    builder.dismiss();
                    ShowTodos();
                }, 0);
            });
            glTd.requestFocus();
            builder.show();

            //--
            final int todoIdCount = _s.GetTodoIdCount();
            if (todoIdCount <= 0) {
                if (!isTodoSelectAlreadyWarned) {
                    final Handler handler = new Handler();
                    handler.postDelayed(() -> {
                        isTodoSelectAlreadyWarned = true;
                        PCommon.ShowDialog(MainActivity.this, R.string.tdSelect, false, null, R.string.tdSelectMsg);
                    }, 0);
                }
            }
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    private void ShowTodo(final boolean isNewTodo, final int tdId) {
        try {
            CheckLocalInstance(this);

            final Typeface typeface = PCommon.GetTypeface(this);
            final int fontSize = PCommon.GetFontSize(this);

            //Dialog
            final LayoutInflater inflater = this.getLayoutInflater();
            final View view = inflater.inflate(R.layout.fragment_todo, this.findViewById(R.id.llTodo));

            final String builderTitle = getString(R.string.mnuTodos);
            final AlertDialog builder = new AlertDialog.Builder(this).create();
            builder.setCancelable(true);
            builder.setTitle(builderTitle);
            builder.setView(view);
            builder.setOnCancelListener(dialog -> ShowTodos());

            final TextView tvTdTitleDesc = view.findViewById(R.id.tvTdTitleDesc);
            final TextView tvTdTitleCommentIssues = view.findViewById(R.id.tvTdTitleCommentIssues);
            final TextView tvTdTitlePriorityType = view.findViewById(R.id.tvTdTitlePriorityType);
            final TextView tvTdTitleStatusType = view.findViewById(R.id.tvTdTitleStatusType);

            final EditText etTdDesc = view.findViewById(R.id.etTdDesc);
            final EditText etTdCommentIssues = view.findViewById(R.id.etTdCommentIssues);

            PCommon.SetTextAppareance(tvTdTitleDesc, this, R.style.TextAppearance_AppCompat_Headline);
            PCommon.SetTextAppareance(tvTdTitleCommentIssues, this, R.style.TextAppearance_AppCompat_Headline);
            PCommon.SetTextAppareance(tvTdTitlePriorityType, this, R.style.TextAppearance_AppCompat_Headline);
            PCommon.SetTextAppareance(tvTdTitleStatusType, this, R.style.TextAppearance_AppCompat_Headline);

            if (typeface != null) {
                tvTdTitleDesc.setTypeface(typeface);
                tvTdTitleCommentIssues.setTypeface(typeface);
                tvTdTitlePriorityType.setTypeface(typeface);
                tvTdTitleStatusType.setTypeface(typeface);

                etTdDesc.setTypeface(typeface);
                etTdCommentIssues.setTypeface(typeface);
            }

            etTdDesc.setTextSize(fontSize);
            etTdCommentIssues.setTextSize(fontSize);

            final ToggleButton btnTdTodoStatusType = view.findViewById(R.id.btnTdTodoStatusType);
            final ToggleButton btnTdDoneStatusType = view.findViewById(R.id.btnTdDoneStatusType);
            final ToggleButton btnTdWaitStatusType = view.findViewById(R.id.btnTdWaitStatusType);
            final ToggleButton btnTdNormalPriorityType = view.findViewById(R.id.btnTdNormalPriorityType);
            final ToggleButton btnTdHighPriorityType = view.findViewById(R.id.btnTdHighPriorityType);
            final ToggleButton btnTdLowPriorityType = view.findViewById(R.id.btnTdLowPriorityType);

            final Button btnTdDelete = view.findViewById(R.id.btnTdDelete);
            final Button btnTdSave = view.findViewById(R.id.btnTdSave);

            btnTdTodoStatusType.setChecked(false);
            btnTdDoneStatusType.setChecked(false);
            btnTdWaitStatusType.setChecked(false);

            btnTdNormalPriorityType.setChecked(false);
            btnTdHighPriorityType.setChecked(false);
            btnTdLowPriorityType.setChecked(false);

            btnTdDelete.setVisibility(View.GONE);
            btnTdTodoStatusType.requestFocus();

            if (isNewTodo) {
                btnTdTodoStatusType.setChecked(true);
                btnTdNormalPriorityType.setChecked(true);

                etTdDesc.setText(getString(R.string.textEmpty));
                etTdCommentIssues.setText(getString(R.string.textEmpty));
            } else {
                final TodoBO td = _s.GetTodo(tdId);
                if (td != null) {
                    btnTdDelete.setVisibility(View.VISIBLE);

                    etTdDesc.setText(td.tdDesc);
                    etTdCommentIssues.setText(td.tdCommentIssues);

                    if (td.tdStatus.compareTo("DONE") == 0) {
                        btnTdDoneStatusType.setChecked(true);
                    } else if (td.tdStatus.compareTo("WAIT") == 0) {
                        btnTdWaitStatusType.setChecked(true);
                    } else {
                        btnTdTodoStatusType.setChecked(true);
                    }

                    if (td.tdPriority.compareTo("HIGH") == 0) {
                        btnTdHighPriorityType.setChecked(true);
                    } else if (td.tdPriority.compareTo("LOW") == 0) {
                        btnTdLowPriorityType.setChecked(true);
                    } else {
                        btnTdNormalPriorityType.setChecked(true);
                    }
                }
            }

            btnTdTodoStatusType.setOnClickListener(v -> {
                btnTdTodoStatusType.setChecked(true);
                btnTdDoneStatusType.setChecked(false);
                btnTdWaitStatusType.setChecked(false);
            });
            btnTdDoneStatusType.setOnClickListener(v -> {
                btnTdTodoStatusType.setChecked(false);
                btnTdDoneStatusType.setChecked(true);
                btnTdWaitStatusType.setChecked(false);
            });
            btnTdWaitStatusType.setOnClickListener(v -> {
                btnTdTodoStatusType.setChecked(false);
                btnTdDoneStatusType.setChecked(false);
                btnTdWaitStatusType.setChecked(true);
            });

            btnTdNormalPriorityType.setOnClickListener(v -> {
                btnTdNormalPriorityType.setChecked(true);
                btnTdHighPriorityType.setChecked(false);
                btnTdLowPriorityType.setChecked(false);
            });
            btnTdHighPriorityType.setOnClickListener(v -> {
                btnTdNormalPriorityType.setChecked(false);
                btnTdHighPriorityType.setChecked(true);
                btnTdLowPriorityType.setChecked(false);
            });
            btnTdLowPriorityType.setOnClickListener(v -> {
                btnTdNormalPriorityType.setChecked(false);
                btnTdHighPriorityType.setChecked(false);
                btnTdLowPriorityType.setChecked(true);
            });
            btnTdDelete.setOnClickListener(v -> {
                final Handler handler = new Handler();
                handler.postDelayed(() -> ShowTodoMenu(builder, 0, tdId), 0);
            });
            btnTdSave.setOnClickListener(v -> {
                try {
                    final Handler handler = new Handler();
                    handler.postDelayed(() -> {
                        etTdDesc.setText(etTdDesc.getText().toString().trim().replaceAll("\n", ""));
                        etTdCommentIssues.setText(etTdCommentIssues.getText().toString().trim().replaceAll("\n", ""));

                        if (etTdDesc.length() == 0) {
                            etTdDesc.requestFocus();
                            return;
                        }

                        final TodoBO td = new TodoBO();
                        td.tdId = isNewTodo ? _s.GetNewTodoId() : tdId;
                        td.tdStatus = btnTdDoneStatusType.isChecked() ? "DONE" : btnTdWaitStatusType.isChecked() ? "WAIT" : "TODO";
                        td.tdPriority = btnTdHighPriorityType.isChecked() ? "HIGH" : btnTdLowPriorityType.isChecked() ? "LOW" : "NORMAL";
                        td.tdDesc = etTdDesc.getText().toString();
                        td.tdCommentIssues = etTdCommentIssues.getText().toString();

                        if (isNewTodo) {
                            _s.AddTodo(td);
                        } else {
                            _s.UpdateTodo(td);
                        }

                        builder.dismiss();
                        ShowTodos();
                    }, 0);
                } catch (Exception ex) {
                    if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
                }
            });

            builder.show();
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    /***
     * Show To do context menu
     * @param dlgTodo   Parent dialog
     * @param action    0 = Delete, 1 = Reset
     * @param tdId      To do Id
     */
    private void ShowTodoMenu(final AlertDialog dlgTodo, final int action, final int tdId) {
        try {
            CheckLocalInstance(this);

            if (action == 0) {
                final TodoBO td = _s.GetTodo(tdId);
                if (td == null) return;
            }

            final Typeface typeface = PCommon.GetTypeface(this);
            final int fontSize = PCommon.GetFontSize(this);

            final LayoutInflater inflater = this.getLayoutInflater();
            final View view = inflater.inflate(R.layout.fragment_todo_menu, this.findViewById(R.id.llTodoMenu));

            final AlertDialog builder = new AlertDialog.Builder(this).create();
            builder.setCancelable(true);
            builder.setTitle(R.string.mnuTodos);
            builder.setView(view);

            final String tdTitleMenu = PCommon.ConcaT("<b>", getString(action == 0 ? R.string.tdTask : R.string.tdReset), ":</b>");
            final TextView tvTdTitleMenu = view.findViewById(R.id.tvTdTitleMenu);
            tvTdTitleMenu.setText(Html.fromHtml(tdTitleMenu));
            tvTdTitleMenu.setTextSize(fontSize);
            if (typeface != null) tvTdTitleMenu.setTypeface(typeface);

            final TextView tvTdMsgMenu = view.findViewById(R.id.tvTdMsgMenu);
            tvTdMsgMenu.setVisibility(View.GONE);
            if (action == 1) {
                final String tdMsgMenu = getString(R.string.tdResetMsg);
                tvTdMsgMenu.setVisibility(View.VISIBLE);
                tvTdMsgMenu.setText(Html.fromHtml(tdMsgMenu));
                tvTdMsgMenu.setTextSize(fontSize);
                if (typeface != null) tvTdMsgMenu.setTypeface(typeface);
                tvTdMsgMenu.setFocusable(true);
                tvTdMsgMenu.setBackground(PCommon.GetDrawable(getApplicationContext(), R.drawable.focus_text));
            }

            final Button btnTdActionMenu = view.findViewById(R.id.btnTdActionMenu);
            btnTdActionMenu.setText(action == 0 ? R.string.tdDelete : R.string.tdReset);
            btnTdActionMenu.setOnClickListener(v -> {
                btnTdActionMenu.setEnabled(false);

                final Handler handler = new Handler();
                handler.postDelayed(() -> {
                    if (action == 0) {
                        _s.DeleteTodo(tdId);
                    } else if (action == 1) {
                        PCommon.SavePref(v.getContext(), IProject.APP_PREF_KEY.TODO_STATUS, "TODO");
                        _s.ResetTodoStatus();
                    }
                    builder.dismiss();
                    dlgTodo.dismiss();
                    ShowTodos();
                }, 0);
            });
            builder.show();
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    private void ShowTools(final Context context) {
        try {
            CheckLocalInstance(context);

            if (tabLayout == null) return;

            final Typeface typeface = PCommon.GetTypeface(this);
            final int fontSize = PCommon.GetFontSize(this);

            final LayoutInflater inflater = this.getLayoutInflater();
            final View view = inflater.inflate(R.layout.fragment_tools, this.findViewById(R.id.llToolsMenu));

            final AlertDialog builder = new AlertDialog.Builder(this).create();
            builder.setCancelable(true);
            builder.setTitle(R.string.mnuTools);
            builder.setView(view);

            final int tabCount = tabLayout.getTabCount();
            final String resToolTabsTitle = PCommon.ConcaT("<u><b>", getString(R.string.toolCloseTabsTitle), "</b> (", tabCount, ")</u>");
            final TextView tvToolTabsTitle = view.findViewById(R.id.tvToolTabsTitle);
            tvToolTabsTitle.setText(Html.fromHtml(resToolTabsTitle));
            if (typeface != null) tvToolTabsTitle.setTypeface(typeface);
            tvToolTabsTitle.setTextSize(fontSize);

            final String resToolImportExportDbTitle = PCommon.ConcaT("<u><b>", getString(R.string.toolImportExportDbTitle), "</b></u>");
            final TextView tvToolImportExportDbTitle = view.findViewById(R.id.tvToolImportExportDbTitle);
            tvToolImportExportDbTitle.setText(Html.fromHtml(resToolImportExportDbTitle));
            if (typeface != null) tvToolImportExportDbTitle.setTypeface(typeface);
            tvToolImportExportDbTitle.setTextSize(fontSize);

            final Button btnToolSortTabs = view.findViewById(R.id.btnToolSortTabs);
            final Button btnToolOpenTabs = view.findViewById(R.id.btnToolOpenTabsTest);
            final Button btnToolCloseTabs = view.findViewById(R.id.btnToolCloseTabs);
            final Button btnToolImportDb = view.findViewById(R.id.btnToolImportDb);
            final Button btnToolExportDb = view.findViewById(R.id.btnToolExportDb);

            btnToolSortTabs.setOnClickListener(v -> {
                btnToolSortTabs.setEnabled(false);
                btnToolOpenTabs.setEnabled(false);
                btnToolCloseTabs.setEnabled(false);
                btnToolImportDb.setEnabled(false);
                btnToolExportDb.setEnabled(false);

                final Handler handler = new Handler();
                handler.postDelayed(() -> {
                    builder.dismiss();
                    ShowToolSortTabs(context);
                }, 0);
            });
            btnToolOpenTabs.setOnClickListener(v -> {
                btnToolSortTabs.setEnabled(false);
                btnToolOpenTabs.setEnabled(false);
                btnToolCloseTabs.setEnabled(false);
                btnToolImportDb.setEnabled(false);
                btnToolExportDb.setEnabled(false);

                final Handler handler = new Handler();
                handler.postDelayed(() -> {
                    try
                    {
                        for (int bnumber = 1; bnumber <= 10; bnumber++) {
                            for (int cnumber = 1; cnumber <= 15; cnumber++) {
                                final String fullQuery = PCommon.ConcaT(bnumber, " ", cnumber);
                                Tab.AddTab(context, "k", bnumber, cnumber, 1, fullQuery);
                            }
                        }

                        builder.dismiss();
                        final int status = PCommon.TryQuitApplication(getApplicationContext());
                        if (status == 0) finishAffinity();
                    } catch (Exception ex) {
                        if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
                    }
                }, 0);
            });
            btnToolCloseTabs.setOnClickListener(v -> {
                btnToolSortTabs.setEnabled(false);
                btnToolOpenTabs.setEnabled(false);
                btnToolCloseTabs.setEnabled(false);
                btnToolImportDb.setEnabled(false);
                btnToolExportDb.setEnabled(false);

                final Handler handler = new Handler();
                handler.postDelayed(() -> {
                    builder.dismiss();
                    ShowToolCloseTabs(context);
                }, 0);
            });
            btnToolImportDb.setOnClickListener(v -> {
                btnToolSortTabs.setEnabled(false);
                btnToolOpenTabs.setEnabled(false);
                btnToolCloseTabs.setEnabled(false);
                btnToolImportDb.setEnabled(false);
                btnToolExportDb.setEnabled(false);

                final Handler handler = new Handler();
                handler.postDelayed(() -> {
                    builder.dismiss();

                    final String fullPathDir = Environment.getExternalStorageDirectory().getPath();
                    PCommon.SelectFile(MainActivity.this, fullPathDir, "", null);
                }, 0);
            });
            btnToolExportDb.setOnClickListener(v -> {
                btnToolSortTabs.setEnabled(false);
                btnToolOpenTabs.setEnabled(false);
                btnToolCloseTabs.setEnabled(false);
                btnToolImportDb.setEnabled(false);
                btnToolExportDb.setEnabled(false);

                final AlertDialog pgr = PCommon.ShowProgressDialog(MainActivity.this, R.string.btnToolExportDb, R.string.planCreating);
                final Handler handler = new Handler();
                handler.postDelayed(() -> {
                    try
                    {
                        builder.dismiss();
                        PCommon.ExportDb(context);
                    } catch (Exception ex) {
                        if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
                    } finally {
                        if (pgr != null) pgr.dismiss();
                    }
                }, 500);
            });
            builder.show();
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    private void ShowToolCloseTabs(final Context context) {
        try {
            CheckLocalInstance(this);

            if (tabLayout == null) return;

            final Typeface typeface = PCommon.GetTypeface(this);
            final int fontSize = PCommon.GetFontSize(this);

            final LayoutInflater inflater = this.getLayoutInflater();
            final View view = inflater.inflate(R.layout.fragment_tools_close_menu, this.findViewById(R.id.llToolsMenu));
            final CheckBox chkToolCloseBookTabs = view.findViewById(R.id.chkToolCloseBookTabs);
            final CheckBox chkToolCloseArtTabs = view.findViewById(R.id.chkToolCloseArtTabs);
            final CheckBox chkToolClosePrblTabs = view.findViewById(R.id.chkToolClosePrblTabs);
            final CheckBox chkToolCloseCRTabs = view.findViewById(R.id.chkToolCloseCRTabs);
            final CheckBox chkToolCloseSearchTabs = view.findViewById(R.id.chkToolCloseSearchTabs);
            final CheckBox chkToolClosePlanTabs = view.findViewById(R.id.chkToolClosePlanTabs);

            if (typeface != null) {
                chkToolCloseBookTabs.setTypeface(typeface);
                chkToolCloseArtTabs.setTypeface(typeface);
                chkToolClosePrblTabs.setTypeface(typeface);
                chkToolCloseCRTabs.setTypeface(typeface);
                chkToolCloseSearchTabs.setTypeface(typeface);
                chkToolClosePlanTabs.setTypeface(typeface);

                chkToolCloseBookTabs.setTextSize(fontSize);
                chkToolCloseArtTabs.setTextSize(fontSize);
                chkToolClosePrblTabs.setTextSize(fontSize);
                chkToolCloseCRTabs.setTextSize(fontSize);
                chkToolCloseSearchTabs.setTextSize(fontSize);
                chkToolClosePlanTabs.setTextSize(fontSize);
            }

            final AlertDialog builder = new AlertDialog.Builder(this).create();
            builder.setCancelable(true);
            builder.setTitle(R.string.mnuTools);
            builder.setView(view);

            final int tabCount = tabLayout.getTabCount();
            final Button btnToolCloseTabs = view.findViewById(R.id.btnToolCloseTabs);
            btnToolCloseTabs.setOnClickListener(v -> {
                btnToolCloseTabs.setEnabled(false);
                chkToolCloseBookTabs.setEnabled(false);
                chkToolCloseArtTabs.setEnabled(false);
                chkToolClosePrblTabs.setEnabled(false);
                chkToolCloseCRTabs.setEnabled(false);
                chkToolCloseSearchTabs.setEnabled(false);
                chkToolClosePlanTabs.setEnabled(false);

                final boolean shouldCloseBooks = chkToolCloseBookTabs.isChecked();
                final boolean shouldCloseArts = chkToolCloseArtTabs.isChecked();
                final boolean shouldClosePrbls = chkToolClosePrblTabs.isChecked();
                final boolean shouldCloseCRs = chkToolCloseCRTabs.isChecked();
                final boolean shouldCloseSearches = chkToolCloseSearchTabs.isChecked();
                final boolean shouldClosePlans = chkToolClosePlanTabs.isChecked();

                final AlertDialog pgr = PCommon.ShowProgressDialog(MainActivity.this, R.string.btnToolCloseTabs, R.string.toolCloseTabsClosing);
                final Handler handler = new Handler();
                handler.postDelayed(() -> {
                    try {
                        CheckLocalInstance(context);

                        if (tabCount <= 1) {
                            builder.dismiss();
                            if (pgr != null) pgr.dismiss();
                            return;
                        }

                        CacheTabBO ct;
                        for (int tabId = tabCount - 1; tabId >= 0; tabId--) {
                            if (tabId == 0) {
                                final int retabCount = tabLayout.getTabCount();
                                if (retabCount <= 1) continue;
                            }

                            ct = _s.GetCacheTab(tabId);
                            if (ct == null) continue;
                            else
                            {
                                if (shouldCloseArts && ct.tabType.compareToIgnoreCase("A") == 0) Tab.RemoveTabAt(context, tabId);
                                if (shouldClosePlans && ct.tabType.compareToIgnoreCase("P") == 0) Tab.RemoveTabAt(context, tabId);
                                if (shouldCloseBooks && ct.tabType.compareToIgnoreCase("S") == 0 && ct.isBook && ct.isChapter && !ct.isVerse && ct.bNumber > 0 && ct.cNumber > 0 && ct.vNumber == 0) Tab.RemoveTabAt(context, tabId);
                                if (shouldClosePrbls && ct.tabType.compareToIgnoreCase("S") == 0 && ct.isBook && !ct.isChapter && ct.bNumber > 0 && ct.cNumber > 0 && ct.tabTitle.matches("PRBL.*")) Tab.RemoveTabAt(context, tabId);
                                if (shouldCloseCRs && ct.tabType.compareToIgnoreCase("S") == 0 && ct.isBook && !ct.isChapter && ct.bNumber > 0 && ct.cNumber > 0 && ct.tabTitle.matches("CR")) Tab.RemoveTabAt(context, tabId);
                                if (shouldCloseSearches && ct.tabType.compareToIgnoreCase("S") == 0 && (
                                        //Range
                                        (ct.isBook && !ct.isChapter && ct.bNumber > 0 && ct.cNumber > 0 && !ct.tabTitle.matches("CR") && !ct.tabTitle.matches("PRBL.*")) ||
                                        //Verse
                                        (ct.isBook && ct.isChapter && ct.isVerse && ct.bNumber > 0 && ct.cNumber > 0 && ct.vNumber > 0) ||
                                        //Search in book
                                        (ct.isBook && !ct.isChapter && ct.bNumber > 0 && ct.cNumber == 0 && !ct.tabTitle.matches("CR") && !ct.tabTitle.matches("PRBL.*")) ||
                                        //Search expr
                                        (ct.isBook && !ct.isChapter && !ct.isVerse && ct.bNumber == 0 && ct.cNumber == 0 && ct.vNumber == 0 && !ct.tabTitle.matches("CR") && !ct.tabTitle.matches("PRBL.*"))
                                    )
                                ) Tab.RemoveTabAt(context, tabId);
                            }
                        }

                        PCommon.SavePref(context, IProject.APP_PREF_KEY.TAB_SELECTED, "0");
                        Tab.SelectTabByTabId(0);

                        builder.dismiss();
                        pgr.dismiss();
                    } catch (Exception ex) {
                        if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
                    }
                }, 500);
            });
            builder.show();
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    private void ShowToolSortTabs(final Context context)
    {
        try
        {
            CheckLocalInstance(this);

            if (tabLayout == null) return;

            final boolean areSorted = _s.SortAllCache();
            if (areSorted)
                Create(true);
            else {
                if (PCommon._isDebug) PCommon.ShowToast(context, R.string.toastOperationFailure, Toast.LENGTH_LONG);
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    @SuppressLint("SetTextI18n")
    private void ShowAbout(final Context context) {
        try {
            final AlertDialog builder = new AlertDialog.Builder(context).create();                  //R.style.DialogStyleKaki
            builder.setTitle(R.string.mnuAbout);
            builder.setCancelable(true);

            final Typeface typeface = PCommon.GetTypeface(this);
            final int fontSize = PCommon.GetFontSize(this);

            final ScrollView sv = new ScrollView(context);
            sv.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            sv.setSmoothScrollingEnabled(false);
            sv.setPadding(20, 20, 20, 20);

            final LinearLayout llSv = new LinearLayout(context);
            llSv.setOrientation(LinearLayout.VERTICAL);
            llSv.setLayoutParams(PCommon._layoutParamsMatch);
            llSv.setPadding(10, 10, 10, 10);
            llSv.setVerticalGravity(Gravity.CENTER_VERTICAL);
            llSv.setHorizontalGravity(Gravity.CENTER_HORIZONTAL);
            sv.addView(llSv);

            final PackageInfo pi = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            final int dbVersion = _s.GetDbVersion();
            final String app = PCommon.ConcaT("Bible Multi\n", getString(R.string.appName));
            final String devEmail = context.getString(R.string.devEmail).replaceAll("r", "");
            final String aboutDev = PCommon.ConcaT(app, "\n", pi.versionName, " (", dbVersion, ") ", pi.versionCode, "\n@", devEmail.split("@")[0], "\n");
            final String aboutContent = PCommon.ConcaT(context.getString(R.string.aboutContactMe));
            final Button btnDebug = new Button(context);
            btnDebug.setVisibility(PCommon._isDebug ? View.VISIBLE : View.GONE);
            btnDebug.setLayoutParams(PCommon._layoutParamsWrap);
            btnDebug.setText(R.string.mnuDebug);
            btnDebug.setOnClickListener(v -> ShowDebug(v.getContext()));
            btnDebug.setFocusable(true);
            btnDebug.setBackground(PCommon.GetDrawable(context, R.drawable.focus_button));

            //---
            final ImageView iv = new ImageView(context);
            iv.setMaxWidth(100);
            iv.setMaxHeight(100);
            iv.setImageResource(R.drawable.thelightlogo);
            iv.setAdjustViewBounds(true);
            iv.setOnClickListener(v -> {
                PCommon._debugCounter++;
                if (PCommon._debugCounter == 7) {
                    PCommon._debugCounter = 0;
                    PCommon._isDebug = !PCommon._isDebug;
                    btnDebug.setVisibility(PCommon._isDebug ? View.VISIBLE : View.GONE);
                    final String debugStatus = PCommon._isDebug ? "ON" : "OFF";
                    PCommon.ShowToast(iv.getContext(), PCommon.ConcaT("* Debug: ", debugStatus, " *"), Toast.LENGTH_SHORT);
                }
            });
            llSv.addView(iv);

            //---
            final TextView tvDev = new TextView(context);
            tvDev.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            tvDev.setPadding(0, 5, 0, 0);
            tvDev.setText(aboutDev);
            tvDev.setGravity(Gravity.CENTER_HORIZONTAL);
            tvDev.setCursorVisible(true);
            if (typeface != null) tvDev.setTypeface(typeface);
            tvDev.setTextSize(fontSize);
            tvDev.setFocusable(false);
            llSv.addView(tvDev);

            //---
            final TextView tvContent = new TextView(context);
            tvContent.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            tvContent.setPadding(0, 5, 0, 5);
            tvContent.setText(aboutContent);
            tvContent.setGravity(Gravity.CENTER_HORIZONTAL);
            tvContent.setCursorVisible(true);
            if (typeface != null) tvContent.setTypeface(typeface);
            tvContent.setTextSize(fontSize);
            tvContent.setFocusable(true);
            tvContent.setBackground(PCommon.GetDrawable(context, R.drawable.focus_text));
            llSv.addView(tvContent);

            //---
            final TextView tv7 = new TextView(context);
            tv7.setFocusable(false);
            llSv.addView(tv7);

            //---
            final TextView tvGlory = new TextView(context);
            tvGlory.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            tvGlory.setPadding(0, 5, 0, 5);
            tvGlory.setText("All The Glory To God.");
            tvGlory.setGravity(Gravity.CENTER_HORIZONTAL);
            tvGlory.setCursorVisible(true);
            if (typeface != null) tvGlory.setTypeface(typeface);
            tvGlory.setTextSize(fontSize);
            tvGlory.setFocusable(true);
            tvGlory.setBackground(PCommon.GetDrawable(context, R.drawable.focus_text));
            llSv.addView(tvGlory);

            //---
            final TextView tv1 = new TextView(context);
            tv1.setFocusable(false);
            llSv.addView(tv1);

            //---
            llSv.addView(btnDebug);

            //---
            final TextView tv1b = new TextView(context);
            tv1b.setFocusable(false);
            llSv.addView(tv1b);

            //---
            final Button btnWebSite = new Button(context);
            btnWebSite.setLayoutParams(PCommon._layoutParamsWrap);
            btnWebSite.setText(R.string.sendWebSite);
            btnWebSite.setOnClickListener(v -> PCommon.OpenUrl(v.getContext(), "XXhXtXtXpXsX:XX/X/XhXotliXttleXwhiXXtXedXogX.XXgXitlXabX.iXoX/XXXbXibXlXemultXiX".replaceAll("X", "")));
            btnWebSite.setFocusable(true);
            btnWebSite.setBackground(PCommon.GetDrawable(context, R.drawable.focus_button));
            llSv.addView(btnWebSite);

            //---
            final TextView tv97 = new TextView(context);
            tv97.setFocusable(false);
            llSv.addView(tv97);

            //---
            final Button btnTw = new Button(context);
            btnTw.setLayoutParams(PCommon._layoutParamsWrap);
            btnTw.setText(R.string.btnTw);
            btnTw.setOnClickListener(v -> PCommon.OpenUrl(v.getContext(), "XXhXtXtXXpXsX:XX/XXX/XtXXwXiXXtXtXeXrXX.XcXXXoXmX/_XXhXlXwdX".replaceAll("X", "")));
            btnTw.setFocusable(true);
            btnTw.setBackground(PCommon.GetDrawable(context, R.drawable.focus_button));
            llSv.addView(btnTw);

            //---
            final TextView tv98 = new TextView(context);
            tv98.setFocusable(false);
            llSv.addView(tv98);

            //---
            final Button btnTl = new Button(context);
            btnTl.setLayoutParams(PCommon._layoutParamsWrap);
            btnTl.setText(R.string.btnTl);
            btnTl.setOnClickListener(v -> PCommon.OpenUrl(v.getContext(), "XhXtXtXpXsXX:X/XXX/wXwwXX.XXtX.XmXXeXXX/XbXiXXbXlXemXXuXlXXtXXiXtXXXhXeXXliXgXXhXtX".replaceAll("X", "")));
            btnTl.setFocusable(true);
            btnTl.setBackground(PCommon.GetDrawable(context, R.drawable.focus_button));
            llSv.addView(btnTl);

            //---
            final TextView tv99 = new TextView(context);
            tv99.setFocusable(false);
            llSv.addView(tv99);

            //---
            final Button btnEmail = new Button(context);
            btnEmail.setLayoutParams(PCommon._layoutParamsWrap);
            btnEmail.setText(R.string.sendEmail);
            btnEmail.setOnClickListener(v -> PCommon.SendEmail(context,
                    new String[]{devEmail},
                    app,
                    ""));
            btnEmail.setFocusable(true);
            btnEmail.setBackground(PCommon.GetDrawable(context, R.drawable.focus_button));
            llSv.addView(btnEmail);

            //---
            final TextView tv4 = new TextView(context);
            tv4.setFocusable(false);
            llSv.addView(tv4);

            //---
            final Button btnGitlab = new Button(context);
            btnGitlab.setLayoutParams(PCommon._layoutParamsWrap);
            btnGitlab.setText(R.string.btnGitlab);
            btnGitlab.setOnClickListener(v -> PCommon.OpenUrl(v.getContext(), "XhXttXpXsX:X/X/XXgXitXlabX.XcoXXmX/XhXotlXitXtlewXhitXXedoXgX/XBXibXlXeXXXMXultiXThXeLXighXtX/XiXssXueXs".replaceAll("X", "")));
            btnGitlab.setFocusable(true);
            btnGitlab.setBackground(PCommon.GetDrawable(context, R.drawable.focus_button));
            llSv.addView(btnGitlab);

            //---
            final TextView tv2 = new TextView(context);
            tv2.setFocusable(false);
            llSv.addView(tv2);

            //---
            final Button btnXda = new Button(context);
            btnXda.setLayoutParams(PCommon._layoutParamsWrap);
            btnXda.setText(R.string.btnXda);
            btnXda.setOnClickListener(v -> PCommon.OpenUrl(v.getContext(), "ZhZtZtZpZsZ:Z/Z/fZorZuZmZ.ZxZdZa-ZdZeZvZZZZeZloZpZerZsZ.ZcZoZmZ/ZtZ/ZapZpZ-Z5-0-ZbiZbZlZe-ZmuZlZtZi-tZhZe-lZiZgZhZtZ-oZpZen-ZsouZrcZe.Z4033Z799".replace("Z", "")));
            btnXda.setFocusable(true);
            btnXda.setBackground(PCommon.GetDrawable(context, R.drawable.focus_button));
            llSv.addView(btnXda);

            //---
            final TextView tv5 = new TextView(context);
            tv5.setFocusable(false);
            llSv.addView(tv5);

            //---
            final Button btnFb = new Button(context);
            btnFb.setLayoutParams(PCommon._layoutParamsWrap);
            btnFb.setText(R.string.btnFb);
            btnFb.setOnClickListener(v -> PCommon.OpenUrl(v.getContext(), "XhXtXtXpXsXX:X/XXX/wXwwXX.fXaXXXcXeXXXboXok.XXcXoXXXmX/XBXiXblXXeXMXXuXlXXtiXTXXheXXLXXiXgXhtX".replaceAll("X", "")));
            btnFb.setFocusable(true);
            btnFb.setBackground(PCommon.GetDrawable(context, R.drawable.focus_button));
            llSv.addView(btnFb);

            //---
            final TextView tv6 = new TextView(context);
            tv6.setFocusable(false);
            llSv.addView(tv6);

            builder.setView(sv);
            builder.show();
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    private void ShowDebug(final Context context)
    {
        try
        {
            CheckLocalInstance(context);

            final Typeface typeface = PCommon.GetTypeface(context);
            final int fontSize = PCommon.GetFontSize(context);

            final LayoutInflater inflater = getLayoutInflater();
            final View svLog = inflater.inflate(R.layout.fragment_debug, findViewById(R.id.svLog));
            final LinearLayout llLog = svLog.findViewById(R.id.llLog);
            final Button btnLogClear = svLog.findViewById(R.id.btnLogClear);
            btnLogClear.setOnClickListener(v -> {
                _s.DeleteAllLogs(true);
                llLog.removeAllViews();
            });

            final AlertDialog builder = new AlertDialog.Builder(context).create();
            builder.setCancelable(true);
            builder.setTitle(R.string.mnuDebug);
            builder.setView(svLog);

            TextView tvLog;
            final ArrayList<String> lstLog = _s.GetListAllLog();
            for (String log : lstLog)
            {
                tvLog = new TextView(context);
                tvLog.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                tvLog.setFocusable(true);
                tvLog.setBackground(PCommon.GetDrawable(context, R.drawable.focus_text));
                tvLog.setText(log);
                if (typeface != null) tvLog.setTypeface(typeface);
                tvLog.setTextSize(fontSize);

                llLog.addView(tvLog);
            }

            builder.show();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    private void InviteFriend() {
        try {
            final String text1 = PCommon.ConcaT(getString(R.string.inviteFriendClipboardMsg1), getString(R.string.appUrls1), getString(R.string.inviteFriendClipboardMsg2));
            final String text2 = PCommon.ConcaT(getString(R.string.inviteFriendClipboardMsg1), getString(R.string.appUrls2), getString(R.string.inviteFriendClipboardMsg2));
            PCommon.CopyTextToClipboard(getApplicationContext(), "", PCommon.ConcaT(text1, "\n\n---\n\n", text2), true);
            PCommon.ShowDialog(MainActivity.this, R.string.mnuInvite, true, null, R.string.inviteFriendDlgMsg);
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    static class Tab {
        @SuppressLint("StaticFieldLeak")
        static SCommon _s = null;
        private static boolean isThemeWhite;

        static Boolean IsThemeWhite() {
            return isThemeWhite;
        }

        static void SetCurrentTabTitle(final String title) {
            //noinspection EmptyCatchBlock
            try {
                final int tabId = GetCurrentTabPosition();
                if (tabId < 0)
                    return;

                //noinspection ConstantConditions
                tabLayout.getTabAt(tabId).setText(title);
                PCommon.SavePref(tabLayout.getContext(), IProject.APP_PREF_KEY.TAB_SELECTED, Integer.toString(tabId));
            } catch (Exception ex) {
            }
        }

        static int GetCurrentTabPosition() {
            if (tabLayout == null)
                return -1;

            final int tabSelected = tabLayout.getSelectedTabPosition();
            if (tabSelected < 0)
                return -1;

            return tabSelected;
        }

        static void SelectTabByTabId(final int tabId) {
            if (tabId >= 0) {
                tabLayout.post(() -> {
                    //noinspection EmptyCatchBlock
                    try {
                        //noinspection ConstantConditions
                        tabLayout.getTabAt(tabId).select();
                    } catch (Exception ex) {
                    }
                });
            }
        }

        static int GetTabCount() {
            if (tabLayout == null)
                return -1;

            return tabLayout.getTabCount();
        }

        /***
         * Add tab for Open chapter SEARCH_TYPE
         * @param context
         * @param tbbName
         * @param bNumber
         * @param cNumber
         * @param fullQuery
         * @param vNumber
         */
        @SuppressWarnings("JavaDoc")
        static void AddTab(final Context context, final String tbbName, final int bNumber, final int cNumber, final String fullQuery, final int vNumber) {
            try {
                if (tabLayout == null)
                    return;

                CheckLocalInstance(context);

                final int tabNumber = tabLayout.getTabCount();
                final String bbname = tbbName.substring(0, 1);
                final int pos = (vNumber - 1) * tbbName.length();
                final CacheTabBO t = new CacheTabBO(tabNumber, "S", context.getString(R.string.tabTitleDefault), fullQuery, pos, bbname, true, true, false, bNumber, cNumber, 0, tbbName);
                _s.SaveCacheTab(t);

                final TabLayout.Tab tab = tabLayout.newTab().setText(R.string.tabTitleDefault);
                tabLayout.addTab(tab);
                FullScrollTab(context, HorizontalScrollView.FOCUS_RIGHT);
            } catch (Exception ex) {
                if (PCommon._isDebug) PCommon.LogR(context, ex);
            }
        }

        /***
         * Add tab for Open Cross references
         * @param context
         * @param tbbName
         * @param bNumber
         * @param cNumber
         * @param fullQuery
         * @param vNumber
         */
        @SuppressWarnings("JavaDoc")
        static void AddTab(final Context context, final String tbbName, final int bNumber, final int cNumber, final int vNumber, final String fullQuery) {
            try {
                if (tabLayout == null)
                    return;

                CheckLocalInstance(context);

                final int tabNumber = tabLayout.getTabCount();
                final String bbname = tbbName.substring(0, 1);
                final int pos = 0;
                final CacheTabBO t = new CacheTabBO(tabNumber, "S", context.getString(R.string.tabTitleDefault), fullQuery, pos, bbname, true, false, false, bNumber, cNumber, vNumber, tbbName);
                _s.SaveCacheTab(t);

                final TabLayout.Tab tab = tabLayout.newTab().setText(R.string.tabTitleDefault);
                tabLayout.addTab(tab);
                FullScrollTab(context, HorizontalScrollView.FOCUS_RIGHT);
            } catch (Exception ex) {
                if (PCommon._isDebug) PCommon.LogR(context, ex);
            }
        }

        /***
         * Add tab for Open result PRBL | ARTICLE | PLAN | INTENT | HGNr
         * @param context
         * @param cacheTabType
         * @param tbbName
         * @param fullQuery bNumber, cNumber, vNumberFrom, vNumberTo OR HGNr
         * @param shouldFullScroll
         */
        @SuppressWarnings("JavaDoc")
        static void AddTab(final Context context, final String cacheTabType, final String tbbName, final String fullQuery, final boolean shouldFullScroll) {
            try {
                if (tabLayout == null)
                    return;

                CheckLocalInstance(context);

                final String bbname = tbbName.substring(0, 1);
                final int tabNumber = tabLayout.getTabCount();
                final String tabTitle;
                final CacheTabBO t;
                if (cacheTabType.equalsIgnoreCase("A")) {
                    final String resString;
                    final int tabNameSize = Integer.parseInt(context.getString(R.string.tabSizeName));

                    if (fullQuery.startsWith("ART")) {
                        final int resId = PCommon.GetResId(context, fullQuery);
                        resString = context.getString(resId);
                    } else {
                        final String myArtPrefix = context.getString(R.string.tabMyArtPrefix);
                        resString = PCommon.ConcaT(myArtPrefix, fullQuery);
                    }

                    tabTitle = (resString.length() <= tabNameSize) ? resString : fullQuery;
                    t = new CacheTabBO(tabNumber, cacheTabType, tabTitle, fullQuery, 0, bbname, true, false, false, 0, 0, 0, tbbName);
                    _s.SaveCacheTab(t);
                } else if (cacheTabType.equalsIgnoreCase("P")) {
                    final String[] cols = fullQuery.split("\\s");
                    if (cols.length != 3) return;
                    final int planId = Integer.parseInt(cols[0]);
                    final PlanDescBO pd = _s.GetPlanDesc(planId);
                    if (pd == null) return;
                    final int resId = PCommon.GetResId(context, pd.planRef);
                    tabTitle = context.getString(resId);
                    t = new CacheTabBO(tabNumber, cacheTabType, tabTitle, fullQuery, 0, bbname, true, false, false, 0, 0, 0, tbbName);
                    _s.SaveCacheTab(t);

                    final int planDayNumber = Integer.parseInt(cols[1]);
                    final PlanCalBO pc = _s.GetPlanCalByDay(bbname, planId, planDayNumber);
                    final int bNumberStart = pc.bNumberStart, cNumberStart = pc.cNumberStart, vNumberStart = pc.vNumberStart;
                    final int bNumberEnd = pc.bNumberEnd, cNumberEnd = pc.cNumberEnd, vNumberEnd = pc.vNumberEnd;
                    final int tabIdTo = MainActivity.Tab.GetTabCount();
                    final boolean copy = _s.CopyCacheSearchForOtherBible(tabIdTo, tbbName, bNumberStart, cNumberStart, vNumberStart, bNumberEnd, cNumberEnd, vNumberEnd);
                    if (!copy) return;
                } else if (cacheTabType.equalsIgnoreCase("SL")) {
                    tabTitle = fullQuery;
                    t = new CacheTabBO(tabNumber, "S", tabTitle, fullQuery, 0, bbname, true, false, false, 0, 0, 0, tbbName);
                    _s.SaveCacheTab(t);
                    _s.AddAllLexRefToCacheSearch(tbbName, fullQuery, t.tabNumber);
                } else {
                    tabTitle = context.getString(R.string.tabTitleDefault);
                    t = new CacheTabBO(tabNumber, cacheTabType, tabTitle, fullQuery, 0, bbname, true, false, false, 0, 0, 0, tbbName);
                    _s.SaveCacheTab(t);
                }
                final TabLayout.Tab tab = tabLayout.newTab().setText(tabTitle);
                tabLayout.addTab(tab);
                if (shouldFullScroll) FullScrollTab(context, HorizontalScrollView.FOCUS_RIGHT);
            } catch (Exception ex) {
                if (PCommon._isDebug) PCommon.LogR(context, ex);
            }
        }

        /***
         * Add tab for Open result SEARCH_TYPE
         * @param context
         * @param tabNumberFrom
         * @param bbNameTo
         * @param vNumber
         */
        @SuppressWarnings("JavaDoc")
        static void AddTab(final Context context, final int tabNumberFrom, final String bbNameTo, final int vNumber) {
            try {
                if (tabLayout == null)
                    return;

                CheckLocalInstance(context);

                final int pos = (vNumber - 1) * bbNameTo.length();
                final int tabNumberTo = tabLayout.getTabCount();
                final CacheTabBO t = _s.GetCacheTab(tabNumberFrom);
                t.tabType = "S";
                t.tabNumber = tabNumberTo;
                t.bbName = bbNameTo.substring(0, 1);
                t.scrollPosY = pos;
                t.isBook = true;
                t.isChapter = false;
                t.isVerse = false;
                t.trad = bbNameTo;
                _s.SaveCacheTab(t);
                _s.CopyCacheSearchForOtherBible(tabNumberFrom, tabNumberTo, bbNameTo);

                final TabLayout.Tab tab = tabLayout.newTab().setText(R.string.tabTitleDefault);
                tabLayout.addTab(tab);
                FullScrollTab(context, HorizontalScrollView.FOCUS_RIGHT);
            } catch (Exception ex) {
                if (PCommon._isDebug) PCommon.LogR(context, ex);
            }
        }

        static void RemoveCurrentTab(final Context context) {
            try {
                if (tabLayout == null)
                    return;

                CheckLocalInstance(context);

                final int tabNumberToRemove = tabLayout.getSelectedTabPosition();

                Tab.RemoveTabAt(context, tabNumberToRemove);
            } catch (Exception ex) {
                if (PCommon._isDebug) PCommon.LogR(context, ex);
            }
        }

        static void RemoveTabAt(final Context context, final int tabNumberToRemove) {
            try {
                if (tabLayout == null)
                    return;

                CheckLocalInstance(context);

                final int tabCount = tabLayout.getTabCount();
                if (tabCount <= 1)
                    return;

                _s.DeleteCache(tabNumberToRemove);

                int toTabId;
                for (int fromTabId = tabNumberToRemove + 1; fromTabId < tabCount; fromTabId++) {
                    toTabId = fromTabId - 1;

                    _s.UpdateCacheId(fromTabId, toTabId);
                }

                //Finally
                tabLayout.removeTabAt(tabNumberToRemove);
            } catch (Exception ex) {
                if (PCommon._isDebug) PCommon.LogR(context, ex);
            }
        }

        private static void CheckLocalInstance(final Context context) {
            try {
                if (_s == null) {
                    _s = SCommon.GetInstance(context);
                }
            } catch (Exception ex) {
                if (PCommon._isDebug) PCommon.LogR(context, ex);
            }
        }

        private static void FullScrollTab(final Context context, final int direction) {
            try {
                tabLayout.post(() -> {
                    //noinspection EmptyCatchBlock
                    try {
                        if (direction == HorizontalScrollView.FOCUS_RIGHT) {
                            final int tabId = tabLayout.getTabCount() - 1;
                            //noinspection ConstantConditions
                            tabLayout.getTabAt(tabId).select();
                        } else if (direction == HorizontalScrollView.FOCUS_LEFT) {
                            //noinspection ConstantConditions
                            tabLayout.getTabAt(0).select();
                        }
                        tabLayout.fullScroll(direction);
                    } catch (Exception ex) {
                    }
                });
            } catch (Exception ex) {
                if (PCommon._isDebug) PCommon.LogR(context, ex);
            }
        }

        private static void LongPress(final Context context) {
            tabLayout.post(() -> {
                try {
                    final View vw = tabLayout.getChildAt(0);
                    final int count = ((LinearLayout) vw).getChildCount();
                    for (int i = 0; i < count; i++) {
                        final int index = i;
                        ((LinearLayout) vw).getChildAt(index).setOnLongClickListener(v -> {
                            //noinspection EmptyCatchBlock
                            try {
                                Tab.RemoveTabAt(context, index);
                                final int tabSelect = (index == 0) ? 0 : index - 1;
                                //noinspection ConstantConditions
                                tabLayout.getTabAt(tabSelect).select();
                                //Focus correction
                                for (int j = 0; j <= tabSelect; j++) {
                                    tabLayout.arrowScroll(HorizontalScrollView.FOCUS_RIGHT);
                                }
                            } catch (Exception ex) {
                            }

                            return true;
                        });
                    }
                } catch (Exception ex) {
                    if (PCommon._isDebug) PCommon.LogR(context, ex);
                }
            });
        }

        /***
         * Save ScrollPosY
         */
        static void SaveScrollPosY(final Context context) {
            try {
                CheckLocalInstance(context);

                CacheTabBO t = _s.GetCurrentCacheTab();
                if (t == null) return;

                t.scrollPosY = SearchFragment.GetScrollPosY();
                _s.SaveCacheTab(t);
            } catch (Exception ex) {
                if (PCommon._isDebug) PCommon.LogR(context, ex);
            }
        }
    }

    private void ReloadFavTabTv(final Context context, final String tbbName, final int bNumber, final int cNumber, final String fullQuery, @SuppressWarnings("SameParameterValue") final int vNumber) {
        try {
            if (tabLayout == null)
                return;

            CheckLocalInstance(context);

            final int tabNumber = 0;
            final String bbname = tbbName.substring(0, 1);
            final int pos = 0;
            final CacheTabBO t = new CacheTabBO(tabNumber, "F", context.getString(R.string.tabTitleDefault), fullQuery, pos, bbname, false, false, false, bNumber, cNumber, vNumber, tbbName);
            _s.SaveCacheTab(t);

            final FragmentManager fm = getSupportFragmentManager();
            final FragmentTransaction ft = fm.beginTransaction();
            final Fragment frag = new SearchFragment(SearchFragment.FRAGMENT_TYPE.FAV_TYPE);
            ft.replace(R.id.content_frame, frag, Integer.toString(tabNumber));
            /* Gives exceptions in Android Q
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            */
            ft.commit();
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    private void Slide(final boolean showMnu) {
        if (slideViewMenu == null) return;

        if (showMnu)
        {
            final int INSTALL_STATUS = PCommon.GetInstallStatus(getApplicationContext());
            if (INSTALL_STATUS != PCommon.GetInstallStatusShouldBe()) {
                PCommon.ShowToast(getApplicationContext(), R.string.installQuit, Toast.LENGTH_SHORT);
                return;
            }
        }

        final int mnuTvVisibility = showMnu ? View.VISIBLE : View.GONE;
        final int tabVisibility = showMnu ? View.GONE : View.VISIBLE;

        slideViewMenuTop.setVisibility(mnuTvVisibility);
        slideViewMenu.setVisibility(mnuTvVisibility);
        slideViewTabHandleMain.setVisibility(tabVisibility);
        slideViewTab.setVisibility(tabVisibility);

        if (showMnu) {
            slideViewMenuHandle.requestFocus();
        } else {
            slideViewTabHandle.requestFocus();
        }
    }
/*
final TranslateAnimation animate = new TranslateAnimation(
        0,           // fromXDelta
        -1 * slideViewMenu.getWidth(),          // toXDelta
        0,           // fromYDelta
        0);            // toYDelta
animate.setDuration(500);
animate.setFillAfter(true);
slideViewMenu.startAnimation(animate);
*/

    /***
     * Search dialog
     * @param context
     * @param isSearchBible True for normal search, False for fav
     */
    void SearchDialog(final Context context, final boolean isSearchBible)
    {
        //TODO FAB: cancel not working
        try {
            final int searchFullQueryLimit = isSearchBible ? PCommon.GetSearchFullQueryLimit() : 0;
            final int INSTALL_STATUS = PCommon.GetInstallStatus(context);
            if (INSTALL_STATUS < 1) return;

            final String bibleAppType = PCommon.GetPrefBibleAppType(context);
            final Typeface typeface = PCommon.GetTypeface(context);
            final int fontSize = PCommon.GetFontSize(context);
            final String favSearchText;
            if (isSearchBible) {
                favSearchText = "";
            } else {
                CheckLocalInstance(context);

                final CacheTabBO favCacheTabBO = _s.GetCacheTabFav();
                favSearchText = (favCacheTabBO == null) ? "" : (favCacheTabBO.fullQuery == null) ? "" : favCacheTabBO.fullQuery;
            }

            final String bbname = PCommon.GetPrefBibleName(context);
            final boolean isSpecialCall = bbname.equalsIgnoreCase("y") || bbname.equalsIgnoreCase("w"); //NOT USED
            final AlertDialog builderText = new AlertDialog.Builder(context).create();
            final LayoutInflater inflater = getLayoutInflater();
            final View vw = inflater.inflate(R.layout.fragment_search_dialog, findViewById(R.id.clSearch));
            final EditText etSearchText = vw.findViewById(R.id.etSearchText);
            etSearchText.setText(favSearchText);
            etSearchText.setTextSize(fontSize);
            if (typeface != null) etSearchText.setTypeface(typeface);
            final NumberPicker npSearchLanguage = vw.findViewById(R.id.npSearchLanguage);
            if (bibleAppType.compareTo("1") == 0) npSearchLanguage.setVisibility(View.GONE);
            final Button btnFavFilter = vw.findViewById(R.id.btnFavFilter);
            final Button btnFavOrder = vw.findViewById(R.id.btnFavOrder);
            final String searchTextHintClassic = PCommon.ConcaT("<i>", getString(isSearchBible ? R.string.searchBibleHint : R.string.mnuSearchAll),  "</i>");
            etSearchText.setHint(Html.fromHtml(searchTextHintClassic));

            if (!isSearchBible) {
                final TreeMap<Integer, BookmarkBO> mapBm = _s.GenerateBookmarkCompleteMap(context, R.string.favHeader, R.string.itemAll);
                npSearchLanguage.setVisibility(View.GONE);
                final int filterBy = PCommon.GetFavFilter(context);
                final String filterByText = PCommon.ConcaT(Objects.requireNonNull(mapBm.get(filterBy)).bmCurrent, " ", Objects.requireNonNull(mapBm.get(filterBy)).bmDesc);
                btnFavFilter.setText(filterByText);
                btnFavFilter.setOnClickListener(v -> {
                    //Construct all fav here
                    final ArrayList<Integer> lstId = _s.GetListAllBookmarkId();
                    final ArrayList<String> mnuFav = new ArrayList<String>();
                    for(Integer bmId : mapBm.keySet()) {
                        mnuFav.add(Objects.requireNonNull(mapBm.get(bmId)).bmRepresentation);
                    }

                    final AlertDialog builderFavMenu = new AlertDialog.Builder(context).create();
                    PCommon.ShowMenu(builderFavMenu, R.string.mnuFav, mnuFav, lstId);
                    builderFavMenu.setOnDismissListener(dialogInterface -> {
                        final String mnu_dialog = PCommon.GetPref(context, IProject.APP_PREF_KEY.MENU_DIALOG, "");
                        if (mnu_dialog.isEmpty()) return;
                        final int mark = Integer.parseInt(mnu_dialog);
                        btnFavFilter.setText(Objects.requireNonNull(mapBm.get(mark)).bmRepresentation);
                        final String searchTextHintCustom = PCommon.ConcaT("<i>", context.getString(R.string.mnuSearchAll), " ", Objects.requireNonNull(mapBm.get(mark)).bmRepresentation, "</i>");
                        etSearchText.setHint(Html.fromHtml(searchTextHintCustom));
                        PCommon.SavePrefInt(context, IProject.APP_PREF_KEY.FAV_FILTER, mark);
                    });
                });
                final int orderBy = PCommon.GetFavOrder(context);
                btnFavOrder.setText(context.getString(orderBy == 1 ? R.string.favOrder1Short : R.string.favOrder2Short));
                btnFavOrder.setOnClickListener(v -> {
                    final int orderBy1 = PCommon.GetFavOrder(v.getContext()) == 1 ? 2 : 1;
                    btnFavOrder.setText(v.getContext().getString(orderBy1 == 1 ? R.string.favOrder1Short : R.string.favOrder2Short));
                    PCommon.SavePrefInt(v.getContext(), IProject.APP_PREF_KEY.FAV_ORDER, orderBy1);
                });
                final String searchTextHintCustom = PCommon.ConcaT("<i>", context.getString(R.string.mnuSearchAll), " ", filterByText, "</i>");
                etSearchText.setHint(Html.fromHtml(searchTextHintCustom));
            } else {
                btnFavFilter.setVisibility(View.GONE);
                btnFavOrder.setVisibility(View.GONE);
            }

            final String[] npLanguageValues = new String[]{getString(R.string.languageEn).replace("\n", " "), getString(R.string.languageEn2).replace("\n", " "), getString(R.string.languageEs).replace("\n", " "), getString(R.string.languageEs2).replace("\n", " "), getString(R.string.languagePt).replace("\n", " "), getString(R.string.languageFrSegond).replace("\n", " "), getString(R.string.languageFrOstervald).replace("\n", " "), getString(R.string.languageIt).replace("\n", " "), getString(R.string.languageIt2).replace("\n", " "), getString(R.string.languageIn).replace("\n", " "), getString(R.string.languageBd).replace("\n", " "), getString(R.string.languageAr).replace("\n", " "), getString(R.string.languageCn).replace("\n", " "), getString(R.string.languageDeElberfelder).replace("\n", " "), getString(R.string.languageDeSchlachter).replace("\n", " "), getString(R.string.languageJp).replace("\n", " "), getString(R.string.languageRo).replace("\n", " "), getString(R.string.languagePl).replace("\n", " "), getString(R.string.languageRu).replace("\n", " "), getString(R.string.languageTr).replace("\n", " "), getString(R.string.languageSw).replace("\n", " "), getString(R.string.languageHeEl).replace("\n", " ")};
            npSearchLanguage.setDisplayedValues(npLanguageValues);
            npSearchLanguage.setMinValue(1);
            npSearchLanguage.setMaxValue(PCommon.GetInstallStatusShouldBe());
            npSearchLanguage.setValue(bbname.equalsIgnoreCase("k")
                ? 1
                : bbname.equalsIgnoreCase("2")
                    ? 2
                    : bbname.equalsIgnoreCase("v")
                        ? 3
                        : bbname.equalsIgnoreCase("9")
                            ? 4
                            : bbname.equalsIgnoreCase("a")
                                ? 5
                                : bbname.equalsIgnoreCase("l")
                                    ? 6
                                    : bbname.equalsIgnoreCase("o")
                                        ? 7
                                        : bbname.equalsIgnoreCase("d")
                                            ? 8
                                            : bbname.equalsIgnoreCase("1")
                                                ? 9
                                                : bbname.equalsIgnoreCase("i")
                                                    ? 10
                                                    : bbname.equalsIgnoreCase("b")
                                                        ? 11
                                                        : bbname.equalsIgnoreCase("y")
                                                            ? 12
                                                            : bbname.equalsIgnoreCase("c")
                                                                ? 13
                                                                : bbname.equalsIgnoreCase("e")
                                                                    ? 14
                                                                    : bbname.equalsIgnoreCase("s")
                                                                        ? 15
                                                                        : bbname.equalsIgnoreCase("j")
                                                                            ? 16
                                                                            : bbname.equalsIgnoreCase("u")
                                                                                ? 17
                                                                                : bbname.equalsIgnoreCase("z")
                                                                                    ? 18
                                                                                    : bbname.equalsIgnoreCase("r")
                                                                                        ? 19
                                                                                        : bbname.equalsIgnoreCase("t")
                                                                                            ? 20
                                                                                            : bbname.equalsIgnoreCase("h")
                                                                                                ? 21
                                                                                                : 22);
            npSearchLanguage.setOnClickListener(v -> etSearchText.requestFocus());
            final Button btnSearchContinue = vw.findViewById(R.id.btnSearchContinue);
            btnSearchContinue.setOnClickListener(v -> {
                etSearchText.setText(etSearchText.getText().toString().replaceAll("\n", "").replaceAll("%+", "%"));
                if (etSearchText.getText().toString().length() < searchFullQueryLimit) {
                    PCommon.ShowToast(v.getContext(), isSearchBible ? R.string.toastMin3 : R.string.toastEmpty3, Toast.LENGTH_SHORT);
                    return;
                }
                builderText.dismiss();
            });
            final Button btnSearchClear = vw.findViewById(R.id.btnSearchClear);
            btnSearchClear.setOnClickListener(v -> {
                etSearchText.setText("");
                etSearchText.requestFocus();
            });
            builderText.setOnDismissListener(dialog -> {
                try {
                    final String bbname1 = (npSearchLanguage.getValue() == 1)
                        ? "k"
                        : (npSearchLanguage.getValue() == 2)
                            ? "2"
                            : (npSearchLanguage.getValue() == 3)
                                ? "v"
                                : (npSearchLanguage.getValue() == 4)
                                    ? "9"
                                    : (npSearchLanguage.getValue() == 5)
                                        ? "a"
                                        : (npSearchLanguage.getValue() == 6)
                                            ? "l"
                                            : (npSearchLanguage.getValue() == 7)
                                                ? "o"
                                                : (npSearchLanguage.getValue() == 8)
                                                    ? "d"
                                                    : (npSearchLanguage.getValue() == 9)
                                                        ? "1"
                                                        : (npSearchLanguage.getValue() == 10)
                                                            ? "i"
                                                            : (npSearchLanguage.getValue() == 11)
                                                                ? "b"
                                                                : (npSearchLanguage.getValue() == 12)
                                                                    ? "y"
                                                                    : (npSearchLanguage.getValue() == 13)
                                                                        ? "c"
                                                                        : (npSearchLanguage.getValue() == 14)
                                                                            ? "e"
                                                                            : (npSearchLanguage.getValue() == 15)
                                                                                ? "s"
                                                                                : (npSearchLanguage.getValue() == 16)
                                                                                    ? "j"
                                                                                    : (npSearchLanguage.getValue() == 17)
                                                                                        ? "u"
                                                                                        : (npSearchLanguage.getValue() == 18)
                                                                                            ? "z"
                                                                                            : (npSearchLanguage.getValue() == 19)
                                                                                                ? "r"
                                                                                                : (npSearchLanguage.getValue() == 20)
                                                                                                    ? "t"
                                                                                                    : (npSearchLanguage.getValue() == 21)
                                                                                                        ? "h"
                                                                                                        : "w";
                    PCommon.SavePref(etSearchText.getContext(), IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, bbname1);
                    SearchTvBook(context, bbname1, etSearchText.getText().toString(), isSearchBible, isSpecialCall);
                } catch (Exception ex) {
                    if (PCommon._isDebug) PCommon.LogR(context, ex);
                }
            });
            builderText.setTitle(R.string.mnuSearchAll);
            builderText.setCancelable(true);
            builderText.setView(vw);
            builderText.show();
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    private void SearchTvBook(final Context context, final String bbNameSelected, final String searchText, final boolean isSearchBible, final boolean isSpecialCall) {
        try {
            final int INSTALL_STATUS = PCommon.GetInstallStatus(context);
            if (INSTALL_STATUS < 1) return;

            if (!isSearchBible) {
                final String bbName = PCommon.GetPrefBibleName(context);
                final String bbname = PCommon.GetPref(context, IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, bbName);
                if (bbname.isEmpty()) return;
                ShowFav();
                ReloadFavTabTv(context, bbname, 0, 0, searchText, 0);
                return;
            }

            final Typeface typeface = PCommon.GetTypeface(context);
            final int fontSize = PCommon.GetFontSize(context);

            final AlertDialog builderBook = new AlertDialog.Builder(context).create();
            final LayoutInflater inflater = getLayoutInflater();

            final ScrollView sv = new ScrollView(context);
            sv.setLayoutParams(PCommon._layoutParamsMatchAndWrap);

            final ArrayList<BibleRefBO> lstRef = _s.GetListAllBookByName(bbNameSelected);
            final LinearLayout llBooks = new LinearLayout(context);
            llBooks.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            llBooks.setOrientation(LinearLayout.VERTICAL);
            llBooks.setPadding(20, 20, 20, 20);

            final AlertDialog builderChapter = new AlertDialog.Builder(context).create();
            final View vwSvSelection = inflater.inflate(R.layout.fragment_selection_items, findViewById(R.id.svSelection));

            int bNumber;
            String refText;
            String refNr;
            boolean isBookExist;
            int bNumberParam;
            boolean shouldWarn = false;

            for (BibleRefBO ref : lstRef) {
                bNumber = ref.bNumber;
                refNr = String.format(Locale.US, "%2d", bNumber);
                if (bbNameSelected.equalsIgnoreCase("y") || bbNameSelected.equalsIgnoreCase("w") || bbNameSelected.equalsIgnoreCase("c") || bbNameSelected.equalsIgnoreCase("j") || bbNameSelected.equalsIgnoreCase("r") || bbNameSelected.equalsIgnoreCase("t")) {
                    refText = PCommon.ConcaT(refNr, Html.fromHtml("&nbsp;"), ref.bName);
                } else {
                    refText = PCommon.ConcaT(refNr, Html.fromHtml("&nbsp;"), "(", ref.bsName, ") ", ref.bName);
                }

                final TextView tvBook = new TextView(context);
                tvBook.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                tvBook.setPadding(10, 20, 10, 20);
                tvBook.setText(refText);
                tvBook.setTag(bNumber);

                bNumberParam = (bNumber != 66) ? bNumber + 1 : 66;
                isBookExist = (INSTALL_STATUS == PCommon.GetInstallStatusShouldBe()) || _s.IsBookExist(bNumberParam);
                if (isBookExist) {
                    tvBook.setOnClickListener(v -> {
                        try {
                            final int bNumber1 = (int) v.getTag();
                            final int chapterMax = _s.GetBookChapterMax(bNumber1);
                            if (chapterMax < 1) {
                                PCommon.ShowToast(v.getContext(), R.string.toastBookNotInstalled, Toast.LENGTH_SHORT);
                                return;
                            }
                            final String[] titleArr = ((TextView) v).getText().toString().substring(3).split("\\(");
                            final String title = PCommon.ConcaT(getString(R.string.mnuBook), ": ", titleArr[0]);

                            PCommon.SelectItem(builderChapter, v.getContext(), vwSvSelection, title, R.string.tvChapter, "", true, chapterMax, true);
                            builderChapter.setOnDismissListener(dialogInterface -> {
                                final String bbName = PCommon.GetPrefBibleName(context);
                                final String bbname = PCommon.GetPref(v.getContext(), IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, bbName);
                                if (bbname.isEmpty()) return;
                                //not used: final String tbbName = PCommon.GetPrefTradBibleName(view.getContext(), true);
                                final int cNumber = Integer.parseInt(PCommon.GetPref(v.getContext(), IProject.APP_PREF_KEY.BOOK_CHAPTER_DIALOG, "0"));
                                final String fullQuery = PCommon.ConcaT(bNumber1,
                                        cNumber != 0 ? PCommon.ConcaT(" ", cNumber) : "",
                                        searchText != null ? PCommon.ConcaT(" ", searchText) : "");
                                //noinspection ConstantConditions
                                if (isSearchBible) {
                                    //TODO: ARABIC, to remove and replace current tab
                                    //if (isSpecialCall) {
                                    //} //TODO: else here
                                    Tab.AddTab(v.getContext(), bbname, bNumber1, cNumber, fullQuery, 1);
                                } else {
                                    ShowFav();
                                    ReloadFavTabTv(v.getContext(), bbname, bNumber1, cNumber, fullQuery, 0);
                                }
                            });
                            builderChapter.show();
                        } catch (Exception ex) {
                            if (PCommon._isDebug) PCommon.LogR(v.getContext(), ex);
                        } finally {
                            builderBook.dismiss();
                        }
                    });
                } else {
                    if (!shouldWarn) shouldWarn = true;
                    tvBook.setEnabled(false);
                }
                //TODO FAB: slow GetDrawable
                tvBook.setFocusable(true);
                tvBook.setBackground(PCommon.GetDrawable(context, R.drawable.focus_text));

                //Font
                if (typeface != null) tvBook.setTypeface(typeface);
                tvBook.setTextSize(fontSize);

                llBooks.addView(tvBook);
            }

            final Typeface tfTitle = Typeface.defaultFromStyle(Typeface.BOLD);
            final TextView tvNT = new TextView(context);
            tvNT.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            tvNT.setPadding(10, 60, 10, 20);
            tvNT.setGravity(Gravity.CENTER_HORIZONTAL);
            tvNT.setText(Html.fromHtml(getString(R.string.tvBookNT)));
            PCommon.SetTextAppareance(tvNT, context, R.style.TextAppearance_AppCompat_Headline);
            if (tfTitle != null) tvNT.setTypeface(tfTitle);
            llBooks.addView(tvNT, 39);

            final TextView tvOT = new TextView(context);
            tvOT.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            tvOT.setPadding(10, 20, 10, 20);
            tvOT.setGravity(Gravity.CENTER_HORIZONTAL);
            tvOT.setText(Html.fromHtml(getString(R.string.tvBookOT)));
            PCommon.SetTextAppareance(tvOT, context, R.style.TextAppearance_AppCompat_Headline);
            if (tfTitle != null) tvOT.setTypeface(tfTitle);
            llBooks.addView(tvOT, 0);

            final String refNr0 = String.format(Locale.US, "%2d", 0);
            final String refText0 = PCommon.ConcaT(refNr0, ": ", context.getString(R.string.itemAll));
            final TextView tvALL = new TextView(context);
            tvALL.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
            tvALL.setPadding(10, 20, 10, 20);
            tvALL.setText(refText0);
            if (typeface != null) tvALL.setTypeface(typeface);

            tvALL.setTextSize(fontSize);
            //TODO FAB: slow GetDrawable
            tvALL.setOnClickListener(v -> {
                final String bbName = PCommon.GetPrefBibleName(context);
                final String bbname = PCommon.GetPref(v.getContext(), IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, bbName);
                if (bbname.isEmpty()) return;
                //noinspection ConstantConditions
                if (isSearchBible) {
                    Tab.AddTab(v.getContext(), "S", bbname, searchText, true);
                } else {
                    ShowFav();
                    ReloadFavTabTv(v.getContext(), bbname, 0, 0, searchText, 0);
                }
                builderBook.dismiss();
            });
            tvALL.setFocusable(true);
            tvALL.setBackground(PCommon.GetDrawable(context, R.drawable.focus_text));
            llBooks.addView(tvALL, 0);

            if (shouldWarn) {
                final TextView tvWarn = new TextView(context);
                tvWarn.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                tvWarn.setPadding(10, 10, 10, 20);
                tvWarn.setGravity(Gravity.CENTER_HORIZONTAL);
                tvWarn.setText(R.string.tvBookInstall);
                tvWarn.setTextSize(fontSize);
                llBooks.addView(tvWarn, 0);
            }
            sv.addView(llBooks);

            builderBook.setTitle(R.string.mnuBooks);
            builderBook.setCancelable(true);
            builderBook.setView(sv);
            builderBook.show();
        } catch (Exception ex) {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }
}
