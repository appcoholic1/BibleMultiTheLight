package com.appcoholic.bible;

public class LexTbesBO
{
    int id;
    String eStrong;
    String dStrong;
    String uStrong;
    String hg;
    String transliteration;
    String grammar;
    String english;
    String meaning;

    LexTbesBO()
    {
        this.id = -1;
        this.eStrong = "";
        this.dStrong = "";
        this.uStrong = "";
        this.hg = "";
        this.transliteration = "";
        this.grammar = "";
        this.english = "";
        this.meaning = "";
    }

    LexTbesBO(final int id, final String eStrong, final String dStrong, final String uStrong, final String hg, final String transliteration, final String grammar, final String english, final String meaning)
    {
        this.id = id;
        this.eStrong = eStrong;
        this.dStrong = dStrong;
        this.uStrong = uStrong;
        this.hg = hg;
        this.transliteration = transliteration;
        this.grammar = grammar;
        this.english = english;
        this.meaning = meaning;
    }
}
