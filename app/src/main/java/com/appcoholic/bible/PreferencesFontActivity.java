
package com.appcoholic.bible;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class PreferencesFontActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        try
        {
            super.onCreate(savedInstanceState);

            final int themeId = PCommon.GetPrefThemeId(this);
            setTheme(themeId);
            ShowTypefaces();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    private void ShowTypefaces()
    {
        try
        {
            final Context context = this;
            final LayoutInflater inflater = getLayoutInflater();
            final LinearLayout llFont = (LinearLayout) (inflater.inflate(R.layout.activity_font_preferences, (LinearLayout) findViewById(R.id.llFont)));
            final RadioGroup radioGroup = new RadioGroup(context);
            final String fontNameSelected = PCommon.GetPref(context, IProject.APP_PREF_KEY.FONT_NAME, "");
            final ScrollView svFont = new ScrollView(context);
            svFont.setPadding(20, 20, 20, 20);
            llFont.addView(radioGroup);
            svFont.addView(llFont);

            int index = 0;
            RadioButton radioFont;
            TextView tvEx;
            Typeface fontEx;
            final int fontSize = PCommon.GetFontSize(context);

            for (String fontName : context.getResources().getStringArray(R.array.FONT_VALUES_ARRAY))
            {
                fontEx = (fontName == null || fontName.isEmpty())
                        ? Typeface.defaultFromStyle(Typeface.NORMAL)
                        : Typeface.createFromAsset(context.getAssets(), PCommon.ConcaT("fonts/", fontName, ".ttf"));

                radioFont = new RadioButton(context);
                radioFont.setMinHeight(48);
                radioFont.setChecked( fontNameSelected.equalsIgnoreCase( fontName ));
                radioFont.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                radioFont.setText((fontName == null || fontName.isEmpty()) ? getString(R.string.mnuTypefaceDefault) : fontName );
                radioFont.setTextSize(fontSize);
                radioFont.setTag( fontName );
                radioFont.setOnClickListener(v -> {
                    try
                    {
                        final String fontName1 = (String) v.getTag();
                        PCommon.SavePref(v.getContext(), IProject.APP_PREF_KEY.FONT_NAME, fontName1);
                        finish();
                    }
                    catch (Exception ex)
                    {
                        if (PCommon._isDebug) PCommon.LogR(v.getContext(), ex);
                    }
                });

                tvEx = new TextView(context);
                tvEx.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                tvEx.setPadding(20, 0, 0, 40);
                tvEx.setMinHeight(48);
                tvEx.setText( R.string.tvFontExample );
                tvEx.setTextSize(fontSize);
                tvEx.setTag( R.id.tv1, index );
                tvEx.setTag( R.id.tv2, fontName );
                tvEx.setTypeface(fontEx);//Font
                tvEx.setOnClickListener(v -> {
                    final int index1 = (int) v.getTag(R.id.tv1);
                    final String fontName12 = (String) v.getTag(R.id.tv2);

                    ((RadioButton)(radioGroup.getChildAt(index1))).setChecked(true);
                    PCommon.SavePref(v.getContext(), IProject.APP_PREF_KEY.FONT_NAME, fontName12);
                    finish();
                });
                tvEx.setFocusable(true);
                tvEx.setBackground(PCommon.GetDrawable(getApplicationContext(), R.drawable.focus_text));

                radioGroup.addView(radioFont);
                radioGroup.addView(tvEx);

                index = index + 2;
            }

            setContentView(svFont);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }
}
