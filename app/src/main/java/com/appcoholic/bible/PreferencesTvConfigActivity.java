
package com.appcoholic.bible;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class PreferencesTvConfigActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle bundle)
    {
        try
        {
            super.onCreate(bundle);

            final String action = Objects.requireNonNull(getIntent().getStringExtra("ACTION")).toUpperCase();
            if (action.equals("CONFIGURE"))
            {
                final String borders = PCommon.GetUiLayoutTVBorders(getApplicationContext(), PCommon.APP_PREF_KEY.UI_LAYOUT_TV_BORDERS);
                PCommon.SavePref(getApplicationContext(), IProject.APP_PREF_KEY.UI_LAYOUT_TV_BORDERS_DIALOG, borders);
            }

            PCommon.SetLocale(PreferencesTvConfigActivity.this, -1,false);

            final int themeId = PCommon.GetPrefThemeId(getApplicationContext());
            setTheme(themeId);

            setContentView(R.layout.activity_main_tv_configure);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onPostResume()
    {
        try
        {
            super.onPostResume();

            ShowLayout(getApplicationContext());

            ConfigUI(getApplicationContext());
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    private void ClickListener(int leftStep, int topStep, int rightStep, int bottomStep)
    {
        try
        {
            final String bordersDialog = PCommon.GetUiLayoutTVBorders(getApplicationContext(), IProject.APP_PREF_KEY.UI_LAYOUT_TV_BORDERS_DIALOG);
            String[] borders = bordersDialog.split(",");
            if (borders.length != 4) borders = PCommon.GetUiLayoutTVBorders(getApplicationContext(),null).split(",");

            int left = Integer.parseInt(borders[0]);
            int top = Integer.parseInt(borders[1]);
            int right = Integer.parseInt(borders[2]);
            int bottom = Integer.parseInt(borders[3]);

            left = left + leftStep;
            top = top + topStep;
            right = right + rightStep;
            bottom = bottom + bottomStep;

            final String bordersDialogUpd = PCommon.ConcaT(left, ",", top, ",", right, ",", bottom);
            PCommon.SavePref(getApplicationContext(), IProject.APP_PREF_KEY.UI_LAYOUT_TV_BORDERS_DIALOG, bordersDialogUpd);

            ShowLayout(getApplicationContext());
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    private void ConfigUI(final Context context)
    {
        try
        {
            final View cornerPlusLeftRight = findViewById(R.id.cornerPlusLeftRight);
            cornerPlusLeftRight.setOnClickListener(v -> ClickListener(1, 0, 1, 0));
            final View cornerPlusTopBottom = findViewById(R.id.cornerPlusTopBottom);
            cornerPlusTopBottom.setOnClickListener(v -> ClickListener(0, 1, 0, 1));
            final View cornerMinusLeftRight = findViewById(R.id.cornerMinusLeftRight);
            cornerMinusLeftRight.setOnClickListener(v -> ClickListener(-1,0,-1,0));
            final View cornerMinusTopBottom = findViewById(R.id.cornerMinusTopBottom);
            cornerMinusTopBottom.setOnClickListener(v -> ClickListener(0,-1,0,-1));
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    private void ShowLayout(final Context context)
    {
        try
        {
            final String bordersDialog = PCommon.GetUiLayoutTVBorders(getApplicationContext(), IProject.APP_PREF_KEY.UI_LAYOUT_TV_BORDERS_DIALOG);
            String[] borders = bordersDialog.split(",");
            if (borders.length != 4) borders = PCommon.GetUiLayoutTVBorders(getApplicationContext(),null).split(",");

            int left = Integer.parseInt(borders[0]);        left = PCommon.ConvertDpToPx(context, left);
            int top = Integer.parseInt(borders[1]);         top = PCommon.ConvertDpToPx(context, top);
            int right = Integer.parseInt(borders[2]);       right = PCommon.ConvertDpToPx(context, right);
            int bottom = Integer.parseInt(borders[3]);      bottom = PCommon.ConvertDpToPx(context, bottom);

            final RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            layoutParams.setMargins(left, top, right, bottom);

            final RelativeLayout rlOverscan = findViewById(R.id.rlOverscan);
            rlOverscan.setLayoutParams(layoutParams);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }
}
