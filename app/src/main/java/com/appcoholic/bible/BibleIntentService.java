package com.appcoholic.bible;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import java.util.Objects;

public class BibleIntentService extends IntentService
{
    private Context _context = null;
    private SCommon _s = null;

    public BibleIntentService() {
        super("BibleIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        try
        {
            _context = getApplicationContext();

            CheckLocalInstance(_context);

            final String action = intent.getAction();
            if (Objects.requireNonNull(action).equalsIgnoreCase("STOP"))
            {
                _s.SayStop();
            }
            else if (action.equalsIgnoreCase("PREVIOUS"))
            {
                _s.SayGetPreviousVerseNumber();
                _s.Say();
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * Check local instance
     */
    private void CheckLocalInstance(final Context context)
    {
        try
        {
            if (_s == null)
            {
                _s = SCommon.GetInstance(context);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }
}
