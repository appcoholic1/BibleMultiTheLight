
package com.appcoholic.bible;

class ArtOriginalContentBO
{
    final String title;
    final String originalContent;

    ArtOriginalContentBO(final String title, final String originalContent)
    {
        this.title = title;
        this.originalContent = originalContent;
    }
}
