
package com.appcoholic.bible;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.MaskFilterSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.text.style.SubscriptSpan;
import android.text.style.SuperscriptSpan;
import android.text.style.TextAppearanceSpan;
import android.text.style.TypefaceSpan;
import android.text.style.UnderlineSpan;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.io.FileOutputStream;

public class SharePictureActivity extends AppCompatActivity
{
    enum IMPACT_COLOR_CHANGE_TYPE
    {
        PARTIALLY_FOREGROUND,
        PARTIALLY_BACKGROUND,
        WHOLE_TEXT_COLOR
    }

    enum IMPACT_TYPEFACE_CHANGE_TYPE
    {
        PARTIALLY,
        WHOLE_TEXT
    }

    private SCommon _s = null;
    private Context context = null;
    private boolean isUiTelevision = false;
    private boolean isSaving = false;
    private float scaleFactor = 1.0f;
    private RelativeLayout rlImg;
    private ImageView ivImg;
    private EditText etVerse;

    final private float textSize = 90; //Px
    final private float arrowSize = 40; //Px
    final private int textSizeIncrement = 2; //Px //TODO: PIC, allow to +- this value
    final private int textSizeExtraIncrement = 10;
    final private int moveIncrement = 10; //Px //TODO: PIC, allow to +- this value
    final private int moveExtraIncrement = 40; //Px
    final private int widthIncrement = 10; //Px //TODO: PIC, allow to +- this value
    final private int widthExtraIncrement = 40; //Px
    private int shadowColor = Color.BLACK;

    /*
    private ScaleGestureDetector scaleGestureDetector;

    private class LayoutScaleGestureListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        //final private ImageView iv;
        //float mScaleFactor;

        public LayoutScaleGestureListener() {
            //this.iv = imageView;
            //this.mScaleFactor = scaleFactor;
        }

        @Override
        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
            scaleFactor *= scaleGestureDetector.getScaleFactor();
            if (scaleFactor < 0.1f)
                scaleFactor = 0.1f;
            else if (scaleFactor > 1.0f)
                scaleFactor = 1.0f;

            rlImg.setScaleX(scaleFactor);
            rlImg.setScaleY(scaleFactor);
            rlImg.setPivotX(0.0f);
            rlImg.setPivotY(0.0f);
            
            return true;
        }
    }
     */

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        context = getApplicationContext();
        isUiTelevision = PCommon.IsUiTelevision(context);

        CheckLocalInstance(context);

        PCommon.SetLocale(SharePictureActivity.this, -1,false);

        final int themeId = PCommon.GetPrefThemeId(context);
        setTheme(themeId);

        ShowPicture();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed()
    {
        final String quit = PCommon.ConcaT(this.getResources().getString(R.string.mnuQuit), "?");
        final AlertDialog builder = new AlertDialog.Builder(SharePictureActivity.this).create();
        builder.setTitle(quit);
        builder.setCancelable(true);
        builder.setButton(DialogInterface.BUTTON_POSITIVE, context.getString(R.string.btnYes), (dialog, which) -> SharePictureActivity.super.onBackPressed());
        builder.setButton(DialogInterface.BUTTON_NEGATIVE, context.getString(R.string.btnNo), (dialog, which) -> builder.dismiss());
        builder.show();
    }

    /***
     * Check local instance (to copy reader all activities that use it)
     */
    private void CheckLocalInstance(final Context context)
    {
        try
        {
            if (_s == null) _s = SCommon.GetInstance(context);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    //====================================================
    //=== MAKE IT SIMPLE (NOT PHOTOSHOP) FOR EVERYBODY
    //====================================================
    //TODO: PIC, REPLACE SPAN IN ALL SPANS => if there is a span there in selection: replace it
    //TODO: PIC, SEVERAL TEXTS WITH PERSONAL PROPERTIES
    //TODO: PIC, VIGNETTE
    //TODO: PIC, TRANSLATE BUTTONS
    //TODO: PIC, SAVE IN PNG
    //TODO: PIC, MORE COLORS
    //TODO: PIC, TOOLBAR ON LEFT BORDER WITH SECTIONS AND MAYBE COLORS, FIXED
    //TODO: PIC, SORT FILES BY DATE
    //TODO: PIC, SET SAVE DIR
    //TODO: PIC, DOWNLOAD WITH FILTER (to save and load)
    //TODO: PIC, FIND HOW TO NAME SAVED FILES
    //TODO: PIC, CURRENT WORK TO SAVE (TEMP SAVE)???
    //TODO: PIC, SEL-TEXT-SIZE

    @SuppressLint("ClickableViewAccessibility")
    private void ShowPicture()
    {
        try
        {
            final Typeface typeface = PCommon.GetTypeface(context);
            //PCommon.GetFontSize(context); //TODO: PIC, set SIZE

            final LayoutInflater inflater = this.getLayoutInflater();
            final View view = inflater.inflate(R.layout.activity_share_picture, findViewById(R.id.svImg));

            rlImg = view.findViewById(R.id.rlImg);
            ivImg = view.findViewById(R.id.ivImg);

            /*
            scaleGestureDetector = new ScaleGestureDetector(context, new LayoutScaleGestureListener());
            rlImg.setOnTouchListener((v, event) -> {
                scaleGestureDetector.onTouchEvent(event);
                return true;
            });*/

            final int verseWidth = view.getWidth() == 0 ? 600 : view.getWidth(); //TODO: PIC, add height?
            //TODO: PIC, get width

            etVerse = view.findViewById(R.id.etVerse);
            etVerse.setPadding(0, 0, 0, 16);
            etVerse.setMinWidth(300);
            etVerse.setMaxWidth(verseWidth); //TODO: PIC, idk if need to remove it
            etVerse.setTextColor(context.getResources().getColor(R.color.white));  //TODO: PIC, LOAD COLOR
            if (typeface != null) { etVerse.setTypeface(typeface); }
            etVerse.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize); //TODO: PIC, size text

            if (isUiTelevision && etVerse.getText().length() == 0)
            {
                String clipContent = "";

                try
                {
                    final ClipboardBO clip = new ClipboardBO(context);
                    clipContent = clip.GenerateTextForClipboard(false);
                }
                catch (Exception ignored)
                { }

                etVerse.setText(clipContent);
            }

            //Toolbar2
            //final LinearLayout lvToolbarImg = view.findViewById(R.id.lvToolbarImg); //not used yet
            final LinearLayout lvToolbarZoom = view.findViewById(R.id.lvToolbarZoom);
            final LinearLayout lvToolbarTextSize = view.findViewById(R.id.lvToolbarTextSize);
            final LinearLayout lvToolbarTextPosCustom = view.findViewById(R.id.lvToolbarTextPosCustom);
            final LinearLayout lvToolbarTextBoxSize = view.findViewById(R.id.lvToolbarTextBoxSize);
            final LinearLayout lvToolbarTextPosGlobal = view.findViewById(R.id.lvToolbarTextPosGlobal);
            final LinearLayout lvToolbarTextAlignment = view.findViewById(R.id.lvToolbarTextAlignment);
            final LinearLayout lvToolbarTextShadow = view.findViewById(R.id.lvToolbarTextShadow);
            final LinearLayout lvToolbarTextFont = view.findViewById(R.id.lvToolbarTextFont);
            final LinearLayout lvToolbarTextColor = view.findViewById(R.id.lvToolbarTextColor);
            final LinearLayout lvToolbarTextLineHeight = view.findViewById(R.id.lvToolbarTextLineHeight);
            final LinearLayout lvToolbarTextSel = view.findViewById(R.id.lvToolbarTextSel);
            final LinearLayout lvToolbar3 = view.findViewById(R.id.lvToolbar3);
            final LinearLayout lvToolbarSelEmpty = view.findViewById(R.id.lvToolbarSelEmpty);
            final LinearLayout lvToolbarSelTextSize = view.findViewById(R.id.lvToolbarSelTextSize);

            //Toolbar ZOOM
            final Button btnToolbarZoomIn = view.findViewById(R.id.btnToolbarZoomIn);
            btnToolbarZoomIn.setOnClickListener(v -> RescaleLayout(scaleFactor + 0.1f));

            final Button btnToolbarZoomOut = view.findViewById(R.id.btnToolbarZoomOut);
            btnToolbarZoomOut.setOnClickListener(v -> RescaleLayout(scaleFactor - 0.1f));

            final Button btnToolbarZoom100 = view.findViewById(R.id.btnToolbarZoom100);
            btnToolbarZoom100.setOnClickListener(v -> RescaleLayout(1.0f));

            //Toolbar IMG
            final Button btnToolbarImgSelect = view.findViewById(R.id.btnToolbarImgSelect);
            btnToolbarImgSelect.setOnClickListener(v -> {
                final String fullPathDir = Environment.getExternalStorageDirectory().getPath();
                PCommon.SelectFile(SharePictureActivity.this, fullPathDir, "", ivImg);
            });

            final Button btnToolbarImgNoImg = view.findViewById(R.id.btnToolbarImgNoImg);
            btnToolbarImgNoImg.setOnClickListener(v -> {
                final int bgColor = PCommon.GetPrefThemeName(v.getContext()).compareTo("LIGHT") == 0 ? R.color.white : R.color.black; //TODO: harcoded, need to find from attribute
                ivImg.setImageResource(bgColor);
                //TODO: PIC, BUG: not BGCOLOR SELECTED
            });

            //Toolbar TEXT-SIZE
            final Button btnToolbarTextSizeUp = view.findViewById(R.id.btnToolbarTextSizeUp);
            btnToolbarTextSizeUp.setOnClickListener(v -> {
                final float fontsize = etVerse.getTextSize() + textSizeIncrement + textSizeExtraIncrement;
                etVerse.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontsize);
                v.requestFocus();
            });

            final Button btnToolbarTextSizeDown = view.findViewById(R.id.btnToolbarTextSizeDown);
            btnToolbarTextSizeDown.setOnClickListener(v -> {
                final float fontsize = etVerse.getTextSize() - textSizeIncrement;
                if (fontsize > 0) etVerse.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontsize);
                v.requestFocus();
            });

            //Toolbar TEXT-POS-GLOBAL
            final ToggleButton btnToolbarTextPosGlobalUp = view.findViewById(R.id.btnToolbarTextPosGlobalUp);
            final ToggleButton btnToolbarTextPosGlobalDown = view.findViewById(R.id.btnToolbarTextPosGlobalDown);
            final ToggleButton btnToolbarTextPosGlobalLeft = view.findViewById(R.id.btnToolbarTextPosGlobalLeft);
            final ToggleButton btnToolbarTextPosGlobalRight = view.findViewById(R.id.btnToolbarTextPosGlobalRight);
            final Button btnToolbarTextPosGlobalApply = view.findViewById(R.id.btnToolbarTextPosGlobalApply);
            btnToolbarTextPosGlobalApply.setOnClickListener(v -> {
                SetRule(etVerse, btnToolbarTextPosGlobalUp, btnToolbarTextPosGlobalLeft, btnToolbarTextPosGlobalDown, btnToolbarTextPosGlobalRight);
            });

            //Toolbar TEXT-BOX-SIZE
            final Button btnToolbarTextBoxSizeUp = view.findViewById(R.id.btnToolbarTextBoxSizeUp);
            btnToolbarTextBoxSizeUp.setOnClickListener(v -> {
                final int newWidth = etVerse.getWidth() + widthIncrement + widthExtraIncrement;
                if (newWidth < 0) return;
                etVerse.setWidth(newWidth);
            });

            final Button btnToolbarTextBoxSizeDown = view.findViewById(R.id.btnToolbarTextBoxSizeDown);
            btnToolbarTextBoxSizeDown.setOnClickListener(v -> {
                final int newWidth = etVerse.getWidth() - widthIncrement;
                if (newWidth < 0) return;
                etVerse.setWidth(newWidth);
            });

            //Toolbar TEXT-POS-CUSTOM (move)
            final Button btnToolbarTextPosCustomUp = view.findViewById(R.id.btnToolbarTextPosCustomUp);
            btnToolbarTextPosCustomUp.setTextSize(TypedValue.COMPLEX_UNIT_PX, arrowSize);
            btnToolbarTextPosCustomUp.setOnClickListener(v -> {
                final ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) etVerse.getLayoutParams();
                int newMargin = mlp.topMargin - moveIncrement;

                final int minMargin = 0;
                final int maxMargin = ivImg.getHeight();
                if ((newMargin + etVerse.getHeight()) > maxMargin) newMargin = maxMargin - etVerse.getHeight();
                if (newMargin < minMargin) newMargin = 0;

                mlp.topMargin = newMargin;
                etVerse.setLayoutParams(mlp);
            });

            final Button btnToolbarTextPosCustomDown = view.findViewById(R.id.btnToolbarTextPosCustomDown);
            btnToolbarTextPosCustomDown.setTextSize(TypedValue.COMPLEX_UNIT_PX, arrowSize);
            btnToolbarTextPosCustomDown.setOnClickListener(v -> {
                final ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) etVerse.getLayoutParams();
                int newMargin = mlp.topMargin + moveIncrement + moveExtraIncrement;

                final int minMargin = 0;
                final int maxMargin = ivImg.getHeight();
                if ((newMargin + etVerse.getHeight()) > maxMargin) newMargin = maxMargin - etVerse.getHeight();
                if (newMargin < minMargin) newMargin = 0;

                mlp.topMargin = newMargin;
                etVerse.setLayoutParams(mlp);
            });

            final Button btnToolbarTextPosCustomLeft = view.findViewById(R.id.btnToolbarTextPosCustomLeft);
            btnToolbarTextPosCustomLeft.setTextSize(TypedValue.COMPLEX_UNIT_PX, arrowSize);
            btnToolbarTextPosCustomLeft.setOnClickListener(v -> {
                final ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) etVerse.getLayoutParams();
                int newMargin = mlp.leftMargin - moveIncrement;

                final int minMargin = 0;
                final int maxMargin = ivImg.getWidth();
                if ((newMargin + etVerse.getWidth()) > maxMargin) newMargin = maxMargin - etVerse.getWidth();
                if (newMargin < minMargin) newMargin = 0;

                mlp.leftMargin = newMargin;
                etVerse.setLayoutParams(mlp);
            });

            final Button btnToolbarTextPosCustomRight = view.findViewById(R.id.btnToolbarTextPosCustomRight);
            btnToolbarTextPosCustomRight.setTextSize(TypedValue.COMPLEX_UNIT_PX, arrowSize);
            btnToolbarTextPosCustomRight.setOnClickListener(v -> {
                final ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) etVerse.getLayoutParams();
                int newMargin = mlp.leftMargin + moveIncrement + moveExtraIncrement;

                final int minMargin = 0;
                final int maxMargin = ivImg.getWidth();
                if ((newMargin + etVerse.getWidth()) > maxMargin) newMargin = maxMargin - etVerse.getWidth();
                if (newMargin < minMargin) newMargin = 0;

                mlp.leftMargin = newMargin;
                etVerse.setLayoutParams(mlp);
            });

            //Toolbar TEXT-ALIGNMENT
            final Button btnToolbarTextAlignmentLeft = view.findViewById(R.id.btnToolbarTextAlignmentLeft);
            btnToolbarTextAlignmentLeft.setTextSize(TypedValue.COMPLEX_UNIT_PX, arrowSize);
            btnToolbarTextAlignmentLeft.setOnClickListener(v -> {
                etVerse.setGravity(Gravity.LEFT);
            });

            final Button btnToolbarTextAlignmentCenter = view.findViewById(R.id.btnToolbarTextAlignmentCenter);
            btnToolbarTextAlignmentCenter.setOnClickListener(v -> {
                etVerse.setGravity(Gravity.CENTER);
            });

            final Button btnToolbarTextAlignmentRight = view.findViewById(R.id.btnToolbarTextAlignmentRight);
            btnToolbarTextAlignmentRight.setTextSize(TypedValue.COMPLEX_UNIT_PX, arrowSize);
            btnToolbarTextAlignmentRight.setOnClickListener(v -> {
                etVerse.setGravity(Gravity.RIGHT);
            });

            //Toolbar TEXT-SHADOW
            final Button btnToolbarTextShadowUp = view.findViewById(R.id.btnToolbarTextShadowUp);
            btnToolbarTextShadowUp.setOnClickListener(v -> {
                final float newShadowDx = etVerse.getShadowDx() + 2;
                final float newShadowDy = etVerse.getShadowDy() + 2;
                etVerse.setShadowLayer(2, newShadowDx < 0 ? 0 : newShadowDx, newShadowDy < 0 ? 0 : newShadowDy, shadowColor);
            });

            final Button btnToolbarTextShadowDown = view.findViewById(R.id.btnToolbarTextShadowDown);
            btnToolbarTextShadowDown.setOnClickListener(v -> {
                final float newShadowDx = etVerse.getShadowDx() - 2;
                final float newShadowDy = etVerse.getShadowDy() - 2;
                etVerse.setShadowLayer(2, newShadowDx < 0 ? 0 : newShadowDx, newShadowDy < 0 ? 0 : newShadowDy, shadowColor);
            });

            //Toolbar TEXT-FONT
            ShowFonts(lvToolbarTextFont, etVerse, IMPACT_TYPEFACE_CHANGE_TYPE.WHOLE_TEXT);

            //Toolbar TEXT-COLOR
            ShowColors(lvToolbarTextColor, etVerse, IMPACT_COLOR_CHANGE_TYPE.WHOLE_TEXT_COLOR);

            //Toolbar TEXT-LINE-HEIGHT
            final Button btnToolbarTextLineHeightUp = view.findViewById(R.id.btnToolbarTextLineHeightUp);
            btnToolbarTextLineHeightUp.setOnClickListener(v -> {
                final int newLineHeight = etVerse.getLineHeight() + 2;
                etVerse.setLineHeight(Math.max(newLineHeight, 1));
            });

            final Button btnToolbarTextLineHeightDown = view.findViewById(R.id.btnToolbarTextLineHeightDown);
            btnToolbarTextLineHeightDown.setOnClickListener(v -> {
                final int newLineHeight = etVerse.getLineHeight() - 2;
                etVerse.setLineHeight(Math.max(newLineHeight, 1));
            });

            //Toolbar TEXT-SEL
            //ScaleXSpan
            //Text-size
            //Remove spans
            //StyleSpan(/EXTRABOLD?/THIN/EXTRALIGHT/LIGHT
            final Button btnToolbarTextSelTypeface = view.findViewById(R.id.btnToolbarTextSelFont);
            btnToolbarTextSelTypeface.setOnClickListener(v -> {
                lvToolbar3.setVisibility(View.VISIBLE);
                lvToolbarSelTextSize.setVisibility(View.GONE);

                ShowFonts(lvToolbarSelEmpty, etVerse, IMPACT_TYPEFACE_CHANGE_TYPE.PARTIALLY);
            });

            final Button btnToolbarTextSelForegroundColor = view.findViewById(R.id.btnToolbarTextSelForegroundColor);
            btnToolbarTextSelForegroundColor.setOnClickListener(v -> {
                lvToolbar3.setVisibility(View.VISIBLE);
                lvToolbarSelTextSize.setVisibility(View.GONE);

                ShowColors(lvToolbarSelEmpty, etVerse, IMPACT_COLOR_CHANGE_TYPE.PARTIALLY_FOREGROUND);
            });

            final Button btnToolbarTextSelBackgroundColor = view.findViewById(R.id.btnToolbarTextSelBackgroundColor);
            btnToolbarTextSelBackgroundColor.setOnClickListener(v -> {
                lvToolbar3.setVisibility(View.VISIBLE);
                lvToolbarSelTextSize.setVisibility(View.GONE);

                ShowColors(lvToolbarSelEmpty, etVerse, IMPACT_COLOR_CHANGE_TYPE.PARTIALLY_BACKGROUND);
            });

            final Button btnToolbarTextSelBold = view.findViewById(R.id.btnToolbarTextSelBold);
            btnToolbarTextSelBold.setOnClickListener(v -> {
                lvToolbar3.setVisibility(View.GONE);

                SetSpan(etVerse, new StyleSpan(Typeface.BOLD));
            });

            final Button btnToolbarTextSelItalic = view.findViewById(R.id.btnToolbarTextSelItalic);
            btnToolbarTextSelItalic.setOnClickListener(v -> {
                lvToolbar3.setVisibility(View.GONE);

                SetSpan(etVerse, new StyleSpan(Typeface.ITALIC));
            });

            final Button btnToolbarTextSelUnderline = view.findViewById(R.id.btnToolbarTextSelUnderline);
            btnToolbarTextSelUnderline.setOnClickListener(v -> {
                lvToolbar3.setVisibility(View.GONE);

                SetSpan(etVerse, new UnderlineSpan());
            });

            final Button btnToolbarTextSelStrikeThrough = view.findViewById(R.id.btnToolbarTextSelStrikeThrough);
            btnToolbarTextSelStrikeThrough.setOnClickListener(v -> {
                lvToolbar3.setVisibility(View.GONE);

                SetSpan(etVerse, new StrikethroughSpan());
            });

            final Button btnToolbarTextSelBlur = view.findViewById(R.id.btnToolbarTextSelBlur);
            btnToolbarTextSelBlur.setOnClickListener(v -> {
                lvToolbar3.setVisibility(View.GONE);

                SetSpan(etVerse, new MaskFilterSpan(new BlurMaskFilter(5f, BlurMaskFilter.Blur.NORMAL))); //TODO: PIC, BLUR
            });

            final Button btnToolbarTextSelSub = view.findViewById(R.id.btnToolbarTextSelSub);
            btnToolbarTextSelSub.setOnClickListener(v -> {
                lvToolbar3.setVisibility(View.GONE);

                SetSpan(etVerse, new SubscriptSpan());
            });

            final Button btnToolbarTextSelSup = view.findViewById(R.id.btnToolbarTextSelSup);
            btnToolbarTextSelSup.setOnClickListener(v -> {
                lvToolbar3.setVisibility(View.GONE);

                SetSpan(etVerse, new SuperscriptSpan());
            });

            final Button btnToolbarTextSelRelative = view.findViewById(R.id.btnToolbarTextSelRelative);
            btnToolbarTextSelRelative.setOnClickListener(v -> {
                lvToolbar3.setVisibility(View.GONE);

                SetSpan(etVerse, new RelativeSizeSpan(1.5f));   //TODO: PIC, RELATIVESIZE + -
            });

            final Button btnToolbarTextSelTextSize = view.findViewById(R.id.btnToolbarTextSelTextSize);
            btnToolbarTextSelTextSize.setOnClickListener(v -> {
                lvToolbar3.setVisibility(View.VISIBLE);
                lvToolbarSelEmpty.setVisibility(View.GONE);
                lvToolbarSelTextSize.setVisibility(View.VISIBLE);
            });

            final Button btnToolbarSelTextSizeUp = view.findViewById(R.id.btnToolbarSelTextSizeUp);
            btnToolbarSelTextSizeUp.setOnClickListener(v -> {
                int selStart = etVerse.getSelectionStart();
                int selEnd = etVerse.getSelectionEnd();
                if (selEnd < selStart) {
                    int tmp = selStart;
                    selStart = selEnd;
                    selEnd = tmp;
                }
                if (selEnd - selStart <= 0) return;

                final TextAppearanceSpan[] spanArr = etVerse.getText().getSpans(selStart, selEnd, TextAppearanceSpan.class);
                float fontsize = (spanArr.length > 0) ? spanArr[spanArr.length - 1].getTextSize() : etVerse.getTextSize();
                fontsize += textSizeIncrement + textSizeExtraIncrement;
                if (fontsize < 0) fontsize = 1;
                SetSpan(etVerse, new TextAppearanceSpan(null, 0, (int) fontsize, null, null));
            });

            final Button btnToolbarSelTextSizeDown = view.findViewById(R.id.btnToolbarSelTextSizeDown);
            btnToolbarSelTextSizeDown.setOnClickListener(v -> {
                int selStart = etVerse.getSelectionStart();
                int selEnd = etVerse.getSelectionEnd();
                if (selEnd < selStart) {
                    int tmp = selStart;
                    selStart = selEnd;
                    selEnd = tmp;
                }
                if (selEnd - selStart <= 0) return;

                final TextAppearanceSpan[] spanArr = etVerse.getText().getSpans(selStart, selEnd, TextAppearanceSpan.class);
                float fontsize = (spanArr.length > 0) ? spanArr[spanArr.length - 1].getTextSize() : etVerse.getTextSize();
                fontsize -= textSizeIncrement;
                if (fontsize < 0) fontsize = 1;
                SetSpan(etVerse, new TextAppearanceSpan(null, 0, (int) fontsize, null, null));
            });

            //Toolbar1
            final Button btnToolbarZoom = view.findViewById(R.id.btnToolbarZoom);
            btnToolbarZoom.setOnClickListener(v -> {
                //lvToolbarImg.setVisibility(View.GONE);
                lvToolbarZoom.setVisibility(View.VISIBLE);
                lvToolbarTextPosCustom.setVisibility(View.GONE);
                lvToolbarTextBoxSize.setVisibility(View.GONE);
                lvToolbarTextPosGlobal.setVisibility(View.GONE);
                lvToolbarTextAlignment.setVisibility(View.GONE);
                lvToolbarTextShadow.setVisibility(View.GONE);
                lvToolbarTextFont.setVisibility(View.GONE);
                lvToolbarTextColor.setVisibility(View.GONE);
                lvToolbarTextSize.setVisibility(View.GONE);
                lvToolbarTextLineHeight.setVisibility(View.GONE);
                lvToolbarTextSel.setVisibility(View.GONE);

                lvToolbar3.setVisibility(View.GONE);
            });

            final Button btnToolbarImg = view.findViewById(R.id.btnToolbarImg);
            btnToolbarImg.setOnClickListener(v -> {
                //lvToolbarImg.setVisibility(View.GONE);
                lvToolbarZoom.setVisibility(View.GONE);
                lvToolbarTextSize.setVisibility(View.GONE);
                lvToolbarTextPosCustom.setVisibility(View.GONE);
                lvToolbarTextBoxSize.setVisibility(View.GONE);
                lvToolbarTextPosGlobal.setVisibility(View.GONE);
                lvToolbarTextAlignment.setVisibility(View.GONE);
                lvToolbarTextShadow.setVisibility(View.GONE);
                lvToolbarTextFont.setVisibility(View.GONE);
                lvToolbarTextColor.setVisibility(View.GONE);
                lvToolbarTextLineHeight.setVisibility(View.GONE);
                lvToolbarTextSel.setVisibility(View.GONE);

                lvToolbar3.setVisibility(View.GONE);

                final String fullPathDir = Environment.getExternalStorageDirectory().getPath();
                PCommon.SelectFile(SharePictureActivity.this, fullPathDir, "", ivImg);
            });

            final Button btnToolbarTextSize = view.findViewById(R.id.btnToolbarTextSize);
            btnToolbarTextSize.setOnClickListener(v -> {
                //lvToolbarImg.setVisibility(View.GONE);
                lvToolbarZoom.setVisibility(View.GONE);
                lvToolbarTextPosCustom.setVisibility(View.GONE);
                lvToolbarTextBoxSize.setVisibility(View.GONE);
                lvToolbarTextPosGlobal.setVisibility(View.GONE);
                lvToolbarTextAlignment.setVisibility(View.GONE);
                lvToolbarTextShadow.setVisibility(View.GONE);
                lvToolbarTextFont.setVisibility(View.GONE);
                lvToolbarTextColor.setVisibility(View.GONE);
                lvToolbarTextLineHeight.setVisibility(View.GONE);
                lvToolbarTextSel.setVisibility(View.GONE);
                lvToolbarTextSize.setVisibility(View.VISIBLE);

                lvToolbar3.setVisibility(View.GONE);
            });

            final Button btnToolbarTextPosCustom = view.findViewById(R.id.btnToolbarTextPosCustom);
            btnToolbarTextPosCustom.setOnClickListener(v -> {
                //lvToolbarImg.setVisibility(View.GONE);
                lvToolbarZoom.setVisibility(View.GONE);
                lvToolbarTextSize.setVisibility(View.GONE);
                lvToolbarTextBoxSize.setVisibility(View.GONE);
                lvToolbarTextPosGlobal.setVisibility(View.GONE);
                lvToolbarTextAlignment.setVisibility(View.GONE);
                lvToolbarTextShadow.setVisibility(View.GONE);
                lvToolbarTextFont.setVisibility(View.GONE);
                lvToolbarTextColor.setVisibility(View.GONE);
                lvToolbarTextLineHeight.setVisibility(View.GONE);
                lvToolbarTextSel.setVisibility(View.GONE);
                lvToolbarTextPosCustom.setVisibility(View.VISIBLE);

                lvToolbar3.setVisibility(View.GONE);
            });

            final Button btnToolbarTextBoxSize = view.findViewById(R.id.btnToolbarTextBoxSize);
            btnToolbarTextBoxSize.setOnClickListener(v -> {
                //lvToolbarImg.setVisibility(View.GONE);
                lvToolbarZoom.setVisibility(View.GONE);
                lvToolbarTextSize.setVisibility(View.GONE);
                lvToolbarTextPosCustom.setVisibility(View.GONE);
                lvToolbarTextPosGlobal.setVisibility(View.GONE);
                lvToolbarTextAlignment.setVisibility(View.GONE);
                lvToolbarTextShadow.setVisibility(View.GONE);
                lvToolbarTextFont.setVisibility(View.GONE);
                lvToolbarTextColor.setVisibility(View.GONE);
                lvToolbarTextLineHeight.setVisibility(View.GONE);
                lvToolbarTextSel.setVisibility(View.GONE);
                lvToolbarTextBoxSize.setVisibility(View.VISIBLE);

                lvToolbar3.setVisibility(View.GONE);
            });

            final Button btnToolbarTextPosGlobal = view.findViewById(R.id.btnToolbarTextPosGlobal);
            btnToolbarTextPosGlobal.setOnClickListener(v -> {
                //lvToolbarImg.setVisibility(View.GONE);
                lvToolbarZoom.setVisibility(View.GONE);
                lvToolbarTextSize.setVisibility(View.GONE);
                lvToolbarTextPosCustom.setVisibility(View.GONE);
                lvToolbarTextBoxSize.setVisibility(View.GONE);
                lvToolbarTextAlignment.setVisibility(View.GONE);
                lvToolbarTextShadow.setVisibility(View.GONE);
                lvToolbarTextFont.setVisibility(View.GONE);
                lvToolbarTextColor.setVisibility(View.GONE);
                lvToolbarTextLineHeight.setVisibility(View.GONE);
                lvToolbarTextSel.setVisibility(View.GONE);
                lvToolbarTextPosGlobal.setVisibility(View.VISIBLE);

                lvToolbar3.setVisibility(View.GONE);
            });

            final Button btnToolbarTextAlignment = view.findViewById(R.id.btnToolbarTextAlignment);
            btnToolbarTextAlignment.setOnClickListener(v -> {
                //lvToolbarImg.setVisibility(View.GONE);
                lvToolbarZoom.setVisibility(View.GONE);
                lvToolbarTextSize.setVisibility(View.GONE);
                lvToolbarTextPosCustom.setVisibility(View.GONE);
                lvToolbarTextBoxSize.setVisibility(View.GONE);
                lvToolbarTextPosGlobal.setVisibility(View.GONE);
                lvToolbarTextShadow.setVisibility(View.GONE);
                lvToolbarTextFont.setVisibility(View.GONE);
                lvToolbarTextColor.setVisibility(View.GONE);
                lvToolbarTextLineHeight.setVisibility(View.GONE);
                lvToolbarTextSel.setVisibility(View.GONE);
                lvToolbarTextAlignment.setVisibility(View.VISIBLE);

                lvToolbar3.setVisibility(View.GONE);
            });

            final Button btnToolbarTextShadow = view.findViewById(R.id.btnToolbarTextShadow);
            btnToolbarTextShadow.setOnClickListener(v -> {
                //lvToolbarImg.setVisibility(View.GONE);
                lvToolbarZoom.setVisibility(View.GONE);
                lvToolbarTextSize.setVisibility(View.GONE);
                lvToolbarTextPosCustom.setVisibility(View.GONE);
                lvToolbarTextAlignment.setVisibility(View.GONE);
                lvToolbarTextBoxSize.setVisibility(View.GONE);
                lvToolbarTextPosGlobal.setVisibility(View.GONE);
                lvToolbarTextFont.setVisibility(View.GONE);
                lvToolbarTextColor.setVisibility(View.GONE);
                lvToolbarTextLineHeight.setVisibility(View.GONE);
                lvToolbarTextSel.setVisibility(View.GONE);
                lvToolbarTextShadow.setVisibility(View.VISIBLE);

                lvToolbar3.setVisibility(View.GONE);
            });

            final Button btnToolbarTextFont = view.findViewById(R.id.btnToolbarTextFont);
            btnToolbarTextFont.setOnClickListener(v -> {
                //lvToolbarImg.setVisibility(View.GONE);
                lvToolbarZoom.setVisibility(View.GONE);
                lvToolbarTextSize.setVisibility(View.GONE);
                lvToolbarTextPosCustom.setVisibility(View.GONE);
                lvToolbarTextAlignment.setVisibility(View.GONE);
                lvToolbarTextBoxSize.setVisibility(View.GONE);
                lvToolbarTextPosGlobal.setVisibility(View.GONE);
                lvToolbarTextShadow.setVisibility(View.GONE);
                lvToolbarTextColor.setVisibility(View.GONE);
                lvToolbarTextLineHeight.setVisibility(View.GONE);
                lvToolbarTextSel.setVisibility(View.GONE);
                lvToolbarTextFont.setVisibility(View.VISIBLE);

                lvToolbar3.setVisibility(View.GONE);
            });

            final Button btnToolbarTextColor = view.findViewById(R.id.btnToolbarTextColor);
            btnToolbarTextColor.setOnClickListener(v -> {
                //lvToolbarImg.setVisibility(View.GONE);
                lvToolbarZoom.setVisibility(View.GONE);
                lvToolbarTextSize.setVisibility(View.GONE);
                lvToolbarTextPosCustom.setVisibility(View.GONE);
                lvToolbarTextAlignment.setVisibility(View.GONE);
                lvToolbarTextBoxSize.setVisibility(View.GONE);
                lvToolbarTextPosGlobal.setVisibility(View.GONE);
                lvToolbarTextShadow.setVisibility(View.GONE);
                lvToolbarTextFont.setVisibility(View.GONE);
                lvToolbarTextLineHeight.setVisibility(View.GONE);
                lvToolbarTextSel.setVisibility(View.GONE);
                lvToolbarTextColor.setVisibility(View.VISIBLE);

                lvToolbar3.setVisibility(View.GONE);
            });

            final Button btnToolbarTextLineHeight = view.findViewById(R.id.btnToolbarTextLineHeight);
            btnToolbarTextLineHeight.setOnClickListener(v -> {
                //lvToolbarImg.setVisibility(View.GONE);
                lvToolbarZoom.setVisibility(View.GONE);
                lvToolbarTextSize.setVisibility(View.GONE);
                lvToolbarTextPosCustom.setVisibility(View.GONE);
                lvToolbarTextAlignment.setVisibility(View.GONE);
                lvToolbarTextBoxSize.setVisibility(View.GONE);
                lvToolbarTextPosGlobal.setVisibility(View.GONE);
                lvToolbarTextShadow.setVisibility(View.GONE);
                lvToolbarTextFont.setVisibility(View.GONE);
                lvToolbarTextColor.setVisibility(View.GONE);
                lvToolbarTextSel.setVisibility(View.GONE);
                lvToolbarTextLineHeight.setVisibility(View.VISIBLE);

                lvToolbar3.setVisibility(View.GONE);
            });

            final Button btnToolbarTextSel = view.findViewById(R.id.btnToolbarTextSel);
            btnToolbarTextSel.setOnClickListener(v -> {
                //lvToolbarImg.setVisibility(View.GONE);
                lvToolbarZoom.setVisibility(View.GONE);
                lvToolbarTextSize.setVisibility(View.GONE);
                lvToolbarTextPosCustom.setVisibility(View.GONE);
                lvToolbarTextAlignment.setVisibility(View.GONE);
                lvToolbarTextBoxSize.setVisibility(View.GONE);
                lvToolbarTextPosGlobal.setVisibility(View.GONE);
                lvToolbarTextShadow.setVisibility(View.GONE);
                lvToolbarTextFont.setVisibility(View.GONE);
                lvToolbarTextColor.setVisibility(View.GONE);
                lvToolbarTextLineHeight.setVisibility(View.GONE);
                lvToolbarTextSel.setVisibility(View.VISIBLE);

                lvToolbar3.setVisibility(View.GONE);
            });

            //Default: Toolbar1
            //lvToolbarImg.setVisibility(View.GONE);
            lvToolbarZoom.setVisibility(View.GONE);
            lvToolbarTextSize.setVisibility(View.GONE);
            lvToolbarTextPosCustom.setVisibility(View.GONE);
            lvToolbarTextBoxSize.setVisibility(View.GONE);
            lvToolbarTextPosGlobal.setVisibility(View.GONE);
            lvToolbarTextAlignment.setVisibility(View.GONE);
            lvToolbarTextShadow.setVisibility(View.GONE);
            lvToolbarTextFont.setVisibility(View.GONE);
            lvToolbarTextColor.setVisibility(View.GONE);
            lvToolbarTextLineHeight.setVisibility(View.GONE);
            lvToolbarTextSel.setVisibility(View.GONE);

            //Default: Toolbar3
            lvToolbar3.setVisibility(View.GONE);

            //Pre-mesure
            view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

            //Assign a size and position to the view and all of its descendants
            view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

            final ImageButton btnSharePicInfo = view.findViewById(R.id.btnSharePicInfo);
            btnSharePicInfo.setOnClickListener(v -> PCommon.ShowDialog(SharePictureActivity.this, R.string.mnuSharePic, true, null, R.string.sharePicMsg));
            final Button btnSave = view.findViewById(R.id.btnSave);
            btnSave.setOnClickListener(v ->
            {
                if (isSaving) return;

                isSaving = true;
                v.setEnabled(false);

                final Handler handler = new Handler();
                handler.postDelayed(() ->
                {
                    try
                    {
                        final String fullPath = PCommon.ConcaT(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "/thelight-", PCommon.NowYYYYMMDD(), "-", PCommon.TimeFuncShort(), ".png");
                        final File file = new File(fullPath); //TODO: PIC, several ext + name
                        if (file.exists()) if (!file.delete()) return;
                        if (!file.createNewFile()) return;

                        ShowControlsNotToSaveWithImage(false);

                        final Bitmap bitmap = Bitmap.createBitmap(rlImg.getMeasuredWidth(), rlImg.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
                        final Canvas c = new Canvas(bitmap);
                        rlImg.draw(c);

                        final FileOutputStream out = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.PNG, 0, out); //TODO: PIC, several format
                        out.flush();
                        out.close();

                        PCommon.ShowToast(v.getContext(), fullPath, Toast.LENGTH_LONG);

                        //TODO: PIC, BUG WORKS ONLY 1 TIME! (COULD BE A VIEW ON CLICK BUTTON)
                        //ivImgSmall.setImageURI(Uri.fromFile(file));
                    } catch (Exception ex) {
                        if (PCommon._isDebug) PCommon.LogR(v.getContext(), ex);
                    } finally {
                        final Handler handlerFinal = new Handler();
                        handlerFinal.post(() -> {
                            if (isSaving) {
                                v.setEnabled(true);
                                isSaving = false;
                            }

                            ShowControlsNotToSaveWithImage(true);
                         });
                    }
                }, 0);
            });

            setContentView(view);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    //WORKS (get peace of rect)
    /*Rect rect = new Rect();
    rect.left = 0;
    rect.top = 0;
    rect.right = 200;
    rect.bottom = 500;  //intersect => true/false
    ivImg.setClipBounds(rect);
     */

    private void RescaleLayout(final float scalefactor)
    {
        try
        {
            if (scalefactor < 0.1f)
                scaleFactor = 0.1f;
            else if (scalefactor > 1.0f)
                scaleFactor = 1.0f;
            else
                scaleFactor = scalefactor;

            rlImg.setScaleX(scaleFactor);
            rlImg.setScaleY(scaleFactor);
            rlImg.setPivotX(0.0f);
            rlImg.setPivotY(0.0f);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    private void ShowFonts(final LinearLayout lvToolbarFonts, final EditText editText, final IMPACT_TYPEFACE_CHANGE_TYPE impact)
    {
        try
        {
            lvToolbarFonts.setVisibility(View.VISIBLE);
            lvToolbarFonts.removeAllViews();

            final int textColor = PCommon.GetPrefThemeName(context).compareTo("LIGHT") == 0 ? Color.BLACK : Color.WHITE; //TODO: harcoded, need to find from attribute

            Button btnFont;
            Typeface fontEx;
            for (String fontName : context.getResources().getStringArray(R.array.FONT_VALUES_ARRAY))
            {
                if (fontName == null || fontName.isEmpty()) continue;

                fontEx = Typeface.createFromAsset(context.getAssets(), PCommon.ConcaT("fonts/", fontName, ".ttf"));

                btnFont = new Button(context);
                btnFont.setLayoutParams(PCommon._layoutParamsMatch);
                btnFont.setAllCaps(false);
                btnFont.setMinHeight(48);
                btnFont.setText(fontName);
                btnFont.setTextColor(textColor);
                btnFont.setFocusable(true);
                btnFont.setBackground(PCommon.GetDrawable(context, R.drawable.focus_text));
                btnFont.setTag( R.id.tv1, fontName );
                btnFont.setTypeface(fontEx); //Font
                btnFont.setOnClickListener(v -> {
                    final String fontname = (String) v.getTag(R.id.tv1);
                    final Typeface typeface = Typeface.createFromAsset(context.getAssets(), PCommon.ConcaT("fonts/", fontname, ".ttf"));

                    if (impact == IMPACT_TYPEFACE_CHANGE_TYPE.WHOLE_TEXT) {
                        editText.setTypeface(typeface);
                    } else if (impact == IMPACT_TYPEFACE_CHANGE_TYPE.PARTIALLY) {
                        SetSpan(editText, new TypefaceSpan(typeface));
                    }
                });

                lvToolbarFonts.addView(btnFont);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    private void ShowColors(final LinearLayout lvToolbarColors, final EditText editText, final IMPACT_COLOR_CHANGE_TYPE impact)
    {
        try
        {
            lvToolbarColors.setVisibility(View.VISIBLE);
            lvToolbarColors.removeAllViews();

            String textColor;
            Button btnColor;
            Spannable span;
            String[] colorArr;
            for (String colorItem : context.getResources().getStringArray(R.array.COLOR_ARRAY))
            {
                colorArr = colorItem.split("\\|");
                textColor = "ABCDEF".contains(colorArr[1].substring(1, 2)) ? "#000000" : "#FFFFFF";

                span = new SpannableString(PCommon.ConcaT(colorArr[0], " "));
                span.setSpan(PCommon.GetBackgroundColorSpan(colorArr[1]), 0, colorArr[0].length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                span.setSpan(PCommon.GetForegroundColorSpan(textColor), 0, colorArr[0].length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);

                btnColor = new Button(context);
                btnColor.setLayoutParams(PCommon._layoutParamsMatch);
                btnColor.setAllCaps(false);
                btnColor.setMinHeight(48);
                btnColor.setText(span);
                btnColor.setFocusable(true);
                btnColor.setBackground(PCommon.GetDrawable(context, R.drawable.focus_text));
                btnColor.setTag(R.id.tv1, colorArr[1]);
                btnColor.setOnClickListener(v -> {
                    final String colorHex = (String) v.getTag(R.id.tv1);
                    final int colorInt = Color.parseColor(colorHex);
                    shadowColor = colorHex.equalsIgnoreCase("#000000") ? Color.WHITE : Color.BLACK;

                    if (impact == IMPACT_COLOR_CHANGE_TYPE.WHOLE_TEXT_COLOR) {
                        editText.setTextColor(colorInt);
                    } else if (impact == IMPACT_COLOR_CHANGE_TYPE.PARTIALLY_FOREGROUND) {
                        SetSpan(editText, new ForegroundColorSpan(colorInt));
                    } else if (impact == IMPACT_COLOR_CHANGE_TYPE.PARTIALLY_BACKGROUND) {
                        SetSpan(editText, new BackgroundColorSpan(colorInt));
                    }
                });

                lvToolbarColors.addView(btnColor);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    private void SetSpan(final EditText editText, final Object spanType)
    {
        try
        {
            int selStart = editText.getSelectionStart();
            int selEnd = editText.getSelectionEnd();
            if (selEnd < selStart) {
                int tmp = selStart;
                selStart = selEnd;
                selEnd = tmp;
            }
            if (selEnd - selStart <= 0) return;

            final SpannableStringBuilder span = new SpannableStringBuilder(editText.getText());
            span.setSpan(spanType, selStart, selEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            editText.setText(span);
            editText.setSelection(selStart, selEnd);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    private void SetRule(final EditText editText, final ToggleButton toggleTop, final ToggleButton toggleLeft, final ToggleButton toggleBottom, final ToggleButton toggleRight)
    {
        try
        {
            final int etWidth = editText.getWidth();
            final int etHeight = editText.getHeight();
            final int ivWidth = ivImg.getWidth();
            final int ivHeight = ivImg.getHeight();

            //Work only with left/top margins
            int newLeftMargin = 0;
            int newTopMargin = 0;

            //Init
            final ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) editText.getLayoutParams();
            mlp.topMargin = 0;
            mlp.leftMargin = 0;
            mlp.bottomMargin = 0;
            mlp.rightMargin = 0;

            //CENTER HORIZ
            if (toggleLeft.isChecked() && toggleRight.isChecked()) newLeftMargin = (ivWidth - etWidth) / 2;

            //CENTER VERT
            if (toggleTop.isChecked() && toggleBottom.isChecked()) newTopMargin = (ivHeight - etHeight) / 2;

            //LEFT but not RIGHT
            if (toggleLeft.isChecked() && !toggleRight.isChecked()) newLeftMargin = 0;

            //RIGHT but not LEFT
            if (!toggleLeft.isChecked() && toggleRight.isChecked()) newLeftMargin = ivWidth - etWidth;

            //TOP but not BOTTOM
            if (toggleTop.isChecked() && !toggleBottom.isChecked()) newTopMargin = 0;

            //BOTTOM but not TOP
            if (!toggleTop.isChecked() && toggleBottom.isChecked()) newTopMargin = ivHeight - etHeight;

            //APPLY
            mlp.leftMargin = (newLeftMargin < 0) ? 0 : newLeftMargin;
            mlp.topMargin =  (newTopMargin < 0) ? 0 : newTopMargin;
            editText.setLayoutParams(mlp);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Show/Hide controls not to save with image
     * @param toShow    True to show, False to hide
     */
    private void ShowControlsNotToSaveWithImage(final boolean toShow)
    {
        try
        {
            if (toShow)
            {
                etVerse.setBackgroundResource(R.drawable.configure_tv_border);
                etVerse.setCursorVisible(true);
            }
            else
            {
                etVerse.setBackground(null);
                etVerse.setCursorVisible(false);
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }
}
