package com.appcoholic.bible;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;

public class BibleWidgetAudioProvider extends AppWidgetProvider
{
    @Override
    public void onUpdate(final Context context, final AppWidgetManager appWidgetManager, final int[] appWidgetIds)
    {
        try
        {
            if (PCommon._isDebug) System.out.println("onUpdate => count: " + appWidgetIds.length);

            final String bbName = "k";
            final String vRef = "~Gen 1.1";
            for (int appWidgetId : appWidgetIds)
            {
                CommonWidgetAudio.UpdateAppWidget(context, appWidgetId, bbName, vRef);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }
}
