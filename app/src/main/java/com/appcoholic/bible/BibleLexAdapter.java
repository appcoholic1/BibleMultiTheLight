package com.appcoholic.bible;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Map;

class BibleLexAdapter extends RecyclerView.Adapter<BibleLexAdapter.ViewHolder>
{
    private ArrayList<LexTbesBO> lstLex = null;
    private PCommon.LEX_DETAIL_SEARCH_FIELD lexDetailSearchField = null;
    private SCommon _s = null;
    private Context context = null;
    private Activity activity = null;
    private StyleBO dlgStyle = null;

    /***
     * BibleLexAdapter
     * @param word      Expr to search (will be not trimmed)
     */
    BibleLexAdapter(final Activity currentActivity, final String word, final StyleBO dialogStyle)
    {
        final Context ctx = currentActivity.getApplicationContext();

        try
        {
            CheckLocalInstance(ctx);

            activity = currentActivity;
            dlgStyle = dialogStyle;

            Map<String, Object> mapLex = _s.GetLexDetailByWordExtended(word);
            lstLex = (ArrayList<LexTbesBO>) mapLex.get("LEX");
            lexDetailSearchField = (PCommon.LEX_DETAIL_SEARCH_FIELD) mapLex.get("LEX_EXPR");
            mapLex.clear();
            mapLex = null;

            if (lstLex == null || lstLex.isEmpty()) return;
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(ctx, ex);
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        private final TextView tv_text;

        ViewHolder(View view)
        {
            super(view);

            context = view.getContext();
            tv_text = view.findViewById(R.id.tv_text);

            final Typeface typeface = PCommon.GetTypeface(view.getContext());
            if (typeface != null) tv_text.setTypeface(typeface, Typeface.NORMAL);

            final int fontSize = PCommon.GetFontSize(view.getContext());
            tv_text.setTextSize(fontSize);
        }
    }

    @NonNull
    @Override
    public BibleLexAdapter.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int viewType)
    {
        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.lexicon_recipient, viewGroup, false);
        return new BibleLexAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final BibleLexAdapter.ViewHolder viewHolder, int position)
    {
        try
        {
            final LexTbesBO lex = lstLex.get(position);
            if (lex == null) return;

            if (lexDetailSearchField == PCommon.LEX_DETAIL_SEARCH_FIELD.HG_WORD)
            {
                viewHolder.tv_text.setText(PCommon.ConcaT(lex.hg, " (", lex.transliteration, ")"));
            } else if (lexDetailSearchField == PCommon.LEX_DETAIL_SEARCH_FIELD.HG_NR) {
                viewHolder.tv_text.setText(PCommon.ConcaT(lex.dStrong, " (", lex.transliteration, ")"));
            } else {
                viewHolder.tv_text.setText(PCommon.ConcaT(lex.english, " (", lex.transliteration, ")"));
            }

            viewHolder.tv_text.setOnClickListener(v -> {
                PCommon.ShowInterlinearWordDetail((FragmentActivity) activity, lex.dStrong, PCommon.LEX_DETAIL_SEARCH_TYPE.EXACT_SEARCH, dlgStyle);
            });
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    @Override
    public int getItemCount()
    {
        return lstLex == null ? 0 : lstLex.size();
    }

    private void CheckLocalInstance(final Context context)
    {
        try
        {
            if (_s == null)
            {
                _s = SCommon.GetInstance(context);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }
}