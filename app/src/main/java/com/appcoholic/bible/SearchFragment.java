
package com.appcoholic.bible;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.cursoradapter.widget.SimpleCursorAdapter; //Recompile if problem here
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class SearchFragment extends Fragment
{
    private Context _context = null;
    private SCommon _s = null;
    private View v;
    private final FRAGMENT_TYPE fragmentType;
    private TextView tvBookTitle;
    private TextView btnBack;
    private TextView btnForward;
    private TextView btnRndRefresh;
    private TextView btnRndSetup;
    private TextView btnLexInfo;
    private ImageButton btnLexContent;
    private ImageButton btnLexContentDump;
    private ImageButton btnSearchContent;
    private ConstraintLayout clHarm;
    private TextView btnHarmMt;
    private TextView btnHarmMc;
    private TextView btnHarmLc;
    private TextView btnHarmJn;
    private SimpleCursorAdapter cursorAdapter;
    private MatrixCursor matrixCursor;
    private String searchFullQuery;
    private SearchView searchView = null;
    @SuppressLint("StaticFieldLeak")
    private static RecyclerView recyclerView;
    private RecyclerView.Adapter recyclerViewAdapter;
    private ArrayList<ShortSectionBO> lstArtShortSection;

    private boolean isBook = false, isChapter = false, isVerse = false, isPrbl = false, isRnd = false, isHarmony = false, isLex = false, isSearch = false;
    private int     harmId = -1,    bNumber = 0,     cNumber = 0,        vNumber = 0,       scrollPosY = 0;
    private String  bbName = null, tabTitle = null, trad = null;

    //private boolean isChapterListLoading = false;

    private int planId = -1, planDayNumber = -1, planPageNumber = -1;

    public enum FRAGMENT_TYPE {
        FAV_TYPE,
        SEARCH_TYPE,
        ARTICLE_TYPE,
        PLAN_TYPE
    }

    enum CLIPBOARD_ADD_TYPE {
        VERSE,
        CHAPTER,
        ALL
    }

    public SearchFragment() {
        fragmentType = FRAGMENT_TYPE.SEARCH_TYPE;
    }

    public SearchFragment(final FRAGMENT_TYPE type) {
        fragmentType = type;
    }

    static int GetScrollPosY()
    {
        final LinearLayoutManager lm = (LinearLayoutManager) recyclerView.getLayoutManager();
        if (lm == null)
            return 0;

        return lm.findFirstVisibleItemPosition();
    }

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, final ViewGroup container, final Bundle bundle)
    {
        try
        {
            super.onCreateView(inflater, container, bundle);

            CheckLocalInstance();

            v = inflater.inflate(R.layout.fragment_search, container, false);
            setHasOptionsMenu(true);

            tvBookTitle = v.findViewById(R.id.tvBookTitle);
            btnBack = v.findViewById(R.id.btnBack);
            btnForward = v.findViewById(R.id.btnForward);
            btnRndRefresh = v.findViewById(R.id.btnRndRefresh);
            btnRndSetup = v.findViewById(R.id.btnRndSetup);
            btnLexInfo = v.findViewById(R.id.btnLexInfo);
            btnLexContent = v.findViewById(R.id.btnLexContent);
            btnLexContent.setOnClickListener(v -> ShowBookJump(v.getContext()));
            btnLexContentDump = v.findViewById(R.id.btnLexContentDump);
            btnSearchContent = v.findViewById(R.id.btnSearchContent);
            btnSearchContent.setOnClickListener(v -> ShowBookJump(v.getContext()));
            clHarm = v.findViewById(R.id.clHarm);
            //btnHarmSetup = v.findViewById(R.id.btnHarmSetup);
            btnHarmMt = v.findViewById(R.id.btnHarmMt);
            btnHarmMc = v.findViewById(R.id.btnHarmMc);
            btnHarmLc = v.findViewById(R.id.btnHarmLc);
            btnHarmJn = v.findViewById(R.id.btnHarmJn);
            btnBack.setOnClickListener(v -> {
                SetLocalBibleName();

                if (fragmentType == FRAGMENT_TYPE.SEARCH_TYPE)
                {
                    if (isBook && isChapter)
                    {
                        btnForward.setEnabled(true);

                        if (cNumber > 1)
                        {
                            scrollPosY = 0;
                            cNumber--;
                            searchFullQuery = PCommon.ConcaT(bNumber, " ", cNumber);
                            ShowChapter(bbName, bNumber, cNumber);
                        }
                        else
                        {
                            if (bNumber > 1)
                            {
                                final int bNumberTry = bNumber - 1;
                                final int cNumberTry = _s.GetBookChapterMax(bNumberTry);
                                if (IsChapterExist(bbName, bNumberTry, cNumberTry))
                                {
                                    scrollPosY = 0;
                                    bNumber = bNumberTry;
                                    cNumber = cNumberTry;
                                    searchFullQuery = PCommon.ConcaT(bNumber, " ", cNumber);
                                    ShowChapter(bbName, bNumber, cNumber);
                                }
                            }
                            else
                            {
                                btnBack.setEnabled(false);
                            }
                        }
                    }
                    else if (isPrbl)
                    {
                        btnForward.setEnabled(true);
                        ShowPrbl(-1);
                        btnBack.requestFocus();
                    }
                    else if (isHarmony)
                    {
                        btnForward.setEnabled(true);
                        ShowHarmony(-1);
                        btnBack.requestFocus();
                    }
                }
                else if (fragmentType == FRAGMENT_TYPE.PLAN_TYPE)
                {
                    btnForward.setEnabled(true);
                    ShowPlan(-1);
                }
                else if (fragmentType == FRAGMENT_TYPE.ARTICLE_TYPE)
                {
                    btnForward.setEnabled(true);
                    ShowArticle(-1);
                }
            });
            btnForward.setOnClickListener(v -> {
                SetLocalBibleName();

                if (fragmentType == FRAGMENT_TYPE.SEARCH_TYPE)
                {
                    if (isBook && isChapter)
                    {
                        btnBack.setEnabled(true);

                        int cNumberTry = cNumber + 1;
                        if (IsChapterExist(bbName, bNumber, cNumberTry))
                        {
                            scrollPosY = 0;
                            cNumber = cNumberTry;
                            searchFullQuery = PCommon.ConcaT(bNumber, " ", cNumber);
                            ShowChapter(bbName, bNumber, cNumber);
                        }
                        else
                        {
                            final int bNumberTry = bNumber + 1;
                            cNumberTry = 1;
                            if (IsChapterExist(bbName, bNumberTry, cNumberTry))
                            {
                                scrollPosY = 0;
                                bNumber = bNumberTry;
                                cNumber = cNumberTry;
                                searchFullQuery = PCommon.ConcaT(bNumber, " ", cNumber);
                                ShowChapter(bbName, bNumber, cNumber);
                            }
                            else
                            {
                                btnForward.setEnabled(false);
                            }
                        }
                    }
                    else if (isPrbl)
                    {
                        btnBack.setEnabled(true);
                        ShowPrbl(1);
                        btnForward.requestFocus();
                    }
                    else if (isHarmony)
                    {
                        btnBack.setEnabled(true);
                        ShowHarmony(1);
                        btnForward.requestFocus();
                    }
                }
                else if (fragmentType == FRAGMENT_TYPE.PLAN_TYPE)
                {
                    btnBack.setEnabled(true);
                    ShowPlan(1);
                }
                else if (fragmentType == FRAGMENT_TYPE.ARTICLE_TYPE)
                {
                    btnBack.setEnabled(true);
                    ShowArticle(1);
                }
            });
            btnRndRefresh.setOnClickListener(v -> {
                bbName = PCommon.GetPrefBibleName(v.getContext());
                SearchBible(false);
            });
            btnRndSetup.setOnClickListener(v -> ShowRndSetup(v.getContext()));
            btnLexInfo.setOnClickListener(v -> PCommon.ShowInterlinearWordDetail(this.requireActivity(), searchFullQuery, PCommon.LEX_DETAIL_SEARCH_TYPE.EXACT_SEARCH, null));
            btnHarmMt.setOnClickListener(v -> {
                if (v.getTag() == null) return;
                recyclerView.scrollToPosition((int) v.getTag());
            });
            btnHarmMc.setOnClickListener(v -> {
                if (v.getTag() == null) return;
                recyclerView.scrollToPosition((int) v.getTag());
            });
            btnHarmLc.setOnClickListener(v -> {
                if (v.getTag() == null) return;
                recyclerView.scrollToPosition((int) v.getTag());
            });
            btnHarmJn.setOnClickListener(v -> {
                if (v.getTag() == null) return;
                recyclerView.scrollToPosition((int) v.getTag());
            });

            if (fragmentType == FRAGMENT_TYPE.SEARCH_TYPE)
            {
                //Search book autocomplete
                matrixCursor = new MatrixCursor(new String[]{"_id", "text"});
                String[] from = {"text"};
                int[] to = {android.R.id.text1};                                                                         //android.R.ciId.text1,  R.ciId.listBnameEntry

                cursorAdapter = new SimpleCursorAdapter(_context, PCommon.GetPrefThemeName(_context).compareTo("LIGHT") == 0 ? R.layout.book_entry_light : R.layout.book_entry_dark, matrixCursor, from, to, SimpleCursorAdapter.NO_SELECTION); //android.R.layout.select_dialog_item,      R.layout.book_entry)
                cursorAdapter.setFilterQueryProvider(query -> {
                    if (PCommon._isDebug) System.out.println(PCommon.ConcaT("RunQuery: ", query));

                    return GetCursor(query);
                });
            }

            ShowBookTitle(false, false, false, false, false);   //May change in resume
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            if (PCommon._isDebug) System.out.println(PCommon.ConcaT("SearchFragment: OnCreateView, tab:", MainActivity.Tab.GetCurrentTabPosition()));
        }

        return v;
    }

    @Override
    public void onResume()
    {
        try
        {
            super.onResume();

            CheckLocalInstance();
            SetLocalBibleName();

            if (fragmentType == FRAGMENT_TYPE.FAV_TYPE)
            {
                tabTitle = _context.getString(R.string.favHeader);
                searchFullQuery = null;
                scrollPosY = 0;
                isBook = false;
                isChapter = false;
                isVerse = false;
                bNumber = 0;
                cNumber = 0;
                vNumber = 0;
                trad = bbName;
                harmId = -1;

                final CacheTabBO t = _s.GetCurrentCacheTab();
                if (t != null)
                {
                    searchFullQuery = t.fullQuery;
                    scrollPosY = t.scrollPosY;
                }

                //Set objects
                SetTabTitle(tabTitle);
                ShowBookTitle(false, false, false, false, false);
                CreateRecyclerView();

                final int favOrder = PCommon.GetFavOrder(_context);
                final int favType = PCommon.GetFavFilter(_context);
                recyclerViewAdapter = new BibleAdapter(_context, bbName, searchFullQuery, favOrder, favType);
                if (WhenTabIsEmptyOrNull(true)) return;
                recyclerView.setAdapter(recyclerViewAdapter);
                recyclerView.setHasFixedSize(true);
                recyclerView.scrollToPosition(scrollPosY);

                return;
            }
            else if (fragmentType == FRAGMENT_TYPE.PLAN_TYPE)
            {
                final CacheTabBO t = _s.GetCurrentCacheTab();
                if (t == null)
                {
                    WhenTabIsEmptyOrNull(false);
                    return;
                }

                tabTitle = t.tabTitle;
                searchFullQuery = t.fullQuery;
                scrollPosY = t.scrollPosY;
                bbName = t.bbName;
                isBook = t.isBook;
                isChapter = t.isChapter;
                isVerse = t.isVerse;
                bNumber = t.bNumber;
                cNumber = t.cNumber;
                vNumber = t.vNumber;
                trad = t.trad;
                harmId = -1;

                @SuppressWarnings("UnusedAssignment") String title = null;
                final String[] cols = searchFullQuery.split("\\s");
                if (cols.length == 3)
                {
                    planId = Integer.parseInt(cols[0]);
                    planDayNumber = Integer.parseInt(cols[1]);
                    planPageNumber = Integer.parseInt(cols[2]);
                    PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.PLAN_ID, planId);
                    PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.PLAN_PAGE, planPageNumber);
                    final PlanDescBO pd = _s.GetPlanDesc(planId);
                    if (pd != null)
                    {
                        final PlanCalBO pc = _s.GetPlanCalByDay(bbName, planId, planDayNumber);
                        if (pc != null)
                        {
                            title = PCommon.ConcaT(_context.getString(R.string.planCalTitleDay).toUpperCase(), " ", pc.dayNumber, "/", pd.dayCount, "  ");
                            tvBookTitle.setText(title);
                            ShowBookTitle(true, false, false, false, false, pc);
                        }
                    }
                    else
                    {
                        t.tabType = "S";
                        _s.SaveCacheTab(t);

                        ShowBookTitle(false, false, false, false, false);
                    }
                }

                //Set objects
                SetTabTitle(tabTitle);

                SearchBible(true);

                return;
            }
            else if (fragmentType == FRAGMENT_TYPE.ARTICLE_TYPE)
            {
                tabTitle = _context.getString(R.string.tabTitleDefault);
                searchFullQuery = null;
                scrollPosY = 0;
                isBook = false;
                isChapter = false;
                isVerse = false;
                bNumber = 0;
                cNumber = 0;
                vNumber = 0;
                trad = bbName;
                harmId = -1;

                final CacheTabBO t = _s.GetCurrentCacheTab();
                if (t != null)
                {
                    tabTitle = t.tabTitle;
                    searchFullQuery = t.fullQuery;
                    scrollPosY = t.scrollPosY;
                }

                //Set objects
                SetTabTitle(tabTitle);

                if (PCommon._isDebug) System.out.println(PCommon.ConcaT("searchFullQuery:'", searchFullQuery, "', is null:", searchFullQuery == null));
                final int artId = PCommon.GetResId(_context, searchFullQuery);
                final String artTitle = searchFullQuery == null || (!searchFullQuery.toUpperCase().startsWith("ART")) ? null : _context.getString(artId).toUpperCase();
                if (artTitle != null) tvBookTitle.setText(artTitle);

                ShowBookTitle(searchFullQuery != null && searchFullQuery.toUpperCase().startsWith("ART"), false, false, false, false);

                CreateRecyclerView();

                //Show article
                final ArtOriginalContentBO artOriginalContent = GetArticle(t);
                recyclerViewAdapter = new BibleArticleAdapter(getActivity(), bbName, artOriginalContent);
                recyclerView.setAdapter(recyclerViewAdapter);
                recyclerView.setHasFixedSize(true);
                recyclerView.scrollToPosition(scrollPosY);

                lstArtShortSection = ((BibleArticleAdapter) recyclerViewAdapter).GetArticleShortSections();

                return;
            }

            //Search type
            //Get tab info
            final CacheTabBO t = _s.GetCurrentCacheTab();
            if (t == null)
            {
                WhenTabIsEmptyOrNull(false);
                return;
            }

            //tabNumber is critical => function
            tabTitle = t.tabTitle;
            searchFullQuery = t.fullQuery;
            scrollPosY = t.scrollPosY;
            bbName = t.bbName;
            isBook = t.isBook;
            isChapter = t.isChapter;
            isVerse = t.isVerse;
            bNumber = t.bNumber;
            cNumber = t.cNumber;
            vNumber = t.vNumber;
            trad = t.trad;

            //Set objects
            SetTabTitle(tabTitle);

            isPrbl = fragmentType == FRAGMENT_TYPE.SEARCH_TYPE && GetPrblName(searchFullQuery) != null;
            isRnd = fragmentType == FRAGMENT_TYPE.SEARCH_TYPE && searchFullQuery.equalsIgnoreCase("RAND:");
            isHarmony = fragmentType == FRAGMENT_TYPE.SEARCH_TYPE && IsHarmonyQuery(searchFullQuery);
            isLex = fragmentType == FRAGMENT_TYPE.SEARCH_TYPE && IsLexQuery(searchFullQuery);
            isSearch = fragmentType == FRAGMENT_TYPE.SEARCH_TYPE && (!isPrbl) && (!isRnd) && (!isHarmony) && (!isLex);

            ShowBookTitle(isPrbl, isRnd, isHarmony, isLex, isSearch);

            if ((isBook && isChapter) || isPrbl || isHarmony)
            {
                SearchBible(false);
            }
            else if (isRnd || isLex)
            {
                SearchBible(true);
            }
            else //Search
            {
                SearchBible(true);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
        finally
        {
            if (PCommon._isDebug) System.out.println(PCommon.ConcaT("SearchFragment: OnResume, tab:", MainActivity.Tab.GetCurrentTabPosition()));
        }
    }

    private boolean WhenTabIsEmptyOrNull(final boolean shouldCheckRecycler)
    {
        try
        {
            if (!shouldCheckRecycler)
            {
                CreateRecyclerView();
            }
            else
            {
                if (recyclerViewAdapter != null && recyclerViewAdapter.getItemCount() > 0)
                {
                    return false;
                }
            }

            final String content = _context.getString(R.string.tabEmpty);
            final ArtOriginalContentBO artOriginalContent = new ArtOriginalContentBO("", content);
            recyclerViewAdapter = new BibleArticleAdapter(getActivity(), bbName, artOriginalContent);
            recyclerView.setAdapter(recyclerViewAdapter);
            recyclerView.setHasFixedSize(true);
            recyclerView.scrollToPosition(0);

            return true;
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return false;
    }

    @Override
    public void onCreateContextMenu(@NonNull final ContextMenu menu, @NonNull final View v, final ContextMenu.ContextMenuInfo menuInfo)
    {
        try
        {
            super.onCreateContextMenu(menu, v, menuInfo);

            final Context context = v.getContext();
            final MenuInflater menuInflater = requireActivity().getMenuInflater();
            menuInflater.inflate(R.menu.context_menu_search, menu);

            final int bibleId = Integer.parseInt(PCommon.GetPref(context, IProject.APP_PREF_KEY.BIBLE_ID, "0"));
            final VerseBO verse = (bibleId > -1) ? _s.GetVerse(bibleId) : null;
            final boolean bible_cmd_visibility = (bibleId > -1);
            menu.findItem(R.id.mnu_open).setVisible(bible_cmd_visibility);
            menu.findItem(R.id.mnu_clipboard).setVisible(bible_cmd_visibility);
            menu.findItem(R.id.mnu_interlinear).setVisible(bible_cmd_visibility);
            menu.findItem(R.id.mnu_ask_biblegpt).setVisible(bible_cmd_visibility);


            //~~~ Cross references
            final boolean cr_cmd_visibility = verse != null && (_s.GetCrossReferencesCount(verse.bNumber, verse.cNumber, verse.vNumber) > 0);
            menu.findItem(R.id.mnu_cr).setVisible(cr_cmd_visibility);


            //~~~ Listen

            //~~~ Edit items
            final int editStatus = PCommon.GetEditStatus(context);
            final int editArtId = PCommon.GetEditArticleId(context);
            //TODO NEXT: Create customized view -- final String editArtName = editStatus == 1 ? _s.GetMyArticleName(editArtId) : "";         //"<small>Un example</small>" : "";
            final int tabArtId = editStatus == 1 && fragmentType == FRAGMENT_TYPE.ARTICLE_TYPE && tabTitle.startsWith(context.getString(R.string.tabMyArtPrefix))
                    ? Integer.parseInt(tabTitle.replaceAll(context.getString(R.string.tabMyArtPrefix), ""))
                    : -1;
            final String editTitle = editStatus == 0 ? context.getString(R.string.mnuEditOn) :
                        PCommon.ConcaT(context.getString(R.string.mnuEditOff),
                        " (",
                        context.getString(R.string.tabMyArtPrefix),
                        editArtId,
                        ")");

            final boolean edit_art_cmd_visibility = editStatus == 1 && editArtId == tabArtId;
            final boolean edit_search_cmd_visibility = editStatus == 1 && (fragmentType == FRAGMENT_TYPE.SEARCH_TYPE || fragmentType == FRAGMENT_TYPE.PLAN_TYPE);
            final boolean edit_fav_cmd_visibility = editStatus == 1 && fragmentType == FRAGMENT_TYPE.FAV_TYPE;
            menu.findItem(R.id.mnu_edit_select_from).setVisible(edit_search_cmd_visibility);
            menu.findItem(R.id.mnu_edit_select_to).setVisible(edit_search_cmd_visibility);
            menu.findItem(R.id.mnu_edit_select_from_to).setVisible(edit_search_cmd_visibility || edit_fav_cmd_visibility);
            menu.findItem(R.id.mnu_edit_move).setVisible(edit_art_cmd_visibility);
            menu.findItem(R.id.mnu_edit_add).setVisible(edit_art_cmd_visibility);
            menu.findItem(R.id.mnu_edit_update).setVisible(edit_art_cmd_visibility);
            menu.findItem(R.id.mnu_edit_remove).setVisible(edit_art_cmd_visibility);


            //~~~ Article
            if (fragmentType == FRAGMENT_TYPE.ARTICLE_TYPE)
            {
                menu.findItem(R.id.mnu_fav).setVisible(false);
            }


            //~~~ Listen & Edit
            final int INSTALL_STATUS = PCommon.GetInstallStatus(context);
            if (INSTALL_STATUS != PCommon.GetInstallStatusShouldBe())
            {
                menu.findItem(R.id.mnu_listen).setVisible(false);
                menu.findItem(R.id.mnu_edit).setVisible(false);
            }
            else
            {
                menu.findItem(R.id.mnu_listen).setVisible(true);
                menu.findItem(R.id.mnu_edit).setTitle(editTitle).setVisible(true);                  //.setActionView(textView);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    @Override
    public boolean onContextItemSelected(@NonNull final MenuItem item)
    {
        try
        {
            final Context context = requireContext();
            final int itemId = item.getItemId();
            final int bibleId = Integer.parseInt(PCommon.GetPref(context, IProject.APP_PREF_KEY.BIBLE_ID, "0"));
            final int position = Integer.parseInt(PCommon.GetPref(context, IProject.APP_PREF_KEY.VIEW_POSITION, "0"));
            final VerseBO verse = (bibleId > -1) ? _s.GetVerse(bibleId) : null;

            if (itemId == R.id.mnu_ask_biblegpt){
                if (verse == null) return false;
                Intent intent = new Intent(context, DefaultMessagesActivity.class);
                intent.putExtra("reference", "Context: " + verse.bName + " " + verse.cNumber + "." + verse.vNumber);
                startActivity(intent);
            }
            else if (itemId == R.id.mnu_open) {
                if (verse == null) return false;

                final String mnuOpenVerse = context.getString(R.string.mnuOpenVerse);
                final String mnuOpenChapter = context.getString(R.string.mnuOpenChapter);
                final String mnuOpenResult = context.getString(R.string.mnuOpenResult);

                final ArrayList<String> mnuOpen = new ArrayList<>();
                if (fragmentType != FRAGMENT_TYPE.ARTICLE_TYPE) mnuOpen.add(mnuOpenVerse);
                mnuOpen.add(mnuOpenChapter);
                if (fragmentType != FRAGMENT_TYPE.ARTICLE_TYPE && fragmentType != FRAGMENT_TYPE.FAV_TYPE && (!isRnd) && (!isHarmony))
                    mnuOpen.add(mnuOpenResult);

                final AlertDialog builderOpenMenu = new AlertDialog.Builder(context).create();
                PCommon.ShowMenu(builderOpenMenu, R.string.mnuOpen, mnuOpen, null);
                builderOpenMenu.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    final AlertDialog builder = new AlertDialog.Builder(context).create();
                    final LayoutInflater inflater = requireActivity().getLayoutInflater();
                    final View vllLanguages = inflater.inflate(R.layout.fragment_languages_multi, (ViewGroup) requireActivity().findViewById(R.id.llLanguages));

                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        final String mnu_dialog = PCommon.GetPref(context, IProject.APP_PREF_KEY.MENU_DIALOG, "");
                        if (mnu_dialog.compareTo(mnuOpenVerse) == 0) {
                            final String msg = PCommon.ConcaT(context.getString(R.string.mnuOpenVerse), "");
                            PCommon.SelectBibleLanguageMulti(builder, context, vllLanguages, msg, "", true, false);
                            builder.setOnDismissListener(dialogInterface1 -> {
                                final Context dlgContext = ((AlertDialog) dialogInterface1).getContext();
                                final String bbname = PCommon.GetPref(dlgContext, IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, verse.bbName);
                                if (bbname.isEmpty()) return;
                                final String fullquery = PCommon.ConcaT(verse.bNumber, " ", verse.cNumber, " ", verse.vNumber);
                                final String tbbName = PCommon.GetPrefTradBibleName(dlgContext, true);
                                MainActivity.Tab.AddTab(dlgContext, tbbName, verse.bNumber, verse.cNumber, fullquery, verse.vNumber);
                            });
                            builder.show();
                        } else if (mnu_dialog.compareTo(mnuOpenChapter) == 0) {
                            final String msg = PCommon.ConcaT(context.getString(R.string.mnuOpenChapter), "");
                            PCommon.SelectBibleLanguageMulti(builder, context, vllLanguages, msg, "", true, false);
                            builder.setOnDismissListener(dialogInterface12 -> {
                                final Context dlgContext = ((AlertDialog) dialogInterface12).getContext();
                                final String bbname = PCommon.GetPref(dlgContext, IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, verse.bbName);
                                if (bbname.isEmpty()) return;
                                final String fullquery = PCommon.ConcaT(verse.bNumber, " ", verse.cNumber);
                                final String tbbName = PCommon.GetPrefTradBibleName(dlgContext, true);
                                MainActivity.Tab.AddTab(dlgContext, tbbName, verse.bNumber, verse.cNumber, fullquery, verse.vNumber);
                            });
                            builder.show();
                        } else if (mnu_dialog.compareTo(mnuOpenResult) == 0) {
                            final int tabIdFrom = tabNumber();
                            if (tabIdFrom >= 0) {
                                final String msg = PCommon.ConcaT(context.getString(R.string.mnuOpenResult), "");
                                PCommon.SelectBibleLanguageMulti(builder, context, vllLanguages, msg, "", true, false);
                                builder.setOnDismissListener(dialogInterface13 -> {
                                    final Context dlgContext = ((AlertDialog) dialogInterface13).getContext();
                                    final String bbname = PCommon.GetPref(dlgContext, IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, verse.bbName);
                                    if (bbname.isEmpty()) return;
                                    final String tbbName = PCommon.GetPrefTradBibleName(dlgContext, true);

                                    if (isVerse) {
                                        final String fullquery = PCommon.ConcaT(verse.bNumber, " ", verse.cNumber, " ", verse.vNumber);
                                        MainActivity.Tab.AddTab(dlgContext, tbbName, verse.bNumber, verse.cNumber, fullquery, verse.vNumber);
                                    } else if (isChapter) {
                                        final String fullquery = PCommon.ConcaT(verse.bNumber, " ", verse.cNumber);
                                        MainActivity.Tab.AddTab(dlgContext, tbbName, verse.bNumber, verse.cNumber, fullquery, verse.vNumber);
                                    } else {
                                        MainActivity.Tab.AddTab(dlgContext, tabIdFrom, tbbName, verse.vNumber);
                                    }
                                });
                                builder.show();
                            }
                        }
                    }
                });
            } else if (itemId == R.id.mnu_clipboard) {
                if (verse == null) return false;

                final ClipboardBO clipboard = new ClipboardBO(context);
                final String mnuCopyVerseToClipboard = context.getString(R.string.mnuAddVerseToClipboard);
                final String mnuCopyChapterToClipboard = context.getString(R.string.mnuAddChapterToClipboard);
                final String mnuCopyResultToClipboard = context.getString(R.string.mnuAddResultToClipboard);
                final String mnuClearClipboard = context.getString(R.string.mnuClipboardClear);
                final String mnuShareClipboard = context.getString(R.string.mnuShare);

                final ArrayList<String> mnuCopy = new ArrayList<>();
                if (fragmentType != FRAGMENT_TYPE.ARTICLE_TYPE)
                    mnuCopy.add(mnuCopyVerseToClipboard);
                mnuCopy.add(mnuCopyChapterToClipboard);
                if (fragmentType != FRAGMENT_TYPE.ARTICLE_TYPE)
                    mnuCopy.add(mnuCopyResultToClipboard);
                mnuCopy.add(mnuClearClipboard);
                mnuCopy.add(mnuShareClipboard);

                final AlertDialog builderCopyMenu = new AlertDialog.Builder(context).create();
                PCommon.ShowMenu(builderCopyMenu, R.string.mnuClipboard, mnuCopy, null);
                builderCopyMenu.setOnDismissListener(dialogInterface -> {
                    final String mnu_dialog = PCommon.GetPref(context, IProject.APP_PREF_KEY.MENU_DIALOG, "");
                    if (mnu_dialog.compareTo(mnuShareClipboard) == 0) {
                        final String textToShare = clipboard.GenerateTextForClipboard(false);
                        PCommon.ShareText(context, textToShare);
                    } else if (mnu_dialog.compareTo(mnuClearClipboard) == 0) {
                        clipboard.ClearClipboard();
                        builderCopyMenu.show();
                    } else if (mnu_dialog.compareTo(mnuCopyVerseToClipboard) == 0) {
                        clipboard.AddToClipboard(bibleId, CLIPBOARD_ADD_TYPE.VERSE, null);
                    } else if (mnu_dialog.compareTo(mnuCopyChapterToClipboard) == 0) {
                        clipboard.AddToClipboard(bibleId, CLIPBOARD_ADD_TYPE.CHAPTER, null);
                    } else if (mnu_dialog.compareTo(mnuCopyResultToClipboard) == 0) {
                        final int tabIdFrom = tabNumber();
                        if (tabIdFrom >= 0) {
                            if (isVerse) {
                                clipboard.AddToClipboard(bibleId, CLIPBOARD_ADD_TYPE.VERSE, null);
                            } else if (isChapter) {
                                clipboard.AddToClipboard(bibleId, CLIPBOARD_ADD_TYPE.CHAPTER, null);
                            } else {
                                final CacheTabBO t = _s.GetCurrentCacheTab();
                                if (t == null) return;

                                final String searchString = t.fullQuery;
                                ArrayList<Integer> lstAllId = new ArrayList<>();

                                if (fragmentType == FRAGMENT_TYPE.FAV_TYPE) {
                                    //Fav
                                    final int favOrder = 0;
                                    final int favType = PCommon.GetFavFilter(context);
                                    final ArrayList<VerseBO> lstVerse = _s.SearchFav(verse.bbName, searchString, favOrder, favType);
                                    for (VerseBO verse1 : lstVerse) {
                                        lstAllId.add(verse1.id);
                                    }
                                } else {
                                    //CR
                                    final String[] words = searchString.split("\\s");
                                    final String patternDigit = "\\d+";
                                    if (words.length == 4 && (words[0].matches(patternDigit) && words[1].matches(patternDigit) && words[2].matches(patternDigit) && words[3].equalsIgnoreCase("CR:"))) {
                                        final Map<String, Object> mapSearchBible = _s.GetCrossReferences(verse.bbName, t.bNumber, t.cNumber, t.vNumber);
                                        final ArrayList<VerseBO> lstVerse = mapSearchBible != null && mapSearchBible.containsKey("LSTVERSE") ? (ArrayList<VerseBO>) mapSearchBible.get("LSTVERSE") : null;
                                        if (lstVerse != null)
                                        {
                                            for (VerseBO verse1 : lstVerse) {
                                                lstAllId.add(verse1.id);
                                            }
                                        }
                                    } else {
                                        //Others
                                        final int tabIdTo = MainActivity.Tab.GetTabCount();
                                        if (tabIdTo < 0) return;
                                        lstAllId = _s.GetResultVersesId(tabIdFrom, tabIdTo, verse.bbName);
                                        _s.DeleteCache(tabIdTo);
                                    }
                                }
                                clipboard.AddToClipboard(bibleId, CLIPBOARD_ADD_TYPE.ALL, lstAllId);
                            }
                        }
                    }
                });
            } else if (itemId == R.id.mnu_cr) {
                if (verse == null) return false;

                final AlertDialog builder = new AlertDialog.Builder(context).create();
                final LayoutInflater inflater = requireActivity().getLayoutInflater();
                final View vllLanguages = inflater.inflate(R.layout.fragment_languages_multi, (ViewGroup) requireActivity().findViewById(R.id.llLanguages));

                final String msg = PCommon.ConcaT(context.getString(R.string.mnuCrossReferences), "");
                PCommon.SelectBibleLanguageMulti(builder, context, vllLanguages, msg, "", true, false);
                builder.setOnDismissListener(dialogInterface -> {
                    final Context dlgContext = ((AlertDialog) dialogInterface).getContext();
                    final String bbname = PCommon.GetPref(dlgContext, IProject.APP_PREF_KEY.BIBLE_NAME_DIALOG, verse.bbName);
                    if (bbname.isEmpty()) return;
                    final String fullquery = PCommon.ConcaT(verse.bNumber, " ", verse.cNumber, " ", verse.vNumber, " CR:");
                    final String tbbName = PCommon.GetPrefTradBibleName(dlgContext, true);
                    MainActivity.Tab.AddTab(dlgContext, tbbName, verse.bNumber, verse.cNumber, verse.vNumber, fullquery);
                });
                builder.show();
            } else if (itemId == R.id.mnu_fav) {
                if (verse == null) return false;

                //Construct all fav here
                final TreeMap<Integer, BookmarkBO> mapBm = _s.GenerateBookmarkCompleteMap(context, R.string.favRemove, R.string.mnuNoteRemove);
                final ArrayList<Integer> lstId = _s.GetListAllBookmarkId();
                final ArrayList<String> mnuFav = new ArrayList<>();
                for (Integer bmId : mapBm.keySet()) {
                    mnuFav.add(Objects.requireNonNull(mapBm.get(bmId)).bmRepresentation);
                }

                final AlertDialog builderFavMenu = new AlertDialog.Builder(context).create();
                PCommon.ShowMenu(builderFavMenu, R.string.mnuOpen, mnuFav, lstId);
                builderFavMenu.setOnDismissListener(dialogInterface -> {
                    final String mnu_dialog = PCommon.GetPref(context, IProject.APP_PREF_KEY.MENU_DIALOG, "");
                    if (mnu_dialog.isEmpty()) return;

                    final int mark = Integer.parseInt(mnu_dialog);
                    if (mark == 0) {
                        _s.DeleteNote(verse.bNumber, verse.cNumber, verse.vNumber);

                        UpdateViewMark(position, 0);

                        //return true;
                    } else {
                        final String changeDt = PCommon.NowYYYYMMDD();
                        final NoteBO noteBO = new NoteBO(verse.bNumber, verse.cNumber, verse.vNumber, changeDt, mark);
                        _s.SaveNote(noteBO);

                        UpdateViewMark(position, mark);

                        //return true;
                    }
                });

            } else if (itemId == R.id.mnu_interlinear) {
                    if (verse == null) return false;
                    try { ShowInterlinear(this.requireActivity(), verse, null); } catch (Exception ignored) {}
                    return true;

            } else if (itemId == R.id.mnu_listen) {
                PCommon.ShowAudioBible(this.requireActivity(), verse);
                return true;

            } else if (itemId == R.id.mnu_edit) {
                final int edit_status = PCommon.GetEditStatus(context);
                if (edit_status == 1) {
                    //Stop
                    PCommon.SavePrefInt(context, IProject.APP_PREF_KEY.EDIT_STATUS, 0);
                    PCommon.SavePrefInt(context, IProject.APP_PREF_KEY.EDIT_ART_ID, -1);
                    PCommon.SavePref(context, IProject.APP_PREF_KEY.EDIT_SELECTION, "");
                } else {
                    //Selection Start
                    PCommon.ShowArticles(context, true, true);
                }

                return true;
            } else if (itemId == R.id.mnu_edit_select_from) {
                if (verse == null) return false;

                final int artId = PCommon.GetEditArticleId(context);
                if (artId < 0) return false;

                final String selectFrom = PCommon.ConcaT(verse.bNumber, " ", verse.cNumber, " ", verse.vNumber);
                PCommon.SavePref(context, IProject.APP_PREF_KEY.EDIT_SELECTION, selectFrom);

                return true;
            } else if (itemId == R.id.mnu_edit_select_to) {
                if (verse == null) return false;

                final int artId = PCommon.GetEditArticleId(context);
                if (artId < 0) return false;

                final String selectFrom = PCommon.GetPref(context, IProject.APP_PREF_KEY.EDIT_SELECTION, "");
                if (selectFrom.isEmpty()) {
                    PCommon.ShowToast(context, R.string.toastRefIncorrect, Toast.LENGTH_SHORT);
                    return true;
                }
                final String selectTo = PCommon.ConcaT(verse.bNumber, " ", verse.cNumber, " ", verse.vNumber);
                final String[] arrFrom = selectFrom.split("\\s");
                final String[] arrTo = selectTo.split("\\s");

                if (arrFrom.length != 3 || arrTo.length != 3) {
                    PCommon.ShowToast(context, R.string.toastRefIncorrect, Toast.LENGTH_SHORT);
                    return true;
                }
                if (arrFrom[0].compareTo(arrTo[0]) != 0 || arrFrom[1].compareTo(arrTo[1]) != 0) {
                    PCommon.ShowToast(context, R.string.toastRefIncorrect, Toast.LENGTH_SHORT);
                    return true;
                }
                if (Integer.parseInt(arrFrom[2]) > Integer.parseInt(arrTo[2])) {
                    PCommon.ShowToast(context, R.string.toastRefIncorrect, Toast.LENGTH_SHORT);
                    return true;
                }

                PCommon.SavePref(context, IProject.APP_PREF_KEY.EDIT_SELECTION, "");

                final String ref = PCommon.ConcaT("<R>", arrFrom[0], " ", arrFrom[1], " ", arrFrom[2], " ", arrTo[2], "</R>");
                final String source = _s.GetMyArticleSource(artId);
                final String finalSource = PCommon.ConcaT(source, ref);
                _s.UpdateMyArticleSource(artId, finalSource);
                final String toast = PCommon.ConcaT(arrFrom[1], ".", arrFrom[2], "-", arrTo[2], " ", context.getString(R.string.toastRefAdded));
                PCommon.ShowToast(context, toast, Toast.LENGTH_SHORT);

                return true;
            } else if (itemId == R.id.mnu_edit_select_from_to) {
                if (verse == null) return false;

                final int artId = PCommon.GetEditArticleId(context);
                if (artId < 0) return false;

                final String selectFromTo = PCommon.ConcaT(verse.bNumber, " ", verse.cNumber, " ", verse.vNumber, " ", verse.vNumber);

                PCommon.SavePref(context, IProject.APP_PREF_KEY.EDIT_SELECTION, "");

                final String ref = PCommon.ConcaT("<R>", selectFromTo, "</R>");
                final String source = _s.GetMyArticleSource(artId);
                final String finalSource = PCommon.ConcaT(source, ref);
                _s.UpdateMyArticleSource(artId, finalSource);
                final String toast = PCommon.ConcaT(verse.cNumber, ".", verse.vNumber, " ", context.getString(R.string.toastRefAdded));
                PCommon.ShowToast(context, toast, Toast.LENGTH_SHORT);

                return true;
            } else if (itemId == R.id.mnu_edit_move) {
                final String mnuEditMoveUp = context.getString(R.string.mnuEditMoveUp);
                final String mnuEditMoveDown = context.getString(R.string.mnuEditMoveDown);

                final ArrayList<String> mnuEditMove = new ArrayList<String>() {{
                    add(mnuEditMoveUp);
                    add(mnuEditMoveDown);
                }};

                final AlertDialog builderEditMenu = new AlertDialog.Builder(context).create();
                PCommon.ShowMenu(builderEditMenu, R.string.mnuEditMove, mnuEditMove, null);
                builderEditMenu.setOnDismissListener(dialogInterface -> {
                    final String mnu_dialog = PCommon.GetPref(context, IProject.APP_PREF_KEY.MENU_DIALOG, "");
                    if (mnu_dialog.compareTo(mnuEditMoveUp) == 0) {
                        final int artId = Integer.parseInt(tabTitle.replace(context.getString(R.string.tabMyArtPrefix), ""));
                        if (artId >= 0) {
                            final String source = MoveArticleShortSection(position, -1);
                            if (source != null) {
                                _s.UpdateMyArticleSource(artId, source);
                                onResume();
                            }

                            //return true;
                        }
                    } else if (mnu_dialog.compareTo(mnuEditMoveDown) == 0) {
                        final int artId = Integer.parseInt(tabTitle.replace(context.getString(R.string.tabMyArtPrefix), ""));
                        if (artId >= 0) {
                            final String source = MoveArticleShortSection(position, +1);
                            if (source != null) {
                                _s.UpdateMyArticleSource(artId, source);
                                onResume();
                            }

                            //return true;
                        }
                    }
                });
            } else if (itemId == R.id.mnu_edit_add) {
                final String mnuEditAddText = context.getString(R.string.mnuEditAddText);
                final String mnuEditAddTitleSmall = context.getString(R.string.mnuEditAddTitleSmall);
                final String mnuEditAddTitleLarge = context.getString(R.string.mnuEditAddTitleLarge);

                final ArrayList<String> mnuEditAdd = new ArrayList<String>() {{
                    add(mnuEditAddText);
                    add(mnuEditAddTitleSmall);
                    add(mnuEditAddTitleLarge);
                }};

                final AlertDialog builderEditAdd = new AlertDialog.Builder(context).create();
                PCommon.ShowMenu(builderEditAdd, R.string.mnuEditAdd, mnuEditAdd, null);
                builderEditAdd.setOnDismissListener(dialogInterface -> {
                    final String mnu_dialog = PCommon.GetPref(context, IProject.APP_PREF_KEY.MENU_DIALOG, "");
                    if (mnu_dialog.compareTo(mnuEditAddText) == 0 ||
                            mnu_dialog.compareTo(mnuEditAddTitleSmall) == 0 ||
                            mnu_dialog.compareTo(mnuEditAddTitleLarge) == 0) {
                        final int artId = Integer.parseInt(tabTitle.replace(context.getString(R.string.tabMyArtPrefix), ""));
                        if (artId >= 0) {
                            final String editType = mnu_dialog.compareTo(mnuEditAddText) == 0 ? "T"
                                    : mnu_dialog.compareTo(mnuEditAddTitleLarge) == 0 ? "L" : "S";

                            EditArticleDialog(false, editType, getActivity(), R.string.mnuEditAdd, "", position, artId);

                            //return true;
                        }
                    }
                });
            } else if (itemId == R.id.mnu_edit_update) {
                final int artId = Integer.parseInt(tabTitle.replace(context.getString(R.string.tabMyArtPrefix), ""));
                if (artId < 0) return false;

                final ShortSectionBO updateSection = FindArticleShortSectionByPositionId(position);
                final String updateSectionContent = (updateSection == null) ? "" : updateSection.content;
                EditArticleDialog(true, "T", getActivity(), R.string.mnuEditUpdate, updateSectionContent, position, artId);

                return true;
            } else if (itemId == R.id.mnu_edit_remove) {
                final String mnuEditRemoveConfirm = context.getString(R.string.mnuEditRemoveConfirm);
                final ArrayList<String> mnuEditRemove = new ArrayList<String>() {{
                    add(mnuEditRemoveConfirm);
                }};

                final AlertDialog builderEditRemove = new AlertDialog.Builder(context).create();
                PCommon.ShowMenu(builderEditRemove, R.string.mnuEditRemove, mnuEditRemove, null);
                builderEditRemove.setOnDismissListener(dialogInterface -> {
                    final String mnu_dialog = PCommon.GetPref(context, IProject.APP_PREF_KEY.MENU_DIALOG, "");
                    if (mnu_dialog.compareTo(mnuEditRemoveConfirm) == 0) {
                        final int artId = Integer.parseInt(tabTitle.replace(context.getString(R.string.tabMyArtPrefix), ""));
                        if (artId >= 0) {
                            final String source = DeleteArticleShortSection(position);
                            if (source != null) {
                                _s.UpdateMyArticleSource(artId, source);
                                onResume();
                            }

                            //return true;
                        }
                    }
                });
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return super.onContextItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull final Menu menu, @NonNull MenuInflater menuInflater)
    {
        try
        {
            super.onCreateOptionsMenu(menu, menuInflater);

            SetLocalBibleName();

            final MenuItem mnu_group_settings = menu.findItem(R.id.mnu_group_settings);
            if (mnu_group_settings.isEnabled())
            {
                final int settingsDrawable = !MainActivity.Tab.IsThemeWhite() ? R.drawable.ic_settings_white_24dp : R.drawable.ic_settings_black_24dp;
                PCommon.SetIconMenuItem(_context, mnu_group_settings, settingsDrawable);
            }

            if (fragmentType == FRAGMENT_TYPE.SEARCH_TYPE)
            {
                if (bbName.equalsIgnoreCase("y") || bbName.equalsIgnoreCase("w"))
                {
                    //No search
                    return;
                }

                menuInflater.inflate(R.menu.menu_search, menu);
            }
            else if (fragmentType == FRAGMENT_TYPE.PLAN_TYPE)
            {
                menuInflater.inflate(R.menu.menu_plan, menu);
            }
            else if (fragmentType == FRAGMENT_TYPE.FAV_TYPE)
            {
                menuInflater.inflate(R.menu.menu_fav, menu);

                //No search
                return;
            }
            else
            {
                //Article
                final int INSTALL_STATUS = PCommon.GetInstallStatus(_context);
                // if (INSTALL_STATUS == PCommon.GetInstallStatusShouldBe()) menuInflater.inflate(R.menu.menu_art, menu);

                //No search
                return;
            }

            final MenuItem mnu_search_item = menu.findItem(R.id.mnu_search);

            searchView = (SearchView) mnu_search_item.getActionView();
            searchView.setLongClickable(true);
            searchView.setIconifiedByDefault(true);
            searchView.setQueryHint((fragmentType == FRAGMENT_TYPE.SEARCH_TYPE) ? _context.getString(R.string.searchBibleHint) : _context.getString(R.string.mnuSearchAll));

            if (fragmentType == FRAGMENT_TYPE.SEARCH_TYPE && isHarmony)
            {
                mnu_search_item.setVisible(false);
                final MenuItem mnu_search_harmony = menu.findItem(R.id.mnu_search_harmony);
                mnu_search_harmony.setVisible(true);

                //No search
            }
            else if (fragmentType == FRAGMENT_TYPE.SEARCH_TYPE && isLex)
            {
                mnu_search_item.setVisible(false);
                final MenuItem mnu_search_lex = menu.findItem(R.id.mnu_search_lex);
                mnu_search_lex.setVisible(true);

                //No search
            }
            else if (fragmentType == FRAGMENT_TYPE.SEARCH_TYPE)
            {
                final MenuItem mnu_search_prbl = menu.findItem(R.id.mnu_search_prbl);
                mnu_search_prbl.setVisible(isPrbl);

                final MenuItem mnu_search_harmony = menu.findItem(R.id.mnu_search_harmony);
                mnu_search_harmony.setVisible(false);

                final MenuItem mnu_search_lex = menu.findItem(R.id.mnu_search_lex);
                mnu_search_lex.setVisible(false);

                final String bbNameLanguage = (bbName.equalsIgnoreCase("k"))
                        ? _context.getString(R.string.languageEnShort)

                        : (bbName.equalsIgnoreCase("9"))
                        ? _context.getString(R.string.languageEs2Short)

                        : (bbName.equalsIgnoreCase("d"))
                        ? _context.getString(R.string.languageItShort)

                        : (bbName.equalsIgnoreCase("1"))
                        ? _context.getString(R.string.languageIt2Short)

                        : (bbName.equalsIgnoreCase("v"))
                        ? _context.getString(R.string.languageEsShort)

                        : (bbName.equalsIgnoreCase("l"))
                        ? _context.getString(R.string.languageFrShort)

                        : (bbName.equalsIgnoreCase("a"))
                        ? _context.getString(R.string.languagePtShort)

                        : (bbName.equalsIgnoreCase("o"))
                        ? _context.getString(R.string.languageFr2Short)

                        : (bbName.equalsIgnoreCase("i"))
                        ? _context.getString(R.string.languageInShort)

                        : (bbName.equalsIgnoreCase("c"))
                        ? _context.getString(R.string.languageCnShort)

                        : (bbName.equalsIgnoreCase("s"))
                        ? _context.getString(R.string.languageDeShort)

                        : (bbName.equalsIgnoreCase("j"))
                        ? _context.getString(R.string.languageJpShort)

                        : (bbName.equalsIgnoreCase("u"))
                        ? _context.getString(R.string.languageRoShort)

                        : (bbName.equalsIgnoreCase("z"))
                        ? _context.getString(R.string.languagePlShort)

                        : (bbName.equalsIgnoreCase("r"))
                        ? _context.getString(R.string.languageRuShort)

                        : (bbName.equalsIgnoreCase("t"))
                        ? _context.getString(R.string.languageTrShort)

                        : (bbName.equalsIgnoreCase("b"))
                        ? _context.getString(R.string.languageBdShort)

                        : (bbName.equalsIgnoreCase("e"))
                        ? _context.getString(R.string.languageDe2Short)

                        : (bbName.equalsIgnoreCase("h"))
                        ? _context.getString(R.string.languageSwShort)

                        : _context.getString(R.string.languageEn2Short);
                final MenuItem mnu_search_bible_name_item = menu.findItem(R.id.mnu_search_bible_name);
                mnu_search_bible_name_item.setTitle(bbNameLanguage);
                mnu_search_bible_name_item.setVisible(false);

                final String bibleAppType = PCommon.GetPrefBibleAppType(_context);
                final boolean showBibleNameItem = bibleAppType.equalsIgnoreCase("M");

                final MenuItem.OnActionExpandListener expandListener = new MenuItem.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        mnu_search_bible_name_item.setVisible(false);
                        mnu_search_prbl.setVisible(isPrbl);
                        return true;
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        mnu_search_prbl.setVisible(false);
                        if (showBibleNameItem) {
                            mnu_search_bible_name_item.setVisible(true);
                        } else {
                            bbName = PCommon.GetPrefBibleName(_context);
                        }
                        return true;
                    }
                };
                mnu_search_item.setOnActionExpandListener(expandListener);

                if (bbName.equalsIgnoreCase("y") || bbName.equalsIgnoreCase("w"))
                {
                    searchView.setOnSearchClickListener(v -> {
                        try {
                            ((MainActivity) requireActivity()).SearchDialog(v.getContext(), true);
                        } catch (Exception ex) {
                            if (PCommon._isDebug) PCommon.LogR(v.getContext(), ex);
                        }
                    });

                    return; //No search
                }

                searchView.setOnSearchClickListener(v -> searchView.setQuery(searchFullQuery, false));
                searchView.setSuggestionsAdapter(cursorAdapter);
                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
                {
                    @Override
                    public boolean onQueryTextSubmit(String query)
                    {
                        if (PCommon._isDebug) System.out.println(PCommon.ConcaT("TextSubmit: ", query));

                        query = query.replaceAll("%+", "%");
                        if (query.length() < PCommon.GetSearchFullQueryLimit()) {
                            PCommon.ShowToast(_context, R.string.toastEmpty3, Toast.LENGTH_SHORT);
                            return false;
                        }

                        //Save
                        searchFullQuery = query;
                        trad = bbName;

                        mnu_search_item.collapseActionView();

                        isPrbl = GetPrblName(searchFullQuery) != null;
                        isRnd = searchFullQuery.equalsIgnoreCase("RAND:");
                        isHarmony = IsHarmonyQuery(searchFullQuery);
                        isLex = IsLexQuery(searchFullQuery);
                        isSearch = (!isPrbl) && (!isRnd) && (!isHarmony) && (!isLex);

                        mnu_search_prbl.setVisible(isPrbl);
                        mnu_search_harmony.setVisible(isHarmony);
                        mnu_search_lex.setVisible(isLex);
                        mnu_search_item.setVisible(!isHarmony);

                        SearchBible(false);

                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String query)
                    {
                        if (PCommon._isDebug) System.out.println(PCommon.ConcaT("TextChange: ", query));

                        return false;
                    }
                });
                searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener()
                {
                    @Override
                    public boolean onSuggestionSelect(int position)
                    {
                        return false;
                    }

                    @Override
                    public boolean onSuggestionClick(int position)
                    {
                        if (PCommon._isDebug) System.out.println(PCommon.ConcaT("Suggestion Click: pos=", position));

                        //Get selected book
                        @SuppressWarnings("unused") final int bookId = matrixCursor.getInt(0);
                        final String bookName = matrixCursor.getString(1);

                        if (PCommon._isDebug) System.out.println(PCommon.ConcaT("Book (", bookId, ") => ", bookName));

                        //Change SearchView query
                        searchView.setQuery(bookName, false);

                        return true;
                    }
                });

                final AutoCompleteTextView searchViewAutoComplete = searchView.findViewById(R.id.search_src_text);
                searchViewAutoComplete.setThreshold(1);

                return;
            }

            //else
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
            {
                @Override
                public boolean onQueryTextSubmit(String query)
                {
                    if (PCommon._isDebug) System.out.println(PCommon.ConcaT("TextSubmit: ", query));

                    //Save
                    searchFullQuery = query;

                    SearchBible(false);
                    mnu_search_item.collapseActionView();

                    return false;
                }

                @Override
                public boolean onQueryTextChange(String query)
                {
                    if (PCommon._isDebug) System.out.println(PCommon.ConcaT("TextChange: ", query));

                    return false;
                }
            });
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull final MenuItem item)
    {
        try
        {
            final Context context = requireContext();
            final int id = item.getItemId();
            if (id == R.id.mnu_search_bible_name) {
                SetLocalBibleName();
                bbName = RollBookName(bbName);
                if (bbName.equalsIgnoreCase("y") || bbName.equalsIgnoreCase("w")) bbName = RollBookName(bbName);
                final String bbNameLanguage = (bbName.equalsIgnoreCase("k"))
                        ? context.getString(R.string.languageEnShort)

                        : (bbName.equalsIgnoreCase("d"))
                        ? context.getString(R.string.languageItShort)

                        : (bbName.equalsIgnoreCase("1"))
                        ? context.getString(R.string.languageIt2Short)

                        : (bbName.equalsIgnoreCase("v"))
                        ? context.getString(R.string.languageEsShort)

                        : (bbName.equalsIgnoreCase("l"))
                        ? context.getString(R.string.languageFrShort)

                        : (bbName.equalsIgnoreCase("a"))
                        ? context.getString(R.string.languagePtShort)

                        : (bbName.equalsIgnoreCase("i"))
                        ? context.getString(R.string.languageInShort)

                        : (bbName.equalsIgnoreCase("o"))
                        ? context.getString(R.string.languageFr2Short)

                        : (bbName.equalsIgnoreCase("2"))
                        ? context.getString(R.string.languageEn2Short)

                        : (bbName.equalsIgnoreCase("c"))
                        ? context.getString(R.string.languageCnShort)

                        : (bbName.equalsIgnoreCase("s"))
                        ? context.getString(R.string.languageDeShort)

                        : (bbName.equalsIgnoreCase("j"))
                        ? context.getString(R.string.languageJpShort)

                        : (bbName.equalsIgnoreCase("u"))
                        ? _context.getString(R.string.languageRoShort)

                        : (bbName.equalsIgnoreCase("z"))
                        ? _context.getString(R.string.languagePlShort)

                        : (bbName.equalsIgnoreCase("r"))
                        ? context.getString(R.string.languageRuShort)

                        : (bbName.equalsIgnoreCase("t"))
                        ? context.getString(R.string.languageTrShort)

                        : (bbName.equalsIgnoreCase("b"))
                        ? context.getString(R.string.languageBdShort)

                        : (bbName.equalsIgnoreCase("e"))
                        ? context.getString(R.string.languageDe2Short)

                        : (bbName.equalsIgnoreCase("h"))
                        ? context.getString(R.string.languageSwShort)

                        : context.getString(R.string.languageEs2Short);
                item.setTitle(bbNameLanguage);

                return true;
            } else if (id == R.id.mnu_fav_search) {
                try {
                    ((MainActivity) requireActivity()).SearchDialog(context, false);
                } catch (Exception ex) {
                    if (PCommon._isDebug) PCommon.LogR(_context, ex);
                }

                return true;
            } else if (id == R.id.mnu_fav_clear_filter) {
                searchFullQuery = "";
                final int orderBy = PCommon.GetFavOrder(context);

                PCommon.SavePrefInt(context, IProject.APP_PREF_KEY.FAV_FILTER, 0);
                SaveTab();

                recyclerViewAdapter = new BibleAdapter(context, bbName, searchFullQuery, orderBy, 0);
                if (WhenTabIsEmptyOrNull(true)) return true;
                recyclerView.setAdapter(recyclerViewAdapter);
                recyclerView.setHasFixedSize(true);
                recyclerView.scrollToPosition(scrollPosY);

                return true;
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return super.onOptionsItemSelected(item);
    }

    /***
     * Check local instance (to copy reader all activities that use it)
     */
    private void CheckLocalInstance()
    {
        if (_context == null) _context = requireActivity().getApplicationContext();

        CheckLocalInstance(_context);
    }

    /***
     * Check local instance (to copy reader all activities that use it)
     */
    private void CheckLocalInstance(final Context context)
    {
        try
        {
            if (_s == null)
            {
                _s = SCommon.GetInstance(context);
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    private void CreateRecyclerView()
    {
        SetLayoutManager();

        recyclerViewAdapter = new BibleAdapter(_context);
        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerView.setHasFixedSize(true);
    }

    private void SetLayoutManager()
    {
        recyclerView = v.findViewById(R.id.card_recycler_view);

        final RecyclerView.LayoutManager layoutManager;
        if (trad == null || fragmentType == FRAGMENT_TYPE.ARTICLE_TYPE || fragmentType == FRAGMENT_TYPE.FAV_TYPE || isHarmony)
        {
            layoutManager = new LinearLayoutManager(_context);
        }
        else
        {
            final int bibleCount = trad.length();
            final int dc = PCommon.GetDynamicColumnCount(_context, bibleCount);
            layoutManager = (dc <= 1) ? new LinearLayoutManager(_context) : new GridLayoutManager(_context, dc);
        }
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        registerForContextMenu(recyclerView);

        /*recyclerView.addOnItemTouchListener(new RecyclerViewTouchListener(_context, recyclerView, new RecyclerViewTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
               view.showContextMenu();
            }

            @Override
            public boolean onFling(MotionEvent motionEvent1, MotionEvent motionEvent2, float X, float Y) {
                return false;

                *//*
                try
                {
                    if (isChapterListLoading) return false;

                    if ((motionEvent1.getX() - motionEvent2.getX() > 150) && (motionEvent2.getY() - motionEvent1.getY() < 20)) {
                        ShowChapterList(v.getContext());
                        return true;
                    }
                }
                catch(Exception ex)
                {
                    if (PCommon._isDebugVersion) PCommon.LogR(_context, ex);
                }
                return false;
                *//*
            }
        }));*/
    }

    /***
     * Get article
     * @param t     CacheTabBO
     */
    private ArtOriginalContentBO GetArticle(final CacheTabBO t)
    {
        if (t == null) return null;

        ArtOriginalContentBO artOriginalContent = null;

        try
        {
            final int artId;
            final String artTitle;
            final String artHtml;
            final String ha;

            if (t.fullQuery.startsWith("ART"))
            {
                artId = PCommon.GetResId(_context, PCommon.ConcaT(t.fullQuery, "_CONTENT"));
                artHtml = _context.getString(artId);
                artTitle = _context.getString(PCommon.GetResId(_context, t.fullQuery));
                ha = PCommon.ConcaT("<br><H>", artTitle, "</H>");
            }
            else
            {
                artId = Integer.parseInt(t.fullQuery);
                artHtml = _s.GetMyArticleSource(artId);
                ha = null;
            }
            artOriginalContent = new ArtOriginalContentBO(ha, artHtml);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return artOriginalContent;
    }

    private Cursor GetCursor(final CharSequence query)
    {
        if (query == null) return null;

        final MatrixCursor cursor = new MatrixCursor(new String[]{"_id", "text"});

        try
        {
            final ArrayList<BibleRefBO> lstRef = _s.GetListBookByName(bbName, query.toString());

            if (!lstRef.isEmpty())
            {
                String bookId;
                String bookName;

                for (final BibleRefBO r : lstRef)
                {
                    bookId = Integer.toString(r.bNumber);
                    bookName = r.bName;

                    cursor.addRow(new String[]{bookId, bookName});
                }
            }

            matrixCursor = cursor;
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return cursor;
    }

    private boolean IsChapterExist(final String bbName, final int bNumber, final int cNumber)
    {
        final ArrayList<VerseBO> lstVerse = _s.GetVerse(bbName, bNumber, cNumber, 1);

        return !(lstVerse == null || lstVerse.isEmpty());
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private boolean IsVerseExist(final String bbName, final int bNumber, final int cNumber, final int vNumber)
    {
        final ArrayList<VerseBO> lstVerse = _s.GetVerse(bbName, bNumber, cNumber, vNumber);

        return !(lstVerse == null || lstVerse.isEmpty());
    }

    /***
     * Update view at position and show/hide the mark
     * @param position  Position
     * @param markType  Mark type
     */
    private void UpdateViewMark(final int position, final int markType)
    {
        try
        {
            if (fragmentType == FRAGMENT_TYPE.FAV_TYPE)
            {
                SearchBible(false);
                return;
            }

            if (isHarmony)
            {
                final Object obj = ((BibleHarmonyAdapter) recyclerViewAdapter).lstVerse.get(position);
                if (obj instanceof VerseBO)
                {
                    final VerseBO verse = (VerseBO) obj;
                    verse.mark = markType;
                }
            }
            else
            {
                ((BibleAdapter) recyclerViewAdapter).lstVerse.get(position).mark = markType;
            }
            recyclerViewAdapter.notifyItemChanged(position);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    private void ShowArticle(final int step)
    {
        try
        {
            if (!(fragmentType == FRAGMENT_TYPE.ARTICLE_TYPE && searchFullQuery.startsWith("ART"))) return;

            //Get Art
            String[] arr = _context.getResources().getStringArray(R.array.ART_ARRAY);
            final List<String> lstArt = Arrays.asList(arr);
            arr = null;

            final int idxArt = lstArt.indexOf(searchFullQuery);
            if (idxArt <= 0 && step < 0) return;
            if (idxArt == lstArt.size() && step > 0) return;

            //Update Cache
            final String tabTitle;
            final String updSearchFullQuery = lstArt.get(idxArt + step);
            if (updSearchFullQuery.startsWith("ART_APP_")) {
                final int artId = PCommon.GetResId(_context, updSearchFullQuery);
                tabTitle = _context.getString(artId);
            } else {
                tabTitle = updSearchFullQuery;
            }

            final int tabId = tabNumber();
            final CacheTabBO cacheTab = _s.GetCacheTab(tabId);
            cacheTab.isBook = false;
            cacheTab.isChapter = false;
            cacheTab.isVerse = false;
            cacheTab.bNumber = 0;
            cacheTab.cNumber = 0;
            cacheTab.vNumber = 0;
            cacheTab.fullQuery = updSearchFullQuery;
            cacheTab.scrollPosY = 0;
            cacheTab.tabTitle = tabTitle;
            _s.SaveCacheTab(cacheTab);

            onResume();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /**
     * Get prbl name or null if not found
     * @param searchFullQuery   Query
     * @return null if not found
     */
    private String GetPrblName(final String searchFullQuery)
    {
        try
        {
            final String[] words = searchFullQuery.split("\\s");
            final String patternDigit = "\\d+";

            if (words.length != 4) return null;
            if (words[0].matches(patternDigit) && words[1].matches(patternDigit) && words[2].matches(patternDigit) && words[3].matches(patternDigit))
            {
                final String[] arr = _context.getResources().getStringArray(R.array.PRBL_ARRAY);
                final String updQuery = PCommon.ConcaT("|", searchFullQuery);
                for (String prbl : arr) {
                    if (prbl.contains(updQuery)) return prbl.split("\\|")[0];
                }
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return null;
    }

    private boolean IsHarmonyQuery(final String searchFullQuery)
    {
        try
        {
            final String[] words = searchFullQuery.split("\\s");
            final String patternDigit = "\\d+";

            if (words.length != 2) return false;

            if (words[0].matches(patternDigit))
            {
                final int id = Integer.parseInt(words[0]);
                if ((id >= 1 && id <= 159) && words[1].equalsIgnoreCase("HARMONY:")) return true;
            }

            return false;
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return false;
    }

    private boolean IsLexQuery(final String searchFullQuery)
    {
        try
        {
            final String[] arrWord = searchFullQuery.trim().split(" ");
            if (arrWord[0].matches(PCommon.GetHGNrRegex(true))) return true;
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return false;
    }

    private void ShowPrbl(final int step)
    {
        try
        {
            //Get Prbl
            int idxPrbl = -1;
            int idxTmp = -1;
            final String[] arr = _context.getResources().getStringArray(R.array.PRBL_ARRAY);
            final List<String> lstPrbl = Arrays.asList(arr);
            final String updQuery = PCommon.ConcaT("|", searchFullQuery);
            for (String prbl : lstPrbl) {
                idxTmp++;
                if (prbl.contains(updQuery)) {
                    idxPrbl = idxTmp;
                    break;
                }
            }
            if (idxPrbl <= 0 && step < 0) return;
            if (idxPrbl == lstPrbl.size() && step > 0) return;

            //Update Cache
            final String[] resPrbl = lstPrbl.get(idxPrbl + step).split("\\|");
            final int tabId = tabNumber();
            final CacheTabBO cacheTab = _s.GetCacheTab(tabId);
            cacheTab.isBook = false;
            cacheTab.isChapter = false;
            cacheTab.isVerse = false;
            cacheTab.bNumber = 0;
            cacheTab.cNumber = 0;
            cacheTab.vNumber = 0;
            cacheTab.fullQuery = resPrbl[1];
            cacheTab.scrollPosY = 0;
            cacheTab.tabTitle = resPrbl[0];
            _s.SaveCacheTab(cacheTab);

            onResume();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    private void ShowHarmony(final int step)
    {
        try
        {
            final int updHarmId = harmId + step;
            if ( updHarmId < 1 || updHarmId > 159) return;

            harmId = updHarmId;

            //Update Cache
            final int tabId = tabNumber();
            final CacheTabBO cacheTab = _s.GetCacheTab(tabId);
            cacheTab.tabTitle = PCommon.ConcaT("HARMONY", harmId);
            cacheTab.fullQuery = PCommon.ConcaT(harmId, " HARMONY:");
            cacheTab.scrollPosY = 0;
            _s.SaveCacheTab(cacheTab);

            onResume();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    /***
     * Load data of interlinear
     * @param currentActivity   Activity
     * @param verse     Verse
     * @param builder   Builder
     * @param view      View of builder
     */
    private void LoadDataInterlinear(final FragmentActivity currentActivity, final VerseBO verse, final AlertDialog builder, final View view, final StyleBO dlgStyle)
    {
        final Context context = currentActivity.getApplicationContext();

        try
        {
            //Check
            if (verse == null) return;

            final int bNumber = verse.bNumber;
            final int cNumber = verse.cNumber;
            final int vNumber = verse.vNumber;
            final int bibleId = verse.id;

            Map<String, ArrayList<String>> mapInt = _s.GetVerseInterlinear("-", bNumber, cNumber, vNumber, bibleId);
            if (mapInt == null || mapInt.isEmpty()) return;

            ArrayList<String> lstEN = mapInt.get( "EN" );
            ArrayList<String> lstHG = mapInt.get( "HG" );
            ArrayList<String> lstWrd = mapInt.get( "WRD" );
            ArrayList<String> lstVar = mapInt.get( "VAR" );

            if (lstEN == null || lstHG == null || lstWrd == null || lstVar == null ||
                    lstHG.isEmpty()) return;

            //UI
            builder.setTitle(PCommon.ConcaT(context.getString(R.string.mnuInterlinear), ": ", verse.bsName, " ", verse.cNumber, ".", verse.vNumber));
            final LinearLayout llInt = view.findViewById(R.id.llInt);
            llInt.removeAllViewsInLayout();
            final Button btnIntGetAll = view.findViewById(R.id.btnIntGetAll);
            btnIntGetAll.setVisibility(View.GONE);
            final Button btnIntBack = view.findViewById(R.id.btnIntBack);
            btnIntBack.setTag(bibleId);
            btnIntBack.setOnClickListener(v -> {
                CheckLocalInstance(v.getContext());

                final int id = Integer.parseInt(v.getTag().toString());
                final int prevId = id - 1;
                final VerseBO versePrev = _s.GetVerse(prevId);
                if (versePrev == null) return;
                if (versePrev.bNumber > verse.bNumber) return;

                LoadDataInterlinear(currentActivity, versePrev, builder, view, dlgStyle);
            });
            final Button btnIntForward = view.findViewById(R.id.btnIntForward);
            btnIntForward.setTag(bibleId);
            btnIntForward.setOnClickListener(v -> {
                CheckLocalInstance(v.getContext());

                final int id = Integer.parseInt(v.getTag().toString());
                final int nextId = id + 1;
                final VerseBO verseNext = _s.GetVerse(nextId);
                if (verseNext == null) return;
                if (verseNext.bNumber < verse.bNumber) return;

                LoadDataInterlinear(currentActivity, verseNext, builder, view, dlgStyle);
            });

            //Prepare data
            final Typeface typeface = PCommon.GetTypeface(context);
            final int fontSize = PCommon.GetFontSize(context);
            final int textColor = PCommon.GetPrefThemeName(context).compareTo("LIGHT") == 0 ? Color.BLACK : Color.WHITE; //TODO: harcoded, need to find from attribute

            int idx;
            for(idx = lstWrd.size() - 1; idx >= 0; idx--) {
                if (!lstWrd.get(idx).isEmpty()) break;
            }

            final int lastIndex = idx;
            final int paddingLeft = 20, paddingTop = 10, paddingRight = 20, paddingBottom = 10;
            final String fieldEmpty = " ";
            String field, tag;

            for (int row = 0; row <= lastIndex; row++)
            {
                tag = lstWrd.get( row );

                field = lstEN.get( row );
                if (!field.isEmpty())
                {
                    final TextView tv = PCommon.CreateTextView(context, PCommon._layoutParamsMatchAndWrap, true, paddingLeft, paddingTop, paddingRight, paddingBottom, field, typeface, fontSize, textColor, R.drawable.focus_text);
                    tv.setTag(tag);
                    tv.setOnClickListener(v -> {
                        final String word = v.getTag().toString();
                        if (word.isEmpty()) return;
                        PCommon.ShowInterlinearWordDetail(currentActivity, word, PCommon.LEX_DETAIL_SEARCH_TYPE.EXACT_SEARCH, dlgStyle);
                    });
                    llInt.addView(tv);
                }

                field = lstHG.get( row );
                if (!field.isEmpty())
                {
                    final TextView tv = PCommon.CreateTextView(context, PCommon._layoutParamsMatchAndWrap, true, paddingLeft, paddingTop, paddingRight, paddingBottom, fieldEmpty, typeface, fontSize, textColor, R.drawable.focus_text);
                    final SpannableStringBuilder span = PCommon.GetBiggerHGText(context, field);
                    tv.setText(span);
                    tv.setTag(tag);
                    tv.setOnClickListener(v -> {
                        final String word = v.getTag().toString();
                        if (word.isEmpty()) return;
                        PCommon.ShowInterlinearWordDetail(currentActivity, word, PCommon.LEX_DETAIL_SEARCH_TYPE.EXACT_SEARCH, dlgStyle);
                    });
                    llInt.addView(tv);
                }

                field = lstWrd.get( row );
                if (!field.isEmpty())
                {
                    final TextView tv = PCommon.CreateTextView(context, PCommon._layoutParamsMatchAndWrap, true, paddingLeft, paddingTop, paddingRight, paddingBottom, field, typeface, fontSize, textColor, R.drawable.focus_text);
                    tv.setTag(tag);
                    tv.setOnClickListener(v -> {
                        final String word = v.getTag().toString();
                        if (word.isEmpty()) return;
                        PCommon.ShowInterlinearWordDetail(currentActivity, word, PCommon.LEX_DETAIL_SEARCH_TYPE.EXACT_SEARCH, dlgStyle);
                    });
                    llInt.addView(tv);
                }

                /* BUG: not same number of items than others
                field = lstVar.get( row );
                if (!field.isEmpty())
                {
                    final TextView tv = PCommon.CreateTextView(context, PCommon._layoutParamsMatchAndWrap, true, paddingLeft, paddingTop, paddingRight, paddingBottom, field, typeface, fontSize, R.drawable.focus_text);
                    tv.setTag(tag);
                    tv.setOnClickListener(v -> {
                        final String word = v.getTag().toString();
                        if (word.isEmpty()) return;
                        ShowInterlinearWordDetail(v.getContext(), word);
                    });
                    llInt.addView(tv);
                }
                */

                field = fieldEmpty;
                final TextView tvEmpty1 = PCommon.CreateTextView(context, PCommon._layoutParamsMatchAndWrap, false, paddingLeft, 2, paddingRight, 2, field, typeface, 10, textColor,-1);
                llInt.addView(tvEmpty1);

                final View vwSep = PCommon.CreateLineSep(context, 0, 0, 0, 0, textColor);
                llInt.addView(vwSep);

                field = fieldEmpty;
                final TextView tvEmpty2 = PCommon.CreateTextView(context, PCommon._layoutParamsMatchAndWrap, false, paddingLeft, 2, paddingRight, 2, field, typeface, 10, textColor, -1);
                llInt.addView(tvEmpty2);
            }
            lstEN.clear();
            lstHG.clear();
            lstWrd.clear();
            lstVar.clear();
            mapInt.clear();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Show interlinear
     * @param currentActivity   Activity
     * @param verse     Verse
     */
    private void ShowInterlinear(final FragmentActivity currentActivity, final VerseBO verse, final StyleBO dlgStyle)
    {
        final Context context = currentActivity.getApplicationContext();

        try
        {
            CheckLocalInstance(context);

            //UI
            final LayoutInflater inflater = currentActivity.getLayoutInflater();
            final View view = inflater.inflate(R.layout.fragment_interlinear, (ViewGroup) currentActivity.findViewById(R.id.svLex));
            final AlertDialog builder = new AlertDialog.Builder(currentActivity).create();
            builder.setTitle(PCommon.ConcaT(context.getString(R.string.mnuInterlinear), ": ", verse.bsName, " ", verse.cNumber, ".", verse.vNumber));
            builder.setCancelable(true);
            builder.setView(view);
            builder.show();

            //Load data
            LoadDataInterlinear(currentActivity, verse, builder, view, dlgStyle);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    private void ShowChapter(final String bbName, final int bNumber, final int cNumber)
    {
        try
        {
            //Try
            if (!IsChapterExist(bbName, bNumber, cNumber)) return;

            //Book title
            ShowBookTitle(true, false, false, false, false);

            final BibleRefBO ref = _s.GetBookRef(bbName, bNumber);
            tvBookTitle.setText(ref.bName.toUpperCase());

            final String title = PCommon.ConcaT(ref.bsName, cNumber);
            SetTabTitle(title);
            SaveTab();

            //Get chapter
            recyclerViewAdapter = new BibleAdapter(_context, trad, bNumber, cNumber);
            if (WhenTabIsEmptyOrNull(true)) return;
            recyclerView.setAdapter(recyclerViewAdapter);
            recyclerView.setHasFixedSize(true);
            recyclerView.scrollToPosition(scrollPosY);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

/*
    @SuppressLint("DefaultLocale")
    private void ShowChapterList(final Context context) {
        try
        {
            isChapterListLoading = true;

            CheckLocalInstance(context);

            final int count = recyclerViewAdapter.getItemCount();
            if (count <= 0) {
                isChapterListLoading = false;
                return;
            }

            final ArrayList<VerseBO> arrVerse = ((BibleAdapter) recyclerViewAdapter).lstVerse;
            final SortedMap<String, String> mapChapter = new TreeMap();
            final String bbname = PCommon.GetPrefBibleName(context);

            VerseBO verse;
            String k, v;
            BibleRefBO r;
            for (int i = 0; i < count; i++) {
                verse = arrVerse.get(i);
                k = String.format("%2d-%3d", verse.bNumber, verse.cNumber).replaceAll(" ", "0");
                if (!mapChapter.containsKey(k)) {
                    r =_s.GetBookRef(bbname, verse.bNumber);
                    v = PCommon.ConcaT(verse.bNumber, "|", r.bName, "|", verse.cNumber, "|", i);
                    mapChapter.put(k, v);
                }
            }

            final Typeface typeface = PCommon.GetTypeface(context);
            final int fontSize = PCommon.GetFontSize(context);

            final LayoutInflater inflater = this.getLayoutInflater();
            final View svChapter = inflater.inflate(R.layout.fragment_chapter_list, (ViewGroup) requireActivity().findViewById(R.id.glChapter));
            final GridLayout glChapter = svChapter.findViewById(R.id.glChapter);

            final AlertDialog builder = new AlertDialog.Builder(context).create();
            builder.setCancelable(true);
            builder.setTitle(R.string.mnuContent);
            builder.setView(svChapter);
            builder.setOnDismissListener(dialog -> isChapterListLoading = false);
            builder.setOnCancelListener(dialog -> isChapterListLoading = false);

            int prevcbNumber = 0, prevccNumber = 0;
            int cbNumber, ccNumber, cPos;
            String cbName;
            //final Spanned nbsp = Html.fromHtml("&nbsp;"); //NOT USED
            TextView tvChapter, tvPref;
            HorizontalScrollView hsv;
            LinearLayout llChapter = null;
            Button btnChapter;

            for (String K : mapChapter.keySet()) {
                final String[] cValue = Objects.requireNonNull(mapChapter.get(K)).split("\\|");
                cbNumber = Integer.parseInt(cValue[0]);
                cbName = cValue[1];
                ccNumber = Integer.parseInt(cValue[2]);
                cPos = Integer.parseInt(cValue[3]);

                if (cbNumber > prevcbNumber)
                {
                    tvChapter = new TextView(context);
                    tvChapter.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                    tvChapter.setPadding(0, 20, 10, 0);
                    tvChapter.setFocusable(true);
                    tvChapter.setBackground(PCommon.GetDrawable(context, R.drawable.focus_text));
                    tvChapter.setText(cbName);
                    tvChapter.setTag(cPos);
                    if (typeface != null) tvChapter.setTypeface(typeface);
                    tvChapter.setTextSize(fontSize);
                    tvChapter.setOnClickListener(v1 -> {
                        builder.dismiss();
                        final int pos = (int) v1.getTag();
                        recyclerView.scrollToPosition(pos);
                    });

                    tvPref = new TextView(context);
                    tvPref.setLayoutParams(PCommon._layoutParamsWrap);
                    tvPref.setPadding(16,0,0,0);
                    tvPref.setText("");

                    llChapter = new LinearLayout(context);
                    llChapter.setLayoutParams(PCommon._layoutParamsWrap);
                    llChapter.setOrientation(LinearLayout.HORIZONTAL);
                    llChapter.addView(tvPref);

                    hsv = new HorizontalScrollView(context);
                    hsv.setLayoutParams(PCommon._layoutParamsWrap);
                    hsv.addView(llChapter);

                    glChapter.addView(tvChapter);
                    glChapter.addView(hsv);

                    prevccNumber = 0;
                }

                if (ccNumber > prevccNumber)
                {
                    btnChapter = new Button(context);
                    btnChapter.setLayoutParams(PCommon._layoutParamsWrap);
                    btnChapter.setFocusable(true);
                    btnChapter.setText(String.valueOf(ccNumber));
                    btnChapter.setTag(cPos);
                    btnChapter.setOnClickListener(v12 -> {
                        builder.dismiss();
                        final int pos = (int) v12.getTag();
                        recyclerView.scrollToPosition(pos);
                    });
                    if (llChapter != null) llChapter.addView(btnChapter);
                }

                prevcbNumber = cbNumber;
                prevccNumber = ccNumber;
            }

            builder.show();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }
*/

    private void ShowBookJump(final Context context)
    {
        try
        {
            CheckLocalInstance(context);

            if (recyclerViewAdapter instanceof BibleArticleAdapter) return;

            final TreeMap<Integer, Integer> mapBookJump = ((BibleAdapter) recyclerViewAdapter).mapBookJump;
            if (mapBookJump == null || mapBookJump.isEmpty()) return;

            final String bbname = PCommon.GetPrefBibleName(context);
            final TreeMap<Integer, BibleRefBO> mapBook = _s.GetListAllBookMapByName(bbname);

            final Typeface typeface = PCommon.GetTypeface(context);
            final int fontSize = PCommon.GetFontSize(context);

            final LayoutInflater inflater = this.getLayoutInflater();
            final View svChapter = inflater.inflate(R.layout.fragment_chapter_list, (ViewGroup) requireActivity().findViewById(R.id.glChapter));
            final GridLayout glChapter = svChapter.findViewById(R.id.glChapter);

            final AlertDialog builder = new AlertDialog.Builder(context).create();
            builder.setCancelable(true);
            builder.setTitle(R.string.mnuContent);
            builder.setView(svChapter);

            int bNumber, pos;
            TextView tvChapter;
            BibleRefBO r;

            for (Integer k : mapBookJump.keySet())
            {
                bNumber = k;
                r = mapBook.get(bNumber);
                pos = mapBookJump.get(k);

                tvChapter = new TextView(context);
                tvChapter.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                tvChapter.setPadding(20, 20, 20, 20);
                tvChapter.setMinHeight(48);
                tvChapter.setFocusable(true);
                tvChapter.setBackground(PCommon.GetDrawable(context, R.drawable.focus_text));
                tvChapter.setText(r.bName);
                tvChapter.setTag(pos);
                if (typeface != null) tvChapter.setTypeface(typeface);
                tvChapter.setTextSize(fontSize);
                tvChapter.setOnClickListener(v -> {
                    builder.dismiss();
                    final int position = (int) v.getTag();
                    recyclerView.scrollToPosition(position);
                });

                glChapter.addView(tvChapter);
            }

            builder.show();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    /***
     * Show plan
     * @param planDayMove   Move number of days in plan calendar
     */
    private void ShowPlan(final int planDayMove)
    {
        try
        {
            //Check
            final PlanDescBO pd = _s.GetPlanDesc(planId);
            if (pd == null) return;

            final int newPlanDayNumber = planDayNumber + planDayMove;
            if (newPlanDayNumber > pd.dayCount)
            {
                final Button btnForward = v.findViewById(R.id.btnForward);
                btnForward.setEnabled(false);
                return;
            }
            if (newPlanDayNumber < 1)
            {
                final Button btnBack = v.findViewById(R.id.btnBack);
                btnBack.setEnabled(false);
                return;
            }

            //Save
            final CacheTabBO t = _s.GetCacheTab(tabNumber());
            if (t == null) return;
            t.fullQuery = PCommon.ConcaT(planId, " ", newPlanDayNumber, " ", planPageNumber);
            t.scrollPosY = 0;
            _s.SaveCacheTab(t);

            final PlanCalBO pc =_s.GetPlanCalByDay(t.bbName, planId, newPlanDayNumber);
            final int bNumberStart = pc.bNumberStart, cNumberStart = pc.cNumberStart, vNumberStart = pc.vNumberStart;
            final int bNumberEnd = pc.bNumberEnd, cNumberEnd = pc.cNumberEnd, vNumberEnd = pc.vNumberEnd;
            final boolean copy =_s.CopyCacheSearchForOtherBible(t.tabNumber, t.trad, bNumberStart, cNumberStart, vNumberStart, bNumberEnd, cNumberEnd, vNumberEnd);
            if (!copy) return;

            onResume();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    private void ShowRndSetup(final Context context)
    {
        try
        {
            CheckLocalInstance(context);

            final Typeface typeface = PCommon.GetTypeface(context);
            final int fontSize = PCommon.GetFontSize(context);

            final LayoutInflater inflater = this.getLayoutInflater();
            final View view = inflater.inflate(R.layout.fragment_rnds, (ViewGroup) requireActivity().findViewById(R.id.svSelection));

            final AlertDialog builder = new AlertDialog.Builder(context).create();
            builder.setCancelable(true);
            builder.setTitle(R.string.mnuRnds);
            builder.setView(view);

            final LinearLayout llItem = view.findViewById(R.id.llFile);

            final String bbnm = PCommon.GetPrefBibleName(context);
            final ArrayList<BibleRefBO> lstRef = _s.GetListAllBookByName(bbnm);

            int i = 0;
            int bnumber;
            String refNr;
            String refText;
            final CheckBox[] chkBook = new CheckBox[66];
            for(BibleRefBO ref : lstRef) {
                bnumber = ref.bNumber;
                refNr = String.format(Locale.US, "%2d", bnumber);
                refText = PCommon.ConcaT(refNr, Html.fromHtml("&nbsp;"), ref.bName);

                chkBook[i] = new CheckBox(context);
                chkBook[i].setLayoutParams(PCommon._layoutParamsWrap);
                chkBook[i].setPadding(10, 10, 10, 10);
                chkBook[i].setEnabled(true);
                chkBook[i].setTag(i);
                chkBook[i].setText(refText);
                chkBook[i].setTextSize(fontSize);
                if (typeface != null) chkBook[i].setTypeface(typeface);
                chkBook[i].setFocusable(true);
                chkBook[i].setBackground(PCommon.GetDrawable(context, R.drawable.focus_text));

                llItem.addView(chkBook[i]);

                i++;
            }

            //Clear
            lstRef.clear();

            //Init
            String catsSelection = PCommon.GetPref(context, IProject.APP_PREF_KEY.RANDOM_CATS_SELECTED, "");
            if (catsSelection.length() != 7) {
                final StringBuilder sb = new StringBuilder();
                for(int j=0; j <= 6; j++) {
                    sb.append("0");
                }
                catsSelection = sb.toString();
            }

            String booksSelection = PCommon.GetPref(context, IProject.APP_PREF_KEY.RANDOM_BOOKS_SELECTED, "");
            if (booksSelection.length() != 66) {
                final StringBuilder sb = new StringBuilder();
                for(int j=0; j <= 65; j++) {
                    sb.append("0");
                }
                booksSelection = sb.toString();
            }

            for(i=0; i <= 65; i++) {
                chkBook[i].setChecked(booksSelection.substring(i, i+1).equalsIgnoreCase("1"));
            }

            //Cats
            class InnerClass {
                void CheckBooks(final ToggleButton btnCat, final List<Integer> lstBookNumber) {
                    try
                    {
                        boolean newStatus = btnCat.isChecked();
                        for (int bNumber : lstBookNumber) chkBook[bNumber - 1].setChecked(newStatus);
                    }
                    catch (Exception ex)
                    {
                        if (PCommon._isDebug) PCommon.LogR(context, ex);
                    }
                }
            }
            final InnerClass innerClass = new InnerClass();

            final ToggleButton btnPentaCat = view.findViewById(R.id.btnPentaCat);
            if (catsSelection.substring(0, 1).equalsIgnoreCase("1")) btnPentaCat.setChecked(true);
            btnPentaCat.setOnClickListener(v -> innerClass.CheckBooks(btnPentaCat, Arrays.asList(1,2,3,4,5)));

            final ToggleButton btnHistoCat = view.findViewById(R.id.btnHistoCat);
            if (catsSelection.substring(1, 2).equalsIgnoreCase("1")) btnHistoCat.setChecked(true);
            btnHistoCat.setOnClickListener(v -> innerClass.CheckBooks(btnHistoCat, Arrays.asList(6,7,8,9,10,11,12,13,14,15,16,17)));

            final ToggleButton btnPoetCat = view.findViewById(R.id.btnPoetCat);
            if (catsSelection.substring(2, 3).equalsIgnoreCase("1")) btnPoetCat.setChecked(true);
            btnPoetCat.setOnClickListener(v -> innerClass.CheckBooks(btnPoetCat, Arrays.asList(18,19,20,21,22)));

            final ToggleButton btnProphetCat = view.findViewById(R.id.btnProphetCat);
            if (catsSelection.substring(3, 4).equalsIgnoreCase("1")) btnProphetCat.setChecked(true);
            btnProphetCat.setOnClickListener(v -> innerClass.CheckBooks(btnProphetCat, Arrays.asList(23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39)));

            final ToggleButton btnEvgCat = view.findViewById(R.id.btnEvgCat);
            if (catsSelection.substring(4, 5).equalsIgnoreCase("1")) btnEvgCat.setChecked(true);
            btnEvgCat.setOnClickListener(v -> innerClass.CheckBooks(btnEvgCat, Arrays.asList(40,41,42,43)));

            final ToggleButton btnPaulCat = view.findViewById(R.id.btnPaulCat);
            if (catsSelection.substring(5, 6).equalsIgnoreCase("1")) btnPaulCat.setChecked(true);
            btnPaulCat.setOnClickListener(v -> innerClass.CheckBooks(btnPaulCat, Arrays.asList(45,46,47,48,49,50,51,52,53,54,55,56,57,58)));

            final ToggleButton btnOeptCat = view.findViewById(R.id.btnOeptCat);
            if (catsSelection.substring(6, 7).equalsIgnoreCase("1")) btnOeptCat.setChecked(true);
            btnOeptCat.setOnClickListener(v -> innerClass.CheckBooks(btnOeptCat, Arrays.asList(59,60,61,62,63,64,65)));

            //Save
            final Button btnSave = view.findViewById(R.id.btnSave);
            btnSave.setOnClickListener(v -> {
                final StringBuilder sbCat = new StringBuilder();
                sbCat.append(btnPentaCat.isChecked() ? "1" : "0");
                sbCat.append(btnHistoCat.isChecked() ? "1" : "0");
                sbCat.append(btnPoetCat.isChecked() ? "1" : "0");
                sbCat.append(btnProphetCat.isChecked() ? "1" : "0");
                sbCat.append(btnEvgCat.isChecked() ? "1" : "0");
                sbCat.append(btnPaulCat.isChecked() ? "1" : "0");
                sbCat.append(btnOeptCat.isChecked() ? "1" : "0");
                final String catSelection = sbCat.toString();
                PCommon.SavePref(v.getContext(), IProject.APP_PREF_KEY.RANDOM_CATS_SELECTED, catSelection);

                final StringBuilder sbBook = new StringBuilder();
                for(int j=0; j <= 65; j++) {
                    sbBook.append(chkBook[j].isChecked() ? "1" : "0");
                }
                final String bookSelection = sbBook.toString();
                PCommon.SavePref(v.getContext(), IProject.APP_PREF_KEY.RANDOM_BOOKS_SELECTED, bookSelection);
                builder.dismiss();
            });

            builder.show();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    private void ShowVerse(final String bbName, final int bNumber, final int cNumber, final int vNumber)
    {
        try
        {
            //Try
            if (!IsVerseExist(bbName, bNumber, cNumber, vNumber)) return;

            //Book title
            ShowBookTitle(false, false, false, false, false);

            final BibleRefBO ref = _s.GetBookRef(bbName, bNumber);
            tvBookTitle.setText(ref.bName.toUpperCase());

            final String title = PCommon.ConcaT(ref.bsName, cNumber, " ", vNumber);
            SetTabTitle(title);
            SaveTab();

            //Get verse
            recyclerViewAdapter = new BibleAdapter(_context, trad, bNumber, cNumber, vNumber);
            if (WhenTabIsEmptyOrNull(true)) return;
            recyclerView.setAdapter(recyclerViewAdapter);
            recyclerView.setHasFixedSize(true);
            recyclerView.scrollToPosition(0);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    private void ShowVerses(final String bbName, final int bNumber, final int cNumber, final int vNumber, final int vNumberTo)
    {
        try
        {
            //Try
            if (!IsVerseExist(bbName, bNumber, cNumber, vNumber)) return;

            //Book title
            final String prblName = GetPrblName(searchFullQuery);
            isPrbl = fragmentType == FRAGMENT_TYPE.SEARCH_TYPE && prblName != null;
            ShowBookTitle(isPrbl, false, false, false, false);

            String tabTitle;
            if (prblName == null)
            {
                final BibleRefBO ref = _s.GetBookRef(bbName, bNumber);
                tvBookTitle.setText(ref.bName.toUpperCase());
                tabTitle = PCommon.ConcaT(ref.bsName, cNumber, " ", vNumber, "-", vNumberTo);
            }
            else
            {
                final int prblId = PCommon.GetResId(_context, prblName);
                tvBookTitle.setText(_context.getString(prblId).toUpperCase());
                tabTitle = prblName;
            }

            SetTabTitle(tabTitle);
            SaveTab();

            //Get verses
            recyclerViewAdapter = new BibleAdapter(_context, trad, bNumber, cNumber, vNumber, vNumberTo);
            if (WhenTabIsEmptyOrNull(true)) return;
            recyclerView.setAdapter(recyclerViewAdapter);
            recyclerView.setHasFixedSize(true);
            recyclerView.scrollToPosition(scrollPosY);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    private void ShowBookTitle(final boolean showBook, final boolean showRnd, final boolean showHarmony, final boolean showLex, final boolean showSearch)
    {
        ShowBookTitle(showBook, showRnd, showHarmony, showLex, showSearch, null);
    }

    private void ShowBookTitle(final boolean showBook, final boolean showRnd, final boolean showHarmony, final boolean showLex, boolean showSearch, final PlanCalBO pc) {
        try
        {
            tvBookTitle.setVisibility(showBook || showHarmony ? View.VISIBLE : View.GONE);
            btnBack.setVisibility(showBook || showHarmony ? View.VISIBLE : View.GONE);
            btnForward.setVisibility(showBook || showHarmony ? View.VISIBLE : View.GONE);
            btnRndRefresh.setVisibility(showRnd ? View.VISIBLE : View.GONE);
            btnRndSetup.setVisibility(showRnd ? View.VISIBLE : View.GONE);
            clHarm.setVisibility(showHarmony ? View.VISIBLE : View.GONE);
            btnLexInfo.setVisibility(showLex ? View.VISIBLE : View.GONE);
            btnLexContent.setVisibility(showLex ? View.VISIBLE : View.GONE);
            btnLexContentDump.setVisibility(showLex ? View.INVISIBLE : View.GONE);
            btnSearchContent.setVisibility(showSearch && (!isLex) ? View.VISIBLE : View.GONE);

            if (showLex && btnLexInfo.getText().toString().isEmpty()) {
                final ArrayList<LexTbesBO> arrLexDetail = _s.GetLexDetailByHGNr(searchFullQuery);
                if (arrLexDetail != null) {
                    if (!arrLexDetail.isEmpty()) {
                        final LexTbesBO lexDetail = arrLexDetail.get(0);
                        final String desc = (lexDetail.english == null || lexDetail.english.isEmpty())
                                ? lexDetail.hg
                                : PCommon.ConcaT(PCommon.GetRTL(), "(", lexDetail.english.toUpperCase(), ") ", lexDetail.hg);
                        final SpannableStringBuilder spanLexInfo = PCommon.GetBiggerHGText(_context, desc);
                        btnLexInfo.setText(spanLexInfo);
                    }
                }
            }

            if (fragmentType == FRAGMENT_TYPE.PLAN_TYPE && pc != null)
            {
                final int checkDrawable = !MainActivity.Tab.IsThemeWhite() ? R.drawable.ic_check_white_24dp : R.drawable.ic_check_black_24dp;
                final int uncheckDrawable = !MainActivity.Tab.IsThemeWhite() ? R.drawable.ic_uncheck_white_24dp : R.drawable.ic_uncheck_black_24dp;
                final boolean isReadOrig = pc.isRead == 1;
                PCommon.SetIconTextView(_context, tvBookTitle, isReadOrig ? checkDrawable : uncheckDrawable);
                tvBookTitle.setTag(R.id.tv1, planId);
                tvBookTitle.setTag(R.id.tv2, planDayNumber);
                tvBookTitle.setOnClickListener(v -> {
                    final int planId = (int) v.getTag(R.id.tv1);
                    final int dayNumber = (int) v.getTag(R.id.tv2);
                    final PlanCalBO pc1 = _s.GetPlanCalByDay(bbName, planId, dayNumber);
                    if (pc1 == null) return;

                    final boolean isRead = pc1.isRead == 1;
                    final int updIsRead = isRead ? 0 : 1;
                    PCommon.SetIconTextView(v.getContext(), tvBookTitle, updIsRead == 1 ? checkDrawable : uncheckDrawable);

                    _s.MarkPlanCal(planId, planDayNumber, updIsRead);
                });
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    private void SaveRef(final boolean isBook, final boolean isChapter, final boolean isVerse, final int bNumber, final int cNumber, final int vNumber)
    {
        this.isBook = isBook;
        this.isChapter = isChapter;
        this.isVerse = isVerse;
        this.bNumber = bNumber;
        this.cNumber = cNumber;
        this.vNumber = vNumber;
    }

    private int tabNumber()
    {
        return MainActivity.Tab.GetCurrentTabPosition();
    }

    private void SaveTab()
    {
        final String tabType = (fragmentType == FRAGMENT_TYPE.SEARCH_TYPE) ? "S" : (fragmentType == FRAGMENT_TYPE.PLAN_TYPE) ? "P" : (fragmentType == FRAGMENT_TYPE.FAV_TYPE) ? "F" : "A";
        if (trad == null || trad.isEmpty()) trad = bbName;
        final CacheTabBO cacheTab = new CacheTabBO(tabNumber(), tabType, tabTitle, searchFullQuery, scrollPosY, bbName, isBook, isChapter, isVerse, bNumber, cNumber, vNumber, trad);
        _s.SaveCacheTab(cacheTab);
        SetLayoutManager();
    }

    /***
     * Set local bible name
     */
    private void SetLocalBibleName()
    {
        if (bbName == null) bbName = PCommon.GetPrefBibleName(_context);
    }

    private String RollBookName(final String bbName)
    {
        try
        {
            final int INSTALL_STATUS = PCommon.GetInstallStatus(_context);
            switch (INSTALL_STATUS)
            {
                case 22:
                    return (bbName.equalsIgnoreCase("k")
                            ? "2"
                            : (bbName.equalsIgnoreCase("2"))
                            ? "v"
                            : (bbName.equalsIgnoreCase("v"))
                            ? "9"
                            : (bbName.equalsIgnoreCase("9"))
                            ? "l"
                            : (bbName.equalsIgnoreCase("l"))
                            ? "o"
                            : (bbName.equalsIgnoreCase("o"))
                            ? "d"
                            : (bbName.equalsIgnoreCase("d"))
                            ? "1"
                            : (bbName.equalsIgnoreCase("1"))
                            ? "a"
                            : (bbName.equalsIgnoreCase("a"))
                            ? "e"
                            : (bbName.equalsIgnoreCase("e"))
                            ? "s"
                            : (bbName.equalsIgnoreCase("s"))
                            ? "i"
                            : (bbName.equalsIgnoreCase("i"))
                            ? "b"
                            : (bbName.equalsIgnoreCase("b"))
                            ? "y"
                            : (bbName.equalsIgnoreCase("y"))
                            ? "c"
                            : (bbName.equalsIgnoreCase("c"))
                            ? "j"
                            : (bbName.equalsIgnoreCase("j"))
                            ? "u"
                            : (bbName.equalsIgnoreCase("u"))
                            ? "z"
                            : (bbName.equalsIgnoreCase("z"))
                            ? "r"
                            : (bbName.equalsIgnoreCase("r"))
                            ? "t"
                            : (bbName.equalsIgnoreCase("t"))
                            ? "h"
                            : (bbName.equalsIgnoreCase("h"))
                            ? "w"
                            : "k");
                case 21:
                    return (bbName.equalsIgnoreCase("k")
                            ? "2"
                            : (bbName.equalsIgnoreCase("2"))
                            ? "v"
                            : (bbName.equalsIgnoreCase("v"))
                            ? "9"
                            : (bbName.equalsIgnoreCase("9"))
                            ? "l"
                            : (bbName.equalsIgnoreCase("l"))
                            ? "o"
                            : (bbName.equalsIgnoreCase("o"))
                            ? "d"
                            : (bbName.equalsIgnoreCase("d"))
                            ? "1"
                            : (bbName.equalsIgnoreCase("1"))
                            ? "a"
                            : (bbName.equalsIgnoreCase("a"))
                            ? "e"
                            : (bbName.equalsIgnoreCase("e"))
                            ? "s"
                            : (bbName.equalsIgnoreCase("s"))
                            ? "i"
                            : (bbName.equalsIgnoreCase("i"))
                            ? "b"
                            : (bbName.equalsIgnoreCase("b"))
                            ? "y"
                            : (bbName.equalsIgnoreCase("y"))
                            ? "c"
                            : (bbName.equalsIgnoreCase("c"))
                            ? "j"
                            : (bbName.equalsIgnoreCase("j"))
                            ? "u"
                            : (bbName.equalsIgnoreCase("u"))
                            ? "r"
                            : (bbName.equalsIgnoreCase("r"))
                            ? "t"
                            : (bbName.equalsIgnoreCase("t"))
                            ? "h"
                            : (bbName.equalsIgnoreCase("h"))
                            ? "w"
                            : "k");
                case 20:
                    return (bbName.equalsIgnoreCase("k")
                            ? "2"
                            : (bbName.equalsIgnoreCase("2"))
                            ? "v"
                            : (bbName.equalsIgnoreCase("v"))
                            ? "9"
                            : (bbName.equalsIgnoreCase("9"))
                            ? "l"
                            : (bbName.equalsIgnoreCase("l"))
                            ? "o"
                            : (bbName.equalsIgnoreCase("o"))
                            ? "d"
                            : (bbName.equalsIgnoreCase("d"))
                            ? "1"
                            : (bbName.equalsIgnoreCase("1"))
                            ? "a"
                            : (bbName.equalsIgnoreCase("a"))
                            ? "e"
                            : (bbName.equalsIgnoreCase("e"))
                            ? "s"
                            : (bbName.equalsIgnoreCase("s"))
                            ? "i"
                            : (bbName.equalsIgnoreCase("i"))
                            ? "b"
                            : (bbName.equalsIgnoreCase("b"))
                            ? "y"
                            : (bbName.equalsIgnoreCase("y"))
                            ? "c"
                            : (bbName.equalsIgnoreCase("c"))
                            ? "j"
                            : (bbName.equalsIgnoreCase("j"))
                            ? "r"
                            : (bbName.equalsIgnoreCase("r"))
                            ? "t"
                            : (bbName.equalsIgnoreCase("t"))
                            ? "h"
                            : (bbName.equalsIgnoreCase("h"))
                            ? "w"
                            : "k");
                case 19:
                    return (bbName.equalsIgnoreCase("k")
                            ? "2"
                            : (bbName.equalsIgnoreCase("2"))
                            ? "v"
                            : (bbName.equalsIgnoreCase("v"))
                            ? "9"
                            : (bbName.equalsIgnoreCase("9"))
                            ? "l"
                            : (bbName.equalsIgnoreCase("l"))
                            ? "o"
                            : (bbName.equalsIgnoreCase("o"))
                            ? "d"
                            : (bbName.equalsIgnoreCase("d"))
                            ? "1"
                            : (bbName.equalsIgnoreCase("1"))
                            ? "a"
                            : (bbName.equalsIgnoreCase("a"))
                            ? "i"
                            : (bbName.equalsIgnoreCase("i"))
                            ? "b"
                            : (bbName.equalsIgnoreCase("b"))
                            ? "y"
                            : (bbName.equalsIgnoreCase("y"))
                            ? "c"
                            : (bbName.equalsIgnoreCase("c"))
                            ? "s"
                            : (bbName.equalsIgnoreCase("s"))
                            ? "j"
                            : (bbName.equalsIgnoreCase("j"))
                            ? "r"
                            : (bbName.equalsIgnoreCase("r"))
                            ? "t"
                            : (bbName.equalsIgnoreCase("t"))
                            ? "h"
                            : (bbName.equalsIgnoreCase("h"))
                            ? "w"
                            : "k");
                case 18:
                    return (bbName.equalsIgnoreCase("k")
                            ? "2"
                            : (bbName.equalsIgnoreCase("2"))
                            ? "v"
                            : (bbName.equalsIgnoreCase("v"))
                            ? "9"
                            : (bbName.equalsIgnoreCase("9"))
                            ? "l"
                            : (bbName.equalsIgnoreCase("l"))
                            ? "o"
                            : (bbName.equalsIgnoreCase("o"))
                            ? "d"
                            : (bbName.equalsIgnoreCase("d"))
                            ? "1"
                            : (bbName.equalsIgnoreCase("1"))
                            ? "a"
                            : (bbName.equalsIgnoreCase("a"))
                            ? "i"
                            : (bbName.equalsIgnoreCase("i"))
                            ? "b"
                            : (bbName.equalsIgnoreCase("b"))
                            ? "y"
                            : (bbName.equalsIgnoreCase("y"))
                            ? "c"
                            : (bbName.equalsIgnoreCase("c"))
                            ? "s"
                            : (bbName.equalsIgnoreCase("s"))
                            ? "j"
                            : (bbName.equalsIgnoreCase("j"))
                            ? "r"
                            : (bbName.equalsIgnoreCase("r"))
                            ? "t"
                            : "k");
                case 17:
                    return (bbName.equalsIgnoreCase("k")
                            ? "2"
                            : (bbName.equalsIgnoreCase("2"))
                            ? "v"
                            : (bbName.equalsIgnoreCase("v"))
                            ? "9"
                            : (bbName.equalsIgnoreCase("9"))
                            ? "l"
                            : (bbName.equalsIgnoreCase("l"))
                            ? "o"
                            : (bbName.equalsIgnoreCase("o"))
                            ? "d"
                            : (bbName.equalsIgnoreCase("d"))
                            ? "1"
                            : (bbName.equalsIgnoreCase("1"))
                            ? "a"
                            : (bbName.equalsIgnoreCase("a"))
                            ? "i"
                            : (bbName.equalsIgnoreCase("i"))
                            ? "y"
                            : (bbName.equalsIgnoreCase("y"))
                            ? "c"
                            : (bbName.equalsIgnoreCase("c"))
                            ? "s"
                            : (bbName.equalsIgnoreCase("s"))
                            ? "j"
                            : (bbName.equalsIgnoreCase("j"))
                            ? "r"
                            : (bbName.equalsIgnoreCase("r"))
                            ? "w"
                            : (bbName.equalsIgnoreCase("w"))
                            ? "t"
                            : "k");
                case 16:
                    return (bbName.equalsIgnoreCase("k")
                            ? "2"
                            : (bbName.equalsIgnoreCase("2"))
                            ? "v"
                            : (bbName.equalsIgnoreCase("v"))
                            ? "9"
                            : (bbName.equalsIgnoreCase("9"))
                            ? "l"
                            : (bbName.equalsIgnoreCase("l"))
                            ? "o"
                            : (bbName.equalsIgnoreCase("o"))
                            ? "d"
                            : (bbName.equalsIgnoreCase("d"))
                            ? "1"
                            : (bbName.equalsIgnoreCase("1"))
                            ? "a"
                            : (bbName.equalsIgnoreCase("a"))
                            ? "i"
                            : (bbName.equalsIgnoreCase("i"))
                            ? "y"
                            : (bbName.equalsIgnoreCase("y"))
                            ? "c"
                            : (bbName.equalsIgnoreCase("c"))
                            ? "s"
                            : (bbName.equalsIgnoreCase("s"))
                            ? "j"
                            : (bbName.equalsIgnoreCase("j"))
                            ? "r"
                            : (bbName.equalsIgnoreCase("r"))
                            ? "w"
                            : "k");
                case 15:
                    return (bbName.equalsIgnoreCase("k")
                            ? "2"
                            : (bbName.equalsIgnoreCase("2"))
                            ? "v"
                            : (bbName.equalsIgnoreCase("v"))
                            ? "9"
                            : (bbName.equalsIgnoreCase("9"))
                            ? "l"
                            : (bbName.equalsIgnoreCase("l"))
                            ? "o"
                            : (bbName.equalsIgnoreCase("o"))
                            ? "d"
                            : (bbName.equalsIgnoreCase("d"))
                            ? "1"
                            : (bbName.equalsIgnoreCase("1"))
                            ? "a"
                            : (bbName.equalsIgnoreCase("a"))
                            ? "i"
                            : (bbName.equalsIgnoreCase("i"))
                            ? "y"
                            : (bbName.equalsIgnoreCase("y"))
                            ? "c"
                            : (bbName.equalsIgnoreCase("c"))
                            ? "s"
                            : (bbName.equalsIgnoreCase("s"))
                            ? "j"
                            : (bbName.equalsIgnoreCase("j"))
                            ? "r"
                            : "k");
                case 14:
                    return (bbName.equalsIgnoreCase("k")
                            ? "2"
                            : (bbName.equalsIgnoreCase("2"))
                            ? "v"
                            : (bbName.equalsIgnoreCase("v"))
                            ? "9"
                            : (bbName.equalsIgnoreCase("9"))
                            ? "l"
                            : (bbName.equalsIgnoreCase("l"))
                            ? "o"
                            : (bbName.equalsIgnoreCase("o"))
                            ? "d"
                            : (bbName.equalsIgnoreCase("d"))
                            ? "1"
                            : (bbName.equalsIgnoreCase("1"))
                            ? "a"
                            : (bbName.equalsIgnoreCase("a"))
                            ? "i"
                            : (bbName.equalsIgnoreCase("i"))
                            ? "y"
                            : (bbName.equalsIgnoreCase("y"))
                            ? "c"
                            : (bbName.equalsIgnoreCase("c"))
                            ? "s"
                            : (bbName.equalsIgnoreCase("s"))
                            ? "j"
                            : "k");
                case 13:
                    return (bbName.equalsIgnoreCase("k")
                            ? "2"
                            : (bbName.equalsIgnoreCase("2"))
                            ? "v"
                            : (bbName.equalsIgnoreCase("v"))
                            ? "9"
                            : (bbName.equalsIgnoreCase("9"))
                            ? "l"
                            : (bbName.equalsIgnoreCase("l"))
                            ? "o"
                            : (bbName.equalsIgnoreCase("o"))
                            ? "d"
                            : (bbName.equalsIgnoreCase("d"))
                            ? "1"
                            : (bbName.equalsIgnoreCase("1"))
                            ? "a"
                            : (bbName.equalsIgnoreCase("a"))
                            ? "i"
                            : (bbName.equalsIgnoreCase("i"))
                            ? "y"
                            : (bbName.equalsIgnoreCase("y"))
                            ? "c"
                            : (bbName.equalsIgnoreCase("c"))
                            ? "s"
                            : "k");
                case 12:
                    return (bbName.equalsIgnoreCase("k")
                            ? "2"
                            : (bbName.equalsIgnoreCase("2"))
                            ? "v"
                            : (bbName.equalsIgnoreCase("v"))
                            ? "9"
                            : (bbName.equalsIgnoreCase("9"))
                            ? "l"
                            : (bbName.equalsIgnoreCase("l"))
                            ? "o"
                            : (bbName.equalsIgnoreCase("o"))
                            ? "d"
                            : (bbName.equalsIgnoreCase("d"))
                            ? "1"
                            : (bbName.equalsIgnoreCase("1"))
                            ? "a"
                            : (bbName.equalsIgnoreCase("a"))
                            ? "i"
                            : (bbName.equalsIgnoreCase("i"))
                            ? "y"
                            : (bbName.equalsIgnoreCase("y"))
                            ? "c"
                            : "k");
                case 11:
                    return (bbName.equalsIgnoreCase("k")
                            ? "2"
                            : (bbName.equalsIgnoreCase("2"))
                            ? "v"
                            : (bbName.equalsIgnoreCase("v"))
                            ? "9"
                            : (bbName.equalsIgnoreCase("9"))
                            ? "l"
                            : (bbName.equalsIgnoreCase("l"))
                            ? "o"
                            : (bbName.equalsIgnoreCase("o"))
                            ? "d"
                            : (bbName.equalsIgnoreCase("d"))
                            ? "1"
                            : (bbName.equalsIgnoreCase("1"))
                            ? "a"
                            : (bbName.equalsIgnoreCase("a"))
                            ? "i"
                            : (bbName.equalsIgnoreCase("i"))
                            ? "y"
                            : "k");
                case 10:
                    return (bbName.equalsIgnoreCase("k")
                            ? "2"
                            : (bbName.equalsIgnoreCase("2"))
                            ? "v"
                            : (bbName.equalsIgnoreCase("v"))
                            ? "9"
                            : (bbName.equalsIgnoreCase("9"))
                            ? "l"
                            : (bbName.equalsIgnoreCase("l"))
                            ? "o"
                            : (bbName.equalsIgnoreCase("o"))
                            ? "d"
                            : (bbName.equalsIgnoreCase("d"))
                            ? "1"
                            : (bbName.equalsIgnoreCase("1"))
                            ? "a"
                            : (bbName.equalsIgnoreCase("a"))
                            ? "i"
                            : "k");
                case 9:
                {
                    return (bbName.equalsIgnoreCase("k")
                        ? "2"
                        : (bbName.equalsIgnoreCase("2"))
                            ? "v"
                            : (bbName.equalsIgnoreCase("v"))
                                ? "9"
                                : (bbName.equalsIgnoreCase("9"))
                                    ? "l"
                                    : (bbName.equalsIgnoreCase("l"))
                                        ? "o"
                                        : (bbName.equalsIgnoreCase("o"))
                                            ? "d"
                                            : (bbName.equalsIgnoreCase("d"))
                                                ? "1"
                                                : (bbName.equalsIgnoreCase("1"))
                                                    ? "a"
                                                    : "k");
                }
                case 8:
                {
                    return (bbName.equalsIgnoreCase("k")
                        ? "2"
                        : (bbName.equalsIgnoreCase("2"))
                            ? "v"
                            : (bbName.equalsIgnoreCase("v"))
                                ? "9"
                                : (bbName.equalsIgnoreCase("9"))
                                    ? "l"
                                    : (bbName.equalsIgnoreCase("l"))
                                        ? "o"
                                        : (bbName.equalsIgnoreCase("o"))
                                            ? "d"
                                            : (bbName.equalsIgnoreCase("d"))
                                                ? "a"
                                                : "k");
                }
                case 7:
                {
                    return (bbName.compareToIgnoreCase("l") == 0) ? "d" : (bbName.compareToIgnoreCase("v") == 0) ? "l" : (bbName.compareToIgnoreCase("d") == 0) ? "a" : (bbName.compareToIgnoreCase("a") == 0) ? "o" : (bbName.compareToIgnoreCase("o") == 0) ? "2" : (bbName.compareToIgnoreCase("k") == 0) ? "v" : "k";
                }
                case 6:
                {
                    return (bbName.compareToIgnoreCase("l") == 0) ? "d" : (bbName.compareToIgnoreCase("v") == 0) ? "l" : (bbName.compareToIgnoreCase("d") == 0) ? "a" : (bbName.compareToIgnoreCase("a") == 0) ? "o" : (bbName.compareToIgnoreCase("k") == 0) ? "v" : "k";
                }
                case 1:
                {
                    return "k";
                }
                case 2:
                {
                    return (bbName.compareToIgnoreCase("v") == 0) ? "k" : "v";
                }
                case 3:
                {
                    return (bbName.compareToIgnoreCase("l") == 0) ? "k" : (bbName.compareToIgnoreCase("v") == 0) ? "l" : "v";
                }
                case 4:
                {
                    return (bbName.compareToIgnoreCase("l") == 0) ? "d" : (bbName.compareToIgnoreCase("v") == 0) ? "l" : (bbName.compareToIgnoreCase("k") == 0) ? "v" : "k";
                }
                case 5:
                {
                    return (bbName.compareToIgnoreCase("l") == 0) ? "d" : (bbName.compareToIgnoreCase("v") == 0) ? "l" : (bbName.compareToIgnoreCase("d") == 0) ? "a" : (bbName.compareToIgnoreCase("k") == 0) ? "v" : "k";
                }
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }

        return "k";
    }

/*
    private void ShowFavSearch()
    {
        try
        {
            final AlertDialog builder = new AlertDialog.Builder(getContext()).create();
            final LayoutInflater inflater = getActivity().getLayoutInflater();
            final View view = inflater.inflate(R.layout.fragment_fav_search, (ViewGroup) getActivity().findViewById(R.id.llFavOrderBy));

            class InnerClass
            {
                private void OnDismiss(View view)
                {
                    try
                    {
                        final int orderBy = Integer.parseInt(view.getTag().toString());
                        PCommon.SavePrefInt(_context, IProject.APP_PREF_KEY.FAV_ORDER, orderBy);
                        builder.dismiss();

                        recyclerViewAdapter = new BibleAdapter(getContext(), bbName, searchFullQuery, orderBy, null);
                        if (WhenTabIsEmptyOrNull(true)) return;
                        recyclerView.setAdapter(recyclerViewAdapter);
                        recyclerView.setHasFixedSize(true);
                        recyclerView.scrollToPosition(scrollPosY);
                    }
                    catch (Exception ex)
                    {
                        if (PCommon._isDebugVersion) PCommon.LogR(view.getContext(), ex);
                    }
                }
            }
            final InnerClass innerClass = new InnerClass();

            final Button btn1 = view.findViewById(R.id.btnFavOrder1);
            btn1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    innerClass.OnDismiss(view);
                }
            });
            final Button btn2 = view.findViewById(R.id.btnFavOrder2);
            btn2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    innerClass.OnDismiss(view);
                }
            });

            builder.setCancelable(true);
            builder.setView(view);
            builder.setTitle(R.string.favOrderTitle);
            builder.show();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebugVersion) PCommon.LogR(getContext(), ex);
        }
    }
*/

    private void SetTabTitle(final String title)
    {
        tabTitle = title;
        MainActivity.Tab.SetCurrentTabTitle(title);
    }

    @SuppressWarnings({"UnusedAssignment", "ConstantConditions"})
    private void SearchBible(final boolean useCache)
    {
        try
        {
            if (fragmentType == FRAGMENT_TYPE.FAV_TYPE)
            {
                /* if (searchFullQuery != null)
                {
                    if (searchFullQuery.isEmpty()) searchFullQuery = null;
                }
                */

                SaveTab();

                final int orderBy = PCommon.GetFavOrder(_context);
                final int favType = PCommon.GetFavFilter(_context);
                recyclerViewAdapter = new BibleAdapter(getContext(), bbName, searchFullQuery, orderBy, favType);
                if (WhenTabIsEmptyOrNull(true)) return;
                recyclerView.setAdapter(recyclerViewAdapter);
                recyclerView.setHasFixedSize(true);
                recyclerView.scrollToPosition(scrollPosY);

                return;
                //TODO: this is a menu_divider. After I have to create all SearchFav methods or add a param to searchbible to convert the queries
            }
            else if (fragmentType == FRAGMENT_TYPE.ARTICLE_TYPE)
            {
                //No recycler
                return;
            }

            if (useCache)
            {
                recyclerViewAdapter = new BibleAdapter(_context, tabNumber());
                final int itemCount = recyclerViewAdapter.getItemCount();
                if (itemCount > 0)
                {
                    SetLayoutManager();

                    recyclerView.setAdapter(recyclerViewAdapter);
                    recyclerView.setHasFixedSize(true);
                    recyclerView.scrollToPosition(scrollPosY);

                    return;
                }
            }

            SetLocalBibleName();

            class InnerClass
            {
                private String MergeWords(final int fromWordPos, final String[] words)
                {
                    final int toWordPos = words.length - 1;

                    String mergedString = "";
                    for(int i=fromWordPos; i <= toWordPos; i++)
                    {
                        if (i != fromWordPos)
                            mergedString = PCommon.ConcaT(mergedString, " ", words[ i ]);
                        else
                            mergedString = PCommon.ConcaT(mergedString, words[ i ]);
                    }

                    return mergedString;
                }
            }

            boolean isBook = false,  isChapter = false,  isVerse = false;
            int     bNumber = 0,     cNumber = 0,        vNumber = 0,       wCount;
            final   String patternDigit = "\\d+";
            final   int splitTimes = searchFullQuery.split("\\s").length;
            String[] words;

            if (!searchFullQuery.contains(","))
            {
                words = searchFullQuery.split("\\s", splitTimes);
            }
            else
            {
                final String[] testWords = searchFullQuery.split("\\s");
                final int testWcount = testWords.length;
                final List<String> genWords = new ArrayList<>();
                String testNumbersExpr = "";

                if (testWcount >= 2) {
                    final int testBnumber = testWords[0].matches(patternDigit) ? Integer.parseInt(testWords[0]) : _s.GetBookNumberByName(bbName, testWords[0]);
                    if (testBnumber > 0) {
                        testNumbersExpr = PCommon.ConcaT(testWords[ 0 ], " ");
                        genWords.add(testWords[ 0 ]);

                        final int testCnumber = testWords[1].matches(patternDigit) ? Integer.parseInt(testWords[1]) : -1;
                        if (testCnumber > 0) {
                            testNumbersExpr = PCommon.ConcaT(testNumbersExpr, testCnumber, " ");
                            genWords.add(PCommon.ConcaT(testCnumber));
                        }
                    }
                }

                final String testCommaExpr = searchFullQuery.substring(testNumbersExpr.length());
                genWords.add(testCommaExpr);

                words = new String[genWords.size()];
                genWords.toArray(words);
            }
            wCount = words.length;

            //--------------------------------------------------------------------------------------
            ShowBookTitle(false, isRnd, isHarmony, isLex, isSearch);

            if (wCount == 0) return;
            //...so minimum is one

            if (isRnd && wCount == 1)
            {
                //RAND:
                isBook = true;
                //noinspection ConstantConditions
                isChapter = false;
                //noinspection ConstantConditions
                isVerse = false;

                bNumber = 0;
                cNumber = 0;
                vNumber = 0;

                // noinspection ConstantConditions
                SaveRef(isBook, isChapter, isVerse, bNumber, cNumber, vNumber);

                final String searchExpr = "RAND:";
                if (PCommon._isDebug) System.out.println(PCommon.ConcaT("SearchExpr: ", searchExpr));

                SetTabTitle("RND");
                SaveTab();

                String booksSelection = PCommon.GetPref(_context, IProject.APP_PREF_KEY.RANDOM_BOOKS_SELECTED, "");
                if (booksSelection.length() != 66 || !booksSelection.contains("1")) {
                    final StringBuilder sb = new StringBuilder();
                    for(int j=0; j <= 65; j++) {
                        sb.append("1");
                    }
                    booksSelection = sb.toString();
                }

                recyclerViewAdapter = new BibleAdapter(_context, bbName, booksSelection, 5);
                if (WhenTabIsEmptyOrNull(true)) return;
                recyclerView.setAdapter(recyclerViewAdapter);
                recyclerView.setHasFixedSize(true);
                recyclerView.scrollToPosition(scrollPosY);

                return;
            }

            if (isHarmony && wCount == 2)
            {
                harmId = Integer.parseInt(words[ 0 ]);

                //HARMONY:
                isBook = true;
                //noinspection ConstantConditions
                isChapter = false;
                //noinspection ConstantConditions
                isVerse = false;

                bNumber = 0;
                cNumber = 0;
                vNumber = 0;

                // noinspection ConstantConditions
                SaveRef(isBook, isChapter, isVerse, bNumber, cNumber, vNumber);

                final String searchExpr = "HARMONY:";
                if (PCommon._isDebug) System.out.println(PCommon.ConcaT("SearchExpr: ", searchExpr));

                SetTabTitle(PCommon.ConcaT("HARMONY", harmId));
                SaveTab();

                recyclerViewAdapter = new BibleHarmonyAdapter(_context, harmId, bbName, tvBookTitle, btnHarmMt, btnHarmMc, btnHarmLc, btnHarmJn);
                if (WhenTabIsEmptyOrNull(true)) return;
                recyclerView.setAdapter(recyclerViewAdapter);
                recyclerView.setHasFixedSize(true);
                recyclerView.scrollToPosition(scrollPosY);

                return;
            }

            //Try to get bName and convert it to bNumber
            bNumber = words[ 0 ].matches(patternDigit) ? Integer.parseInt(words[ 0 ]) : _s.GetBookNumberByName( bbName, words[ 0 ] );

            if (wCount == 4)
            {
                if ((words[0].matches(patternDigit) && words[1].matches(patternDigit) && words[2].matches(patternDigit) && words[3].matches(patternDigit))
                        || (bNumber > 0 && words[1].matches(patternDigit) && words[2].matches(patternDigit) && words[3].matches(patternDigit)))
                {
                    //<bNumber> <cNumber> <vNumber> <vNumberTo>
                    isBook = true;
                    //noinspection ConstantConditions
                    isChapter = false;
                    //noinspection ConstantConditions
                    isVerse = false;

                    if (bNumber <= 0) bNumber = Integer.parseInt(words[0]);
                    cNumber = Integer.parseInt(words[1]);
                    vNumber = Integer.parseInt(words[2]);
                    final int vNumberTo = Integer.parseInt(words[3]);

                    // noinspection ConstantConditions
                    SaveRef(isBook, isChapter, isVerse, bNumber, cNumber, vNumber);
                    ShowVerses(bbName, bNumber, cNumber, vNumber, vNumberTo);

                    return;
                }
                else if ((words[0].matches(patternDigit) && words[1].matches(patternDigit) && words[2].matches(patternDigit) && words[3].equalsIgnoreCase("CR:")))
                {
                    //<bNumber> <cNumber> <vNumber> CR:
                    isBook = true;
                    //noinspection ConstantConditions
                    isChapter = false;
                    //noinspection ConstantConditions
                    isVerse = false;

                    if (bNumber <= 0) bNumber = Integer.parseInt(words[0]);
                    cNumber = Integer.parseInt(words[1]);
                    vNumber = Integer.parseInt(words[2]);

                    // noinspection ConstantConditions
                    SaveRef(isBook, isChapter, isVerse, bNumber, cNumber, vNumber);

                    final String searchExpr = "";
                    if (PCommon._isDebug) System.out.println(PCommon.ConcaT("SearchExpr: ", searchExpr));

                    SetTabTitle("CR");
                    SaveTab();

                    recyclerViewAdapter = new BibleAdapter(_context, trad, bNumber, cNumber, vNumber, "");
                    if (WhenTabIsEmptyOrNull(true)) return;
                    recyclerView.setAdapter(recyclerViewAdapter);
                    recyclerView.setHasFixedSize(true);
                    recyclerView.scrollToPosition(scrollPosY);

                    return;
                }
            }

            if (wCount == 3)
            {
                //<bNumber> <cNumber> <vNumber>
                if ((words[0].matches(patternDigit) && words[1].matches(patternDigit) && words[2].matches(patternDigit)) ||
                        (bNumber > 0 && words[1].matches(patternDigit) && words[2].matches(patternDigit)))
                {
                    isBook = true;
                    isChapter = true;
                    isVerse = true;

                    if (bNumber <= 0) bNumber = Integer.parseInt(words[0]);
                    cNumber = Integer.parseInt(words[1]);
                    vNumber = Integer.parseInt(words[2]);

                    // noinspection ConstantConditions
                    SaveRef(isBook, isChapter, isVerse, bNumber, cNumber, vNumber);
                    ShowVerse(bbName, bNumber, cNumber, vNumber);

                    return;
                }
            }

            if (wCount >= 3)
            {
                //<bNumber> <cNumber> <expr>...
                if ((words[0].matches(patternDigit) && words[1].matches(patternDigit)) ||
                        (bNumber > 0 && words[1].matches(patternDigit)))
                {
                    isBook = true;
                    //noinspection ConstantConditions
                    isChapter = false;
                    //noinspection ConstantConditions
                    isVerse = false;

                    if (bNumber <= 0) bNumber = Integer.parseInt(words[0]);
                    cNumber = Integer.parseInt(words[1]);
                    //noinspection ConstantConditions
                    vNumber = 0;

                    // noinspection ConstantConditions
                    SaveRef(isBook, isChapter, isVerse, bNumber, cNumber, vNumber);

                    final InnerClass innerClass = new InnerClass();
                    final String searchExpr = innerClass.MergeWords(2, words);
                    if (PCommon._isDebug) System.out.println(PCommon.ConcaT("SearchExpr: ", searchExpr));

                    SetTabTitle(searchExpr);
                    SaveTab();

                    recyclerViewAdapter = new BibleAdapter(_context, bbName, bNumber, cNumber, searchExpr);
                    if (WhenTabIsEmptyOrNull(true)) return;
                    recyclerView.setAdapter(recyclerViewAdapter);
                    recyclerView.setHasFixedSize(true);
                    recyclerView.scrollToPosition(scrollPosY);

                    return;
                }
            }

            if (wCount == 2)
            {
                //<bNumber> <cNumber>
                if (( words[ 0 ].matches(patternDigit) && words[ 1 ].matches(patternDigit) ) ||
                    (bNumber > 0 && words[ 1 ].matches(patternDigit) ))
                {
                    isBook = true;
                    isChapter = true;
                    //noinspection ConstantConditions
                    isVerse = false;

                    if (bNumber <= 0) bNumber = Integer.parseInt(words[ 0 ]);
                    cNumber = Integer.parseInt(words[ 1 ]);
                    //noinspection ConstantConditions
                    vNumber = 0;

                    // noinspection ConstantConditions
                    SaveRef(isBook, isChapter, isVerse, bNumber, cNumber, vNumber);
                    ShowChapter(bbName, bNumber, cNumber);

                    return;
                }
            }

            if (wCount >= 2)
            {
                //<bNumber> <expr>...
                if ((words[0].matches(patternDigit)) ||
                        (bNumber > 0 ))
                {
                    isBook = true;
                    //noinspection ConstantConditions
                    isChapter = false;
                    //noinspection ConstantConditions
                    isVerse = false;

                    if (bNumber <= 0) bNumber = Integer.parseInt(words[0]);
                    //noinspection ConstantConditions
                    cNumber = 0;
                    //noinspection ConstantConditions
                    vNumber = 0;

                    // noinspection ConstantConditions
                    SaveRef(isBook, isChapter, isVerse, bNumber, cNumber, vNumber);

                    final InnerClass innerClass = new InnerClass();
                    final String searchExpr = innerClass.MergeWords(1, words);
                    if (PCommon._isDebug) System.out.println(PCommon.ConcaT("SearchExpr: ", searchExpr));

                    SetTabTitle(searchExpr);
                    SaveTab();

                    recyclerViewAdapter = new BibleAdapter(_context, bbName, bNumber, searchExpr);
                    if (WhenTabIsEmptyOrNull(true)) return;
                    recyclerView.setAdapter(recyclerViewAdapter);
                    recyclerView.setHasFixedSize(true);
                    recyclerView.scrollToPosition(scrollPosY);

                    return;
                }
            }

            //Finally <expr>...
            isBook = true;
            //noinspection ConstantConditions
            isChapter = false;
            //noinspection ConstantConditions
            isVerse = false;

            bNumber = 0;
            //noinspection ConstantConditions
            cNumber = 0;
            //noinspection ConstantConditions
            vNumber = 0;

            // noinspection ConstantConditions
            SaveRef(isBook, isChapter, isVerse, bNumber, cNumber, vNumber);
            if (PCommon._isDebug) System.out.println(PCommon.ConcaT("SearchExpr: ", searchFullQuery));
            SetTabTitle(searchFullQuery);
            SaveTab();

            recyclerViewAdapter = new BibleAdapter(_context, bbName, searchFullQuery);
            if (WhenTabIsEmptyOrNull(true)) return;
            recyclerView.setAdapter(recyclerViewAdapter);
            recyclerView.setHasFixedSize(true);
            recyclerView.scrollToPosition(scrollPosY);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(_context, ex);
        }
    }

    private String GetArticleGeneratedSource()
    {
        String source = "";

        try
        {
            for(ShortSectionBO shortSection : lstArtShortSection)
            {
                if (shortSection != null)
                {
                    source = PCommon.ConcaT(source, shortSection.content);
                }
            }
            if (PCommon._isDebug) System.out.println(PCommon.ConcaT("artGeneratedSource: ", source));
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(getContext(), ex);
        }

        return source;
    }

    private ShortSectionBO FindArticleShortSectionByPositionId(final int id)
    {
        ShortSectionBO shortSection = null;

        try
        {
            if (lstArtShortSection == null || lstArtShortSection.isEmpty()) return null;

            final int lastElement = lstArtShortSection.size() - 1;
            for (int i = lastElement; i >= 0; i--)
            {
                shortSection = lstArtShortSection.get(i);

                if (id >= shortSection.from_id)
                {
                    break;
                }
            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(getContext(), ex);
        }

        return shortSection;
    }

    /***
     * Move section of article
     * @param fromPositionId    Position in article
     * @param toMoveStep        Step of move
     * @return code or null if not applicable
     */
    private String MoveArticleShortSection(final int fromPositionId, final int toMoveStep)
    {
        if (toMoveStep == 0) return null;

        final ShortSectionBO fromShortSection = FindArticleShortSectionByPositionId(fromPositionId);
        if (fromShortSection == null) return null;
        final int fromShortPositionId = fromShortSection.blockId;
        if (fromShortPositionId < 0) return null;

        final int toShortPositionId = fromShortPositionId + toMoveStep;
        if (toShortPositionId < 0) return null;
        final String wasToShortSectionContent = lstArtShortSection.get(toShortPositionId).content;

        lstArtShortSection.get(toShortPositionId).content = fromShortSection.content;
        lstArtShortSection.get(fromShortPositionId).content = wasToShortSectionContent;

        return this.GetArticleGeneratedSource();
    }

    /***
     * Delete section of article
     * @param fromPositionId    Position in article
     * @return code or null of not applicable
     */
    private String DeleteArticleShortSection(final int fromPositionId)
    {
        final ShortSectionBO fromShortSection = FindArticleShortSectionByPositionId(fromPositionId);
        if (fromShortSection == null) return null;
        final int fromShortPositionId = fromShortSection.blockId;
        if (fromShortPositionId < 0) return null;

        lstArtShortSection.remove(fromShortPositionId);

        return this.GetArticleGeneratedSource();
    }

    /***
     * Add section to article
     * @param fromPositionId    Position in article
     * @param content           Content
     * @return code or null of not applicable
     */
    private String AddArticleShortSection(final int fromPositionId, final String content)
    {
        final ShortSectionBO fromShortSection = FindArticleShortSectionByPositionId(fromPositionId);
        if (fromShortSection == null) return null;
        final int fromShortPositionId = fromShortSection.blockId;
        if (fromShortPositionId < 0) return null;

        final ShortSectionBO newShortSection = new ShortSectionBO(fromShortPositionId, content, fromPositionId);
        lstArtShortSection.add(fromShortPositionId, newShortSection);

        return this.GetArticleGeneratedSource();
    }

    /***
     * Update section of article
     * @param fromPositionId    Position in article
     * @param content           Content
     * @return code or null of not applicable
     */
    private String UpdateArticleShortSection(final int fromPositionId, final String content)
    {
        final ShortSectionBO fromShortSection = FindArticleShortSectionByPositionId(fromPositionId);
        if (fromShortSection == null) return null;
        final int fromShortPositionId = fromShortSection.blockId;
        if (fromShortPositionId < 0) return null;

        lstArtShortSection.get(fromShortPositionId).content = content;

        return this.GetArticleGeneratedSource();
    }

    /***
     * Show simple edit article dialog
     * @param isUpdate  True for Update, False for Add
     * @param editType  Type: L=Large title, S=Small title, T=Text
     * @param activity
     * @param titleId
     * @param editText
     * @param artId
     * @param position
     */
    @SuppressWarnings("JavaDoc")
    private void EditArticleDialog(final boolean isUpdate, final String editType, final Activity activity, final int titleId, final String editText, final int position, final int artId)
    {
        try
        {
            final LayoutInflater inflater = activity.getLayoutInflater();
            final View view = inflater.inflate(R.layout.fragment_edit_dialog, (ViewGroup) activity.findViewById(R.id.svEdition));
            final TextView tvTitle = view.findViewById(R.id.tvTitle);
            final EditText etEdition = view.findViewById(R.id.etLexSearch);
            final AlertDialog builder = new AlertDialog.Builder(activity).create();
            builder.setCancelable(true);
            builder.setTitle(titleId);
            builder.setView(view);

            tvTitle.setText(titleId);
            etEdition.setText(editText);

            final Button btnClear = view.findViewById(R.id.btnEditionClear);
            btnClear.setOnClickListener(v -> etEdition.setText(""));
            final Button btnContinue = view.findViewById(R.id.btnEditionContinue);
            btnContinue.setOnClickListener(v -> {
                final Handler handler = new Handler();
                handler.postDelayed(() -> {
                    final String text = etEdition.getText().toString().replaceAll("\n", "<br>").trim();
                    PCommon.SavePref(v.getContext(), IProject.APP_PREF_KEY.EDIT_DIALOG, text);
                    builder.dismiss();

                    String source;
                    if (isUpdate)
                    {
                        source = UpdateArticleShortSection(position, text);
                    }
                    else
                    {
                        //was: "<br><br><H>"
                        source = editType.equalsIgnoreCase("T")
                                ? text
                                : editType.equalsIgnoreCase("L")
                                    ? PCommon.ConcaT("<H>", text, "</H>")
                                    : PCommon.ConcaT("<br><u>", text, "</u><br>");

                        source = AddArticleShortSection(position, source);
                    }

                    if (source != null)
                    {
                        _s.UpdateMyArticleSource(artId, source);
                        onResume();
                    }
                }, 0);
            });

            builder.show();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(getContext(), ex);
        }
    }
}
