## Note to Android TV users

* Rem: if you are in version 3.47, you don't need to do this.  
* It would be only necessary for new users and for future changes to The Light if the app is not on Android TV store.  
* Please read until the end, it's an easy solution in 4 steps :D  

Since version 3.48, Bible Multi The Light has been rejected from Android TV!  
(and after several days was restored by Google!)  
According to them, the app was incompatible with the new Google guidelines, blah blah blah...  
For info, the app was on Android TV for years and works fine!  

In the meantime: I tried to solve it but it's really difficult and a constant back and forth.  
I have to move on, there are other changes to implement in The Light and The Life (my other app) and time is short.  

I tried internal tests and open tests but nothing really work because there are +2700 users using The Light on Android TV.  
Asking emails, with problems of privacy and security is not a solution for you and me.  

The solution is easy and you will probably learn something new to do on Android TV for other apps.  

It's sideload!  

If you use my app from F-Droid, you already know it.  
If you never installed my app and want to install it, please follow the steps (on your Android TV).  
If you use my app from Google Play, please follow the steps (on your Android TV).  
 

## 1) You need a good browser:
I tried Puffin TV Browser but it blocks on a blank page!  
Install TV BRO (it's a light browser) and free.  
What! Another light bro!!  


## 2) You need a sideload tool:
Install X-Plore, it's free.  


## 3) We will download the Apk (The Light):

- If you never installed The Light, make the choice: F-Droid or Gitlab.  

- If you use The Light from F-Droid, go here and download the latest version of The Light:  
https://f-droid.org/en/packages/com.appcoholic.bible/  
Rem: the Apk on F-Droid has been resigned by F-Droid.  

- If you use The Light from Google or Gitlab, go here:  
https://gitlab.com/hotlittlewhitedog/BibleMultiTheLight/-/tree/master/apk  
Rem: the Apk on Gitlab and Google have the same signature: hotlittlewhitedog.  
Rem2: you can also reach more easily that folder by clicking About/Gitlab in my app and go to the folder.  
 
- If you use The Light from another repo, use that repo or the app could be reinstalled.  


## 4) We will install the file downloaded:
Run X-Plore.  
Locate the file downloaded (probably in /storage/emulated/0/Download).  
Long click the file => Rename to thelight.apk (if it's thelight.zip)  
Long click thelight.apk => Install.  

Enjoy!
