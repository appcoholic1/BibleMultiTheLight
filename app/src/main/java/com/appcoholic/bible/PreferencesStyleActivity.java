
package com.appcoholic.bible;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.Map;
import java.util.TreeMap;

import androidx.appcompat.app.AppCompatActivity;

public class PreferencesStyleActivity extends AppCompatActivity
{
    private BibleStyleBO bibleStyleBO = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        try
        {
            super.onCreate(savedInstanceState);

            final int themeId = PCommon.GetPrefThemeId(this);
            setTheme(themeId);
            ShowStyles();
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }

    private void ShowStyles()
    {
        try
        {
            final Context context = this;
            final LayoutInflater inflater = getLayoutInflater();
            final LinearLayout llStyle = (LinearLayout) (inflater.inflate(R.layout.activity_style_preferences, (LinearLayout) findViewById(R.id.llStyle)));
            final RadioGroup radioGroup = new RadioGroup(context);
            final String styleSelected = PCommon.GetPref(context, IProject.APP_PREF_KEY.STYLE_HIGHLIGHT_SEARCH, context.getString(R.string.highlightSearchStyleDefault));
            final ScrollView svStyle = new ScrollView(context);
            svStyle.setPadding(20, 20, 20, 20);
            llStyle.addView(radioGroup);
            svStyle.addView(llStyle);

            int index = 0;
            RadioButton radioStyle;
            TextView tvEx;
            final Typeface styleEx = Typeface.defaultFromStyle(Typeface.NORMAL);
            final int fontSize = PCommon.GetFontSize(context);

            bibleStyleBO = new BibleStyleBO(context);
            final TreeMap<String, String> mapStyles = bibleStyleBO.GetAllStyles();
            String styleName, styleFgColor, styleBgColor;
            Map<String, String> mapStyleProp = null;

            for (Map.Entry entryStyle : mapStyles.entrySet())
            {
                if (entryStyle == null) continue;
                styleName = entryStyle.getKey().toString();
                mapStyleProp = bibleStyleBO.GetStylePropertiesFromId(styleName);
                if (mapStyleProp == null || mapStyleProp.size() != 2) continue;
                styleFgColor = mapStyleProp.get("fg");
                if (styleFgColor == null) continue;
                styleBgColor = mapStyleProp.get("bg");
                if (styleBgColor == null) continue;

                radioStyle = new RadioButton(context);
                radioStyle.setChecked( styleSelected.equalsIgnoreCase( styleName ));
                radioStyle.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                radioStyle.setText(styleName);
                radioStyle.setTextSize(fontSize);
                radioStyle.setTag( styleName );
                radioStyle.setTypeface(styleEx);//Font
                radioStyle.setOnClickListener(v -> {
                    try
                    {
                        final String fontName1 = (String) v.getTag();
                        PCommon.SavePref(v.getContext(), IProject.APP_PREF_KEY.STYLE_HIGHLIGHT_SEARCH, fontName1);
                        finish();
                    }
                    catch (Exception ex)
                    {
                        if (PCommon._isDebug) PCommon.LogR(v.getContext(), ex);
                    }
                });

                final Spannable span = new SpannableString(context.getString(R.string.tvStyleExample));
                span.setSpan(PCommon.GetBackgroundColorSpan(styleBgColor), 4, 9, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                span.setSpan(PCommon.GetForegroundColorSpan(styleFgColor), 4, 9, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);

                tvEx = new TextView(context);
                tvEx.setLayoutParams(PCommon._layoutParamsMatchAndWrap);
                tvEx.setPadding(20, 0, 0, 40);
                tvEx.setText( span );
                tvEx.setTextSize(fontSize);
                tvEx.setTag( R.id.tv1, index );
                tvEx.setTag( R.id.tv2, styleName );
                tvEx.setTypeface(styleEx);//Font
                tvEx.setOnClickListener(v -> {
                    final int index1 = (int) v.getTag(R.id.tv1);
                    final String fontName12 = (String) v.getTag(R.id.tv2);

                    ((RadioButton)(radioGroup.getChildAt(index1))).setChecked(true);
                    PCommon.SavePref(v.getContext(), IProject.APP_PREF_KEY.STYLE_HIGHLIGHT_SEARCH, fontName12);
                    finish();
                });
                tvEx.setFocusable(true);
                tvEx.setBackground(PCommon.GetDrawable(getApplicationContext(), R.drawable.focus_text));

                radioGroup.addView(radioStyle);
                radioGroup.addView(tvEx);

                index = index + 2;
            }

            setContentView(svStyle);
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(getApplicationContext(), ex);
        }
    }
}
