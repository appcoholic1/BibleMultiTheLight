package com.appcoholic.bible;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.TypedValue;
import android.widget.RemoteViews;

final class CommonWidgetVerse
{
    static void UpdateAppWidget(final Context context, final int appWidgetId, WidgetVerseBO widgetVerse)
    {
        try
        {
            if (PCommon._isDebug) System.out.println("My UpdateAppWidget => " + appWidgetId);

            //*** Set Verse
            if (widgetVerse == null) widgetVerse = CommonWidget.GetWidgetRandomVerse(context);
            if (widgetVerse == null) return;

            //*** Set params
            final Intent intent_LANG_CLICK = new Intent(context, BibleWidgetVerseService.class);
            intent_LANG_CLICK.setAction("WIDGET_LANG_CLICK");
            intent_LANG_CLICK.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            intent_LANG_CLICK.putExtra("ID",      widgetVerse.id);
            intent_LANG_CLICK.putExtra("BBNAME",  widgetVerse.bbName);
            intent_LANG_CLICK.putExtra("BNUMBER", widgetVerse.bNumber);
            intent_LANG_CLICK.putExtra("CNUMBER", widgetVerse.cNumber);
            intent_LANG_CLICK.putExtra("VNUMBER", widgetVerse.vNumber);

            final Intent intent_REFRESH_CLICK = new Intent(context, BibleWidgetVerseService.class);
            intent_REFRESH_CLICK.setAction("WIDGET_REFRESH_CLICK");
            intent_REFRESH_CLICK.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);

            final Intent intent_PREV_CLICK = new Intent(context, BibleWidgetVerseService.class);
            intent_PREV_CLICK.setAction("WIDGET_PREV_CLICK");
            intent_PREV_CLICK.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            intent_PREV_CLICK.putExtra("ID", widgetVerse.id);

            final Intent intent_FORWARD_CLICK = new Intent(context, BibleWidgetVerseService.class);
            intent_FORWARD_CLICK.setAction("WIDGET_FORWARD_CLICK");
            intent_FORWARD_CLICK.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            intent_FORWARD_CLICK.putExtra("ID", widgetVerse.id);

            final Intent intent_DOWN_CLICK = new Intent(context, BibleWidgetVerseService.class);
            intent_DOWN_CLICK.setAction("WIDGET_DOWN_CLICK");
            intent_DOWN_CLICK.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            intent_DOWN_CLICK.putExtra("ID", widgetVerse.id);
            intent_DOWN_CLICK.putExtra("VTEXT", widgetVerse.vText);

            final Intent intent_FAV_CLICK = new Intent(context, BibleWidgetVerseService.class);
            intent_FAV_CLICK.setAction("WIDGET_FAV_CLICK");
            intent_FAV_CLICK.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            intent_FAV_CLICK.putExtra("ID", widgetVerse.id);

            final Intent intent_PLAY_CLICK = new Intent(context, BibleWidgetVerseService.class);
            intent_PLAY_CLICK.setAction("WIDGET_PLAY_CLICK");
            intent_PLAY_CLICK.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            intent_PLAY_CLICK.putExtra("ID", widgetVerse.id);

            final Intent intent_STOP_CLICK = new Intent(context, BibleWidgetVerseService.class);
            intent_STOP_CLICK.setAction("WIDGET_STOP_CLICK");
            intent_STOP_CLICK.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);


            //*** Set events
            final PendingIntent pendingIntent_LANG_CLICK;
            final PendingIntent pendingIntent_REFRESH_CLICK;
            final PendingIntent pendingIntent_PREV_CLICK;
            final PendingIntent pendingIntent_FORWARD_CLICK;
            final PendingIntent pendingIntent_DOWN_CLICK;
            final PendingIntent pendingIntent_FAV_CLICK;
            final int flags = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) ? PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE : PendingIntent.FLAG_UPDATE_CURRENT;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            {
                pendingIntent_LANG_CLICK = PendingIntent.getForegroundService(context, appWidgetId, intent_LANG_CLICK, flags);
                pendingIntent_REFRESH_CLICK = PendingIntent.getForegroundService(context, appWidgetId, intent_REFRESH_CLICK, flags);
                pendingIntent_PREV_CLICK = PendingIntent.getForegroundService(context, appWidgetId, intent_PREV_CLICK, flags);
                pendingIntent_FORWARD_CLICK = PendingIntent.getForegroundService(context, appWidgetId, intent_FORWARD_CLICK, flags);
                pendingIntent_DOWN_CLICK = PendingIntent.getForegroundService(context, appWidgetId, intent_DOWN_CLICK, flags);
                pendingIntent_FAV_CLICK = PendingIntent.getForegroundService(context, appWidgetId, intent_FAV_CLICK, flags);
            }
            else
            {
                pendingIntent_LANG_CLICK = PendingIntent.getService(context, appWidgetId, intent_LANG_CLICK, flags);
                pendingIntent_REFRESH_CLICK = PendingIntent.getService(context, appWidgetId, intent_REFRESH_CLICK, flags);
                pendingIntent_PREV_CLICK = PendingIntent.getService(context, appWidgetId, intent_PREV_CLICK, flags);
                pendingIntent_FORWARD_CLICK = PendingIntent.getService(context, appWidgetId, intent_FORWARD_CLICK, flags);
                pendingIntent_DOWN_CLICK = PendingIntent.getService(context, appWidgetId, intent_DOWN_CLICK, flags);
                pendingIntent_FAV_CLICK = PendingIntent.getService(context, appWidgetId, intent_FAV_CLICK, flags);
            }

            //*** Set components
            CommonWidget.SaveTheme(context, CommonWidget.MODE.VERSE);

            final String lang = widgetVerse.bbName.compareToIgnoreCase("k") == 0 ? "EN1611"
                    : widgetVerse.bbName.compareToIgnoreCase("v") == 0 ? "ES"
                    : widgetVerse.bbName.compareToIgnoreCase("l") == 0 ? "FRS"
                    : widgetVerse.bbName.compareToIgnoreCase("o") == 0 ? "FRO"
                    : widgetVerse.bbName.compareToIgnoreCase("d") == 0 ? "IT"
                    : widgetVerse.bbName.compareToIgnoreCase("a") == 0 ? "PT"
                    : "EN2000";

            final RemoteViews views = new RemoteViews(context.getPackageName(), CommonWidget.WIDGET_LAYOUT_ID);
            views.setTextViewText(R.id.widget_tv_lang, lang);
            views.setTextViewText(R.id.widget_tv_ref, widgetVerse.vRef);
            views.setTextViewText(R.id.widget_tv_text, widgetVerse.vText);

            if (!CommonWidget.IS_WIDGET_LAYOUT_DARK) {
                views.setImageViewResource(R.id.widget_iv_fav, (widgetVerse.mark <= 0) ? PCommon.GetDrawableId(context, "ic_star_border_black_18dp") : PCommon.GetDrawableId(context, "ic_star_black_18dp"));
            }
            else {
                views.setImageViewResource(R.id.widget_iv_fav, (widgetVerse.mark <= 0) ? PCommon.GetDrawableId(context, "ic_star_border_white_18dp") : PCommon.GetDrawableId(context, "ic_star_white_18dp"));
            }

            views.setTextViewTextSize(R.id.widget_tv_text, TypedValue.COMPLEX_UNIT_SP, 18f);
            views.setOnClickPendingIntent(R.id.widget_tv_lang, pendingIntent_LANG_CLICK);
            views.setOnClickPendingIntent(R.id.widget_iv_refresh, pendingIntent_REFRESH_CLICK);
            views.setOnClickPendingIntent(R.id.widget_btn_back, pendingIntent_PREV_CLICK);
            views.setOnClickPendingIntent(R.id.widget_btn_forward, pendingIntent_FORWARD_CLICK);
            views.setOnClickPendingIntent(R.id.widget_btn_down, pendingIntent_DOWN_CLICK);
            views.setOnClickPendingIntent(R.id.widget_iv_fav, pendingIntent_FAV_CLICK);

            //*** Update widget
            final AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }
}
