package com.appcoholic.bible;

class TodoBO
{
    enum TODO_STATUS {
        TODO,
        DONE,
        WAIT
    }

    enum TODO_PRIORITY {
        HIGH,
        NORMAL,
        LOW
    }

    int tdId;
    String tdDesc;
    String tdCommentIssues;
    String tdPriority;
    String tdStatus;

    TodoBO()
    {
        tdId = -1;
        tdDesc = "";
        tdCommentIssues = "";
        tdPriority = "NORMAL"; //TODO_PRIORITY.NORMAL.name();
        tdStatus = "TODO"; //TODO_STATUS.TODO.name();
    }
}