package com.appcoholic.bible;

import android.content.Context;

import java.util.ArrayList;

final class CommonWidget
{
    static int WIDGET_LAYOUT_ID = R.layout.bible_widget_audio_light;

    @SuppressWarnings("unused")
    static boolean IS_WIDGET_LAYOUT_DARK = false;
    private static int INSTALL_STATUS = 1;

    enum MODE
    {
        VERSE,
        AUDIO,
    }

    static SCommon CheckLocalInstance(final Context context)
    {
        return SCommon.GetInstance(context);
    }

    static String RollBookName(final Context context, final String bbName)
    {
        try
        {
            if (INSTALL_STATUS != 7) {
                INSTALL_STATUS = PCommon.GetInstallStatus(context);
            }

            switch (INSTALL_STATUS)
            {
                case 7:
                {
                    return (bbName.compareToIgnoreCase("l") == 0) ? "d" : (bbName.compareToIgnoreCase("v") == 0) ? "l" : (bbName.compareToIgnoreCase("d") == 0) ? "a" : (bbName.compareToIgnoreCase("a") == 0) ? "o" : (bbName.compareToIgnoreCase("o") == 0) ? "2" : (bbName.compareToIgnoreCase("k") == 0) ? "v" : "k";
                }
                case 6:
                {
                    return (bbName.compareToIgnoreCase("l") == 0) ? "d" : (bbName.compareToIgnoreCase("v") == 0) ? "l" : (bbName.compareToIgnoreCase("d") == 0) ? "a" : (bbName.compareToIgnoreCase("a") == 0) ? "o" : (bbName.compareToIgnoreCase("k") == 0) ? "v" : "k";
                }
                case 1:
                {
                    return "k";
                }
                case 2:
                {
                    return (bbName.compareToIgnoreCase("v") == 0) ? "k" : "v";
                }
                case 3:
                {
                    return (bbName.compareToIgnoreCase("l") == 0) ? "k" : (bbName.compareToIgnoreCase("v") == 0) ? "k" : "v";
                }
                case 4:
                {
                    return (bbName.compareToIgnoreCase("l") == 0) ? "d" : (bbName.compareToIgnoreCase("v") == 0) ? "l" : (bbName.compareToIgnoreCase("k") == 0) ? "v" : "k";
                }
                case 5:
                {
                    return (bbName.compareToIgnoreCase("l") == 0) ? "d" : (bbName.compareToIgnoreCase("v") == 0) ? "l" : (bbName.compareToIgnoreCase("d") == 0) ? "a" : (bbName.compareToIgnoreCase("k") == 0) ? "v" : "k";
                }

            }
        }
        catch (Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }

        return "k";
    }

    static void SaveTheme(final Context context, final MODE mode)
    {
        try
        {
            final int themeId = PCommon.GetPrefThemeId(context);

            if (themeId == R.style.AppThemeLight)
            {
                if (mode == MODE.VERSE)
                {
                    WIDGET_LAYOUT_ID = R.layout.bible_widget_verse_light;
                }
                else if (mode == MODE.AUDIO)
                {
                    WIDGET_LAYOUT_ID = R.layout.bible_widget_audio_light;
                }

                IS_WIDGET_LAYOUT_DARK = false;
            }
            else
            {
                if (mode == MODE.VERSE)
                {
                    WIDGET_LAYOUT_ID = R.layout.bible_widget_verse_dark;
                }
                else if (mode == MODE.AUDIO)
                {
                    WIDGET_LAYOUT_ID = R.layout.bible_widget_audio_dark;
                }

                IS_WIDGET_LAYOUT_DARK = true;
            }
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }
    }

    static WidgetVerseBO GetWidgetRandomVerse(final Context context)
    {
        try
        {
            final SCommon _s = CheckLocalInstance(context);

            String bbName = PCommon.GetPref(context, IProject.APP_PREF_KEY.BIBLE_NAME, "k");
            if (bbName.equals("")) bbName = "k";

            final int min = _s.GetBibleIdMin(bbName);
            final int max = _s.GetBibleIdMax(bbName);

            int rndBibleId = PCommon.GetRandomInt(context, min, max);
            VerseBO verse = _s.GetVerse(rndBibleId);
            if (verse == null)
            {
                rndBibleId = PCommon.GetRandomInt(context, 1, 31);
                verse = _s.GetVerse(rndBibleId);
            }

            final String verseRef = PCommon.ConcaT(verse.bsName, " ", verse.cNumber, ".", verse.vNumber);
            final String verseText = verse.vText;

            return new WidgetVerseBO(rndBibleId, verseRef, verseText, verse.bbName, verse.bNumber, verse.cNumber, verse.vNumber, verse.mark);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);
        }

        return null;
    }

    static WidgetVerseBO GetWidgetVerse(final Context context, final String bbName, final int bNumber, final int cNumber, final int vNumber)
    {
        try
        {
            final SCommon _s = CheckLocalInstance(context);

            final ArrayList<VerseBO> lstVerse = _s.GetVerse(bbName, bNumber, cNumber, vNumber);
            final VerseBO verse = lstVerse.get(0);
            final String verseRef = PCommon.ConcaT(verse.bsName, " ", verse.cNumber, ".", verse.vNumber);
            final String verseText = verse.vText;

            return new WidgetVerseBO(verse.id, verseRef, verseText, verse.bbName, verse.bNumber, verse.cNumber, verse.vNumber, verse.mark);
        }
        catch(Exception ex)
        {
            if (PCommon._isDebug) PCommon.LogR(context, ex);

            return null;
        }
    }
}
