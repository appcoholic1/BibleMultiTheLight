package com.appcoholic.bible;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.media3.session.MediaSession;
import androidx.media3.session.SessionCommand;
import androidx.media3.session.SessionResult;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

class CustomMediaSessionCallback implements MediaSession.Callback
{
    @Override
    public MediaSession.ConnectionResult onConnect(@NonNull MediaSession session, @NonNull MediaSession.ControllerInfo controller)
    {
        if (PCommon._isDebug) System.out.println(PCommon.ConcaT("CustomMediaSessionCallback.onConnect..."));
        //---

        /*
        SessionCommands sessionCommands = MediaSession.ConnectionResult.DEFAULT_SESSION_COMMANDS.buildUpon()
                        .add(new SessionCommand("COMMAND_CODE_SESSION_SET_RATING", new Bundle()))
                        .build();
        return new MediaSession.ConnectionResult.AcceptedResultBuilder(session)
                .setAvailableSessionCommands(sessionCommands)
                .build();
         */

        //---
        MediaSession.ConnectionResult connectionResult = MediaSession.Callback.super.onConnect(session, controller);
        return connectionResult;

        //return MediaSession.ConnectionResult.reject();

/*
        //---

        //if (controller.getPackageName().equals("org.hlwd.bible"))
        //{
            ArrayList<Player.Commands> availablePlayerCommands = new ArrayList<>();
            availablePlayerCommands.add(new Player.Commands.Builder().add(Player.COMMAND_PLAY_PAUSE).build());

            // Disallow myClient from being able to skip to the next media item
            Player.Commands commandToRemove = new Player.Commands.Builder().add(Player.COMMAND_SEEK_TO_NEXT_MEDIA_ITEM).build();
            availablePlayerCommands.remove(commandToRemove);

            //Convert
            Player.Commands.Builder builder = new Player.Commands.Builder();
            for (Player.Commands command : availablePlayerCommands) {
                builder.addAll(command);
            }

            //return MediaSession.ConnectionResult.reject();
            return MediaSession.ConnectionResult.accept(connectionResult.availableSessionCommands, builder.build());
        //}

        //return connectionResult; // Other clients retain normal command access
*/
    }

    @Override
    public boolean onMediaButtonEvent(@NonNull MediaSession session, @NonNull MediaSession.ControllerInfo controllerInfo, @NonNull Intent intent)
    {
        if (PCommon._isDebug) System.out.println("CustomMediaSessionCallback.onMediaButtonEvent...");

        return MediaSession.Callback.super.onMediaButtonEvent(session, controllerInfo, intent);
    }

    @Override
    public ListenableFuture<SessionResult> onCustomCommand(MediaSession session, MediaSession.ControllerInfo controller, SessionCommand customCommand, Bundle args)
    {
        if (PCommon._isDebug) System.out.println("CustomMediaSessionCallback.onCustomCommand...");

        return Futures.immediateFuture(new SessionResult(SessionResult.RESULT_SUCCESS));

/*
        if (customCommand.customAction.equals("XXX"))
        {
            // Do custom action
            return Futures.immediateFuture(new SessionResult(SessionResult.RESULT_SUCCESS));
        }

        // Return error for invalid custom command
        return Futures.immediateFuture(new SessionResult(SessionResult.RESULT_ERROR_BAD_VALUE));
*/
    }
}
