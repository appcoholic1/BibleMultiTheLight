
package com.appcoholic.bible;

class BookmarkBO
{
    int bmId;
    String bmCurrent;
    String bmDesc;

    //Not in db
    String bmRepresentation;

    BookmarkBO()
    {
        this.bmId = -1;
        this.bmCurrent = null;
        this.bmDesc = null;
        this.bmRepresentation = null;
    }
}
